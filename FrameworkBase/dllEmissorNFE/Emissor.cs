using System;
using System.Collections.Generic;
using System.Text;
using ArquivosTXT;

namespace dllEmissorNFE
{
    /// <summary>
    /// Emissor
    /// </summary>
    public class Emissor
    {
        private cTabelaTXT ArquivoSaida;
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        /*
        public bool gerarArquivo(string Nome, int Numero, string AssinaturaDigital, DateTime dataemissao,string NomeCli,string Email,string IE,long CNPJCLI,
        

            string xLgr,
            string nro,
            string xCpl,
            string xBairro,
            int    cMun,
            string xMun,
            string CliUF,
            int    CliCEP,
            long   CLIFone)*/

        /// <summary>
        /// 
        /// </summary>
        public enum TipoDePag
        {
            /// <summary>
            /// 
            /// </summary>
            vista,
            /// <summary>
            /// 
            /// </summary>
            prazo
        }

        private dEmissor dEmissor1;

        /// <summary>
        /// 
        /// </summary>
        public dEmissor dEmissorProdutos
        {
            get { return dEmissor1 == null ? (dEmissor1 = new dEmissor()) : dEmissor1; }
            set { dEmissor1 = value; }
        }
            
                
                //dEmissor1.Produtos.AddProdutosRow("10A","nome prod",25232910,5405,"pc",4,10.10M,0M);
                //dEmissor1.Produtos.AddProdutosRow("10B","nome  com",25232910,5102,"pc",14, 150.00M, 18M);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Nome"></param>
        /// <param name="Numero"></param>
        /// <param name="dataemissao"></param>
        /// <param name="NomeCli"></param>
        /// <param name="Email"></param>
        /// <param name="IE"></param>
        /// <param name="CNPJCLI"></param>
        /// <param name="xLgr"></param>
        /// <param name="nro"></param>
        /// <param name="xCpl"></param>
        /// <param name="xBairro"></param>
        /// <param name="cMun"></param>
        /// <param name="xMun"></param>
        /// <param name="CliUF"></param>
        /// <param name="CliCEP"></param>
        /// <param name="CLIFone"></param>
        /// <param name="tipopag"></param>
        /// <returns></returns>
        public bool gerarArquivo(string Nome, int Numero, DateTime dataemissao,string NomeCli,string Email,string IE,long CNPJCLI,
        

            string xLgr,
            string nro,
            string xCpl,
            string xBairro,
            int    cMun,
            string xMun,
            string CliUF,
            int    CliCEP,
            long   CLIFone,
            TipoDePag tipopag)
        {
            if (dEmissor1 == null)
                return false;
            bool retorno = true;
            try 
            {
                ArquivoSaida = new cTabelaTXT(ModelosTXT.delimitado);
                ArquivoSaida.Separador = new char[] { '|' };
                ArquivoSaida.Formatodata = FormatoData.yyyy_MM_dd;
                ArquivoSaida.PontoDecima = ".";
                ArquivoSaida.Encoding = Encoding.UTF8;
                ArquivoSaida.IniciaGravacao(Nome);
                Reg0 reg0 = new Reg0(ArquivoSaida,null);
                RegB regB = new RegB(ArquivoSaida, null);
                regB.DataEmissao = dataemissao;
                regB.nNF = Numero;
                regB.intPAG = (tipopag == TipoDePag.vista) ? 0 : 1;
                RegC regC = new RegC(ArquivoSaida, null);
                RegC02 regC02 = new RegC02(ArquivoSaida, null);
                RegA regA = new RegA(ArquivoSaida,null,regB,regC02);
                reg0.GravaLinha();
                regA.GravaLinha();                
                regB.GravaLinha();
                regC.GravaLinha();
                regC02.GravaLinha();
                new RegC05(ArquivoSaida, null).GravaLinha();
                RegE regE = new RegE(ArquivoSaida, null);
                regE.IE = IE;
                regE.xNome = NomeCli;
                regE.email = Email;
                regE.GravaLinha();

                DocBacarios.CPFCNPJ cpfcnpj = new DocBacarios.CPFCNPJ(CNPJCLI);
                if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CNPJ)
                {
                    RegE02 regE02 = new RegE02(ArquivoSaida, null);
                    regE02.CNPJ = CNPJCLI;
                    regE02.GravaLinha();
                }
                else if (cpfcnpj.Tipo == DocBacarios.TipoCpfCnpj.CPF)
                {
                    RegE03 regE03 = new RegE03(ArquivoSaida, null);
                    regE03.CPF = CNPJCLI;
                    regE03.GravaLinha();
                }
                else
                {
                    UltimoErro = new Exception("CPF/CNPJ inv�lido");
                    return false;
                }
                
                RegE05 regE05 = new RegE05(ArquivoSaida, null);
                regE05.xLgr = xLgr;
                regE05.nro = nro;
                regE05.xCpl = xCpl;
                regE05.xBairro = xBairro;
                regE05.cMun = cMun;
                regE05.xMun = xMun;
                regE05.UF = CliUF;
                regE05.CEP = CliCEP;
                regE05.fone = CLIFone;
                regE05.GravaLinha();

                

                RegH regH = new RegH(ArquivoSaida, null);
                RegI regI = new RegI(ArquivoSaida, null);
                RegBase regM = new RegBase(ArquivoSaida, "M", null);
                RegBase regN = new RegBase(ArquivoSaida, "N", null);
                RegN02 regN02 = new RegN02(ArquivoSaida, null);
                RegN08 regN08 = new RegN08(ArquivoSaida, null);
                RegO regO = new RegO(ArquivoSaida, null);
                RegO08 regO08 = new RegO08(ArquivoSaida, null);
                RegBase regQ = new RegBase(ArquivoSaida, "Q", null);
                RegQ02 regQ02 = new RegQ02(ArquivoSaida, null);
                RegBase regS = new RegBase(ArquivoSaida, "S", null);
                RegS02 regS02 = new RegS02(ArquivoSaida, null);
                RegBase regW = new RegBase(ArquivoSaida, "W", null);
                RegW02 regW02 = new RegW02(ArquivoSaida, null);
                RegBase regX = new RegBase(ArquivoSaida, "X|0", null);
                foreach (dEmissor.ProdutosRow row in dEmissor1.Produtos)
                {
                    regW02.vProd += row.VProd;
                    regW02.vPIS += row.VPIS;
                    regW02.vCOFINS += row.VCOFINS;
                    regW02.vNF += row.VProd;
                    regH.GravaLinha(row);                    
                    regI.GravaLinha(row);                    
                    regM.GravaLinha();                    
                    regN.GravaLinha();
                    if (row.PICMS == 0)
                    {
                        regN08.GravaLinha();                        
                    }
                    else
                    {
                        //row.VICMS = (row.VProd * row.PICMS / 100)
                        regN02.GravaLinha(row);
                        regW02.vBC += row.VProd;
                        regW02.vICMS += row.VICMS;
                    }
                    regO.GravaLinha();                    
                    regO08.GravaLinha();                    
                    regQ.GravaLinha();                    
                    regQ02.GravaLinha(row);                    
                    regS.GravaLinha();                    
                    regS02.GravaLinha(row);
                };

                
                regW.GravaLinha();
                regW02.GravaLinha();
                regX.GravaLinha();
                
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return false;
            }
            finally
            {
                if (!ArquivoSaida.TerminaGravacao())
                    retorno = false;
            }
            return retorno;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegBase : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegBase(cTabelaTXT _cTabTXT, string Tipo, string Lido)
            : base(_cTabTXT, Lido)
        {
            TipoReg = Tipo;
        }

        /// <summary>
        /// 
        /// </summary>
        public string TipoReg;


        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoReg", 1, 1, true);            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Reg0 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public Reg0(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {
           
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoReg", 1, 10, "NOTAFISCAL");
            RegistraMapa("nNotas", 2, 1, 1);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegA : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegA(cTabelaTXT _cTabTXT, string Lido, RegB regB, RegC02 regC02)
            : base(_cTabTXT, "A",Lido)
        {
            if (regB != null)
            {
                string Chave = string.Format("{0:d2}{1:yyMM}{2:d14}{3:d2}{4:d3}{5:d9}1{6:d8}",
                    regB.UF,
                    regB.DataEmissao,
                    regC02.cnpj,
                    regB.mod,
                    regB.serie,
                    regB.nNF,
                    regB.cNF                   
                    );
                regB.cdv = DocBacarios.calcModulos.Modulo11_2_9(Chave);
                id = string.Format("NFe{0}{1}",
                    Chave,
                    regB.cdv
                    );
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string id;

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            //RegistraMapa("TipoReg", 1, 1, "A");
            RegistraMapa("versao", 2, 4, "2.00");
            RegistraFortementeTipado("id", 3, 1,true);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegB : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegB(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string TipoReg = "B";
        /// <summary>
        /// 
        /// </summary>
        public int UF = 35;
        /// <summary>
        /// 
        /// </summary>
        public int cNF = 407408;
        /// <summary>
        /// 
        /// </summary>
        public string Nat = "VENDA";
        /// <summary>
        /// 
        /// </summary>
        public int intPAG = 0;
        /// <summary>
        /// 
        /// </summary>
        public int mod = 55;
        /// <summary>
        /// 
        /// </summary>
        public int serie = 1;
        /// <summary>
        /// 
        /// </summary>
        public int nNF = 0;                       //*
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataEmissao;              //*
        /// <summary>
        /// 
        /// </summary>
        public int tiponota = 1;
        /// <summary>
        /// 
        /// </summary>
        public int cmunfg = 3557006;
        /// <summary>
        /// 
        /// </summary>
        public int tpimp = 1;
        /// <summary>
        /// 
        /// </summary>
        public int tpemis = 1;
        /// <summary>
        /// 
        /// </summary>
        public int cdv = 3;
        /// <summary>
        /// 
        /// </summary>
        public int tpamb = 2;
        /// <summary>
        /// 
        /// </summary>
        public int finNfe = 1;
        /// <summary>
        /// 
        /// </summary>
        public int proemi = 3;
        /// <summary>
        /// 
        /// </summary>
        public string verproc = "2.0.7";
        /// <summary>
        /// 
        /// </summary>
        public string dhcont;
        /// <summary>
        /// 
        /// </summary>
        public string xjust;

        

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("TipoReg", 1, 1, true);
        RegistraFortementeTipado("UF", 2,1, true);
        RegistraFortementeTipado("cNF", 3, 8);
        RegistraFortementeTipado("Nat", 4, 5, true);
        RegistraFortementeTipado("intPAG", 5, 1, true);
        RegistraFortementeTipado("mod", 6, 1, true);
        RegistraFortementeTipado("serie", 7, 1, true);
        RegistraFortementeTipado("nNF", 8, 1, true);
        RegistraFortementeTipado("DataEmissao", 9, 1, true);
        RegistraFortementeTipado("tiponota", 12, 1, true);
        RegistraFortementeTipado("cmunfg", 13, 1, true);
        RegistraFortementeTipado("tpimp", 14, 1, true);
        RegistraFortementeTipado("tpemis", 15, 1, true);
        RegistraFortementeTipado("cdv", 16, 1, true);
        RegistraFortementeTipado("tpamb", 17, 1, true);
        RegistraFortementeTipado("finNfe", 18, 1, true);
        RegistraFortementeTipado("proemi", 19, 1, true);
        RegistraFortementeTipado("verproc", 20, 1, true);
        RegistraFortementeTipado("dhcont", 21, 1, true);
        RegistraFortementeTipado("xjust", 22, 1, true);
        }
        
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegC : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegC(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string Tipo = "C";
        /// <summary>
        /// 
        /// </summary>
        public string XNome = "Matercol Materiais de Constru��es e Transportes Ltda";
        /// <summary>
        /// 
        /// </summary>
        public string XFant = "Matercol";
        /// <summary>
        /// 
        /// </summary>
        public long IE = 717013064118;
        /// <summary>
        /// 
        /// </summary>
        public int IM = 4554;
        /// <summary>
        /// 
        /// </summary>
        public int CNAE = 5244299;
        /// <summary>
        /// 
        /// </summary>
        public int CRT = 3;
        #endregion



        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("Tipo", 1, 1,true);
        RegistraFortementeTipado("XNome", 2, 100,true);
        RegistraFortementeTipado("XFant", 3, 100,true);
        RegistraFortementeTipado("IE", 4, 100,true);
        //RegistraFortementeTipado("IEST", 5, 1,true);
        RegistraFortementeTipado("IM", 6, 100,true);
        RegistraFortementeTipado("CNAE", 7, 100,true);
        RegistraFortementeTipado("CRT", 8, 100, true);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegC02 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegC02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public long cnpj = 45421690000271;

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("TipoReg", 1, 3, "C02");
            RegistraFortementeTipado("cnpj", 2, 14);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegC05 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegC05(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraMapa("Total", 1, 87, "C05|Av S�o Jo�o|729||Jardim Icatu|3557006|Votorantim|SP|18110210|1058|BRASIL|1532433311");            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegE : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegE(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string TipoReg = "E";
        /// <summary>
        /// 
        /// </summary>
        public string xNome;
        /// <summary>
        /// 
        /// </summary>
        public string IE;
        /// <summary>
        /// 
        /// </summary>
        public string email;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("TipoReg", 1,1 );
        RegistraFortementeTipado("xNome", 2,1,true );
        RegistraFortementeTipado("IE", 3, 1,true);
        //RegistraFortementeTipado("ISUF", , );
        RegistraFortementeTipado("email", 5,1,true );
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegE02 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegE02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        
        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string Tiporeg = "E02";

        /// <summary>
        /// 
        /// </summary>
        public long CNPJ;               

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Tiporeg", 1, 3);
            RegistraFortementeTipado("CNPJ", 2, 14);
        }

            
            
        
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegE03 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegE03(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }


        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string Tiporeg = "E03";

        /// <summary>
        /// 
        /// </summary>
        public long CPF;               

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Tiporeg", 1, 3);
            RegistraFortementeTipado("CPF", 2, 11);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegE05 : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegE05(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string Tiporeg = "E05";
        /// <summary>
        /// 
        /// </summary>
        public string xLgr;
        /// <summary>
        /// 
        /// </summary>
        public string nro;
        /// <summary>
        /// 
        /// </summary>
        public string xCpl;
        /// <summary>
        /// 
        /// </summary>
        public string xBairro;
        /// <summary>
        /// 
        /// </summary>
        public int cMun;
        /// <summary>
        /// 
        /// </summary>
        public string xMun;
        /// <summary>
        /// 
        /// </summary>
        public string UF;
        /// <summary>
        /// 
        /// </summary>
        public int CEP;
        /// <summary>
        /// 
        /// </summary>
        public int cPais = 1058;
        /// <summary>
        /// 
        /// </summary>
        public string xPais = "BRASIL";
        /// <summary>
        /// 
        /// </summary>
        public long fone;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("Tiporeg", 1, 3);
        RegistraFortementeTipado("xLgr", 2, 1,true);
        RegistraFortementeTipado("nro", 3, 1,true);
        RegistraFortementeTipado("xCpl", 4, 1,true);
        RegistraFortementeTipado("xBairro", 5, 1,true);
        RegistraFortementeTipado("cMun", 6, 1,true);
        RegistraFortementeTipado("xMun", 7, 1,true);
        RegistraFortementeTipado("UF", 8, 2);
        RegistraFortementeTipado("CEP", 9, 8);
        RegistraFortementeTipado("cPais", 10, 1,true);
        RegistraFortementeTipado("xPais", 11, 1,true);
        RegistraFortementeTipado("fone", 12, 1,true);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegH : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegH(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string TipoReg = "H";

        /// <summary>
        /// 
        /// </summary>
        public string infAdProd;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoReg", 1, 1);
            //RegistraFortementeTipado("nItem", 2, 1, true);
            RegistraMapa("nItem", 2, 1,1);
            RegistraFortementeTipado("infAdProd", 3, 1, true);
        }

        
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegI : RegistroBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegI(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, Lido)
        {

        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public string TipoReg = "I";
        /// <summary>
        /// 
        /// </summary>
        public string CProd;
        /// <summary>
        /// 
        /// </summary>
        public string XProd;
        /// <summary>
        /// 
        /// </summary>
        public int NCM;
        /// <summary>
        /// 
        /// </summary>
        public int EXTIPI;
        /// <summary>
        /// 
        /// </summary>
        public int CFOP;
        /// <summary>
        /// 
        /// </summary>
        public string UCom;
        /// <summary>
        /// 
        /// </summary>
        public int QCom;
        /// <summary>
        /// 
        /// </summary>
        public int copyQCom;
        /// <summary>
        /// 
        /// </summary>
        public decimal VUnCom;
        /// <summary>
        /// 
        /// </summary>
        public decimal VProd;
        /// <summary>
        /// 
        /// </summary>
        public string CEANTrib;
        /// <summary>
        /// 
        /// </summary>
        public string copyUCom;
        /// <summary>
        /// 
        /// </summary>
        public decimal copyVUnCom;
        /// <summary>
        /// 
        /// </summary>
        public decimal VFrete;
        /// <summary>
        /// 
        /// </summary>
        public decimal VSeg;
        /// <summary>
        /// 
        /// </summary>
        public decimal VDesc;
        /// <summary>
        /// 
        /// </summary>
        public decimal vOutro;
        /// <summary>
        /// 
        /// </summary>
        public int indTot = 1;
        /// <summary>
        /// 
        /// </summary>
        public string xPed;
        /// <summary>
        /// 
        /// </summary>
        public int nItemPed;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoReg", 1, 1);
            RegistraFortementeTipado("CProd", 2, 1, true);
            //RegistraFortementeTipado("CEAN", , );
            RegistraFortementeTipado("XProd", 4, 1, true);
            RegistraFortementeTipado("NCM", 5, 8);
            //RegistraFortementeTipado("EXTIPI", , );
            RegistraFortementeTipado("CFOP", 7, 4);
            RegistraFortementeTipado("UCom", 8, 2);
            RegistraFortementeTipado("QCom", 9, 1, true);
            RegistraFortementeTipado("VUnCom", 10, 1, true);
            RegistraFortementeTipado("VProd", 11, 1, true);
            //RegistraFortementeTipado("CEANTrib", , );
            RegistraFortementeTipado("copyUCom", 13, 1,true);
            RegistraFortementeTipado("copyQCom", 14, 1, true);
            RegistraFortementeTipado("copyVUnCom", 15, 1, true);
            //RegistraFortementeTipado("VFrete", , );
            //RegistraFortementeTipado("VSeg", , );
            //RegistraFortementeTipado("VDesc", , );
            //RegistraFortementeTipado("vOutro", , );
            RegistraFortementeTipado("indTot", 20, 1, true);
            //RegistraFortementeTipado("xPed", 21, 1, true);
            //RegistraFortementeTipado("nItemPed", 22, 1, true);
        }
        
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegN02 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegN02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "N02",Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int Orig = 0;
        /// <summary>
        /// 
        /// </summary>
        public int CST = 0;
        /// <summary>
        /// 
        /// </summary>
        public int ModBC = 3;
        /// <summary>
        /// 
        /// </summary>
        public decimal VProd;
        /// <summary>
        /// 
        /// </summary>
        public decimal PICMS;
        /// <summary>
        /// 
        /// </summary>
        public decimal VICMS;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Orig", 2, 1, true);
            RegistraFortementeTipado("CST", 3, 2);
            RegistraFortementeTipado("ModBC", 4, 1, true);
            RegistraFortementeTipado("VProd", 5, 1, true);
            RegistraFortementeTipado("PICMS", 6, 1, true);
            RegistraFortementeTipado("VICMS", 7, 1, true);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegN08 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegN08(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "N08",Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int Orig = 0;
        /// <summary>
        /// 
        /// </summary>
        public int CST = 60;
        /// <summary>
        /// 
        /// </summary>
        public decimal VBCST;
        /// <summary>
        /// 
        /// </summary>
        public decimal VICMSST;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Orig", 2, 1, true);
            RegistraFortementeTipado("CST", 3, 2);
            RegistraFortementeTipado("VBCST", 4, 1,true);
            RegistraFortementeTipado("VICMSST", 5, 1,true);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegO : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegO(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "O", Lido)
        {
            
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int CEnq = 0;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("CEnq", 6, 1,true);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class RegO08 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegO08(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "O08", Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int CST = 53;        
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();            
            RegistraFortementeTipado("CST", 2, 1,true);            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegQ02 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegQ02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "Q02", Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int CST = 1;
        /// <summary>
        /// 
        /// </summary>
        public decimal VProd;
        /// <summary>
        /// 
        /// </summary>
        public decimal PPIS = 1.65M;
        /// <summary>
        /// 
        /// </summary>
        public decimal VPIS;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("CST", 2, 2);
        RegistraFortementeTipado("VProd", 3, 1,true);
        RegistraFortementeTipado("PPIS", 4 ,1,true) ;
        RegistraFortementeTipado("VPIS", 5, 1,true);
        }    

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegS02 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegS02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "S02", Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int CST = 1;
        /// <summary>
        /// 
        /// </summary>
        public decimal VProd;
        /// <summary>
        /// 
        /// </summary>
        public decimal PCOFINS = 7.6M;
        /// <summary>
        /// 
        /// </summary>
        public decimal VCOFINS;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CST", 2, 2);
            RegistraFortementeTipado("VProd", 3, 1, true);
            RegistraFortementeTipado("PCOFINS", 4, 1, true);
            RegistraFortementeTipado("VCOFINS", 5, 1, true);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class RegW02 : RegBase
    {
        /// <summary>
        /// 
        /// </summary>
        public RegW02(cTabelaTXT _cTabTXT, string Lido)
            : base(_cTabTXT, "W02", Lido)
        {
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public decimal vBC;
        /// <summary>
        /// 
        /// </summary>
        public decimal vICMS;
        /// <summary>
        /// 
        /// </summary>
        public decimal vBCST = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vST = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vProd;
        /// <summary>
        /// 
        /// </summary>
        public decimal vFrete = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vSeg = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vDesc = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vII = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vIPI = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vPIS;
        /// <summary>
        /// 
        /// </summary>
        public decimal vCOFINS;
        /// <summary>
        /// 
        /// </summary>
        public decimal vOutro = 0;
        /// <summary>
        /// 
        /// </summary>
        public decimal vNF;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
        RegistraFortementeTipado("vBC", 2, 1,true);
        RegistraFortementeTipado("vICMS", 3, 1,true);
        RegistraFortementeTipado("vBCST", 4, 1,true);
        RegistraFortementeTipado("vST", 5, 1,true);
        RegistraFortementeTipado("vProd", 6, 1,true);
        RegistraFortementeTipado("vFrete", 7, 1,true);
        RegistraFortementeTipado("vSeg", 8, 1,true);
        RegistraFortementeTipado("vDesc", 9, 1,true);
        RegistraFortementeTipado("vII", 10, 1,true);
        RegistraFortementeTipado("vIPI", 11, 1,true);
        RegistraFortementeTipado("vPIS", 12, 1,true);
        RegistraFortementeTipado("vCOFINS", 13, 1,true);
        RegistraFortementeTipado("vOutro", 14, 1,true);
        RegistraFortementeTipado("vNF", 15, 1, true);
        }
        

    }

    

    

}
