using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TesteNFS_e
{
    public partial class Form1 : Form
    {
        private string Caminho;
        private DocBacarios.CPFCNPJ cnpjSBC;


        public Form1()
        {
            InitializeComponent();
            Caminho = string.Format("{0}\\Arquivos\\", System.IO.Path.GetDirectoryName(Application.ExecutablePath));
            textEdit4.Text = Caminho;
            if (!System.IO.Directory.Exists(Caminho))
                System.IO.Directory.CreateDirectory(Caminho);
        }



        private void simpleButton1_Click(object sender, EventArgs e)
        {
            

            cnpjSBC = new DocBacarios.CPFCNPJ(textEdit1.Text);

            long NumeroRPS = (long)spinEdit2.Value;

            dllEmissorNFS_e.VerssaoXSD Ver = checkEdit1.Checked ? dllEmissorNFS_e.VerssaoXSD.v300 : dllEmissorNFS_e.VerssaoXSD.v202;



            dllEmissorNFS_e.LoteRPS Lote = new dllEmissorNFS_e.LoteRPS(Ver, cnpjSBC, (int)spinIM.Value, (int)spinCidade.Value, (int)spinLote.Value);
            if (Lote.Erro)
            {
                MessageBox.Show("erro no lote\r\n\r\n" + Lote.strErro);
                Clipboard.SetText("erro no lote\r\n\r\n" + Lote.strErro);
                return;
            };
            //dllEmissorNFS_e.Certificado.NomeCertificado = "CN=CONDOMINIO AMSTERDAN RESIDENCE:21997293897, OU=AC CAIXA PJ, OU=Caixa Economica Federal, O=ICP-Brasil, C=br";
            //Lote.NomeCertificado = "CN=CONDOMINIO AMSTERDAN RESIDENCE:21997293897, OU=AC CAIXA PJ, OU=Caixa Economica Federal, O=ICP-Brasil, C=br";
            dllEmissorNFS_e.LoteRPS.virTomador Tomador = new dllEmissorNFS_e.LoteRPS.virTomador(Ver, new DocBacarios.CPFCNPJ(1124173000184), "COND. ED. MANS�O DAS AC�CIAS", "RUA DR. ANTONIO JORGE FRANCO", "115", "B. ASSUN��O", 3548708, "SP", 18055185, "32225277", "luis@virweb.com.br");

            if (!Lote.IncluirRPS(2000.1M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++,"Teste",DateTime.Now))
            {
                MessageBox.Show("erro na nota\r\n\r\n" + Lote.strErro);
                Clipboard.SetText("erro na nota\r\n\r\n" + Lote.strErro);
                return;
            }
            if (!Lote.IncluirRPS(200.11M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++, "Teste", DateTime.Now))
            {
                MessageBox.Show("erro na nota\r\n\r\n" + Lote.strErro);
                Clipboard.SetText("erro na nota\r\n\r\n" + Lote.strErro);
                return;
            };
            Lote.IncluirRPS(200.1800M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++, "Teste", DateTime.Now);
            Lote.IncluirRPS(200.8M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++, "Teste", DateTime.Now);
            Lote.IncluirRPS(200.00M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++, "Teste", DateTime.Now);
            Lote.IncluirRPS(200.02M, 2, dllEmissorNFS_e.LoteRPS.RetIss.comRetencao, Tomador, NumeroRPS++, "Teste", DateTime.Now);

            if (Lote.GeraXML(Caminho, dllEmissorNFS_e.ArquivoNaoAssinado.Gravar))
            {
                if (Ver == dllEmissorNFS_e.VerssaoXSD.v202)
                {
                    textEdit4.Text = string.Format("{0}{1}_{2}_$.xml", Caminho, cnpjSBC.ToString(false), spinLote.Text);
                    textEditArquivoAssinado.Text = string.Format("{0}{1}_{2}.xml", Caminho, cnpjSBC.ToString(false), spinLote.Text);
                }
                else
                {
                    textEdit4.Text = string.Format("{0}{1}_{2}_V3$.xml", Caminho, cnpjSBC.ToString(false), spinLote.Text);
                    textEditArquivoAssinado.Text = string.Format("{0}{1}_{2}_V3.xml", Caminho, cnpjSBC.ToString(false), spinLote.Text);
                }
                MessageBox.Show("ok");
            }
            else
            {
                MessageBox.Show("erro na gera��o\r\n\r\n" + Lote.strErro);
                Clipboard.SetText("erro na gera��o\r\n\r\n" + Lote.strErro);
            }


        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            
           
            
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            //dllEmissorNFS_e.Certificado certificado = new dllEmissorNFS_e.Certificado("CN=CONDOMINIO AMSTERDAN RESIDENCE:21997293897, OU=AC CAIXA PJ, OU=Caixa Economica Federal, O=ICP-Brasil, C=br");
            if (dllEmissorNFS_e.Certificado.CertificadoST.ValidarAssinaturaArquivo(textEditArquivoAssinado.Text))
                MessageBox.Show("Assinatura v�lida");
            else
                MessageBox.Show("ERRO na Assinatura");
        }

        private void simpleButton5_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.OpenFileDialog OF = new OpenFileDialog();
            if (OF.ShowDialog() == DialogResult.OK)
                textEdit4.Text = OF.FileName;
        }

        

        private void simpleButton7_Click(object sender, EventArgs e)
        {
            
            
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {
            cnpjSBC = new DocBacarios.CPFCNPJ(textEdit1.Text);

            //dllEmissorNFS_e.LoteRPS Lote = new dllEmissorNFS_e.LoteRPS(dllEmissorNFS_e.VerssaoXSD.v300, cnpjSBC, (int)spinIM.Value, (int)spinCidade.Value, (int)spinLote.Value);
            //Lote.NomeCertificado = "CN=CONDOMINIO AMSTERDAN RESIDENCE:21997293897, OU=AC CAIXA PJ, OU=Caixa Economica Federal, O=ICP-Brasil, C=br";
            //dllEmissorNFS_e.LoteRPS Lote = new dllEmissorNFS_e.LoteRPS(dllEmissorNFS_e.VerssaoXSD.v202, cnpjSBC, (int)spinIM.Value, (int)spinCidade.Value, (int)spinLote.Value);
            dllEmissorNFS_e.NotaGerada NG = new dllEmissorNFS_e.NotaGerada(cnpjSBC.Valor, (int)spinIM.Value);
            if (NG.ConsultaRPS(dllEmissorNFS_e.VerssaoXSD.v300, Caminho, (int)spinEdit2.Value, dllEmissorNFS_e.ArquivoNaoAssinado.Gravar))
                MessageBox.Show(string.Format("Nota: {0} - {1}",NG.Numero,NG.DataEmissao));
            else
                MessageBox.Show(string.Format("Aviso: {0}", NG.Aviso));


            

            
        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            dllEmissorNFS_e.giss giss = new dllEmissorNFS_e.giss();
            //giss.dgiss.NotasP.AddNotasPRow(1, DateTime.Today, 100.01M, "10.01", true, "Nome Prestador", 12, "", 1124173000184, 0, 18055185, "", "Cam�lias", "151","", "Simus", "SP", "Sorocaba",true,1, "N");
            //giss.dgiss.NotasP.AddNotasPRow(2, "A", DateTime.Today, 100.01M, "10.01", "S", "Nome Prestador", 12, "", 1124173000184, "S", 0, 18055185, "Al", "Cam�lias", "151", "", "Simus", "SP", "Sorocaba", "F");
            //giss.dgiss.NotasP.AddNotasPRow(3, "A", DateTime.Today, 100.01M, "10.01", "S", "Nome Prestador", 12, "", 1124173000184, "S", 0, 18055185, "Al", "Cam�lias", "151", "", "Simus", "SP", "Sorocaba", "F");
            //giss.dgissF.NotasP.AddNotasPRow(0, "", DateTime.Today, 10.01M, "17.11", "S", "Nome Prestador sem nota", 12, "", 13553185805, "S", 0, 18055185, "Al", "Cam�lias", "151", "", "Simus", "SP", "Sorocaba", "F");
            giss.GeraArquivo(@"c:\lixo\teste.dat");            
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            textEdit1.Text = "06.888.260/0001-21";
            spinIM.Value = 161504;
            spinCidade.Value = 3547809;
        }

        private dllEmissorNFS_e.BuscaNFE BuscaNota;

        private void simpleButton7_Click_1(object sender, EventArgs e)
        {
            BuscaNota = new dllEmissorNFS_e.BuscaNFE();
            BuscaNota.CadastraNota += RetornoDaNota;
            BuscaNota.Busca(dllEmissorNFS_e.Prefeituras.SBC, 189, "391228902");
        }

        private void RetornoDaNota(object sender, EventArgs e)
        {
            string Relatorio = BuscaNota.DadosNFeLido.Relatorio();
            MessageBox.Show(Relatorio);
        }

        private void salvaPDF(string Url, string FileName)
        {
            using (var wc = new System.Net.WebClient())
            {
                wc.DownloadFile(Url, FileName);
            }

            /*
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(Url);
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            webContent = reader.ReadToEnd();
            StreamWriter sw = new StreamWriter(FileName);
            sw.WriteLine(webContent);
            sw.Close();*/
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            BuscaNota = new dllEmissorNFS_e.BuscaNFE();
            BuscaNota.CadastraNota += RetornoDaNota;
            BuscaNota.Busca(TELink.Text);

        }

        private void Clica()
        {
            
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            BuscaNota = new dllEmissorNFS_e.BuscaNFE();
            BuscaNota.CadastraNota += RetornoDaNota;
            BuscaNota.Busca(dllEmissorNFS_e.Prefeituras.SBC, 189, "391228902");
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            BuscaNota = new dllEmissorNFS_e.BuscaNFE(webBrowser2);
            BuscaNota.CadastraNota += RetornoDaNota;
            BuscaNota.Busca(TELink.Text);
        }
    }
}
