﻿namespace dllCNAB
{
    /// <summary>
    /// Ocorrencias do retorno
    /// </summary>
    public enum TiposOcorrenciaCAIXA
    {
        /// <summary>
        /// Registrado
        /// </summary>
        EntradaConfirmada = 2,
        /// <summary>
        /// Erro no registro
        /// </summary>
        EntradaRejeitada = 3,
        /// <summary>
        /// Boleto pago
        /// </summary>
        LiquidacaoNormal = 6,
        /// <summary>
        /// Registro cancelado (por varios motivos)
        /// </summary>
        Baixa = 9,
        /// <summary>
        /// Abatimento
        /// </summary>        
        AbatimentoConcedido = 12,
        /// <summary>
        /// Vencimento
        /// </summary>
        VencimentoAlterado = 14,
        /// <summary>
        /// Cliente aceitou no dda
        /// </summary>
        AceiteDDA = 51,
        /// <summary>
        /// 
        /// </summary>
        RecusaDDA = 53,
    }

    /// <summary>
    /// Ocorrencias do retorno
    /// </summary>
    public enum TiposOcorrenciaSANTANDER
    {
        /// <summary>
        /// Registrado
        /// </summary>
        EntradaConfirmada = 2,
        /// <summary>
        /// Erro no registro
        /// </summary>
        EntradaRejeitada = 3,        
        /// <summary>
        /// Boleto pago
        /// </summary>
        LiquidacaoNormal = 6,        
    }
}