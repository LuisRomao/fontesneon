﻿/*
MR - 21/03/2016 20:00 -           - GravaArquivo permite usar um arquivo indicado nos parametros (Alterações indicadas por *** MRC - INICIO (21/03/2016 20:00) ***)
                                  - Inclusão do FinalizaArquivo (funcao deve ser implementada se necessario)
*/

using System;

namespace dllCNAB
{
    
    /// <summary>
    /// CNAB ajustado para caixa
    /// </summary>
    public class BoletoRemessaCNAB_CX : BoletoRemessaCNAB
    {
        /// <summary>
        /// Prefixo Arquivo
        /// </summary>
        public override string PrefixoArquivo
        {
            get
            {
                return "CX";
            }
        }

        

        /// <summary>
        /// Grava arquivo formato CNAB (efetivo)
        /// </summary>        
        public override string GravaArquivo(string Path, string Arquivo = "")
        {
            return GravaArquivo(104);
            /*
            try
            {
                string NomeArquivo = Arquivo;
                if (NomeArquivo == "")
                    NomeArquivo = string.Format("{0}{1}_{2:ddMM}{3:00}.REM", Path,PrefixoArquivo, DateTime.Today, SequencialRemessaArquivo);        
                return GravaArquivo(NomeArquivo, 104);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }*/
        }

        //*** MRC - INICIO (21/03/2016 20:00) ***
        /// <summary>
        /// Finaliza o Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public override string FinalizaArquivo(string NomeArquivo)
        {
            //funcao deve ser implementada se necessario
            return "";
        }
        //*** MRC - TERMINO (21/03/2016 20:00) ***
    }
}
