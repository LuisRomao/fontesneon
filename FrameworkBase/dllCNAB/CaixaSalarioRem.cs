﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using dllCNAB.RegistrosOBJ;

namespace dllCNAB
{
    /// <summary>
    /// 
    /// </summary>
    public class CaixaSalarioRem
    {
        private cTabelaTXT ArquivoSaida;
        /// <summary>
        /// 
        /// </summary>
        public bool EmTeste = true;
        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;
        private regHeader_0_CX regHeader_0_CX1;
        
        /// <summary>
        /// Inicia o arquivo com os dados do cabeçalho
        /// </summary>
        /// <param name="CNPJ">CNPJ conta mãe</param>
        /// <param name="Convenio">Convenio - fornecido pelo banco</param>
        /// <param name="ParametroTr">Parametro transmissao - fornecido pelo banco</param>
        /// <param name="Agencia">Agencia conta mãe</param>
        /// <param name="AgenciaDV">digito Agencia conta mãe</param>
        /// <param name="OperacaoConta">Operação conta mãe</param>
        /// <param name="NumeroConta">número da conta mãe</param>
        /// <param name="ContaDV">digito número da conta mãe</param>
        /// <param name="NomeEmpresa">Nome da empresa mãe</param>
        /// <param name="Sequencia">Numero do arquivo</param>
        /// <param name="Identificador">Identificador do arquivo</param>
        /// <returns></returns>
        public void IniciaCabecalho(CPFCNPJ CNPJ, int Convenio, int ParametroTr,
                                      int Agencia, int AgenciaDV, int OperacaoConta, int NumeroConta,
                                      string ContaDV, string NomeEmpresa, int Sequencia, string Identificador)
        {            
                ArquivoSaida = new cTabelaTXT(ModelosTXT.layout);
                ArquivoSaida.Formatodata = FormatoData.ddMMyyyy;                
                regHeader_0_CX1 = new regHeader_0_CX(ArquivoSaida);
                regHeader_0_CX1.CNPJ = CNPJ;
                regHeader_0_CX1.Convenio = Convenio;
                regHeader_0_CX1.ParametroTr = ParametroTr;
                regHeader_0_CX1.EmTeste = EmTeste;
                regHeader_0_CX1.Agencia = Agencia;
                regHeader_0_CX1.AgenciaDV = AgenciaDV;
                regHeader_0_CX1.OperacaoConta = OperacaoConta;
                regHeader_0_CX1.NumeroConta = NumeroConta;
                //regHeader_0_CX1.ContaDV = ContaDV;
                regHeader_0_CX1.NomeEmpresa = NomeEmpresa;
                regHeader_0_CX1.CodigoRemRet = 1;
                regHeader_0_CX1.DataGeracao = DateTime.Today;
                regHeader_0_CX1.HoraGeracao = string.Format("{0:HHmmss}", DateTime.Now);
                regHeader_0_CX1.Sequencia = Sequencia;
                regHeader_0_CX1.Versaolayout = 80;
                regHeader_0_CX1.Identificador = Identificador;                                
        }

        /// <summary>
        /// 
        /// </summary>
        public bool GravaArquivo(string NomeArquivo)
        {
            if (regHeader_0_CX1 == null)
                return false;
            else
            {
                TerminaGerarArquivo();
                return regHeader_0_CX1.GravaArquivo(NomeArquivo);
            }
        }

        //private int nLotes;
        //private int nregistros;
        //private int nregistroslote;

        private regHeaderLote_1_CX regHeaderLote_1_CX1;

        /// <summary>
        /// 
        /// </summary>
        public void AbreLote(CPFCNPJ CNPJ, int CodigoCompromisso,
                             int Agencia, int AgenciaDV, int OperacaoConta, int NumeroConta,
                             string ContaDV, string NomeEmpresa,
                             string Logradouro, int Numero, string Cidade, int CEP,int CEP_COMP,string UF)
        {
            
                TerminaLote();                 
                regHeaderLote_1_CX1 = new regHeaderLote_1_CX(regHeader_0_CX1);                
                regHeaderLote_1_CX1.CNPJ = CNPJ;
                regHeaderLote_1_CX1.CodigoCompromisso = CodigoCompromisso;
                regHeaderLote_1_CX1.Agencia = Agencia;
                regHeaderLote_1_CX1.AgenciaDV = AgenciaDV;
                regHeaderLote_1_CX1.OperacaoConta = OperacaoConta;
                regHeaderLote_1_CX1.NumeroConta = NumeroConta;
                regHeaderLote_1_CX1.ContaDV = ContaDV;
                regHeaderLote_1_CX1.NomeEmpresa = NomeEmpresa;
                regHeaderLote_1_CX1.Logradouro = Logradouro;
                regHeaderLote_1_CX1.Numero = Numero;
                regHeaderLote_1_CX1.Cidade = Cidade;
                regHeaderLote_1_CX1.CEP = CEP;
                regHeaderLote_1_CX1.CEP_COMP = CEP_COMP;
                regHeaderLote_1_CX1.UF = UF;                            
        }

        /// <summary>
        /// 
        /// </summary>
        public void incluipagamento(int BancoDestino, int AgenciaDestino, int AgenciaDVDestino, int ContaDestino, string ContaDVDestino, string Favorecido,
                                    DateTime DataPag, decimal Valor,CPFCNPJ CPF,
                                    string Logradouro, int Numero, string Cidade, int CEP, int CEP_COMP, string UF)
        {

            if (BancoDestino == 104)
                ContaDestino = 1000000000 + ContaDestino;
                regDetalhe_3_A_CX regDetalhe_3_A_CX1 = new regDetalhe_3_A_CX(regHeaderLote_1_CX1, /*++nregistroslote, ++nregistros,*/Valor);
                regDetalhe_3_A_CX1.BancoDestino = BancoDestino;
                regDetalhe_3_A_CX1.AgenciaDestino = AgenciaDestino;
                regDetalhe_3_A_CX1.AgenciaDVDestino = AgenciaDVDestino;
                regDetalhe_3_A_CX1.ContaDestino = ContaDestino;
                regDetalhe_3_A_CX1.ContaDVDestino = ContaDVDestino;
                regDetalhe_3_A_CX1.Favorecido = Favorecido;
                regDetalhe_3_A_CX1.DataPag = DataPag;                
                regDetalhe_3_B_CX regDetalhe_3_B_CX1 = new regDetalhe_3_B_CX(regHeaderLote_1_CX1 /*++nregistroslote, ++nregistros*/);
                regDetalhe_3_B_CX1.CPF = CPF;
                regDetalhe_3_B_CX1.Logradouro = Logradouro;
                regDetalhe_3_B_CX1.Numero = Numero;
                regDetalhe_3_B_CX1.Cidade = Cidade;
                regDetalhe_3_B_CX1.CEP = CEP;
                regDetalhe_3_B_CX1.CEP_COMP = CEP_COMP;
                regDetalhe_3_B_CX1.UF = UF;
                regDetalhe_3_B_CX1.DataVenc = DataPag;                
        }

        private bool TerminaLote()
        {
            try
            {
                if (regHeaderLote_1_CX1 != null)
                {
                    regTrailerLote_5_30 regTrailerLote_5_30_CX1 = new regTrailerLote_5_30(regHeaderLote_1_CX1,null);
                    //regTrailerLote_5_30_CX1.GravaLinha();
                    regHeaderLote_1_CX1 = null;
                }
                return true;
            }
            catch (Exception e)
            {
                UltimoErro = e;
                //TerminaGerarArquivo();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void TerminaGerarArquivo()
        {
            if (regHeader_0_CX1.Trailer == null)
            {
                TerminaLote();
                regHeader_0_CX1.Trailer = new regTrailer_9(regHeader_0_CX1, null);
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0_CX : regHeader_0
    {
        /// <summary>
        /// 
        /// </summary>
        public int ndetalhes;

        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_CX(cTabelaTXT _cTabelaTXT)
            : base(_cTabelaTXT, null, 104)
        {         
            ndetalhes = 0;
        }

        #region Variaveis Tipadas

        
        /// <summary>
        /// 
        /// </summary>
        public int ParametroTr;
        /// <summary>
        /// 
        /// </summary>
        public bool EmTeste = true;
        /// <summary>
        /// 
        /// </summary>
        public int OperacaoConta;
        /// <summary>
        /// 
        /// </summary>
        public int NumeroConta;
        /// <summary>
        /// 
        /// </summary>
        public string Identificador;

        /// <summary>
        /// 
        /// </summary>
        public string Ambiente
        {
            get { return (EmTeste ? "T" : "P"); }
            set { EmTeste = (value == "T"); }
        }
       
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Convenio", 33, 6);
            RegistraFortementeTipado("ParametroTr", 39, 2);
            RegistraFortementeTipado("Ambiente", 41, 1);
            RegistraMapa("zeros", 46, 4, 0);

            RegistraFortementeTipado("OperacaoConta", 59, 4);
            RegistraFortementeTipado("NumeroConta", 63, 8);

            RegistraMapa("NomeBanco", 103, 30, "CAIXA"); //0.20

            RegistraMapa("Densidade", 167, 5, 1600);     //0.27

            RegistraFortementeTipado("Identificador", 192, 20);

            RegistraMapa("zeros1", 226, 3, 0); // 0.32

            RegistraMapa("OcorrenciaCob", 231, 10, ""); // 0.34
            
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeaderLote_1_CX : regHeaderLote_1
    {
        /// <summary>
        /// 
        /// </summary>
        public regHeader_0_CX regPai;

        /// <summary>
        /// 
        /// </summary>
        public regHeaderLote_1_CX(regHeader_0_CX _regPai/*, int nLote*/)
            : base(_regPai.cTabTXT, null)
        {
            regPai = _regPai;            
            Banco = regPai.Banco;
            //Lote = ++regPai.nLotes;
            regPai.Lotes.Add(this);
            Lote = regPai.Lotes.Count;
            //regPai.nregistros++;
            TipoOperacao = TipoOp_G28.Credito;
            TipoServico = 30;
            FormaLanc = 1;
            VersaoLote = 41;
            Convenio = regPai.Convenio;
            //CodigoCompromisso = 0;
            ParametroTr = regPai.ParametroTr;
        }

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int Convenio;
        /// <summary>
        /// 
        /// </summary>
        public int CodigoCompromisso;
        /// <summary>
        /// 
        /// </summary>
        public int ParametroTr;
        /// <summary>
        /// 
        /// </summary>
        public int Agencia;
        /// <summary>
        /// 
        /// </summary>
        public int AgenciaDV;
        /// <summary>
        /// 
        /// </summary>
        public int OperacaoConta;
        /// <summary>
        /// 
        /// </summary>
        public int NumeroConta;
        /// <summary>
        /// 
        /// </summary>
        public string ContaDV;
        /// <summary>
        /// 
        /// </summary>
        public string AgContaDV;
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa;

        /// <summary>
        /// 
        /// </summary>
        public string Logradouro;

        /// <summary>
        /// 
        /// </summary>
        public int Numero;

        /// <summary>
        /// 
        /// </summary>
        public string Cidade;

        /// <summary>
        /// 
        /// </summary>
        public int CEP;

        /// <summary>
        /// 
        /// </summary>
        public int CEP_COMP;
        /// <summary>
        /// 
        /// </summary>
        public string UF;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("Convenio", 33, 6);
            RegistraMapa("TipoCompromisso", 39, 2, 2);
            RegistraFortementeTipado("CodigoCompromisso", 41, 4);
            RegistraFortementeTipado("ParametroTr", 45, 2);
            RegistraFortementeTipado("Agencia", 53, 5);
            RegistraFortementeTipado("AgenciaDV", 58, 1);
            RegistraFortementeTipado("OperacaoConta", 59, 4);
            RegistraFortementeTipado("NumeroConta", 63, 8);
            RegistraFortementeTipado("ContaDV", 71, 1);
            RegistraFortementeTipado("NomeEmpresa", 73, 30);
            RegistraFortementeTipado("Logradouro", 143, 30);
            RegistraFortementeTipado("Numero", 173, 5);

            RegistraFortementeTipado("Cidade", 193, 20);
            RegistraFortementeTipado("CEP", 213, 5);
            RegistraFortementeTipado("CEP_COMP", 218, 3);
            RegistraFortementeTipado("UF", 221, 2);
            RegistraMapa("Ocorrencias", 231, 10, ""); // 0.34
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3_A_CX : regDetalhe_3_A
    {
        private regHeaderLote_1_CX regPai;

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_A_CX(regHeaderLote_1_CX _regPai, /*int _NumeroRegistro, int _NumeroDoc,*/decimal _Valor)
            : base(_regPai.Banco, _regPai.cTabTXT, null)
        {
            regPai = _regPai;
            //regPai.regPai.nregistros++;
            Lote = regPai.Lote;
            //NumeroRegistro = _NumeroRegistro;
            regPai.Detalhes.Add(this);
            NumeroRegistro = regPai.Detalhes.Count;
            //NumeroRegistro = ++regPai.nregistroslote;
            Valor = _Valor;
            //regPai.TotalLote += Valor;            
            TipoMovimento = 0;
            CodigoMovimento = 0;
            NumeroDoc = ++regPai.regPai.ndetalhes;
        }

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int NumeroDoc;        

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();            
            RegistraFortementeTipado("NumeroDoc", 74, 6); //A.16
            RegistraMapa("Finalidade", 93, 1, 1);         //A.18
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3_B_CX : regDetalhe_3_B
    {
        private regHeaderLote_1_CX regPai;

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_B_CX(regHeaderLote_1_CX _regPai /*int _NumeroRegistro, int _NumeroDoc*/)
            : base(_regPai.Banco, _regPai.cTabTXT, null)
        {
            regPai = _regPai;
            //regPai.regPai.nregistros++;
            Lote = regPai.Lote;
            //NumeroRegistro = _NumeroRegistro;
            //NumeroRegistro = ++regPai.nregistroslote;
            regPai.Detalhes.Add(this);
            NumeroRegistro = regPai.Detalhes.Count;
            NumeroDoc = ++regPai.regPai.ndetalhes;// _NumeroDoc;
        }

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int NumeroDoc;

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumeroDoc", 74, 6); //A.16
            RegistraMapa("Finalidade", 93, 1, 1);         //A.18
        }
    }
    /*
    public class regTrailerLote_5_30_CX : regTrailerLote_5_30
    {
        //private regHeaderLote_1_CX regPai;

        public regTrailerLote_5_30_CX(regHeaderLote_1_CX _regPai)
            : base(_regPai, null)
        {           
            //regPai = _regPai;
            //regPai.regPai.nregistros++;
            //Banco = regPai.Banco;
            //Lote = regPai.Lote;
            //QtdRegistros = regPai.Detalhes.Count;
            //ValorTotal = regPai.TotalLote;
        }
    }*/
    /*
    public class regTrailer_9_CX : regTrailer_9
    {
        private regHeader_0_CX regPai;
        public regTrailer_9_CX(regHeader_0_CX _regPai)
            : base(_regPai, null)
        {
            regPai = _regPai;            
            //Banco = regPai.Banco;
            //QtdLotes = regPai.Lotes.Count;
            //QtdRegistros = ++regPai.nregistros;
        }
 
    }*/
}