﻿using System;

namespace dllCNAB
{
    /// <summary>
    /// CNAB ajustado para caixa
    /// </summary>
    public class BoletoRemessaCNAB_SAN : BoletoRemessaCNAB
    {
        /// <summary>
        /// Prefixo Arquivo
        /// </summary>
        public override string PrefixoArquivo
        {
            get
            {
                return "SAN";
            }
        }

        /// <summary>
        /// Grava arquivo formato CNAB (efetivo)
        /// </summary>                
        public override string GravaArquivo(string Path, string Arquivo = "")
        {
            return GravaArquivo(33);
            /*
            try
            {
                string NomeArquivo = Arquivo;
                if (NomeArquivo == "")
                    NomeArquivo = string.Format("{0}{1}_{2:ddMM}{3:00}.REM", Path,PrefixoArquivo, DateTime.Today, SequencialRemessaArquivo);                
                return GravaArquivo(NomeArquivo, 33);
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }*/
        }
        
        /// <summary>
        /// Finaliza o Arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public override string FinalizaArquivo(string NomeArquivo)
        {
            return "";
        }        
    }
}