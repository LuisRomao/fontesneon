﻿using System;
using System.Collections.Generic;
using EDIBoletos;

namespace dllCNAB
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class BoletoRemessaCNAB : BoletoRemessaBase
    {
        /// <summary>
        /// Header
        /// </summary>
        public RegistrosOBJ.regHeader_0 Header;

        private SortedList<long, regHeaderLote_1_01> AcLotes;       

        private regHeaderLote_1_01 BuscaCriaLote(BoletoReg boleto)
        {
            if (AcLotes == null)
                AcLotes = new SortedList<long, regHeaderLote_1_01>();

            if (!AcLotes.ContainsKey(boleto.CNPJ.Valor))
            {
                regHeaderLote_1_01 novo = new regHeaderLote_1_01(Header, boleto.CNPJ);
                AcLotes.Add(boleto.CNPJ.Valor, novo);
                novo.Convenio = boleto.Convenio;
                novo.agencia = int.Parse(boleto.Agencia);
                novo.djagencia = boleto.DADigAgencia;
                novo.conta = int.Parse(boleto.Conta);
                novo.Nome = boleto.NomeEmpresa;
                novo.DataGravacao = DateTime.Now;
                novo.RemessaRetorno = SequencialRemessa;
                novo.Trailer_01 = new regTrailerLote_5_1(novo);
            }
            return AcLotes[boleto.CNPJ.Valor];
        }

        /// <summary>
        /// 
        /// </summary>
        public void InsereBoletos()
        {            
            foreach (BoletoReg boleto in Boletos)
            {
                regHeaderLote_1_01 Lote = BuscaCriaLote(boleto);
                regDetalhe_3_P regP = new regDetalhe_3_P(Lote, Lote.Detalhes.Count + 1);
                regP.CodOperacao = boleto.IDOcorrencia;
                regP.NossoNumero = boleto.NossoNumero;
                regP.Vencimento = boleto.Vencimento;
                regP.Valor = boleto.Valor;
                regP.EmissaoBoleto = boleto.Emissao;
                regDetalhe_3_Q regQ = new regDetalhe_3_Q(regP, Lote.Detalhes.Count + 1);
                regQ.CPF = boleto.CPF;
                regQ.Nome = boleto.NomeSacado;
                regQ.Endereco = boleto.EnderecoSacado;                
                regQ.Bairro = boleto.BairroSacado;
                if (boleto.CEPSacado != null)
                    regQ.CEP = boleto.CEPSacado.Replace("-","");
                regQ.Cidade = boleto.CidadeSacado;
                regQ.UF = boleto.EstadoSacado;
                regDetalhe_3_R regR = null;
                if (boleto.CobrarMulta)
                {
                    regR = new regDetalhe_3_R(regP, Lote.Detalhes.Count + 1);
                    regR.CobrarMulta = true;
                    regR.Vencimento = boleto.Vencimento;
                    regR.PercMulta = boleto.PercMulta;
                }
                Lote.addDetalhes(regP, regQ, regR);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void CriaHeader(int Banco)
        {
            Header = new RegistrosOBJ.regHeader_0(ArquivoSaida,Banco);
            Header.Sequencia = SequencialRemessa;
            Header.NomeEmpresa = NomeEmpresa;
            Header.nCNPJ = nCNPJ;
            Header.Convenio = CodEmpresa;
            /*
            Header.CodigoRemRet = 1;
            Header.LiteralArquivo = "REMESSA";
            Header.CodServico = 1;
            Header.LiteralServico = "COBRANCA";
            Header.CodEmpresa = CodEmpresa;
            Header.NomeEmpresa = NomeEmpresa;
            Header.DataGeracao = DateTime.Today.ToString("ddMMyy");
            //Header.SequencialRem = SequencialRemessa;
            Header.Sequencial = 1;
            */
        }

        

        /// <summary>
        /// Grava arquivo formato CNAB (efetivo)
        /// </summary>
        protected string GravaArquivo(int Banco, string NomeArquivo = "")
        {
            int? Remessa = null;
            if (NomeArquivo == "")
                NomeArquivo = ProximoNome(out Remessa);
            try
            {
                //if (Remessa.HasValue)
                //    SequencialRemessa = Remessa.Value;
                CriaHeader(Banco);
                if (Header == null)
                    return "";
                else
                {
                    InsereBoletos();
                    if (Header.GravaArquivo(NomeArquivo))
                        return NomeArquivo;
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return "";
            }
        }
    }
}
