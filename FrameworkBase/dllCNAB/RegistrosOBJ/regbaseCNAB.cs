﻿using System;
using System.Collections.Generic;
using ArquivosTXT;
using DocBacarios;

namespace dllCNAB.RegistrosOBJ
{
    /// <summary>
    /// Registros base para cnab
    /// </summary>
    public class regbaseCNAB : RegistroBase
    {
        #region Propriedades tipadas
        private int _Banco;
        private int _Lote;
        private int _Registro;
        /// <summary>
        /// Banco
        /// </summary>
        public int Banco
        {
            get { return _Banco; }
            set { _Banco = value; }
        }
        /// <summary>
        /// Lote
        /// </summary>
        public int Lote
        {
            get { return _Lote; }
            set { _Lote = value; }
        }
        /// <summary>
        /// registro
        /// </summary>
        public int Registro
        {
            get { return _Registro; }
            set { _Registro = value; }
        }
        #endregion

        /// <summary>
        /// Registro base CNAB
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        /// <param name="Banco">Banco</param>
        public regbaseCNAB(cTabelaTXT _cTabelaTXT, string Linha, int? Banco = null)
            : base(_cTabelaTXT)
        {
            if (Banco.HasValue)
                this.Banco = Banco.Value;
            CarregaLinha(Linha);
        }

        /// <summary>
        /// Construtor para gravação
        /// </summary>
        public regbaseCNAB()
        { 
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            RegistraFortementeTipado("Banco", 1, 3);    //01.x
            RegistraFortementeTipado("Lote", 4, 4);     //02.x
            RegistraFortementeTipado("Registro", 8, 1); //03.x
        }
    }
        
    /// <summary>
    /// 
    /// </summary>
    public class regHeader_0 : regbaseCNAB
    {
        /// <summary>
        /// ÚLTIMO ERRO
        /// </summary>
        public Exception UltimoErro;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public bool GravaArquivo(string NomeArquivo)
        {
            bool resultado = true;
            try
            {
                cTabTXT.IniciaGravacao(NomeArquivo);
                GravaLinha();
                if (Lotes != null)
                    foreach (regHeaderLote_1 Lotex in Lotes)
                        Lotex.GravaLote();
                                            
                if (Trailer != null)
                    Trailer.GravaLinha();
            }
            catch (Exception e)
            {
                UltimoErro = e;      
                resultado = false;
            }
            finally 
            {
                cTabTXT.TerminaGravacao();
            }
            return resultado;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<regHeaderLote_1> Lotes;
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9 Trailer;

        /// <summary>
        /// Construtor para leitura
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        /// <param name="Banco">Banco</param>
        public regHeader_0(cTabelaTXT _cTabelaTXT, string Linha, int? Banco = null)
            : base(_cTabelaTXT, Linha, Banco)
        {            
            Lotes = new List<regHeaderLote_1>();
            Lote = 0;
            Registro = 0;
            //nregistros = 1;
        }


        /// <summary>
        /// Construtor para gravação
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Banco"></param>
        public regHeader_0(cTabelaTXT _cTabelaTXT, int Banco):this(_cTabelaTXT, null, Banco)
        {
            this.Banco = Banco;
            CodigoRemRet = 1;
            DataGeracao = DateTime.Today;
            HoraGeracao = DateTime.Now.ToString("HHmmss");            
            switch (Banco)
            {
                case 104:
                    //nCNPJ = 55055651000170;
                    Agencia = 4092;
                    AgenciaDV = 4;
                    //Convenio = 223407;
                    Versaolayout = 50;
                    break;
                case 33:
                    nCNPJ = 55055651000170;
                    Agencia = 1520;
                    AgenciaDV = 7;
                    Convenio = 7441355;
                    Versaolayout = 40;
                    break;
                case 341:
                    Convenio = 0;
                    Versaolayout = 50;
                    break;
                default:
                    throw new NotImplementedException(string.Format("regbaseCNAB - Não implementado para banco = {0}", Banco));
            };            
            Trailer = new regTrailer_9(this, null,Banco);
        }

        #region Variaveis Tipadas

        private int _Versaolayout;

        /// <summary>
        /// 
        /// </summary>
        public int Versaolayout
        {
            get { return _Versaolayout; }
            set { _Versaolayout = value; }
        }

        private CPFCNPJ _CNPJ;

        //private int tipoteste;

        /// <summary>
        /// 
        /// </summary>
        public int tipoCNPJ
        {
            get { return _CNPJ == null ? 2 : _CNPJ.Tipo == TipoCpfCnpj.CPF ? 1 : 2; }
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        public long nCNPJ
        {
            get { return _CNPJ == null ? 0 : _CNPJ.Valor; }
            set { _CNPJ = new CPFCNPJ(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CNPJ
        {
            get { return _CNPJ; }
            set { _CNPJ = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Convenio { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CodigoRemRet;
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataGeracao;
        /// <summary>
        /// 
        /// </summary>
        public string HoraGeracao;
        /// <summary>
        /// 
        /// </summary>
        public int Sequencia;
        /// <summary>
        /// 
        /// </summary>
        public int Agencia;
        /// <summary>
        /// 
        /// </summary>
        public int AgenciaDV;
        /*
        /// <summary>
        /// 
        /// </summary>
        public string ContaDV;*/
        /// <summary>
        /// 
        /// </summary>
        public string AgContaDV;
        /// <summary>
        /// 
        /// </summary>
        public string NomeEmpresa;        

        /// <summary>
        /// Nome do banco
        /// </summary>
        public string NomeBanco
        {
            get
            {
                switch(Banco)
                {
                    case 104:
                        return "CAIXA ECONOMICA FEDERAL";                        
                    case 341:
                        return "ITAU";
                    case 33:
                        return "Banco Santander";
                    case 0:
                        return "";
                    default:
                        throw new NotImplementedException(string.Format("regbaseCNAB (283) - Não implementado para banco = {0}",Banco));
                }                
            }
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataHoraGeracao
        {
            get
            {
                try
                {
                    if (HoraGeracao.Length == 6)
                    {
                        int h = int.Parse(HoraGeracao.Substring(0, 2));
                        int m = int.Parse(HoraGeracao.Substring(2, 2));
                        int s = int.Parse(HoraGeracao.Substring(4, 2));
                        return DataGeracao.AddHours(h).AddMinutes(m).AddSeconds(s);
                    }
                    else
                        return DataGeracao;
                }
                catch
                {
                    return DataGeracao;
                }
            }
            set
            {
                DataGeracao = value.Date;
                HoraGeracao = value.ToString("HHmmss");
            }
        }


        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            if (Banco == 33)
            {
                RegistraFortementeTipado("tipoCNPJ"   ,  17,  1);
                RegistraFortementeTipado("nCNPJ"      ,  18, 15);
                RegistraMapa(            "codTrans"   ,  33, 15, "201500007441355");                
            }
            else
            {
                RegistraFortementeTipado("tipoCNPJ"   ,  18,  1);
                RegistraFortementeTipado("nCNPJ"      ,  19, 14);
                RegistraMapa(            "zeros"      ,  33, 20, 0);
                RegistraFortementeTipado("Agencia"    ,  53,  5);
                RegistraFortementeTipado("AgenciaDV"  ,  58,  1);
                RegistraFortementeTipado("Convenio"   ,  59,  6);                
                RegistraMapa(            "zeros1"     ,  65,  8, 0);
            }
                        
            RegistraFortementeTipado("NomeEmpresa"    ,  73, 30);
            RegistraFortementeTipado("NomeBanco"      , 103, 30);
            RegistraFortementeTipado("CodigoRemRet"   , 143,  1);
            RegistraFortementeTipado("DataGeracao"    , 144,  8);
            if (Banco != 33)
                RegistraFortementeTipado("HoraGeracao", 152,  6);
            RegistraFortementeTipado("Sequencia"      , 158,  6);
            RegistraFortementeTipado("Versaolayout"   , 164,  3);//20.0
            if (Banco != 33)
                RegistraMapa(        "zeros2"         , 167,  5, 0);
            if(Banco == 104)
                RegistraMapa(        "ReservadoE"     , 192, 20, "REMESSA-PRODUCAO");
            RegistraMapa(            "Brancos"        , 216, 25, "");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regHeaderLote_1 : regbaseCNAB
    {

        private regTrailerLote_5 _Trailer;

        /// <summary>
        /// 
        /// </summary>          
        public virtual regTrailerLote_5 Trailer
        {
            get { return _Trailer; }
            set { _Trailer = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void GravaLote()
        {
            GravaLinha();
            foreach (regDetalhe_3 Detx in Detalhes)
                Detx.GravaLinha();
            GravaTrailer();            
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void GravaTrailer()
        {
            if (Trailer != null)
                Trailer.GravaLinha();
        }

        /// <summary>
        /// 
        /// </summary>
        public List<regDetalhe_3> Detalhes;

        /// <summary>
        /// 
        /// </summary>
        public regHeaderLote_1(cTabelaTXT _cTabelaTXT, string Linha,int? Banco = null)
            : base(_cTabelaTXT, Linha,Banco)
        {
            Registro = 1;
            //nregistroslote = 0;
            //TotalLote = 0;
            Detalhes = new List<regDetalhe_3>();
        }

        private int _TipoServico;

        /// <summary>
        /// 
        /// </summary>
        public int TipoServico
        {
            get { return _TipoServico; }
            set { _TipoServico = value; }
        }

        private int _VersaoLote;

        /// <summary>
        /// 
        /// </summary>
        public int VersaoLote
        {
            get { return _VersaoLote; }
            set { _VersaoLote = value; }
        }

        private CPFCNPJ _CNPJ;

        /// <summary>
        /// 
        /// </summary>
        public long nCNPJ
        {
            get { return _CNPJ == null ? 0 : _CNPJ.Valor; }
            set { _CNPJ = new CPFCNPJ(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CNPJ
        {
            get { return _CNPJ; }
            set { _CNPJ = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int tipoCNPJ
        {
            get { return _CNPJ == null ? 2 : _CNPJ.Tipo == TipoCpfCnpj.CPF ? 1 : 2; }
            set { }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int agencia;

        /// <summary>
        /// 
        /// </summary>
        public string djagencia;

        /// <summary>
        /// 
        /// </summary>
        public long conta;

        /// <summary>
        /// 
        /// </summary>
        public string djconta;

        /// <summary>
        /// 
        /// </summary>
        public string Nome;        

        /// <summary>
        /// 
        /// </summary>
        public int FormaLanc;

        /// <summary>
        /// Tipos G28
        /// </summary>
        public enum TipoOp_G28 
        {
            /// <summary>
            /// 
            /// </summary>
            Remessa, 
            /// <summary>
            /// 
            /// </summary>
            Retorno, 
            /// <summary>
            /// 
            /// </summary>
            Credito, 
            /// <summary>
            /// 
            /// </summary>
            Extrato 
        }

        /// <summary>
        /// 
        /// </summary>
        public TipoOp_G28 TipoOperacao;

        /// <summary>
        /// 
        /// </summary>
        public string strTipoOperacao
        {
            get 
            {
                switch (TipoOperacao)
                {
                    case TipoOp_G28.Credito:
                        return "C";
                    case TipoOp_G28.Remessa:
                        return "R";
                    case TipoOp_G28.Retorno:
                        return "T";
                    case TipoOp_G28.Extrato:
                        return "E";
                    default:
                        return "?";
                }                
            }
            set 
            { 
                switch(value)
                {
                    case "C":
                        TipoOperacao = TipoOp_G28.Credito;
                        break;
                    case "R":
                        TipoOperacao = TipoOp_G28.Remessa;
                        break;
                    case "T":
                        TipoOperacao = TipoOp_G28.Retorno;
                        break;
                    case "E":
                        TipoOperacao = TipoOp_G28.Extrato;
                        break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("strTipoOperacao", 9, 1);
            RegistraFortementeTipado("TipoServico", 10, 2);
            if (Banco != 33)
                RegistraFortementeTipado("FormaLanc", 12, 2);
            RegistraFortementeTipado("VersaoLote", 14, 3);
            RegistraFortementeTipado("tipoCNPJ", 18, 1);
            
            //RegistraFortementeTipado("nCNPJ", 19, 14);
            /*         
            RegistraFortementeTipado("Logradouro", 143, 30);
            RegistraFortementeTipado("Numero", 173, 5);

            RegistraFortementeTipado("Cidade", 193, 20);
            RegistraFortementeTipado("CEP", 213, 5);
            RegistraFortementeTipado("CEP_COMP", 218, 3);
            RegistraFortementeTipado("UF", 221, 2);            
            RegistraMapa("Ocorrencias", 231, 10, ""); // 0.34
            */
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3 : regbaseCNAB
    {
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3(cTabelaTXT _cTabelaTXT, string Linha,int? Banco = null)
            : base(_cTabelaTXT, Linha, Banco)
        {
            Registro = 3;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="HLote"></param>
        public regDetalhe_3(regHeaderLote_1_01 HLote)
            : this(HLote.cTabTXT, null, HLote.Banco)
        {
            this.HLote = HLote;
            HLote.Detalhes.Add(this);
            //Banco = HLote.Banco;
            Lote = HLote.Lote;            
        }

        internal regHeaderLote_1_01 HLote;

        private int _NumeroRegistro;

        /// <summary>
        /// 
        /// </summary>
        public int NumeroRegistro
        {
            get { return _NumeroRegistro; }
            set { _NumeroRegistro = value; }
        }


        private string _Segmento = "";

        /// <summary>
        /// 
        /// </summary>
        public string Segmento
        {
            get { return _Segmento; }
            set { _Segmento = value; }
        }


        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("NumeroRegistro", 9, 5);
            RegistraFortementeTipado("Segmento", 14, 1);
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3_A : regDetalhe_3
    {
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_A(int BancoArquivo, cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Segmento = "A";
            Banco = BancoArquivo;
        }

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int TipoMovimento;
        /// <summary>
        /// 
        /// </summary>
        public int CodigoMovimento;
        /// <summary>
        /// 
        /// </summary>
        public int BancoDestino;
        /// <summary>
        /// 
        /// </summary>
        public int AgenciaDestino;
        /// <summary>
        /// 
        /// </summary>
        public int AgenciaDVDestino;
        /// <summary>
        /// 
        /// </summary>
        public int ContaDestino;
        /// <summary>
        /// 
        /// </summary>
        public string ContaDVDestino;
        /// <summary>
        /// 
        /// </summary>
        public string Favorecido;
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataPag;
        /// <summary>
        /// 
        /// </summary>
        public decimal Valor;
        
        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("TipoMovimento", 15, 1);
            RegistraFortementeTipado("CodigoMovimento", 16, 2);

            RegistraFortementeTipado("BancoDestino", 21, 3);
            RegistraFortementeTipado("AgenciaDestino", 24, 5);
            RegistraFortementeTipado("AgenciaDVDestino", 29, 1);
            RegistraFortementeTipado("ContaDestino", 30, 12);            
            RegistraFortementeTipado("ContaDVDestino", 42, 1);
            RegistraFortementeTipado("Favorecido", 44, 30);

            RegistraFortementeTipado("DataPag", 94, 8);
            RegistraMapa("Moeda", 102, 3, "BRL");
            RegistraMapa("zeros", 105, 15, 0);
            RegistraFortementeTipado("Valor", 120, 15);
            RegistraMapa("zeros1", 135, 9, 0);

            RegistraMapa("parcelas", 147, 4, "01N1");

            RegistraMapa("parcelaUnica", 153, 2, 0);
            RegistraMapa("zeros2", 155, 23, 0);

            RegistraMapa("zeros3", 218, 2, 0);

            RegistraMapa("zeros4", 230, 1, 0);

            RegistraMapa("Ocorrencia", 231, 10, "");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3_B : regDetalhe_3
    {
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_B(int BancoArquivo, cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Segmento = "B";
            Banco = BancoArquivo;
        }

        #region Variaveis Tipadas

        private CPFCNPJ _CPF;

        /// <summary>
        /// 
        /// </summary>
        public long nCPF
        {
            get { return _CPF == null ? 0 : _CPF.Valor; }
            set { _CPF = new CPFCNPJ(value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPF
        {
            get { return _CPF; }
            set { _CPF = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int tipoCPF
        {
            get { return _CPF == null ? 2 : _CPF.Tipo == TipoCpfCnpj.CPF ? 1 : 2; }
            set { }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Logradouro;
        /// <summary>
        /// 
        /// </summary>
        public int Numero;
        /// <summary>
        /// 
        /// </summary>
        public string Cidade;
        /// <summary>
        /// 
        /// </summary>
        public int CEP;
        /// <summary>
        /// 
        /// </summary>
        public int CEP_COMP;
        /// <summary>
        /// 
        /// </summary>
        public string UF;
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataVenc;
        
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();

            RegistraFortementeTipado("tipoCPF", 18, 1);
            RegistraFortementeTipado("nCPF", 19, 14);
            RegistraFortementeTipado("Logradouro", 33, 30);
            RegistraFortementeTipado("Numero", 63, 5);

            RegistraFortementeTipado("Cidade", 98, 20);
            RegistraFortementeTipado("CEP", 118, 5);
            RegistraFortementeTipado("CEP_COMP", 123, 3);
            RegistraFortementeTipado("UF", 126, 2);  
            
            RegistraFortementeTipado("DataVenc", 128, 8);
            
            RegistraMapa("zeros1", 136, 75, 0);
            RegistraMapa("Brancos", 226, 15, "");

        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailerLote_5 : regbaseCNAB
    {
        /// <summary>
        /// 
        /// </summary>
        protected regHeaderLote_1 regPai_regHeaderLote_1;

        #region Variaveis Tipadas

        /// <summary>
        /// 
        /// </summary>
        public int QtdRegistros
        {
            get { return ((regPai_regHeaderLote_1 == null) || (regPai_regHeaderLote_1.Detalhes == null)) ? 0 : regPai_regHeaderLote_1.Detalhes.Count; }
        }

        

        #endregion



        /// <summary>
        /// 
        /// </summary>
        public regTrailerLote_5(regHeaderLote_1 _regPai_regHeaderLote_1, string Linha)
            : base(_regPai_regHeaderLote_1.cTabTXT, Linha)
        {
            Registro = 5;
            regPai_regHeaderLote_1 = _regPai_regHeaderLote_1;
            regPai_regHeaderLote_1.Trailer = this;
            Banco = regPai_regHeaderLote_1.Banco;
            Lote = regPai_regHeaderLote_1.Lote;
        }
    }

    /// <summary>
    /// Trailer para lote tipo 30
    /// </summary>
    public class regTrailerLote_5_30 : regTrailerLote_5
    {
        /// <summary>
        /// 
        /// </summary>
        public regTrailerLote_5_30(regHeaderLote_1 _regPai_regHeaderLote_1, string Linha)
            : base(_regPai_regHeaderLote_1, Linha)
        {
            
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTotal
        {
            get 
            { 
                if((regPai_regHeaderLote_1 == null) || (regPai_regHeaderLote_1.Detalhes == null))
                    return 0;
                decimal retornoValorTotal = 0;
                foreach (regDetalhe_3 regDet in regPai_regHeaderLote_1.Detalhes)
                    if (regDet is regDetalhe_3_A)
                    {
                        regDetalhe_3_A regDet_3_A = (regDetalhe_3_A)regDet;
                        retornoValorTotal += regDet_3_A.Valor;
                    }
                return retornoValorTotal;
            }
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("QtdRegistros", 18, 6);            
            RegistraFortementeTipado("ValorTotal", 24, 18);
            RegistraMapa("zeros1", 42, 24, 0);
            RegistraMapa("Brancos", 231, 10, "");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class regTrailer_9 : regbaseCNAB
    {
        private regHeader_0 regPai_regHeader_0;
        /// <summary>
        /// 
        /// </summary>
        public int QtdLotesLidadoArquivo;
        /// <summary>
        /// 
        /// </summary>
        public int QtdRegistrosLidos;
        private int _QtdContas;

        /// <summary>
        /// 
        /// </summary>
        public int QtdLotes
        {
            //get { return _QtdLotes; }
            //set { _QtdLotes = value; }
            get { return (regPai_regHeader_0 == null || regPai_regHeader_0.Lotes == null) ? 0 : regPai_regHeader_0.Lotes.Count; }
            set { QtdLotesLidadoArquivo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int QtdRegistros
        {
            get 
            {
                if ((regPai_regHeader_0 == null) || (regPai_regHeader_0.Lotes == null))
                    return 0;
                int retornoQtdRegistros = 2;
                foreach (regHeaderLote_1 Lotex in regPai_regHeader_0.Lotes)
                {
                    retornoQtdRegistros += (Lotex.Detalhes.Count + 2);
                }
                return retornoQtdRegistros; 
            }
            set { QtdRegistrosLidos = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int QtdContas
        {
            get { return _QtdContas; }
            set { _QtdContas = value; }
        }

        //public regTrailer_9(cTabelaTXT _cTabelaTXT, string Linha)
        /// <summary>
        /// 
        /// </summary>
        public regTrailer_9(regHeader_0 _regPai_regHeader_0, string Linha,int? Banco = null)
            : base(_regPai_regHeader_0.cTabTXT, Linha,Banco)
        {
            Lote = 9999;
            Registro = 9;
            regPai_regHeader_0 = _regPai_regHeader_0;
            regPai_regHeader_0.Trailer = this;
            Banco = regPai_regHeader_0.Banco;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado(    "QtdLotes"    , 18,   6);
            RegistraFortementeTipado(    "QtdRegistros", 24,   6);
            if (Banco != 33)
            {
                RegistraFortementeTipado("QtdContas"   , 30,   6);
                RegistraMapa(            "Brancos"     , 36, 205, "");
            }
            else
                RegistraMapa("Brancos", 30, 211, "");
        }
    }
}