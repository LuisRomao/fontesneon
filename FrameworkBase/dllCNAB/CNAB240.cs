/*
10/06/2016 LH 15.2.9.38 altera��o na verifica��o de total de cr�ditos e d�bitos do lote. N�o somavam os lan�amentos futuros.  
*/


using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using ArquivosTXT;
using DocBacarios;
using dllCNAB.RegistrosOBJ;

namespace dllCNAB
{
    /// <summary>
    /// objeto para tratar arquivos cnab 240
    /// </summary>
    public class CNAB240
    {
        #region Resultado
        /// <summary>
        /// �ltima exception
        /// </summary>
        public Exception UltimoErro;
        /// <summary>
        /// header
        /// </summary>
        public regHeader_0 Header;
        /// <summary>
        /// Lotes de contran�a
        /// </summary>
        public ColLotesCobranca LotesCobranca;
        /// <summary>
        /// Lotes de extrato
        /// </summary>
        public ColLotesExtrato LotesExtrato; 
        #endregion

        private cTabelaTXT _Arquivo;

        private cTabelaTXT Arquivo
        {
            get
            {
                if (_Arquivo == null)
                    _Arquivo = new cTabelaTXT(ModelosTXT.layout);
                _Arquivo.Formatodata = FormatoData.ddMMyyyy;
                return _Arquivo;
            }
        }

        private regbaseCNAB reg;

        private int nLinha = 0;

        private bool CargaOk = false;

        private regbaseCNAB ProximoRegistro(string RegistroEsperado)
        {
            string Linha = Arquivo.ProximaLinha();
            if (Linha.Length != 240)
            {
                UltimoErro = new Exception(string.Format("Linha com tamnha incorreto. Esperado 240 encontrado {0}\r\nLinha:{1} <{2}>", Linha.Length, nLinha, Linha));
                return null;
            }
            if ((Linha == null) || (Linha == ""))
            {
                UltimoErro = new Exception(string.Format("T�rmino inesperado de arquivo\r\nRegisto(s) esperado(s): {0}", RegistroEsperado));
                return null;
            }
            if (reg == null)
                reg = new regbaseCNAB(Arquivo, Linha);
            else
                reg.Decodificalinha(Linha);
            nLinha++;
            return reg;
        }

        /// <summary>
        /// Carrega o arquivo
        /// </summary>
        /// <param name="NomeArquivo"></param>
        /// <returns></returns>
        public bool Carrega(string NomeArquivo)
        {
            UltimoErro = null;
            try
            {
                CargaOk = _Carrega(NomeArquivo);
            }
            finally
            {
                if (_Arquivo != null)
                    _Arquivo.LiberaArquivo();
                    
            }
            return CargaOk;
        }

        /// <summary>
        /// Nome do arquivo
        /// </summary>
        public string NomeArquivo
        {
            get { return Arquivo.NomeArquivo; }
        }

        private bool ignorarSeqLotes(int Banco)
        {
            return Banco == 237;
        }

        private bool PermiteLancamentosAntigos(int Banco)
        {
            if ((Banco == 104) || (Banco == 237))
                return true;
            else
                return false;
        }

        private bool ValidarTotais(int Banco)
        {
            //return true;// Banco != 237;
            return Banco != 237;
        }

        private bool _Carrega(string NomeArquivo)
        {
            bool IgnorarSequenciaDeLoteCobranca = false;
            ArrayList VesaoLayoutTestada = new ArrayList(new int[] { 30,31 ,40, 50, 85 });
            ArrayList VesaoLayoutLoteTestadaCob = new ArrayList(new int[] { 20, 30, 44 });
            ArrayList VesaoLayoutLoteTestadaExt = new ArrayList(new int[] { 20, 30, 31, 33 });
            int nLote = 0;
            LotesCobranca = new ColLotesCobranca();
            LotesExtrato = new ColLotesExtrato();
            Arquivo.NomeArquivo = NomeArquivo;
            if (ProximoRegistro("0") == null)
                return false;
            if (reg.Registro == 0)
            {
                Header = new regHeader_0(Arquivo, reg.LinhaLida);
                if (!VesaoLayoutTestada.Contains(Header.Versaolayout))
                {
                    UltimoErro = new Exception(string.Format("Versao do layout n�o testada: {0}\r\nLinha:{1} <{2}>", Header.Versaolayout, nLinha, reg.LinhaLida));
                    return false;
                };
                if (Header.Banco == 104)
                    IgnorarSequenciaDeLoteCobranca = true;
                if (Header.Lote != 0)
                {
                    UltimoErro = new Exception(string.Format("Lote fora de orderm: Esperado: 0 Encontrado: {0}\r\nLinha:{1} <{2}>", Header.Lote, nLinha, reg.LinhaLida));
                    return false;
                }
                if (ProximoRegistro("1 9") == null)
                    return false;
                while (reg.Registro != 9)
                {
                    bool LoteSemTraler = false;
                    if (reg.Registro != 1)
                    {
                        UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 1 encontrado = {0}\r\nLinha:{1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                        return false;
                    }
                    else
                    {                        
                        regHeaderLote_1 TituloLote = new regHeaderLote_1(Arquivo, reg.LinhaLida,Header.Banco);
                        nLote++;
                        if ((!ignorarSeqLotes(Header.Banco)) && (nLote != TituloLote.Lote))
                        {
                            UltimoErro = new Exception(string.Format("Lote fora de ordem: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nLote, TituloLote.Lote, nLinha, reg.LinhaLida));
                            return false;
                        }
                        if (TituloLote.TipoServico == 1)
                        {
                            regHeaderLote_1_01 TituloLoteCobranca = new regHeaderLote_1_01(Arquivo, reg.LinhaLida);
                            LotesCobranca.Add(TituloLoteCobranca);
                            if (!VesaoLayoutLoteTestadaCob.Contains(TituloLoteCobranca.VersaoLote))
                            {
                                UltimoErro = new Exception(string.Format("Vers�o do layout do lote n�o testada: {0}\r\nLinha:{1} <{2}>", TituloLoteCobranca.VersaoLote, nLinha, reg.LinhaLida));
                                return false;
                            }
                            regDetalhe_3_T reg3T = null;
                            int nRegistroLote = 1;
                            if (ProximoRegistro("3 5") == null)
                                return false;
                            while (reg.Registro != 5)
                            {
                                if (reg.Registro != 3)
                                {
                                    UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3 encontrado = {0}\r\nLinha:{1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                                    return false;
                                }
                                else
                                {
                                    regDetalhe_3 reg3 = new regDetalhe_3(Arquivo, reg.LinhaLida);
                                    if (reg3.NumeroRegistro != nRegistroLote)
                                    {
                                        if (!IgnorarSequenciaDeLoteCobranca)
                                        {
                                            UltimoErro = new Exception(string.Format("Registro fora de ordem no lote: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistroLote, reg3.NumeroRegistro, nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                    };
                                    nRegistroLote++;
                                    if (reg3.Segmento == "T")
                                    {
                                        if (reg3T != null)
                                        {
                                            UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3U encontrado = 3T\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                        else
                                            reg3T = new regDetalhe_3_T(Header.Banco,Arquivo, reg.LinhaLida);
                                    }
                                    else if (reg3.Segmento == "U")
                                    {
                                        if (reg3T == null)
                                        {
                                            UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3T encontrado = 3U\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                        else
                                        {
                                            if (!TituloLoteCobranca.addDetalhes(reg3T, new regDetalhe_3_U(Arquivo, reg.LinhaLida)))
                                            {
                                                UltimoErro = new Exception(string.Format("Registro detalhe com dados errados.\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                                return false;
                                            }
                                            reg3T = null;
                                        }
                                    }
                                    else if (reg3.Segmento == "W")
                                    { 
                                    }
                                    else
                                    {
                                        UltimoErro = new Exception(string.Format("Segmento n�o implementado 3{0}.\r\nLinha:{1} <{2}>", reg3.Segmento, nLinha, reg.LinhaLida));
                                        return false;
                                    }
                                }
                                if (ProximoRegistro("3 5") == null)
                                    return false;
                            }
                            if (reg3T != null)
                            {
                                UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3U encontrado = 5\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                return false;
                            }
                            TituloLoteCobranca.Trailer_01 = new regTrailerLote_5_1(Arquivo, reg.LinhaLida);
                            if (nLote != TituloLoteCobranca.Trailer_01.Lote)
                            {
                                UltimoErro = new Exception(string.Format("Trailer Lote fora de orderm: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nLote, TituloLoteCobranca.Trailer.Lote, nLinha, reg.LinhaLida));
                                return false;
                            };
                            nRegistroLote++;
                            if (TituloLoteCobranca.Trailer_01.QtdRegistros != nRegistroLote)
                            {
                                UltimoErro = new Exception(string.Format("N�mero de registros no lote errado. Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistroLote, TituloLoteCobranca.Trailer.QtdRegistros, nLinha, reg.LinhaLida));
                                return false;
                            };
                        }
                        else if ((TituloLote.TipoServico == 4) && (TituloLote.TipoOperacao == regHeaderLote_1.TipoOp_G28.Extrato))
                        {

                            regHeaderLote_1_E_04 TituloLoteExtrato = new regHeaderLote_1_E_04(Arquivo, reg.LinhaLida);
                            LotesExtrato.Add(TituloLoteExtrato);
                            if (!VesaoLayoutLoteTestadaExt.Contains(TituloLoteExtrato.VersaoLote))
                            {
                                UltimoErro = new Exception(string.Format("Versao do layout do lote n�o testada: {0}\r\nLinha:{1} <{2}>", TituloLoteExtrato.VersaoLote, nLinha, reg.LinhaLida));
                                return false;
                            }
                            int nRegistroLote = 1;
                            if (ProximoRegistro("3 5") == null)
                                return false;
                            LoteSemTraler = false;
                            while (reg.Registro != 5)
                            {
                                if (reg.Registro != 3)
                                {

                                    if ((reg.Registro == 1) || (reg.Registro == 9))
                                    {
                                        LoteSemTraler = true;
                                        break;
                                    }
                                    
                                    UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3 encontrado = {0}\r\nLinha:{1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                                    return false;
                                }
                                else
                                {
                                    regDetalhe_3 reg3 = new regDetalhe_3(Arquivo, reg.LinhaLida);
                                    if (reg3.NumeroRegistro != nRegistroLote)
                                    {
                                        UltimoErro = new Exception(string.Format("Registro fora de ordem no lote: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistroLote, reg3.NumeroRegistro, nLinha, reg.LinhaLida));
                                        return false;
                                    };
                                    nRegistroLote++;
                                    if (reg3.Segmento != "E")
                                    {
                                        UltimoErro = new Exception(string.Format("Registro fora de ordem. Esperado = 3E encontrado = 3{0}\r\nLinha:{1} <{2}>", reg3.Segmento, nLinha, reg.LinhaLida));
                                        return false;
                                    }
                                    else
                                    {
                                        regDetalhe_3_E Lancamento = new regDetalhe_3_E(Arquivo, reg.LinhaLida, Header.Banco);
                                        if (
                                            (Lancamento.agencia != TituloLoteExtrato.agencia)
                                            ||
                                            (Lancamento.conta != TituloLoteExtrato.conta)
                                           )
                                        {
                                            UltimoErro = new Exception(string.Format("Registro incorreto Ag/Conta\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                        if ((Lancamento.Natureza != "INV") || (Lancamento.Nome.Trim() != ""))
                                            TituloLoteExtrato.Lancamentos.Add(Lancamento);
                                    }
                                }
                                if (ProximoRegistro("3 5") == null)
                                    return false;
                            }
                            if (!LoteSemTraler)
                            {
                                regTrailerLote_5_E_4 Trailer = new regTrailerLote_5_E_4(Arquivo, reg.LinhaLida);
                                TituloLoteExtrato.Trailer = Trailer;
                                if (
                                                (Trailer.agencia != TituloLoteExtrato.agencia)
                                                ||
                                                (Trailer.conta != TituloLoteExtrato.conta)
                                               )
                                {
                                    UltimoErro = new Exception(string.Format("Registro incorreto Ag/Conta\r\nLinha:{0} <{1}>", nLinha, reg.LinhaLida));
                                    return false;
                                }

                                nRegistroLote++;
                                if (Trailer.QtdRegistros != nRegistroLote)
                                {
                                    UltimoErro = new Exception(string.Format("N�mero de registros no lote errado. Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nRegistroLote, Trailer.QtdRegistros, nLinha, reg.LinhaLida));
                                    return false;
                                };

                                decimal TotDebitos = 0;
                                decimal TotCreditos = 0;
                                decimal TotDebitosF = 0;
                                decimal TotCreditosF = 0;
                                foreach (regDetalhe_3_E Lancamento in TituloLoteExtrato.Lancamentos)
                                {
                                    if (Lancamento.DataLancamento < Trailer.DataFinal)
                                    {
                                        if (Lancamento.DataLancamento < TituloLoteExtrato.DataInicial)
                                        {
                                            Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.Antiga;
                                            //if(Header.Banco != 104)
                                            if (!PermiteLancamentosAntigos(Header.Banco))
                                            {
                                                UltimoErro = new Exception(string.Format("Lancamento com data anterior � abertura do lote. Esperado >= {0}  encontrado = {1}\r\nLinha:{1} <{2}>", TituloLoteExtrato.DataInicial, Lancamento.DataLancamento, nLinha, reg.LinhaLida));
                                                return false;
                                            }
                                        }
                                        else
                                            Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.Retroativo;
                                    }
                                    else if (Lancamento.DataLancamento == Trailer.DataFinal)
                                        Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.dia;
                                    else
                                        Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.futuro;
                                    // EM TESTE 10/06/2016 : A soma dos debitos e cr�ditos estava somentem para os valores do dia e nao os futuros, foi alterado para TODOS mas precisamos checar se n�o vai dar erro em outros arquivos
                                    // EM 17/06/2016 para o ITAU o valor soma os lan�amentos futuros ent�o o teste de valida��o vai permintir as 2 alternativa independente do banco                                    
                                    if (Lancamento.TipoDeLancamentoExtrato != TiposDeLancamentoExtrato.futuro)
                                    {
                                        if (Lancamento.DebitoCredito == "D")
                                            TotDebitos += Lancamento.ValorLancamento;
                                        else if (Lancamento.DebitoCredito == "C")
                                            TotCreditos += Lancamento.ValorLancamento;
                                        else
                                        {
                                            UltimoErro = new Exception(string.Format("Campo Debito/Credito com valor inv�lido:<{0}>\r\nLinha:{1} <{2}>", Lancamento.DebitoCredito, nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        if (Lancamento.DebitoCredito == "D")
                                            TotDebitosF += Lancamento.ValorLancamento;
                                        else if (Lancamento.DebitoCredito == "C")
                                            TotCreditosF += Lancamento.ValorLancamento;
                                        else
                                        {
                                            UltimoErro = new Exception(string.Format("Campo Debito/Credito com valor inv�lido:<{0}>\r\nLinha:{1} <{2}>", Lancamento.DebitoCredito, nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                    }
                                }


                                if ((Trailer.Debitos != TotDebitos) && (Trailer.Debitos != TotDebitos + TotDebitosF))
                                {
                                    UltimoErro = new Exception(string.Format("Total de d�bitos do lote errado. Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", TotDebitos, Trailer.Debitos, nLinha, reg.LinhaLida));
                                    return false;
                                };
                                if ((Trailer.Creditos != TotCreditos) && (Trailer.Creditos != TotCreditos + TotCreditosF))
                                {
                                    UltimoErro = new Exception(string.Format("Total de cr�ditos do lote errado. Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", TotCreditos, Trailer.Creditos, nLinha, reg.LinhaLida));
                                    return false;
                                };

                            }
                            else
                            {
                                foreach (regDetalhe_3_E Lancamento in TituloLoteExtrato.Lancamentos)
                                {

                                    if (Lancamento.DataLancamento < TituloLoteExtrato.DataInicial)
                                    {
                                        Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.Antiga;
                                        {
                                            UltimoErro = new Exception(string.Format("Lan�amento com data anterior � abertura do lote. Esperado >= {0}  encontrado = {1}\r\nLinha:{1} <{2}>", TituloLoteExtrato.DataInicial, Lancamento.DataLancamento, nLinha, reg.LinhaLida));
                                            return false;
                                        }
                                    }
                                    else
                                        Lancamento.TipoDeLancamentoExtrato = TiposDeLancamentoExtrato.futuro;
                                }
                            }
                        }
                        else
                        {
                            UltimoErro = new Exception(string.Format("Tipo de lote n�o implementado: {0}\r\nLinha:{1} <{2}>", TituloLote.TipoServico, nLinha, reg.LinhaLida));
                            return false;
                            //regHeaderLote_1_01 TituloLoteCobranca = new regHeaderLote_1_01(Arquivo, Linha);
                            //LotesCobranca.Add(TituloLoteCobranca);
                            //while (reg.Registro != 5)
                            //{
                            //    Linha = Arquivo.ProximaLinha();
                            //    if (Linha == null)
                            //    {
                            //        UltimoErro = new Exception("Lote sem registro final 5");
                            //        return false;
                            //    }
                            //    reg.Decodificalinha(Linha);
                            //}
                            //TituloLoteCobranca.Trailer = new regTrailerLote_5(Arquivo, Linha);
                        }
                    };

                    if (!LoteSemTraler)
                        if (ProximoRegistro("1 9") == null)
                            return false;
                }
                //Header.Trailer = new regTrailer_9(Arquivo, reg.LinhaLida);
                new regTrailer_9(Header, reg.LinhaLida);
                if (ValidarTotais(Header.Banco))
                {
                    if (nLote != Header.Trailer.QtdLotesLidadoArquivo)
                    {
                        UltimoErro = new Exception(string.Format("Trailer Quantidade de lotes errada: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nLote, Header.Trailer.QtdLotes, nLinha, reg.LinhaLida));
                        return false;
                    };
                    if (nLinha != Header.Trailer.QtdRegistrosLidos)
                    {
                        UltimoErro = new Exception(string.Format("Trailer Quantidade de registros errada: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", nLinha, Header.Trailer.QtdRegistros, nLinha, reg.LinhaLida));
                        return false;
                    };
                }
                
                if (LotesExtrato.Count != Header.Trailer.QtdContas)
                {
                    UltimoErro = new Exception(string.Format("Trailer Quantidade de lotes de extrato errada: Esperado: {0} Encontrado: {1}\r\nLinha:{2} <{3}>", LotesExtrato.Count, Header.Trailer.QtdContas, nLinha, reg.LinhaLida));
                    return false;
                };
                return true;
            }
            else
            {
                UltimoErro = new Exception(string.Format("Registro inicial incorreto. Esperado 0 encontrado {0}\r\n Linha {1} <{2}>", reg.Registro, nLinha, reg.LinhaLida));
                return false;
            }
        }

        /// <summary>
        /// Gera um relat�rio do conte�do
        /// </summary>
        /// <returns></returns>
        public string Relatorio()
        {
            StringBuilder relatorio = new StringBuilder();
            //bool resultado = testaCnab.Carrega(arquivo);
            relatorio.AppendFormat("Arquivo {0}: ", Arquivo.NomeArquivo);

            if (CargaOk)
            {
                relatorio.AppendFormat("ok {9}\r\nBanco: {0}\r\nVersao: {1}\r\nGeracao:{6:dd/MM/yyyy HH:mm:ss}\r\nSeguencia:{7} {8}\r\nLotes: {2}\r\nLotes Cobran�a: {3}\r\nRegistros: {4}\r\nConc : {5}\r\n\r\n",
                    Header.Banco,
                    Header.Versaolayout,
                    Header.Trailer.QtdLotes,
                    LotesCobranca.Count,
                    Header.Trailer.QtdRegistros,
                    Header.Trailer.QtdContas,
                    Header.DataHoraGeracao,
                    Header.Sequencia,
                    Header.CodigoRemRet == 1? "Remessa":"Retorno",
                    Assinatura()
                    );


                foreach (regHeaderLote_1_01 lote in LotesCobranca)
                {
                    relatorio.AppendFormat("    Lote cobranca: ag/conta {0}-{1}/{2}-{3} Versao:{4}\r\n",
                        lote.agencia,
                        lote.djagencia,
                        lote.conta,
                        lote.djconta,
                        lote.VersaoLote);
                    foreach (ConjuntoDetalheCobranca cj in lote.regTs)
                        relatorio.AppendFormat("        BOL {0}-{9} {11:0} Ocorrencia {10} Data {1:dd/MM/yyyy} Valor {2,8:n2} Tarifa {3:n2} Status {4} Datas {5:dd/MM/yyyy} {6:dd/MM/yyyy} Motivo da ocorrencia:{12}\r\n",
                            cj.reg3T.NossoNumero,
                            cj.reg3T.Vencimento,
                            cj.reg3T.Valor,
                            cj.reg3T.Tarifa,
                            cj.reg3T.Status,
                            cj.reg3U.DataPag,
                            cj.reg3U.DataCred,
                            cj.reg3U.VlPago,//removido do relat�rio
                            cj.reg3U.VlLiquido,//removido do relat�rio
                            cj.reg3T.VerificadorNN,
                            cj.reg3T.Ocorrencia,
                            cj.reg3T.Carteira,
                            cj.reg3T.MotivoOcorrencia);
                    relatorio.AppendFormat("\r\n\r\n    Final do lote Registros:{0} | {1} | {2} | {3} | {4} | {5} | {6} | {7} | {8} | {9} |\r\n",
                        lote.Trailer_01.QtdRegistros,
                        lote.Trailer_01.QtdTitulosSimples,
                        lote.Trailer_01.ValorTitulosSimples,
                        lote.Trailer_01.QtdTitulosVinculada,
                        lote.Trailer_01.ValorTitulosVinculada,
                        lote.Trailer_01.QtdTitulosCaucionada,
                        lote.Trailer_01.ValorTitulosCaucionada,
                        lote.Trailer_01.QtdTitulosDescontada,
                        lote.Trailer_01.ValorTitulosDescontada,
                        lote.Trailer_01.AvisoLancamento);
                }
                foreach (regHeaderLote_1_E_04 lote in LotesExtrato)
                {
                    relatorio.AppendFormat("    Lote Extrato: {0,6}/{1,6} Data: {2:dd/MM/yyyy} Valor: {3,15:n2} Status {4} Seq {5}\r\n",
                        lote.agencia,
                        lote.conta,
                        lote.DataInicial,
                        lote.ValorInicialcomsinal,
                        lote.StatusInicial,
                        lote.SequenciaExtrato);

                    foreach (regDetalhe_3_E lan in lote.Lancamentos)                    
                        relatorio.AppendFormat("           {9,10} {5:dd/MM/yy} {6,3} {7,4} {0,25} {1,15:n2} nat {2,3} <{3,2}>{4,20} doc {8}\r\n",
                            lan.Historico,
                            lan.Valorcomsinal,
                            lan.Natureza,
                            lan.TipoComplemento,
                            lan.Complemento,
                            lan.DataLancamento,
                            lan.Categoria,
                            lan.CodigoHistorico,
                            lan.Documento,
                            lan.TipoDeLancamentoExtrato);                                            
                    if (lote.Trailer != null)
                        relatorio.AppendFormat("    Final do lote Registros:{0} Bloc1 {1:n2} Bloc2 {2:n2} Limite {3:n2}\r\n             Data:{4:dd/MM/yyyy} Saldo {5:n2} <{6}> D: {7:n2} C: {8:n2}\r\n\r\n",
                            lote.Trailer.QtdRegistros,
                            lote.Trailer.SaldoBloqueado2dias,
                            lote.Trailer.SaldoBloqueado2dias,
                            lote.Trailer.Limite,
                            lote.Trailer.DataFinal,
                            lote.Trailer.ValorFinalcomSinal,
                            lote.Trailer.StatusFinal,
                            lote.Trailer.Debitos,
                            lote.Trailer.Creditos);
                    else
                        relatorio.AppendFormat("    Final do lote sem traler\r\n");

                }
            }
            else
            {
                relatorio.AppendFormat("Erro\r\n{0}", UltimoErro.Message);
            }

            relatorio.Append("\r\n-------------------------------------------");

            return relatorio.ToString();

        }

        /// <summary>
        /// Relat�rio dos dados do arquivo Tipo - Data - hora da gera��o
        /// </summary>
        /// <returns></returns>
        public string Assinatura()
        {
            if (CargaOk)
            {
                return string.Format("BANCO {0} TIPO {1} SEQUENCIA {2} DATA {3:dd/MM/yyyy HH:mm:ss}",
                    Header.Banco,
                    Header.CodigoRemRet == 1 ? "REMESSA" : "RETORNO",
                    Header.Sequencia,
                    Header.DataHoraGeracao);
            }
            else
                return "";
        }
    }

    /// <summary>
    /// Cobran�a
    /// </summary>
    public class regHeaderLote_1_01 : regHeaderLote_1
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        /// <param name="Banco"></param>
        public regHeaderLote_1_01(cTabelaTXT _cTabelaTXT, string Linha, int? Banco = null)
            : base(_cTabelaTXT, Linha, Banco)
        {
            TipoServico = 1;
            FormaLanc = 0;
            VersaoLote = 30;
            djconta = "0";
            regTs = new ColConjuntoDetalheCobranca();
        }

        /// <summary>
        /// Construitor
        /// </summary>
        /// <param name="Header"></param>
        /// <param name="cnpj"></param>
        public regHeaderLote_1_01(RegistrosOBJ.regHeader_0 Header,CPFCNPJ cnpj):
            this(Header.cTabTXT,null,Header.Banco)            
        {
            Banco = Header.Banco;
            Header.Lotes.Add(this);
            Lote = Header.Lotes.Count;
            TipoOperacao = TipoOp_G28.Remessa;
            CNPJ = cnpj;
            regPs = new SortedList<int, ConjuntoDetalheCobrancaR>();
        }

        /// <summary>
        /// Conjutos de registros do retorno (poderia se simplificado com SortedList como no regPs)
        /// </summary>
        public ColConjuntoDetalheCobranca regTs;

        /// <summary>
        /// Conjuntos de registros da remessa
        /// </summary>
        public SortedList<int,ConjuntoDetalheCobrancaR> regPs;

        /// <summary>
        /// Grava lote
        /// </summary>
        public override void GravaLote()
        {
            GravaLinha();
            foreach (ConjuntoDetalheCobrancaR Conj in regPs.Values)
            {
                Conj.reg3P.GravaLinha();
                Conj.reg3Q.GravaLinha();
                if (Conj.reg3R != null)
                    Conj.reg3R.GravaLinha();
            }
            GravaTrailer();  
        }
                
        /// <summary>
        /// 
        /// </summary>
        public regTrailerLote_5_1 Trailer_01;

        /// <summary>
        /// GravaTraler
        /// </summary>
        protected override void GravaTrailer()
        {
            if (Trailer_01 != null)
                Trailer_01.GravaLinha();
        }        

        private int _RemessaRetorno;

        /// <summary>
        /// 
        /// </summary>
        public int RemessaRetorno
        {
            get { return _RemessaRetorno; }
            set { _RemessaRetorno = value; }
        }

        private DateTime _DataGravacao;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataGravacao
        {
            get { return _DataGravacao; }
            set { _DataGravacao = value; }
        }

        private DateTime _DataCredito;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataCredito
        {
            get { return _DataCredito; }
            set { _DataCredito = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Convenio;

        /// <summary>
        /// 
        /// </summary>
        public string strConvenio
        {
            get
            {
                if (Banco == 104)
                    return string.Format("{0,6}              ", Convenio);
                else
                    return string.Format("              {0,5}0", Convenio);
            }
            set 
            {
                if (Banco == 104)
                    int.TryParse(value.Substring(0,6), out Convenio);
                else
                    int.TryParse(value.Substring(14, 5), out Convenio);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string strConta
        {
            set 
            {
                conta = 0;
                if (Banco == 104)
                    long.TryParse(value.Substring(0, 6), out conta);
                else
                    long.TryParse(value, out conta);
            }
            get 
            {
                if (Banco == 104)
                    return string.Format("{0,6}{1,6}", Convenio,0); 
                else
                    return conta.ToString(); 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("nCNPJ"         , 19, 15);
            //O campo Convenio tem um mapeamento diferente no itau e na caixa mas os 2 ficam detro do campo especificado no CNAB                       
         
            switch(Banco)
            {
                case 33:
                    RegistraFortementeTipado("agencia", 54, 4);
                    RegistraFortementeTipado("Convenio", 58, 11);
                    break;
                case 104:
                    RegistraFortementeTipado("strConvenio", 34, 6);
                    RegistraMapa("zerosUso", 40, 14, 0);
                    RegistraFortementeTipado("agencia", 54, 5);
                    RegistraFortementeTipado("djagencia", 59, 1);
                    RegistraFortementeTipado("strConta", 60, 6);
                    RegistraMapa("Modelo", 66, 7, 0);
                    RegistraMapa("Zero2", 73, 1, 0);
                    break;
                default:
                    RegistraFortementeTipado("strConvenio", 34, 20);
                    RegistraFortementeTipado("agencia", 54, 5);
                    RegistraFortementeTipado("djagencia", 59, 1);
                    //Mapeamento diferenciado por banco
                    RegistraFortementeTipado("strConta", 60, 12);
                    //RegistraFortementeTipado("djconta"       , 72, 1);
                    RegistraMapa("Zero2", 73, 1, 0);
                    break;
            }                           
            RegistraFortementeTipado("Nome"          , 74, 30);
            RegistraFortementeTipado("RemessaRetorno", 184, 8);
            RegistraFortementeTipado("DataGravacao"  , 192, 8);
            if (Banco != 33)
                RegistraFortementeTipado("DataCredito", 200, 8);
            RegistraMapa(            "Brancos"       , 208, 33, "");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regT"></param>
        /// <param name="regU"></param>
        /// <returns></returns>
        public bool addDetalhes(regDetalhe_3_T regT, regDetalhe_3_U regU)
        {
            regTs.Add(regT, regU);
            //so validar em cobranca registrada
            /* 
            if (agencia != regT.agencia)            
                return false;
            if (djagencia != regT.djAgencia)
                return false;
            if (conta != regT.conta)
                return false;
            if (djconta != regT.djConta)
                return false;
            */ 
            return true;
        }

        /// <summary>
        /// Registros para grava��o
        /// </summary>
        /// <param name="regP"></param>
        /// <param name="regQ"></param>
        /// <param name="regR"></param>
        /// <returns></returns>
        public bool addDetalhes(regDetalhe_3_P regP, regDetalhe_3_Q regQ, regDetalhe_3_R regR)
        {
            regPs.Add(regP.NumeroRegistro, new ConjuntoDetalheCobrancaR(regP, regQ, regR));            
            return true;
        }
    }
   
    /// <summary>
    /// Concilia��o banc�ria
    /// </summary>
    public class regHeaderLote_1_E_04 : regHeaderLote_1
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regHeaderLote_1_E_04(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            TipoServico = 4;
            TipoOperacao = TipoOp_G28.Extrato;
            Lancamentos = new ColLancamentos();
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataInicial;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorInicial;
        /// <summary>
        /// 
        /// </summary>
        public string SituacaoInicial;
        /// <summary>
        /// 
        /// </summary>
        public string StatusInicial;
        /// <summary>
        /// 
        /// </summary>
        public int SequenciaExtrato;

        /// <summary>
        /// C�digo do conv�nio no banco
        /// </summary>
        public string strConvenioE;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorInicialcomsinal
        {
            get { return (SituacaoInicial == "D") ? -ValorInicial : ValorInicial; }
            set
            {
                ValorInicial = Math.Abs(value);
                SituacaoInicial = (value < 0 ? "D" : "C");
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public ColLancamentos Lancamentos;
        /// <summary>
        /// 
        /// </summary>
        public new regTrailerLote_5_E_4 Trailer;

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("nCNPJ"           ,  19, 14);
            RegistraFortementeTipado("strConvenioE"    ,  33, 20);
            RegistraFortementeTipado("agencia"         ,  53,  5);
            RegistraFortementeTipado("djagencia"       ,  58,  1);
            RegistraFortementeTipado("conta"           ,  59, 12);
            RegistraFortementeTipado("djconta"         ,  72,  1);
            RegistraFortementeTipado("Nome"            ,  73, 30);
            RegistraFortementeTipado("DataInicial"     , 143,  8);
            RegistraFortementeTipado("ValorInicial"    , 151, 18);
            RegistraFortementeTipado("SituacaoInicial" , 169,  1);
            RegistraFortementeTipado("StatusInicial"   , 170,  1);
            RegistraFortementeTipado("SequenciaExtrato", 174,  5);
        }
    }
        
    /// <summary>
    /// Trailer Lote 1
    /// </summary>
    public class regTrailerLote_5_1 : regbaseCNAB
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        /// <param name="Banco"></param>
        public regTrailerLote_5_1(cTabelaTXT _cTabelaTXT, string Linha,int? Banco = null)
            : base(_cTabelaTXT, Linha,Banco)
        {          
            Registro = 5;
        }
        
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Header"></param>
        public regTrailerLote_5_1(regHeaderLote_1_01 Header):this(Header.cTabTXT,null,Header.Banco)            
        {
            this.Header = Header;
            Banco = Header.Banco;
            Lote = Header.Lote;
        }

        private regHeaderLote_1_01 Header;

        /// <summary>
        /// Grava Linha
        /// </summary>
        public override void GravaLinha()
        {
            //Somar
            base.GravaLinha();
        }

        private int _QtdRegistros;

        /// <summary>
        /// 
        /// </summary>
        public int QtdRegistros
        {
            get 
            {
                if (Header == null)
                    return _QtdRegistros;
                else
                {
                    if (Header.TipoOperacao == regHeaderLote_1.TipoOp_G28.Remessa)
                        return Header.Detalhes.Count + 2;
                    else
                        return _QtdRegistros;                        
                }
            }
            set { _QtdRegistros = value; }
        }

        private int _QtdTitulosSimples;

        /// <summary>
        /// 
        /// </summary>
        public int QtdTitulosSimples
        {
            get 
            {
                if (Header == null)
                    return _QtdTitulosSimples;
                else
                {
                    if (Header.TipoOperacao == regHeaderLote_1.TipoOp_G28.Remessa)
                        return Header.regPs.Count;
                    else
                        return _QtdRegistros;
                }
            }
            set { _QtdTitulosSimples = value; }
        }

        private decimal _ValorTitulosSimples;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTitulosSimples
        {
            get 
            {
                if (Header == null)
                    return _ValorTitulosSimples;
                else
                {
                    if (Header.TipoOperacao == regHeaderLote_1.TipoOp_G28.Remessa)
                    {
                        decimal Total = 0;
                        foreach (ConjuntoDetalheCobrancaR Cj in Header.regPs.Values)
                            Total += Cj.reg3P.Valor;
                        return Total;
                    }
                    else
                        return _QtdRegistros; 
                }
            }
            set { _ValorTitulosSimples = value; }
        }

        private int _QtdTitulosVinculada;

        /// <summary>
        /// 
        /// </summary>
        public int QtdTitulosVinculada
        {
            get { return _QtdTitulosVinculada; }
            set { _QtdTitulosVinculada = value; }
        }

        private decimal _ValorTitulosVinculada;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTitulosVinculada
        {
            get { return _ValorTitulosVinculada; }
            set { _ValorTitulosVinculada = value; }
        }

        private int _QtdTitulosCaucionada;

        /// <summary>
        /// 
        /// </summary>
        public int QtdTitulosCaucionada
        {
            get { return _QtdTitulosCaucionada; }
            set { _QtdTitulosCaucionada = value; }
        }

        private decimal _ValorTitulosCaucionada;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTitulosCaucionada
        {
            get { return _ValorTitulosCaucionada; }
            set { _ValorTitulosCaucionada = value; }
        }

        private int _QtdTitulosDescontada;

        /// <summary>
        /// 
        /// </summary>
        public int QtdTitulosDescontada
        {
            get { return _QtdTitulosDescontada; }
            set { _QtdTitulosDescontada = value; }
        }

        private decimal _ValorTitulosDescontada;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorTitulosDescontada
        {
            get { return _ValorTitulosDescontada; }
            set { _ValorTitulosDescontada = value; }
        }

        private string _AvisoLancamento;

        /// <summary>
        /// 
        /// </summary>
        public string AvisoLancamento
        {
            get { return _AvisoLancamento; }
            set { _AvisoLancamento = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("QtdRegistros", 18, 6);
            if (Banco != 33)
            {
                RegistraFortementeTipado("QtdTitulosSimples", 24, 6);
                RegistraFortementeTipado("ValorTitulosSimples", 30, 17);
                RegistraFortementeTipado("QtdTitulosVinculada", 47, 6);
                RegistraFortementeTipado("ValorTitulosVinculada", 53, 17);
                RegistraFortementeTipado("QtdTitulosCaucionada", 70, 6);
                RegistraFortementeTipado("ValorTitulosCaucionada", 76, 17);
                RegistraFortementeTipado("QtdTitulosDescontada", 93, 6);
                RegistraFortementeTipado("ValorTitulosDescontada", 99, 17);
                RegistraFortementeTipado("AvisoLancamento", 116, 8);
            }
            RegistraMapa            ("Brancos"               ,124,117, "");
        }

    }

    /// <summary>
    /// TrailerLote E4
    /// </summary>
    public class regTrailerLote_5_E_4 : regbaseCNAB
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regTrailerLote_5_E_4(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Registro = 5;
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int agencia;
        /// <summary>
        /// 
        /// </summary>
        public string djagencia;
        /// <summary>
        /// 
        /// </summary>
        public long conta;
        /// <summary>
        /// 
        /// </summary>
        public string djconta;
        /// <summary>
        /// 
        /// </summary>
        public decimal SaldoBloqueado2dias;
        /// <summary>
        /// 
        /// </summary>
        public decimal Limite;       
        /// <summary>
        /// 
        /// </summary>
        public decimal SaldoBloqueado1dia;        
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataFinal;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorFinal;
        /// <summary>
        /// 
        /// </summary>
        public string SituacaoFina;
        /// <summary>
        /// 
        /// </summary>
        public string StatusFinal;
        /// <summary>
        /// 
        /// </summary>
        public int QtdRegistros;
        /// <summary>
        /// 
        /// </summary>
        public decimal Debitos;
        /// <summary>
        /// 
        /// </summary>
        public decimal Creditos;

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorFinalcomSinal
        {
            get { return (SituacaoFina == "D") ? -ValorFinal : ValorFinal; }
            set
            {
                ValorFinal = Math.Abs(value);
                SituacaoFina = (value < 0 ? "D" : "C");
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("agencia", 53, 5);
            RegistraFortementeTipado("djagencia", 58, 1);
            RegistraFortementeTipado("conta", 59, 12);
            RegistraFortementeTipado("djconta", 71, 1);
            RegistraFortementeTipado("SaldoBloqueado2dias", 89, 18);
            RegistraFortementeTipado("Limite", 107, 18);
            RegistraFortementeTipado("SaldoBloqueado1dia", 125, 18);
            RegistraFortementeTipado("DataFinal", 143, 8);
            RegistraFortementeTipado("ValorFinal", 151, 18);
            RegistraFortementeTipado("SituacaoFina", 169, 1);
            RegistraFortementeTipado("StatusFinal", 170, 1);
            RegistraFortementeTipado("QtdRegistros", 171, 6);
            RegistraFortementeTipado("Debitos", 177, 18);
            RegistraFortementeTipado("Creditos", 195, 18);
        }
        
       
    }
    
    /// <summary>
    /// Detalhe T
    /// </summary>
    public class regDetalhe_3_T : regDetalhe_3
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BancoArquivo"></param>
        /// <param name="_cTabelaTXT"></param>
        /// <param name="Linha"></param>
        public regDetalhe_3_T(int BancoArquivo, cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha,BancoArquivo)
        {
            Segmento = "T";
            Banco = BancoArquivo;
        }

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_U regU;

        private int _agencia;

        /// <summary>
        /// 
        /// </summary>
        public int agencia
        {
            get { return _agencia; }
            set { _agencia = value; }
        }

        private string _djAgencia;

        /// <summary>
        /// 
        /// </summary>
        public string djAgencia
        {
            get { return _djAgencia; }
            set { _djAgencia = value; }
        }

        private int _conta;

        /// <summary>
        /// 
        /// </summary>
        public int conta
        {
            get { return _conta; }
            set { _conta = value; }
        }

        private string _djConta;

        /// <summary>
        /// 
        /// </summary>
        public string djConta
        {
            get { return _djConta; }
            set { _djConta = value; }
        }

        //private int _NossoNumero;

        /// <summary>
        /// 
        /// </summary>
        public string NossoNumeroStr;

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero
        {
            get 
            {
                int retorno = 0;
                string manobra;
                if (Banco == 341)
                {
                    manobra = NossoNumeroStr.Substring(3).Trim();
                    if (manobra != "")
                        manobra = manobra.Substring(0, manobra.Length - 1);
                    int.TryParse(manobra, out retorno);
                }
                else
                {
                    manobra = NossoNumeroStr.Substring(4).Trim();
                    if (manobra != "")
                        manobra = manobra.Substring(0, manobra.Length - 1);
                    int.TryParse(manobra, out retorno);
                }
                return retorno;
            }
            //set { _NossoNumero = value; }
        }

        private DateTime _Vencimento;

        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento
        {
            get { return _Vencimento; }
            set { _Vencimento = value; }
        }

        private decimal _Valor;

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }

        private decimal _Tarifa;

        /// <summary>
        /// 
        /// </summary>
        public decimal Tarifa
        {
            get { return _Tarifa; }
            set { _Tarifa = value; }
        }        

        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int VerificadorNN
        {
            get
            {
                int retorno = 0;
                string manobra = NossoNumeroStr.Trim();
                if(manobra.Length == 0)
                    return 0;
                manobra = manobra.Substring(manobra.Length - 1);
                int.TryParse(manobra, out retorno);
                
                return retorno;
            }
        }

        /// <summary>
        /// Ocorrencia
        /// </summary>
        public int Ocorrencia { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string strOcorrencia
        {
            get
            {
                if (Ocorrencia == 100)
                    return "A4";
                else
                    return Ocorrencia.ToString("00");
            }
            set
            {
                if (value == "A4")
                    Ocorrencia = 100;
                else
                    Ocorrencia = int.Parse(value.Trim());
            }
        }

        /// <summary>
        /// MotivoOcorrencia
        /// </summary>
        public string MotivoOcorrencia { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Carteira { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            if (Banco == 104)
                RegistraFortementeTipado("Ocorrencia", 16, 2);
            else if (Banco == 33)
                RegistraFortementeTipado("strOcorrencia", 16, 2);
            RegistraFortementeTipado("agencia", 18, 5);
            RegistraFortementeTipado("djAgencia", 23, 1);
            RegistraFortementeTipado("conta", 24, 12);
            RegistraFortementeTipado("djConta", 36, 1);
            RegistraFortementeTipado("NossoNumeroStr", 38, 20);
            //if (Banco == 341)
            //{ 
            //    RegistraFortementeTipado("NossoNumero", 41, 17);                
            // }
            //else
            //{
            //    RegistraFortementeTipado("NossoNumero", 42, 15);
            //    RegistraFortementeTipado("VerificadorNN", 57, 1);
            //};
            if (Banco == 104)
                RegistraFortementeTipado("Carteira", 58, 1);
            RegistraFortementeTipado("Vencimento", 74, 8);
            RegistraFortementeTipado("Valor", 82, 15);
            RegistraFortementeTipado("Tarifa", 199, 15);
            if (Banco == 104)
                RegistraFortementeTipado("MotivoOcorrencia", 214, 10);
            else if (Banco == 33)
                RegistraFortementeTipado("MotivoOcorrencia", 209, 10);
        }



    }

    /// <summary>
    /// Detalhe U
    /// </summary>
    public class regDetalhe_3_U : regDetalhe_3
    {
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_U(cTabelaTXT _cTabelaTXT, string Linha)
            : base(_cTabelaTXT, Linha)
        {
            Segmento = "U";
        }
      
                private decimal _VlPago;

                /// <summary>
                /// 
                /// </summary>
                public decimal VlPago
        {
            get { return _VlPago; }
            set { _VlPago = value; }
        }

        private decimal _VlLiquido;

        /// <summary>
        /// 
        /// </summary>
        public decimal VlLiquido
        {
            get { return _VlLiquido; }
            set { _VlLiquido = value; }
        }

        private DateTime _DataPag;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataPag
        {
            get { return _DataPag; }
            set { _DataPag = value; }
        }

        private DateTime _DataCred;

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataCred
        {
            get { return _DataCred; }
            set { _DataCred = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("VlPago", 78, 15);
            RegistraFortementeTipado("VlLiquido", 93, 15);
            RegistraFortementeTipado("DataPag", 138, 8);
            RegistraFortementeTipado("DataCred", 146, 8);
        }
    }

    /// <summary>
    /// Detalhe E
    /// </summary>
    public class regDetalhe_3_E : regDetalhe_3
    {
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_E(cTabelaTXT _cTabelaTXT, string Linha,int? Banco)
            : base(_cTabelaTXT, Linha, Banco)
        {
            Segmento = "E";
        }

        #region Variaveis Tipadas
        /// <summary>
        /// 
        /// </summary>
        public int agencia;
        /// <summary>
        /// 
        /// </summary>
        public string djagencia;
        /// <summary>
        /// 
        /// </summary>
        public long conta;
        /// <summary>
        /// 
        /// </summary>
        public string djconta;
        /// <summary>
        /// 
        /// </summary>
        public string Nome;
        /// <summary>
        /// 
        /// </summary>
        public string Natureza;
        /// <summary>
        /// 
        /// </summary>
        public int TipoComplemento;
        /// <summary>
        /// 
        /// </summary>
        public string Complemento;
        //public DateTime DataContabil;
        /// <summary>
        /// 
        /// </summary>
        public DateTime DataLancamento;
        /// <summary>
        /// 
        /// </summary>
        public decimal ValorLancamento;
        /// <summary>
        /// 
        /// </summary>
        public string DebitoCredito;
        /// <summary>
        /// 
        /// </summary>
        public int Categoria;
        /// <summary>
        /// 
        /// </summary>
        public string CodigoHistorico;
        /// <summary>
        /// 
        /// </summary>
        public string Historico;

        private string _Documento;

        /// <summary>
        /// 
        /// </summary>
        public string Documento
        {
            get => _Documento;
            set
            {

                _Documento = "";
                foreach (char c in value)
                    if (char.IsLetterOrDigit(c))
                        _Documento += c;
            }
        }

        /// <summary>
        /// Documento na forma num�rica
        /// </summary>
        public int nDocumento
        {
            get
            {
                int retorno = 0;
                int.TryParse(Documento, out retorno);
                return retorno;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valorcomsinal
        {
            get { return (DebitoCredito == "D") ? -ValorLancamento : ValorLancamento; }
            set
            {
                ValorLancamento = Math.Abs(value);
                DebitoCredito = (value < 0 ? "D" : "C");
            }
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public TiposDeLancamentoExtrato TipoDeLancamentoExtrato;

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("agencia", 53, 5);
            RegistraFortementeTipado("djagencia", 58, 1);
            RegistraFortementeTipado("conta", 59, 12);
            RegistraFortementeTipado("djconta", 71, 1);
            RegistraFortementeTipado("Nome", 73, 30);
            RegistraFortementeTipado("Natureza", 109, 3);
            RegistraFortementeTipado("TipoComplemento", 112, 2);
            RegistraFortementeTipado("Complemento", 114, 20);
            //RegistraFortementeTipado("DataContabil", 135, 8);
            RegistraFortementeTipado("DataLancamento", 143, 8);
            RegistraFortementeTipado("ValorLancamento", 151, 18);
            RegistraFortementeTipado("DebitoCredito", 169, 1);
            RegistraFortementeTipado("Categoria", 170, 3);
            RegistraFortementeTipado("CodigoHistorico", 173, 4);
            RegistraFortementeTipado("Historico", 177, 25);
            if(Banco == 104)
                RegistraFortementeTipado("Documento", 202, 7);
            else
                RegistraFortementeTipado("Documento", 202, 39);
        }

    }

    /// <summary>
    /// Detalhe P (Remessa)
    /// </summary>
    public class regDetalhe_3_P : regDetalhe_3
    {        
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="HLote"></param>
        /// <param name="NumeroRegistro"></param>
        public regDetalhe_3_P(regHeaderLote_1_01 HLote,int NumeroRegistro)
            : base(HLote)
        {
            Segmento = "P";            
            this.NumeroRegistro = NumeroRegistro;
        }

        
        /// <summary>
        /// 
        /// </summary>              
        public int agencia { get { return HLote == null ? 0 : HLote.agencia; } }

        /// <summary>
        /// 
        /// </summary>                      
        public int agencia1 { get { return agencia; } }

        /// <summary>
        /// 
        /// </summary>
        public string djAgencia { get { return HLote == null ? "" : HLote.djagencia; } }

        /// <summary>
        /// 
        /// </summary>
        public string djAgencia1 { get { return djAgencia; } }

        /// <summary>
        /// 
        /// </summary>
        public int convenio { get { return HLote == null ? 0 : HLote.Convenio; } }

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string NNVer
        {
            get
            {
                return BoletosCalc.BoletoCalc.Modulo11_2_9(NossoNumero.ToString(),true);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero1 { get { return NossoNumero; } }

        /// <summary>
        /// 
        /// </summary>
        public int NossoNumero2 { get { return NossoNumero; } }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal Valor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime EmissaoBoleto { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int CodOperacao { get; set; }

        /// <summary>
        /// Conta
        /// </summary>
        public long Conta { get { return HLote == null ? 0 : HLote.conta; } }

        /// <summary>
        /// 
        /// </summary>
        public long ContaDest { get { return HLote == null ? 0 : HLote.conta; } }

        /// <summary>
        /// digito da conta
        /// </summary>
        public string djconta { get { return HLote == null ? "" : HLote.djconta; } }

        /// <summary>
        /// 
        /// </summary>
        public string djcontaDest { get { return HLote == null ? "" : HLote.djconta; } }

        /// <summary>
        /// Registra Mapa
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodOperacao"    ,  16, 2);
            if (Banco == 33)
            {
                RegistraFortementeTipado("agencia"    ,  18,  4);
                RegistraFortementeTipado("djAgencia"  ,  22,  1);
                RegistraFortementeTipado("Conta"      ,  23,  9);
                RegistraFortementeTipado("djconta"    ,  32,  1);
                RegistraFortementeTipado("ContaDest"  ,  33,  9);
                RegistraFortementeTipado("djcontaDest",  42,  1);
                RegistraFortementeTipado("NossoNumero",  45, 12);
                RegistraFortementeTipado("NNVer"      ,  57,  1);
                RegistraMapa(            "Modalidade1",  58,  1, 5);
                RegistraMapa(            "Cadastramen",  59,  2, "12");
            }
            else
            {
                RegistraFortementeTipado("agencia"    ,  18,  5);
                RegistraFortementeTipado("djAgencia"  ,  23,  1);
                RegistraFortementeTipado("convenio"   ,  24,  6);
                RegistraMapa(            "Zeros"      ,  30, 11, 0);
                RegistraMapa(            "Modalidade" ,  41,  2, 14);
                RegistraFortementeTipado("NossoNumero",  43, 15);
                RegistraMapa(            "Modalidade1",  58,  1, 1);
                RegistraMapa(            "Cadastramen",  59,  4, "1220");
            }                        
                        
            RegistraFortementeTipado("NossoNumero1"   ,  63, 11);
            RegistraFortementeTipado("Vencimento"     ,  78,  8);
            RegistraFortementeTipado("Valor"          ,  86, 15);
            if ((Banco == 33) || (Banco == 104))
            {
                RegistraMapa(        "zagencia1"      ,  101, 5,0);
                //RegistraMapa(        "zdjAgencia1"    ,  106, 1,0);
            }
            else
            {
                RegistraFortementeTipado("agencia1"  ,  101, 5);
                RegistraFortementeTipado("djAgencia1",  106, 1);                
            }
            RegistraMapa(             "Literal"       , 107, 3, "17N");
            RegistraFortementeTipado("EmissaoBoleto"  , 110, 8);
            RegistraMapa(            "CodJuros"       , 118, 1, 3);
            RegistraMapa(            "Zeros1"         , 119,47, 0);
            RegistraMapa(            "Zeros2"         , 166,30, 0);
            RegistraFortementeTipado("NossoNumero2"   , 196,11);
            if (Banco == 33)
            {
                RegistraMapa(        "Protesto"       , 221, 3, 000);
                RegistraMapa(        "Baixa"          , 224, 4, "1030");
                RegistraMapa(        "moeda"          , 228, 2, 0);
                RegistraMapa(        "Branco3"        , 230, 10, "");
            }
            else
            {
                RegistraMapa(        "Protesto"       , 221, 3, 300);
                RegistraMapa(        "Baixa"          , 224, 4, "1030");
                RegistraMapa(        "moeda"          , 228, 2, 9);
                RegistraMapa(        "Zero3"          , 230, 10, 0);
            }                        
            RegistraMapa(            "Branco"         , 240, 1, "");            
        }



    }

    /// <summary>
    /// 
    /// </summary>
    public class regDetalhe_3_Q : regDetalhe_3
    {

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_Q(regDetalhe_3_P regP, int NumeroRegistro)
            : base(regP.HLote)
        {
            Segmento = "Q";
            this.regP = regP;                        
            this.NumeroRegistro = NumeroRegistro;
        }

        private regDetalhe_3_P regP;


        /// <summary>
        /// 
        /// </summary>
        public int CodOperacao { get { return regP == null ? 0 : regP.CodOperacao; } }


        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CPF { get; set; }
        

        /// <summary>
        /// 
        /// </summary>
        public int tipoCPF
        {
            get { return CPF == null ? 2 : CPF.Tipo == TipoCpfCnpj.CPF ? 1 : 2; }            
        }

        /// <summary>
        /// 
        /// </summary>
        public long nCPF
        {
            get { return CPF == null ? 0 : CPF.Valor; }            
        }


        /// <summary>
        /// 
        /// </summary>
        public string Nome { get;set; }

        /// <summary>
        /// 
        /// </summary>
        public string Endereco { get;set; }

        /// <summary>
        /// 
        /// </summary>
        public string Bairro { get;set; }

        /// <summary>
        /// 
        /// </summary>
        public string CEP { get;set; }

        /// <summary>
        /// 
        /// </summary>
        public string Cidade { get;set; }

        /// <summary>
        /// 
        /// </summary>
        public string UF { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();            
            RegistraFortementeTipado("CodOperacao", 16,  2);
            RegistraFortementeTipado("tipoCPF"    , 18,  1);
            RegistraFortementeTipado("nCPF"       , 19, 15);
            RegistraFortementeTipado("Nome"       , 34, 40);
            RegistraFortementeTipado("Endereco"   , 74, 40);
            RegistraFortementeTipado("Bairro"     ,114, 15);
            RegistraFortementeTipado("CEP"        ,129,  8);
            RegistraFortementeTipado("Cidade"     ,137, 15);
            RegistraFortementeTipado("UF"         ,152,  2);
            if (Banco == 33)
            {
                RegistraMapa("Avalista"          , 154, 16, 0);
                RegistraMapa("zerocarne"         , 210, 12, 0);
            };
            if (Banco == 104)
            {
                RegistraMapa("Avalista", 154, 16, 0);                
            };
            RegistraMapa(            "Branco"     ,233,  8, "");
        }



    }

    /// <summary>
    /// Registro tipo R
    /// </summary>
    public class regDetalhe_3_R : regDetalhe_3
    {

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_R(regDetalhe_3_P regP, int NumeroRegistro)
            : base(regP.HLote)
        {
            Segmento = "R";
            this.regP = regP;            
            this.NumeroRegistro = NumeroRegistro;
        }

        private regDetalhe_3_P regP;
        
        /// <summary>
        /// 
        /// </summary>
        public int CodOperacao { get { return regP == null ? 0 : regP.CodOperacao; } }

        /// <summary>
        /// Cobra multa
        /// </summary>
        public bool CobrarMulta;

        /// <summary>
        /// Multa
        /// </summary>
        public int CodMulta { get { return CobrarMulta ? 2 : 0; } }

        /// <summary>
        /// 
        /// </summary>
        public decimal _PercMulta;
        /// <summary>
        /// 
        /// </summary>
        public decimal PercMulta
        {
            get { return CobrarMulta ? _PercMulta : 0; }
            set { _PercMulta = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Vencimento { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected override void RegistraMapa()
        {
            base.RegistraMapa();
            RegistraFortementeTipado("CodOperacao", 16,  2);
            RegistraMapa(            "Desc2"      , 18, 24, 0);
            if(Banco != 33)
                RegistraMapa(        "Desc3"      , 42, 24, 0);
            RegistraFortementeTipado("CodMulta"   , 66,  1);
            if(Banco == 104)
                RegistraFortementeTipado("Vencimento", 67, 8);
            else
                RegistraMapa(            "DataMulta"  , 67,  8, 0);
            RegistraFortementeTipado("PercMulta"  , 75, 15);

            RegistraMapa(            "Branco"     ,230, 11, "");
        }
    }

    /// <summary>
    /// Conjunto cobran�a Retorno
    /// </summary>
    public class ConjuntoDetalheCobranca
    {
        /// <summary>
        /// 
        /// </summary>
        public ConjuntoDetalheCobranca(regDetalhe_3_T _reg3T, regDetalhe_3_U _reg3U)
        {
            reg3T = _reg3T;
            reg3U = _reg3U;
        }

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_T reg3T;
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_U reg3U;
    }

    /// <summary>
    /// Conjunto cobran�a Remessa
    /// </summary>
    public class ConjuntoDetalheCobrancaR
    {
        /// <summary>
        /// 
        /// </summary>
        public ConjuntoDetalheCobrancaR(regDetalhe_3_P _reg3P, regDetalhe_3_Q _reg3Q, regDetalhe_3_R _reg3R)
        {
            reg3P = _reg3P;
            reg3Q = _reg3Q;
            reg3R = _reg3R;
        }

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_P reg3P;
        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_Q reg3Q;

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_R reg3R;
    }

    /// <summary>
    /// 
    /// </summary>
    public class ColLotesCobranca : CollectionBase
    {

        /// <summary>
        /// 
        /// </summary>
        public regHeaderLote_1_01 this[int index]
        {
            get
            {
                return ((regHeaderLote_1_01)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regHeaderLote_1_01 value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regHeaderLote_1_01 value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regHeaderLote_1_01 value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regHeaderLote_1_01 value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regHeaderLote_1_01 value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regHeaderLote_1_01))
                throw new ArgumentException("value must be of type regHeaderLote_1_01.", "value");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ColConjuntoDetalheCobranca : CollectionBase
    {

        /// <summary>
        /// 
        /// </summary>
        public ConjuntoDetalheCobranca this[int index]
        {
            get
            {
                return ((ConjuntoDetalheCobranca)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(ConjuntoDetalheCobranca value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regDetalhe_3_T regT, regDetalhe_3_U regU)
        {
            return (List.Add(new ConjuntoDetalheCobranca(regT,regU)));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(ConjuntoDetalheCobranca value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, ConjuntoDetalheCobranca value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(ConjuntoDetalheCobranca value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(ConjuntoDetalheCobranca value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(ConjuntoDetalheCobranca))
                throw new ArgumentException("value must be of type ConjuntoDetalheCobranca.", "value");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ColLotesExtrato : CollectionBase
    {

        /// <summary>
        /// 
        /// </summary>
        public regHeaderLote_1_E_04 this[int index]
        {
            get
            {
                return ((regHeaderLote_1_E_04)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regHeaderLote_1_E_04 value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regHeaderLote_1_E_04 value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regHeaderLote_1_E_04 value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regHeaderLote_1_E_04 value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regHeaderLote_1_E_04 value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regHeaderLote_1_E_04))
                throw new ArgumentException("value must be of type regHeaderLote_1_01.", "value");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class ColLancamentos : CollectionBase
    {

        /// <summary>
        /// 
        /// </summary>
        public regDetalhe_3_E this[int index]
        {
            get
            {
                return ((regDetalhe_3_E)List[index]);
            }
            set
            {
                List[index] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Add(regDetalhe_3_E value)
        {
            return (List.Add(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public int IndexOf(regDetalhe_3_E value)
        {
            return (List.IndexOf(value));
        }

        /// <summary>
        /// 
        /// </summary>
        public void Insert(int index, regDetalhe_3_E value)
        {
            List.Insert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Remove(regDetalhe_3_E value)
        {
            List.Remove(value);
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Contains(regDetalhe_3_E value)
        {
            // If value is not of type Int16, this will return false.
            return (List.Contains(value));
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnValidate(Object value)
        {
            if (value.GetType() != typeof(regDetalhe_3_E))
                throw new ArgumentException("value must be of type regDetalhe_3_E.", "value");
        }

    }

    /// <summary>
    /// Tipos de lan�amento
    /// </summary>
    public enum TiposDeLancamentoExtrato
    {
        /// <summary>
        /// Antigo
        /// </summary>
        Antiga,
        /// <summary>
        /// Retroativo
        /// </summary>
        Retroativo,
        /// <summary>
        /// Do dia
        /// </summary>
        dia,
        /// <summary>
        /// Futuro
        /// </summary>
        futuro
    }
}
