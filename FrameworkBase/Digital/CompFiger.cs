using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using GrFingerXLib;

namespace Digital
{
    /// <summary>
    /// Leitor de Digitais
    /// </summary>
    internal partial class CompFiger : UserControl
    {
              
        /// <summary>
        /// Ultima digital lida
        /// </summary>
        public System.Array UltimaDigitalLida;
        public int ID;


        public DataTable Imagens;

        /// <summary>
        /// Construtor
        /// </summary>
        public CompFiger()
        {
            InitializeComponent();
            labelControl1.Text = "";
            //Imagens = new System.Collections.ArrayList();
            
        }

        private bool Valida(System.Array Dig1,System.Array Dig2) {
            int nota=0;
            return (((GRConstants)axGrFingerXCtrl1.Verify(ref Dig1, ref Dig2, ref nota, (int)GRConstants.GR_DEFAULT_CONTEXT)) == GRConstants.GR_MATCH);
        }


               
        private int BuscaLocal(System.Array Dig1)
        {
            int nota = 0;
            axGrFingerXCtrl1.IdentifyPrepare(ref Dig1, (int)GRConstants.GR_DEFAULT_CONTEXT);
            foreach (DataRow Candidato in Imagens.Rows) {
                System.Array vetor = (System.Array)Candidato["DIGvetor"];
                if ((GRConstants)axGrFingerXCtrl1.Identify(ref vetor,ref nota, (int)GRConstants.GR_DEFAULT_CONTEXT) == GRConstants.GR_MATCH)
                    return (int)Candidato["DIG_USU"];
            }
            return -1;
        }


        /// <summary>
        /// Cadastra uma digital no banco da ram
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="vetor"></param>
        public void Incluir(int ID,System.Array vetor) {
            DataRow Nova = Imagens.NewRow();
            Nova["DIG_USU"] = ID;
            Nova["DIGVetor"] = vetor;
            Imagens.Rows.Add(Nova);
        }

        

        
        /// <summary>
        /// Grava um ID para a ultima digital lida
        /// </summary>
        /// <param name="ID"></param>
        public void Incluir(int ID)
        {
            Incluir(ID,UltimaDigitalLida);
        }
        

        /// <summary>
        /// Inicia dll
        /// </summary>
        /// <returns></returns>
        public bool IniciaDll(){
            GRConstants Resposta = (GRConstants)axGrFingerXCtrl1.Initialize();
            if (Resposta != GRConstants.GR_OK)
            {
                string Texto = Resposta.ToString();
                MessageBox.Show(Texto);
                return false;
            };
            if(axGrFingerXCtrl1.CapInitialize() == 0){
                CHDLL.Checked =true;
                return true;
            };
            return false;
            
            
        }

        private string idSensor;

        private void axGrFingerXCtrl1_SensorPlug(object sender, AxGrFingerXLib._IGrFingerXCtrlEvents_SensorPlugEvent e)
        {
            if (e.idSensor != "File")
            {
                CHLeitor.Checked = true;
                labelControl1.Text = e.idSensor;
                idSensor = e.idSensor;
            }                        
        }

        public bool Ligado = false;

        public void Liga() {
            if (idSensor != null)
            {
                axGrFingerXCtrl1.CapStartCapture(idSensor);
                Ligado = true;
            }
        }

        public void Desligar(){
            axGrFingerXCtrl1.CapStopCapture(idSensor);
            Ligado = false;
        }

        private void axGrFingerXCtrl1_ImageAcquired(object sender, AxGrFingerXLib._IGrFingerXCtrlEvents_ImageAcquiredEvent e)
        {            
            
            int Tamanho = (int)GRConstants.GR_MAX_SIZE_TEMPLATE;
            UltimaDigitalLida = new byte[Tamanho];
            int result = (int)axGrFingerXCtrl1.Extract(ref e.rawImage, e.width, e.height,e.res,
                ref UltimaDigitalLida, ref Tamanho,
                (int)GRConstants.GR_DEFAULT_CONTEXT);
            
            if (result < 0)
            {
                UltimaDigitalLida = null;              
                ID = -1;
            };
            Retorno.EditValue = result;
            if (Imagens != null)
                ID = BuscaLocal(UltimaDigitalLida);
            if(DigitalLida != null)
               DigitalLida(this,new EventArgs());                       
        }

        public event EventHandler DigitalLida;

        private void axGrFingerXCtrl1_SensorUnplug(object sender, AxGrFingerXLib._IGrFingerXCtrlEvents_SensorUnplugEvent e)
        {
            axGrFingerXCtrl1.CapFinalize();
            axGrFingerXCtrl1.Finalize();
        }
    }
}