namespace Digital
{
    partial class CompFiger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompFiger));
            this.axGrFingerXCtrl1 = new AxGrFingerXLib.AxGrFingerXCtrl();
            this.CHDLL = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.CHLeitor = new DevExpress.XtraEditors.CheckEdit();
            this.Retorno = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.axGrFingerXCtrl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHDLL.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHLeitor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Retorno.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // axGrFingerXCtrl1
            // 
            this.axGrFingerXCtrl1.Enabled = true;
            this.axGrFingerXCtrl1.Location = new System.Drawing.Point(151, 3);
            this.axGrFingerXCtrl1.Name = "axGrFingerXCtrl1";
            this.axGrFingerXCtrl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axGrFingerXCtrl1.OcxState")));
            this.axGrFingerXCtrl1.Size = new System.Drawing.Size(32, 32);
            this.axGrFingerXCtrl1.TabIndex = 0;
            this.axGrFingerXCtrl1.SensorPlug += new AxGrFingerXLib._IGrFingerXCtrlEvents_SensorPlugEventHandler(this.axGrFingerXCtrl1_SensorPlug);
            this.axGrFingerXCtrl1.SensorUnplug += new AxGrFingerXLib._IGrFingerXCtrlEvents_SensorUnplugEventHandler(this.axGrFingerXCtrl1_SensorUnplug);
            this.axGrFingerXCtrl1.ImageAcquired += new AxGrFingerXLib._IGrFingerXCtrlEvents_ImageAcquiredEventHandler(this.axGrFingerXCtrl1_ImageAcquired);
            // 
            // CHDLL
            // 
            this.CHDLL.Location = new System.Drawing.Point(4, 3);
            this.CHDLL.Name = "CHDLL";
            this.CHDLL.Properties.Caption = "DLL ativa";
            this.CHDLL.Size = new System.Drawing.Size(75, 18);
            this.CHDLL.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(91, 31);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "labelControl1";
            // 
            // CHLeitor
            // 
            this.CHLeitor.Location = new System.Drawing.Point(4, 28);
            this.CHLeitor.Name = "CHLeitor";
            this.CHLeitor.Properties.Caption = "Leitor ligado";
            this.CHLeitor.Size = new System.Drawing.Size(81, 18);
            this.CHLeitor.TabIndex = 3;
            // 
            // Retorno
            // 
            this.Retorno.Location = new System.Drawing.Point(6, 53);
            this.Retorno.Name = "Retorno";
            this.Retorno.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(-1, "Aguardando"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "Baixa qualidade"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "M�dia Qualidade"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "Boa Qualidade")});
            this.Retorno.Size = new System.Drawing.Size(177, 96);
            this.Retorno.TabIndex = 4;
            // 
            // CompFiger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Retorno);
            this.Controls.Add(this.CHLeitor);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.CHDLL);
            this.Controls.Add(this.axGrFingerXCtrl1);
            this.Name = "CompFiger";
            this.Size = new System.Drawing.Size(189, 157);
            ((System.ComponentModel.ISupportInitialize)(this.axGrFingerXCtrl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHDLL.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CHLeitor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Retorno.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxGrFingerXLib.AxGrFingerXCtrl axGrFingerXCtrl1;
        private DevExpress.XtraEditors.CheckEdit CHDLL;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit CHLeitor;
        private DevExpress.XtraEditors.RadioGroup Retorno;
    }
}
