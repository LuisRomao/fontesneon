using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace CodBar
{
    /// <summary>
    /// Classe para impressao de etiquetas
    /// </summary>
    /// <remarks>
    /// Existem duas formas de opera��o
    /// 1) IniciaImpressora, [imprime][imprimeln] ... ,TerminaImpressora
    /// 2) new Impress(mascara), Reset, [SetaVariavel] ... ImprimeDOC 
    /// </remarks>
    public class Impress
    {
        /// <summary>
        /// �ltimo erro
        /// </summary>
        public Exception UltimoErro = null;

        /// <summary>
        /// Remove os acentos
        /// </summary>
        public bool RemoverAcentos = false;

        /// <summary>
        /// Nome dos documentos
        /// </summary>
        protected string NomeDoDoc = "Etiqueta Codigo Barras";

        /// <summary>
        /// Mascara de impressao
        /// </summary>
        public string Mascara = "";

        private string MascaraManobra;
        private System.IntPtr lhPrinter;
        private DOCINFO di;

        /// <summary>
        /// Nome da impressora
        /// </summary>
        public static string _NomeImpressoraSt = "";

        private string _NomeImpressora = "";

        /// <summary>
        /// Nome da impressora
        /// </summary>
        /// <remarks>Se n�o for setado uma caixa de di�logo abrir� automaticamente</remarks>
        public string NomeImpressora
        {
            get 
            {
                if (_NomeImpressora != "")
                    return _NomeImpressora;
                else
                {
                    if (_NomeImpressoraSt == "")
                    {
                        _NomeImpressoraSt = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Impressora de c�pias de Cheque");
                        if (_NomeImpressoraSt == "")
                        {
                            PrintDialog PD = new PrintDialog();
                            PD.ShowDialog();
                            _NomeImpressoraSt = PD.PrinterSettings.PrinterName;
                            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Impressora de c�pias de Cheque", _NomeImpressoraSt);
                        }
                    }
                    return _NomeImpressoraSt;
                }
            }
            set { _NomeImpressora = value; }
        }
        private bool _UsarFormFeed = false;

        /// <summary>
        /// Se ao final da impressao � enviado um \f
        /// </summary>
        public bool UsarFormFeed
        {
            get { return _UsarFormFeed; }
            set { _UsarFormFeed = value; }
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct DOCINFO
        {
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pDataType;
        }

        private class PrintDirect
        {
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long OpenPrinter(string pPrinterName, ref IntPtr phPrinter, int pDefault);
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = false,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long StartDocPrinter(IntPtr hPrinter, int Level, ref DOCINFO pDocInfo);
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long StartPagePrinter(IntPtr hPrinter);
            [DllImport("winspool.drv", CharSet = CharSet.Ansi, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]            
            public static extern long WritePrinter(IntPtr hPrinter, string data, int buf, ref int pcWritten);
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long EndPagePrinter(IntPtr hPrinter);
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long EndDocPrinter(IntPtr hPrinter);
            [DllImport("winspool.drv", CharSet = CharSet.Unicode, ExactSpelling = true,
             CallingConvention = CallingConvention.StdCall)]
            public static extern long ClosePrinter(IntPtr hPrinter);

            //[DllImport("winspool.drv", CharSet = CharSet.Ansi, ExactSpelling = true,
            // CallingConvention = CallingConvention.StdCall)]
            //public static extern long WritePrinterdata(IntPtr hPrinter, string data, int buf, ref int pcWritten);
        }

        
        /// <summary>
        /// Inicia a impressao
        /// </summary>        
        public void IniciaImpressora()
        {

            lhPrinter = new System.IntPtr();
            di = new DOCINFO();
            di.pDocName = NomeDoDoc;
            di.pDataType = "RAW";
            if (NomeImpressora == "")
            {                
                    System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                    PD.ShowDialog();                    
                    NomeImpressora = PD.PrinterSettings.PrinterName;                
            };

            PrintDirect.OpenPrinter(NomeImpressora, ref lhPrinter, 0);

            PrintDirect.StartDocPrinter(lhPrinter, 1, ref di);
            PrintDirect.StartPagePrinter(lhPrinter);
        }

        /// <summary>
        /// Termina o documento e faz a impress�o
        /// </summary>
        public void TerminaImpressora()
        {
            if(_UsarFormFeed)
                 Imprime("\f");
            PrintDirect.EndPagePrinter(lhPrinter);
            PrintDirect.EndDocPrinter(lhPrinter);
            PrintDirect.ClosePrinter(lhPrinter);
            if (!Guilhotina)
                MessageBox.Show("Cortar impresso");
        }

        /// <summary>
        /// Imprime uma linha com \r\n no final
        /// </summary>
        /// <param name="Linha"></param>
        /// <returns>Bytes impressos</returns>
        /// <remarks>
        /// Este comando dever ser usado entre IniciaImpressora e TerminaImpressora
        /// Se ocorrer algum erro ele ser� reportado em UltimoErro
        /// </remarks>
        public Int32 ImprimeLn(String Linha)
        {
            return Imprime(Linha + "\r\n");
        }

        /// <summary>
        /// Imprime uma string
        /// </summary>
        /// <param name="Linha"></param>
        /// <returns>Bytes impressos</returns>
        /// <remarks>
        /// Este comando dever ser usado entre IniciaImpressora e TerminaImpressora
        /// Se ocorrer algum erro ele ser� reportado em UltimoErro
        /// </remarks>
        public Int32 Imprime(String Linha)
        {
            try
            {
                int pcWritten = 0;
                PrintDirect.WritePrinter(lhPrinter, Linha, Linha.Length, ref pcWritten);
                return pcWritten;
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return 0;
            };

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"></param>
        /// <param name="Arquivo"></param>
        public void CarregaPCX(string nome,string Arquivo) {
            IniciaImpressora();
            System.IO.FileInfo fi = new System.IO.FileInfo(Arquivo);
            Imprime("\r\nGM\""+nome+"\""+fi.Length.ToString()+"\r\n");
            ImprimeArquivo(Arquivo);
            Imprime("\r\nGI\r\n");
            //ImpTeste.ImprimeArquivo("c:\\modelo 4.txt");
            TerminaImpressora();
        }

        /// <summary>
        /// Imprime um arquivo. Pode ser usado para carga de arquivos na memoria (PCX)
        /// </summary>        
        /// <param name="nome"></param>
        /// <returns>Bytes impressos</returns>
        /// <remarks>
        /// Este comando dever ser usado entre IniciaImpressora e TerminaImpressora
        /// Se ocorrer algum erro ele ser� reportado em UltimoErro
        /// </remarks>
        public Int32 ImprimeArquivo(string nome) {
            
            try
            {
                if (!System.IO.File.Exists(nome))
                    throw new Exception("Arquivo nao existe");
                System.IO.FileStream Arq = new System.IO.FileStream(nome, System.IO.FileMode.Open, System.IO.FileAccess.Read);
                //System.IO.FileStream Teste = new System.IO.FileStream("c:\\teste.pcx", System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);


                int pcWritten = 0;
                int TOTpcWritten = 0;
                byte[] buf = new byte[1024];
                int lidos = 1;
                string Enviar = "";
                while (lidos > 0)
                {
                    lidos = Arq.Read(buf, 0, 1024);
                    if (lidos > 0)
                    {
                        //string Enviar = Encoding.UTF8.GetString(buf, 0, lidos);
                        
                        for (int i = 0; i < lidos; i++)
                        {
                            char c = (char)buf[i];
                            Enviar += c;
                        };                        
                        PrintDirect.WritePrinter(lhPrinter, Enviar, lidos, ref pcWritten);
                        

                        
                        //foreach (char c in Enviar)
                        //    Teste.WriteByte((byte)c);
                        Enviar = "";
                        TOTpcWritten += pcWritten;
                    };
                };
                return TOTpcWritten;
            }
            catch (Exception e)
            {
                UltimoErro = e;
                return 0;
            };
        }

        private LinguagemCodBar _Linguagem = LinguagemCodBar.Indefinida;

        private static LinguagemCodBar _LinguagemSt = LinguagemCodBar.Indefinida;

        private LinguagemCodBar Linguagem
        {
            set { _Linguagem = value; }
            get
            {
                if (_Linguagem != LinguagemCodBar.Indefinida)
                    return _Linguagem;
                else
                {
                    if (_LinguagemSt == LinguagemCodBar.Indefinida)
                    {
                        try
                        {
                            _LinguagemSt = (LinguagemCodBar)Enum.Parse(typeof(CodBar.LinguagemCodBar), CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Linguagem codbar"));
                        }
                        catch
                        {
                            _LinguagemSt = CodBar.LinguagemCodBar.EPL2;
                            CompontesBasicos.dEstacao.dEstacaoSt.SetValor("Linguagem codbar", _LinguagemSt.ToString());
                        }
                    }
                    return _LinguagemSt;
                }
            }
        }

        /// <summary>
        /// Cosntrutor
        /// </summary>
        /// <param name="Linguagem">Linguagem</param>
        /// <param name="NomeImpressora"></param>
        /// <param name="Mascara">Mascara de impress�o</param>
        public Impress(LinguagemCodBar Linguagem = LinguagemCodBar.Indefinida,string NomeImpressora = "", string Mascara = "")
        {
            MascaraManobra = this.Mascara = Mascara;
            this.Linguagem = Linguagem;
            this.NomeImpressora = NomeImpressora;            
        }

     

        /// <summary>
        /// Substitui um coringa na mascara por um valor
        /// </summary>
        /// <param name="Codigo"></param>
        /// <param name="Valor"></param>
        public void SetCampo(string Codigo, string Valor) {
            if (RemoverAcentos) {
                Valor = Valor.Replace('�', 'a');
                Valor = Valor.Replace('�', 'a');
                Valor = Valor.Replace('�', 'a');
                Valor = Valor.Replace('�', 'a');
                Valor = Valor.Replace('�', 'e');
                Valor = Valor.Replace('�', 'e');
                Valor = Valor.Replace('�', 'i');
                Valor = Valor.Replace('�', 'i');
                Valor = Valor.Replace('�', 'o');
                Valor = Valor.Replace('�', 'o');
                Valor = Valor.Replace('�', 'o');
                Valor = Valor.Replace('�', 'u');
                Valor = Valor.Replace('�', 'c');

                Valor = Valor.Replace('�', 'A');
                Valor = Valor.Replace('�', 'A');
                Valor = Valor.Replace('�', 'A');
                Valor = Valor.Replace('�', 'A');
                Valor = Valor.Replace('�', 'E');
                Valor = Valor.Replace('�', 'E');
                Valor = Valor.Replace('�', 'I');
                Valor = Valor.Replace('�', 'I');
                Valor = Valor.Replace('�', 'O');
                Valor = Valor.Replace('�', 'O');
                Valor = Valor.Replace('�', 'O');
                Valor = Valor.Replace('�', 'U');
                Valor = Valor.Replace('�', 'C');
            } 
            MascaraManobra = MascaraManobra.Replace(Codigo, Valor);
            if (MascaraControles != null)
                foreach (PortaControle PC in MascaraControles)
                {
                    PC.SetCampo(Codigo, Valor);
                    
                }
        }

        /// <summary>
        /// Retorna a mascara original sem a substitui��o dos coringas
        /// </summary>
        public void Reset() 
        {
            MascaraManobra = Mascara;
            if (MascaraControles != null)
                foreach (PortaControle PC in MascaraControles)
                    PC.Reset();
        }

        /// <summary>
        /// Imprime a mascara
        /// </summary>
        /// <returns>se a impress�o ocorreu sem erros</returns>
        /// <remarks>N�O usar IniciaImpressora nem TerminaImpressora</remarks>
        public bool ImprimeDOC() {
            try
            {
                if (NomeImpressora != "TELA")
                {
                    if (Linguagem != LinguagemCodBar.Windows)
                    {
                        IniciaImpressora();
                        Imprime(MascaraManobra);
                        TerminaImpressora();
                    }
                    else
                    {
                        if (NomeImpressora == "")
                        {
                            System.Windows.Forms.PrintDialog PD = new System.Windows.Forms.PrintDialog();
                            PD.ShowDialog();
                            NomeImpressora = PD.PrinterSettings.PrinterName;
                        };
                        if (NomeImpressora != "")
                            WinImp.PrinterName = NomeImpressora;
                        WinImp.CreateDocument();
                        WinImp.Print();
                    }
                }
                else
                {
                    System.Windows.Forms.Clipboard.SetText(MascaraManobra);
                    System.Windows.Forms.MessageBox.Show(MascaraManobra);                    
                }
                return true;
            }
            catch (Exception e){
                UltimoErro = e;
                return false;
            }
        }

        /// <summary>
        /// Imprime o DOC
        /// </summary>
        /// <param name="DOC">Conte�do a imprimir</param>
        /// <returns>se a impress�o ocorreu sem erros</returns>
        /// <remarks>N�O usar IniciaImpressora nem TerminaImpressora</remarks>
        public bool ImprimeDOC(string DOC)
        {            
                MascaraManobra = DOC;
                return ImprimeDOC();
        }

        /// <summary>
        /// Temperatura
        /// </summary>
        /// <param name="Temperatura"></param>
        public void ajustaTemperatura(int Temperatura)
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.DPL:
                case LinguagemCodBar.PM42:                   
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}",Linguagem));                    
                case LinguagemCodBar.EPL2:
                    ImprimeDOC(string.Format("D{0}\r\n",Temperatura));
                    break;                
                case LinguagemCodBar.Windows:
                    return;
            }            
        }

        #region Fun��es de alto nivel encapsulando a liguagem
        /// <summary>
        /// Orienta��o do texto
        /// </summary>
        public enum Orientacao
        {
            /// <summary>
            /// vertical
            /// </summary>
            desce = 1,
            /// <summary>
            /// normal
            /// </summary>
            normal = 4,
            /// <summary>
            /// vertical
            /// </summary>
            sobe = 3,
            /// <summary>
            /// de cabe�a para baixo
            /// </summary>
            invertido = 2,
        }

        /// <summary>
        /// Alinhamento do texto
        /// </summary>
        public enum Justificado 
        {
            /// <summary>
            /// 
            /// </summary>
            esquerda, 
            /// <summary>
            /// 
            /// </summary>
            direita ,
            /// <summary>
            /// 
            /// </summary>
            centro
        }

        private static int RotacaoEPL(Orientacao Or)
        {
            switch (Or)
            {
                case Orientacao.sobe:
                    return 0;
                case Orientacao.normal:
                default:
                    return 1;
                case Orientacao.desce:
                    return 2;
                case Orientacao.invertido:
                    return 3;
            }
        }

        private static string FonteEPL(int Tamanho)
        {
            switch (Tamanho)
            {
                case 0:
                    return "1,1,1";
                case 1:
                    return "2,1,1";
                case 2:
                    //return "4,1,1";
                    return "3,1,1";
                case 3:
                    return "1,2,2";
                case 4:
                    return "3,2,1";
                case 5:
                    return "3,2,2";
                case 6:
                    return "4,2,2";
                case 7:
                    return "2,4,3";
                case 8:
                    return "3,4,3";
                case 9:
                    return "3,5,4";
                case 10:
                    return "4,5,4";
                default:
                    return "1,3,3";
            }
        }

        private enum Lado { Vertical,Horizontal }

        private static int FonteEPLCorrecao(int Tamanho, Lado lado)
        {
            //fonte 1 = 12 dots | 2 = 16 | 3 = 20 | 4 = 24 | 5 = 48
            if (lado == Lado.Horizontal)
                switch (Tamanho)
                {
                    case 0:
                        return 10;
                    case 1:
                        return 14;
                    case 2:
                        return 18;//24;
                    case 3:
                        return 2 * 10; ;
                    case 4:
                        return 2 * 18;
                    case 5:
                        return 2 * 18;
                    case 6:
                        return 2 * 22;
                    case 7:
                        return 4 * 14;
                    case 8:
                        return 4 * 18;
                    case 9:
                        return 5 * 18;
                    case 10:
                        return 5 * 22;
                    default:
                        return 20;
                }
            else
                //fonte 1 = 10 dots | 2 = 12 | 3 = 14 | 4 = 16 | 5 = 34
                switch (Tamanho)
                {
                    case 0:
                        return 10;
                    case 1:
                        return 12;
                    case 2:
                        return 14;
                    case 3:
                        return 2 * 10; ;
                    case 4:
                        return 1 * 12;
                    case 5:
                        return 2 * 12;
                    case 6:
                        return 2 * 16;
                    case 7:
                        return 3 * 12;
                    case 8:
                        return 3 * 20;
                    case 9:
                        return 4 * 20;
                    case 10:
                        return 4 * 24;
                    default:
                        return 20;
                }
        }

        private static SizeF MedeTexto(string text, Font font,bool Tomba)
        {
            Graphics gr = Graphics.FromHwnd(IntPtr.Zero);
            gr.PageUnit = GraphicsUnit.Millimeter;
            SizeF size = gr.MeasureString(text, font);
            gr.Dispose();                        
            if (Tomba)
                return new SizeF(size.Height * 10F, size.Width * 10F);
            else
                return new SizeF(size.Width * 10F, size.Height * 10F);
        }

        private void AjustaTamanhoPagina(XRControl Controle)
        {
            if (WinImp.PageSize.Height < Controle.BottomF + 200)
                WinImp.PageSize = new Size(1200, (int)Controle.BottomF + 200);
        }

        private Justificado JusAtual = Justificado.esquerda;

        /// <summary>
        /// Gera c�digo para imprimir texto
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Texto"></param>
        /// <param name="Tamanho"></param>
        /// <param name="Or"></param>
        /// <param name="Reveso"></param>
        /// <param name="MH"></param>
        /// <param name="MV"></param>
        /// <param name="Jus"></param>
        /// <returns></returns>
        public string CodTXT(int X, int Y, string Texto, int Tamanho = 5, Orientacao Or = Orientacao.normal, bool Reveso = false, int MH = 1, int MV = 1, Justificado Jus = Justificado.esquerda)
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Linguagem));
                case LinguagemCodBar.DPL:
                    string retorno = "";
                    if (JusAtual != Jus)
                    {
                        JusAtual = Jus;
                        switch (Jus)
                        {
                            case Justificado.direita:
                                retorno = "JR";
                                break;
                            case Justificado.centro:
                                retorno = "JC";
                                break;
                            case Justificado.esquerda:
                                retorno = "JL";
                                break;
                        }
                    }
                    retorno += string.Format("{0}9{1}{2}{3:000}{4:0000}{5:0000}{6}\r", (int)Or, MH, MV, Tamanho, X, 1000 - Y, Texto);
                    if (Reveso)
                        return string.Format("A5\r{0}A1\r", retorno);
                    else
                        return retorno;
                case LinguagemCodBar.EPL2:
                    int corX = 0;
                    int corY = 0;
                    switch (Or)
                    {
                        case Orientacao.normal:
                            corY = FonteEPLCorrecao(Tamanho, Lado.Vertical);
                            switch(Jus)
                                {
                                    case Justificado.direita:
                                        corX = -(FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length);
                                        break;
                                    case Justificado.centro:
                                        corX = -(FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length) / 2;
                                        break;
                                }
                            break;
                        case Orientacao.invertido:
                            corY = -FonteEPLCorrecao(Tamanho, Lado.Vertical);
                            switch (Jus)
                            {
                                case Justificado.direita:
                                    corX = FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length;
                                    break;
                                case Justificado.centro:
                                    corX = (FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length) / 2;
                                    break;
                            }
                            break;
                        case Orientacao.sobe:
                            corX = -FonteEPLCorrecao(Tamanho, Lado.Vertical);
                            switch (Jus)
                            {
                                case Justificado.direita:
                                    corY = -(FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length);
                                    break;
                                case Justificado.centro:
                                    corY = -(FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length) / 2;
                                    break;
                            }
                            break;
                        case Orientacao.desce:
                            corX = FonteEPLCorrecao(Tamanho, Lado.Vertical);
                            switch (Jus)
                            {
                                case Justificado.direita:
                                    corY = FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length;
                                    break;
                                case Justificado.centro:
                                    corY = (FonteEPLCorrecao(Tamanho, Lado.Horizontal) * Texto.Length) / 2;
                                    break;
                            }
                            break;
                    }
                    return string.Format("A{0},{1},{2},{3},{4},\"{5}\"\r\n", (int)(Y * 0.8) + corY,
                                                                          (int)(X * 0.8) + corX,
                                                                          RotacaoEPL(Or),
                                                                          FonteEPL(Tamanho),
                                                                          Reveso ? "R" : "N",
                                                                          Texto);
                case LinguagemCodBar.Windows:
                    XRLabel XRLabel1 = new XRLabel();
                    if (Texto.Contains("%"))
                        MascaraControles.Add(new PortaControle(XRLabel1, Texto));                    
                    WinImp.Detail.Controls.Add(XRLabel1);                    
                    //XRLabel1.AutoWidth = true;                                        
                    XRLabel1.SizeF = MedeTexto(Texto, WinFontes[Tamanho], (Or == Orientacao.invertido) || (Or == Orientacao.normal));                    
                    XRLabel1.Text = Texto;
                    XRLabel1.Font = WinFontes[Tamanho];
                    switch (Or)
                    {
                        case Orientacao.normal:
                            XRLabel1.Angle = 270;                            
                            switch (Jus)
                            {
                                case Justificado.esquerda:
                                    XRLabel1.LocationF = new PointF(Y, X);
                                    break;
                                case Justificado.direita:
                                    XRLabel1.LocationF = new PointF(Y, X - XRLabel1.HeightF);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
                                    break;
                                case Justificado.centro:
                                    XRLabel1.LocationF = new PointF(Y, X - XRLabel1.HeightF/2);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
                                    break;
                            }
                            break;
                        case Orientacao.desce:
                            XRLabel1.Angle = 180;                            
                            switch (Jus)
                            {
                                case Justificado.esquerda:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF, X);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
                                    break;
                                case Justificado.direita:
                                    XRLabel1.LocationF = new PointF(Y, X);                                    
                                    break;
                                case Justificado.centro:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF/2, X);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
                                    break;
                            } 
                            break;
                        case Orientacao.invertido:
                            XRLabel1.Angle = 90;                            
                            switch (Jus)
                            {
                                case Justificado.esquerda:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF, X - XRLabel1.HeightF);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
                                    break;
                                case Justificado.direita:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF, X);                                    
                                    break;
                                case Justificado.centro:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF, X - XRLabel1.HeightF/2);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
                                    break;
                            }
                            break;
                        case Orientacao.sobe:
                            switch (Jus)
                            {
                                case Justificado.esquerda:
                                    XRLabel1.LocationF = new PointF(Y, X - XRLabel1.HeightF);
                                    break;
                                case Justificado.direita:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF, X - XRLabel1.HeightF);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
                                    break;
                                case Justificado.centro:
                                    XRLabel1.LocationF = new PointF(Y - XRLabel1.WidthF / 2, X - XRLabel1.HeightF);
                                    XRLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
                                    break;
                            }                            
                            break;
                    }
                    
                    AjustaTamanhoPagina(XRLabel1);
                                                                
                    if (Reveso)
                    {
                        XRLabel1.ForeColor = Color.White;
                        XRLabel1.BackColor = Color.Black;
                    }                    
                    return "";
            }
        }

        /*
        /// <summary>
        /// Gera c�digo para imprimir texto
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Texto"></param>
        /// <param name="Tamanho"></param>
        /// <param name="Or"></param>
        /// <param name="Reveso"></param>
        /// <param name="MH"></param>
        /// <param name="MV"></param>
        /// <returns></returns>
        public string CodTXT(int X, int Y, string Texto, int Tamanho = 5, Orientacao Or = Orientacao.normal, bool Reveso = false, int MH = 1, int MV = 1)
        {
            return CodTXT(Linguagem, X, Y, Texto, Tamanho, Or, Reveso, MH, MV);
        }*/

        /// <summary>
        /// Gera c�digo para linha
        /// </summary>        
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Largura"></param>
        /// <param name="altura"></param>
        /// <returns></returns>
        public string CodLinha(int X, int Y, int Largura = 1, int altura = 1)
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Linguagem));
                case LinguagemCodBar.DPL:
                    return string.Format("1X11000{0:0000}{1:0000}l{2:0000}{3:0000}\r", X, 1000 - Y - altura, altura, Largura);
                case LinguagemCodBar.EPL2:
                    int alturacor = (int)(altura * 0.8);
                    int larguracor = (int)(Largura * 0.8);
                    if (alturacor < 1)
                        alturacor = 1;
                    if (larguracor < 1)
                        larguracor = 1;
                    return string.Format("LO{0},{1},{2},{3}\r\n", (int)(Y * 0.8),
                                                                  (int)(X * 0.8),
                                                                  alturacor,
                                                                  larguracor);
                case LinguagemCodBar.Windows:
                    XRLine XRLine1 = new XRLine();                    
                    WinImp.Detail.Controls.Add(XRLine1);
                    int larguraQuadro = altura;
                    int alturaQuadro = Largura;
                    int XQuadro = Y;
                    int YQuadro = X;
                    if (Largura > altura)
                    {
                        XRLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
                        XRLine1.LineWidth = altura;
                        if (larguraQuadro < 4)
                        {
                            XQuadro -= ((4 - larguraQuadro) / 2);
                            larguraQuadro = 4;
                        };
                    }
                    else
                    {
                        XRLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Horizontal;                        
                        XRLine1.LineWidth = Largura;
                        if (larguraQuadro < 4)
                        {
                            YQuadro -= ((4 - larguraQuadro) / 2);
                            larguraQuadro = 4;
                        };
                    }
                                        
                    XRLine1.LocationF = new PointF(XQuadro, YQuadro);
                    XRLine1.SizeF = new SizeF(larguraQuadro, alturaQuadro);

                    AjustaTamanhoPagina(XRLine1);
                                       
                    return "";
            }
        }

        /*
        /// <summary>
        /// Gera c�digo para linha
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Largura"></param>
        /// <param name="altura"></param>
        /// <returns></returns>
        public string CodLinha(int X, int Y, int Largura = 1, int altura = 1)
        {
            return CodLinha(Linguagem, X, Y, Largura, altura);
        }
        */

        /// <summary>
        /// Caixa
        /// </summary>        
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Largura"></param>
        /// <param name="altura"></param>
        /// <param name="EspX"></param>
        /// <param name="EspY"></param>
        /// <returns></returns>
        public string CodCaixa(int X, int Y, int Largura = 1, int altura = 1, int EspX = 10, int EspY = 10)
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:

                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Linguagem));
                case LinguagemCodBar.DPL:
                    return string.Format("1X11000{0:0000}{1:0000}b{2:0000}{3:0000}{4:0000}{5:0000}\r", X, 1000 - Y - altura, altura, Largura, EspY, EspX);
                case LinguagemCodBar.EPL2:
                    int esp = (int)((EspX + EspY) * 0.4);
                    if (esp == 0)
                        esp = 1;
                    return string.Format("X{0},{1},{2},{3},{4}\r\n", (int)(Y * 0.8),
                                                                 (int)(X * 0.8),
                                                                 esp,
                                                                 (int)((Y + altura) * 0.8),
                                                                 (int)((X + Largura) * 0.8));
                case LinguagemCodBar.Windows:
                    XRShape XRShape1 = new XRShape();                    
                    WinImp.Detail.Controls.Add(XRShape1);
                    XRShape1.LineWidth = (EspX + EspY) / 2;
                    XRShape1.LocationF = new PointF(Y, X);                    
                    XRShape1.Shape = new DevExpress.XtraPrinting.Shape.ShapeRectangle();;
                    XRShape1.SizeF = new System.Drawing.SizeF(altura, Largura);
                    AjustaTamanhoPagina(XRShape1);
                    return "";
            }
        }

        /*
        /// <summary>
        /// Caixa
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Largura"></param>
        /// <param name="altura"></param>
        /// <param name="EspX"></param>
        /// <param name="EspY"></param>
        /// <returns></returns>
        public string CodCaixa(int X, int Y, int Largura = 1, int altura = 1, int EspX = 10, int EspY = 10)
        {
            return CodCaixa(Linguagem, X, Y, Largura, altura, EspX, EspY);
        }
        */

        /// <summary>
        /// C�digo de barras
        /// </summary>        
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Texto"></param>
        /// <param name="altura"></param>
        /// <param name="Or"></param>
        /// <param name="ComTexto"></param>
        /// <returns></returns>
        public string CodBarra(int X, int Y, string Texto, int altura = 100, Orientacao Or = Orientacao.normal, bool ComTexto = false)
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Linguagem));
                case LinguagemCodBar.DPL:
                    return string.Format("{0}{1}00{2:000}{3:0000}{4:0000}{5}\r", (int)Or, ComTexto ? "J" : "j", altura, X, 1000 - Y, Texto);
                case LinguagemCodBar.EPL2:
                    int corX = 0;
                    int corY = 0;
                    switch (Or)
                    {
                        case Orientacao.normal:
                            corY = altura;
                            break;
                        case Orientacao.invertido:
                            corY = -altura;
                            break;
                        case Orientacao.sobe:
                            corX = -altura;
                            break;
                        case Orientacao.desce:
                            corX = altura;
                            break;
                    }
                    return string.Format("B{0},{1},{2},2C,1,4,{3},{4},\"{5}\"\r\n", (int)(Y * 0.8) + corY,
                                                                                  (int)(X * 0.8) + corX,
                                                                                  RotacaoEPL(Or),
                                                                                  altura,
                                                                                  ComTexto? "B":"N",
                                                                                  Texto);
                case LinguagemCodBar.Windows:
                    DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator interleaved2of5Generator1 = new DevExpress.XtraPrinting.BarCode.Interleaved2of5Generator();
                    XRBarCode XRBarCode1 = new XRBarCode();
                    if (Texto.Contains("%"))
                        MascaraControles.Add(new PortaControle(XRBarCode1, Texto));                    
                    WinImp.Detail.Controls.Add(XRBarCode1);                    
                    XRBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
                    XRBarCode1.ShowText = ComTexto;                    
                    interleaved2of5Generator1.WideNarrowRatio = 3F;
                    XRBarCode1.Symbology = interleaved2of5Generator1;
                    XRBarCode1.Module = 2.54F;
                    XRBarCode1.Text = Texto;                    
                    int XBarra = Y;
                    int YBarra = X;
                    int AlturaBarra=0;
                    int LarguraBarra=0;
                    //int comprimento = Texto.Length * 20;
                    int comprimento = Texto.Length * 40;
                    switch (Or)
                    {
                        case Orientacao.normal:
                            XRBarCode1.BarCodeOrientation = DevExpress.XtraPrinting.BarCode.BarCodeOrientation.RotateRight;
                            LarguraBarra = altura;
                            AlturaBarra = comprimento;
                            break;
                        case Orientacao.sobe:
                            LarguraBarra = comprimento;
                            AlturaBarra = altura;
                            YBarra -= altura;
                            break;
                        case Orientacao.desce:
                            XRBarCode1.BarCodeOrientation = DevExpress.XtraPrinting.BarCode.BarCodeOrientation.UpsideDown;
                            LarguraBarra = comprimento;
                            AlturaBarra = altura;
                            XBarra -= comprimento;
                            break;
                        case Orientacao.invertido:
                            XRBarCode1.BarCodeOrientation = DevExpress.XtraPrinting.BarCode.BarCodeOrientation.RotateLeft;
                            LarguraBarra = altura;
                            AlturaBarra = comprimento;
                            XBarra -= altura;
                            YBarra -= comprimento;
                            break;
                    };
                    XRBarCode1.LocationF = new PointF(XBarra, YBarra);
                    XRBarCode1.SizeF = new System.Drawing.SizeF(LarguraBarra, AlturaBarra);
                    AjustaTamanhoPagina(XRBarCode1);
                    //XRBarCode1.BackColor = Color.Red;
                    return "";
            }
        }

        /*
        /// <summary>
        /// C�digo de barras
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Texto"></param>
        /// <param name="altura"></param>
        /// <param name="Or"></param>
        /// <param name="ComTexto"></param>
        /// <returns></returns>
        public string CodBarra(int X, int Y, string Texto, int altura = 100, Orientacao Or = Orientacao.normal,bool ComTexto = false)
        {
            return CodBarra(Linguagem, X, Y, Texto, altura, Or, ComTexto);
        }
        */

        private static WinImpress WinImp;
        private List<PortaControle> MascaraControles;
        private static Dictionary<int, Font> WinFontes;

        private bool? _Guilhotina;

        private bool Guilhotina
        {
            get
            {
                if(!_Guilhotina.HasValue)
                    _Guilhotina = CompontesBasicos.dEstacao.dEstacaoSt.GetValor("Corte Automatico", true);
                return _Guilhotina.Value;
            }
        }

        /// <summary>
        /// Comandos para iniciar impresso
        /// </summary>        
        /// <returns></returns>
        public string AbreForm()
        {
            switch (Linguagem)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Linguagem));
                case LinguagemCodBar.DPL:
                    return string.Format(
"^02c0001\r" + //Tamanho da etiqueta m�nimo
"^02m\r" +     //m�trico
"^02L\r" +     //abre
"D11\r" +      //dots sem multiplicar
"^02ySE1\r"    //Char set
                                         );
                case LinguagemCodBar.EPL2:
                    return string.Format(
"\r\n" +
(Guilhotina ? "OC,Fr\r\n" : "OFf\r\n") +
"Q20,0\r\n" +   //Cont�nuo
"q800\r\n" +    //Largura
"N\r\n" +
"I8,A,001\r\n"  //Char set
                                         );
                case LinguagemCodBar.Windows:
                    WinImp = new WinImpress();
                    MascaraControles = new List<PortaControle>();
                    WinFontes = new Dictionary<int, Font>();
                    int i = 0;
                    Graphics gr = Graphics.FromHwnd(IntPtr.Zero);
                    gr.PageUnit = GraphicsUnit.Millimeter;
                    float fTeste = 1;                                      
                    int[] calibracao = new int[] { 24, 31, 44, 53, 59, 70,  86, 118, 144, 172, 219 };
                    
                    while (i < 11)
                    {
                        Font Fteste = new Font(WinImp.Font.FontFamily, fTeste);
                        int Altura = calibracao[i];
                        SizeF size = gr.MeasureString("A", Fteste);
                        if (size.Height * 10 > Altura)
                        {
                            WinFontes.Add(i,new Font(WinImp.Font.FontFamily, fTeste - 0.1F));
                            i++;
                        }
                        fTeste += 0.1F;
                    }
                    gr.Dispose();
                    return "";
            }
        }

        /*
        /// <summary>
        /// Comandos para iniciar impresso
        /// </summary>
        /// <returns></returns>
        public string AbreForm()
        {
            return AbreForm(Linguagem);
        }
        */

        /// <summary>
        /// Comandos para inicira impresso
        /// </summary>
        /// <param name="Ling"></param>
        /// <param name="Copias"></param>
        /// <returns></returns>
        public string FechaForm(LinguagemCodBar Ling,int Copias=1)
        {
            switch (Ling)
            {
                case LinguagemCodBar.Indefinida:
                case LinguagemCodBar.PM42:
                default:
                    throw new NotImplementedException(string.Format("N�o implementado para :{0}", Ling));
                case LinguagemCodBar.DPL:
                    return string.Format("Q{0:0000}\rE\r",Copias);
                case LinguagemCodBar.EPL2:
                    return string.Format("P{0}\r\n{1}",
                                          Copias,
                                          !Guilhotina ? "^@\r\n":"");
                case LinguagemCodBar.Windows:                    
                    return "";
            }
        }

        /// <summary>
        /// Comandos para inicira impresso
        /// </summary>
        /// <returns></returns>
        public string FechaForm(int Copias=1)
        {
            return FechaForm(Linguagem,Copias);
        }

        /// <summary>
        /// Imprime um gabarito
        /// </summary>
        public void ImprimeGabarito()
        {
            int Y = 956;
            string Gabarito = AbreForm();
            /*
            CodTXT(100, 10, "abcdABCD1234�� 0", 0,Orientacao.sobe);
            CodTXT(200, 10, "abcdABCD1234�� 1", 1, Orientacao.sobe);
            CodTXT(300, 10, "abcdABCD1234�� 2", 2, Orientacao.sobe);
            CodTXT(400, 10, "abcdABCD1234�� 3", 3, Orientacao.sobe);
            CodTXT(500, 10, "abcdABCD1234�� 4", 4, Orientacao.sobe);
            CodTXT(600, 10, "abcdABCD1234�� 5", 5, Orientacao.sobe);
            CodTXT(700, 10, "abcdABCD1234�� 6", 6, Orientacao.sobe);
            CodTXT(800, 10, "abcdABCD1234�� 7", 7, Orientacao.sobe);
            CodTXT(900, 10, "abcdABCD1234�� 8", 8, Orientacao.sobe);
            CodTXT(1000, 10, "abcdABCD1234�� 9", 9, Orientacao.sobe);
            CodTXT(1100, 10, "abcdABCD1234�� 10", 10, Orientacao.sobe);
            */


            Gabarito += CodTXT(150, Y, "AabcdABC 0", 0);
            
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 38;
            Gabarito += CodTXT(150, Y, "AabcdABC 1", 1);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 50;
            Gabarito += CodTXT(150, Y, "AabcdABC 2", 2);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 58;
            Gabarito += CodTXT(150, Y, "AabcdABC 3", 3);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 65;
            Gabarito += CodTXT(150, Y, "AabcdABC 4", 4);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 70;
            Gabarito += CodTXT(150, Y, "AabcdABC 5", 5);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 85;
            Gabarito += CodTXT(150, Y, "AabcdABC 6", 6);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 97;
            Gabarito += CodTXT(150, Y, "AabcdABC 7", 7);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 110;
            Gabarito += CodTXT(150, Y, "AabcdABC 8", 8);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 122;
            Gabarito += CodTXT(150, Y, "AabcdABC 9", 9);
            Gabarito += CodLinha(105, Y, 49, 1);
            Y -= 143;
            Gabarito += CodTXT(150, Y, "AabcdABC 10", 10);
            Gabarito += CodLinha(105, Y, 49, 1);
            //canto zero
            Gabarito += CodLinha(0, 0, 1000, 1);
            Gabarito += CodLinha(0, 0, 1, 200);
            Gabarito += CodTXT(10, 10, "Zero (0,0) - eixo X ->", 0);
            Gabarito += CodTXT(15, 40, "eixo Y ->", 0, Orientacao.sobe);
            Gabarito += CodLinha(100, 100, 900, 1);
            Gabarito += CodLinha(100, 100, 1, 900);
            Gabarito += CodTXT(100, 80, "(10,10)", 0);

            //canto final
            Gabarito += CodLinha(800, 999, 200, 1);
            Gabarito += CodLinha(999, 800, 1, 200);
            Gabarito += CodTXT(870, 965, "(100,100)", 0);
            Gabarito += CodLinha(800, 899, 100, 1);
            Gabarito += CodLinha(899, 800, 1, 100);
            Gabarito += CodTXT(800, 870, "(90,90)", 0);

            //as linhas tem com ref a base a esquerda            
            Gabarito += CodLinha(1100, 100, 100, 5);
            Gabarito += CodLinha(1200, 100, 100, 15);
            Gabarito += CodLinha(10, 500, 5, 100);
            Gabarito += CodLinha(10, 600, 15, 100);

            //orienta��o
            Gabarito += CodTXT(1500, 800, "Normal", 3, Orientacao.normal);
            Gabarito += CodTXT(1500, 800, "Desce", 3, Orientacao.desce);
            Gabarito += CodTXT(1500, 800, "Invertido", 3, Orientacao.invertido);
            Gabarito += CodTXT(1500, 800, "Sobe", 3, Orientacao.sobe);
            Gabarito += CodLinha(1400, 800, 200, 1);
            Gabarito += CodLinha(1500, 700, 1, 200);

            //Justificado
            Gabarito += CodTXT(1800, 900, "Esquerda", 3, Orientacao.normal,true,1,1,Justificado.esquerda);
            Gabarito += CodTXT(1800, 850, "Direita", 3, Orientacao.normal, true, 1, 1, Justificado.direita);
            Gabarito += CodTXT(1800, 800, "Centro", 3, Orientacao.normal, true, 1, 1, Justificado.centro);
            Gabarito += CodTXT(1800, 750, "Esquerda", 3, Orientacao.invertido, true, 1, 1, Justificado.esquerda);
            Gabarito += CodTXT(1800, 700, "Direita", 3, Orientacao.invertido, true, 1, 1, Justificado.direita);
            Gabarito += CodTXT(1800, 650, "Centro", 3, Orientacao.invertido, true, 1, 1, Justificado.centro); 
                        
            Gabarito += CodLinha(1800, 500, 1, 500);

            Gabarito += CodTXT(1800, 300, "Esquerda", 3, Orientacao.sobe, true, 1, 1, Justificado.esquerda);
            Gabarito += CodTXT(1850, 300, "Direita", 3, Orientacao.sobe, true, 1, 1, Justificado.direita);
            Gabarito += CodTXT(1900, 300, "Centro", 3, Orientacao.sobe, true, 1, 1, Justificado.centro);
            Gabarito += CodTXT(1950, 300, "Esquerda", 3, Orientacao.desce, true, 1, 1, Justificado.esquerda);
            Gabarito += CodTXT(2000, 300, "Direita", 3, Orientacao.desce, true, 1, 1, Justificado.direita);
            Gabarito += CodTXT(2050, 300, "Centro", 3, Orientacao.desce, true, 1, 1, Justificado.centro);

            Gabarito += CodLinha(1640, 300, 500, 1);

            //Caixas
            Gabarito += CodCaixa(1100, 800, 50, 150, 1, 1);
            Gabarito += CodCaixa(1150, 800, 100, 100, 30, 15);

            //C�digo de barras
            Gabarito += CodBarra(1150, 550, "123456", 70, Orientacao.normal);
            Gabarito += CodBarra(1150, 550, "123456", 70, Orientacao.desce);
            Gabarito += CodBarra(1150, 550, "123456", 70, Orientacao.invertido);
            Gabarito += CodBarra(1150, 550, "123456", 70, Orientacao.sobe);
            Gabarito += CodLinha(1050, 550, 200, 1);
            Gabarito += CodLinha(1150, 450, 1, 200);

            Gabarito += CodBarra(1400, 200, "123456", 150, Orientacao.normal,true);
            Gabarito += CodBarra(1400, 400, "123456", 150, Orientacao.normal,false);

            //Reverso
            Gabarito += CodTXT(750, 650, "Reverso", 5, Orientacao.normal, true);

            //Linhas de refer�ncia
            Gabarito += CodLinha(80, 972, 180, 1);
            Gabarito += CodLinha(80, 939, 180, 1);
            Gabarito += CodLinha(80, 899, 180, 1);
            Gabarito += CodLinha(80, 848, 180, 1);
            Gabarito += CodLinha(80, 788, 180, 1);
            Gabarito += CodLinha(80, 726, 180, 1);
            Gabarito += CodLinha(80, 653, 180, 1);
            Gabarito += CodLinha(80, 579, 180, 1);
            Gabarito += CodLinha(80, 488, 180, 1);
            Gabarito += CodLinha(80, 387, 180, 1);
            Gabarito += CodLinha(80, 280, 180, 1);
            
            Gabarito += FechaForm();
            ImprimeDOC(Gabarito);
        }

        
        #endregion

        private class PortaControle
        {
            private string MascaraOriginal;
            private XRControl Controle;

            public PortaControle(XRControl _Controle, string mascara)
            {
                Controle = _Controle;
                MascaraOriginal = mascara;
            }

            public void Reset()
            {
                Controle.Text = MascaraOriginal;
            }

            public string SetCampo(string Codigo, string Valor)
            {
                Controle.Text = Controle.Text.Replace(Codigo, Valor);
                if (Controle is XRLabel)
                {
                    XRLabel XRLabel1 = (XRLabel)Controle;
                    XRLabel1.SizeF = MedeTexto(XRLabel1.Text, XRLabel1.Font, (XRLabel1.Angle == 90) || (XRLabel1.Angle == 270));
                }
                else if (Controle is XRBarCode)
                {
                    XRBarCode XRBarCode1 = (XRBarCode)Controle;
                    if ((XRBarCode1.BarCodeOrientation == DevExpress.XtraPrinting.BarCode.BarCodeOrientation.RotateRight) || (XRBarCode1.BarCodeOrientation == DevExpress.XtraPrinting.BarCode.BarCodeOrientation.RotateLeft))
                        XRBarCode1.SizeF = new SizeF(XRBarCode1.WidthF, XRBarCode1.Text.Length * 40);
                    else
                        XRBarCode1.SizeF = new SizeF(XRBarCode1.Text.Length * 40, XRBarCode1.HeightF);
                };
                return Controle.Text;
            }
        }
    }

    

    /// <summary>
    /// Lingu�gem da impressora 
    /// </summary>
    public enum LinguagemCodBar
    {
        /// <summary>
        /// Passa os dados direto sem suporte
        /// </summary>
        Indefinida=0,
        /// <summary>
        /// Zebra
        /// </summary>
        EPL2=1,
        /// <summary>
        /// Datamax
        /// </summary>
        DPL=2,
        /// <summary>
        /// Bematech
        /// </summary>
        PM42=3,
        /// <summary>
        /// Windows
        /// </summary>
        Windows=4,
    }
}
