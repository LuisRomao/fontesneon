﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace CodBar
{
    /// <summary>
    /// Impresso versão windows
    /// </summary>
    public partial class WinImpress : DevExpress.XtraReports.UI.XtraReport
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public WinImpress()
        {
            InitializeComponent();
        }

    }
}
