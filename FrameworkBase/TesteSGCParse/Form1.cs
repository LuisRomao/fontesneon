﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteSGCParse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();            
        }

        private SGCParse.Remessa Rem;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
        }

        private void sB2_Click(object sender, EventArgs e)
        {
            
        }

        private void sB3_Click(object sender, EventArgs e)
        {

            
        }

        private void CriarRemessa(object sender, EventArgs e)
        {
            Rem = new SGCParse.Remessa();
            button2.Enabled = true;
            Application.DoEvents();
            button2.Focus();
        }

        private void CriarBoletos(object sender, EventArgs e)
        {
            for (int i = 1; i < 10; i++)
            {
                EDIBoletos.BoletoReg NovoBoleto = new EDIBoletos.BoletoReg();
                NovoBoleto.EnderecoSacado = "";
                NovoBoleto.NomeEmpresa = "";
                NovoBoleto.CNPJ = new DocBacarios.CPFCNPJ(13553185805);
                NovoBoleto.Vencimento = DateTime.Today.AddDays(20);
                NovoBoleto.Valor = 100 + i;
                NovoBoleto.EnderecoSacado = "End Sacado";
                NovoBoleto.NomeSacado = "Nome Sacado";
                NovoBoleto.Ocorrencia = EDIBoletos.OcorrenciasPadrao.Registrar;
                Rem.Boletos.Add(i, NovoBoleto);
            }
            button3.Enabled = true;
            Application.DoEvents();
            button3.Focus();
        }

        private void GerarArquivo(object sender, EventArgs e)
        {
            SaveFileDialog Dialogo = new SaveFileDialog();
            Dialogo.DefaultExt = ".rem";
            if (Dialogo.ShowDialog() == DialogResult.OK)
                try
                {
                    if (Rem.GerarRemessaBoletos(237, 109, 123, "0", null, "Teste", "01124173000184", 602270, "9", 10, Dialogo.FileName))
                        MessageBox.Show("Arquivo Gerado");                    
                    else
                    {
                        if (Rem.UltimoErro != null)
                            MessageBox.Show("Erro" + Rem.UltimoErro.Message);
                        else
                            MessageBox.Show("Não gerou");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro:" + ex.Message);
                }
        }
    }
}
