﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestedllC12
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SortedList<int, decimal> Totais = new SortedList<int, decimal>();
            Totais.Add(10, 11);
            Totais.Add(20, 7);
            Totais.Add(30, 12);
            SortedList<int, decimal> Itens = new SortedList<int, decimal>();
            Itens.Add(10, 6);
            Itens.Add(20, 5);
            Itens.Add(30, 2);
            Itens.Add(40, 4);
            Itens.Add(50, 1);
            Itens.Add(60, 7);
            Itens.Add(70, 3);
            Itens.Add(80, 2);
            dllC12.Agrupador Ag = new dllC12.Agrupador();
            Ag.IniciaLog(@"D:\testeagrupa1.log");
            //Ag.TempoMaximo = 15;
            SortedList<int, List<int>> Resposta = Ag.Agrupar(Totais, Itens);
            if (Resposta == null)
                textBox1.Text += "\r\nNão encontrado";
            else
            {
                textBox1.Text += "\r\nRetorno ok\r\n\r\n";
                foreach (int idGrupo in Resposta.Keys) 
                {
                    textBox1.Text += string.Format("   Grupo {0}\r\n",idGrupo);
                    foreach(int idIten in Resposta[idGrupo])
                        textBox1.Text += string.Format("       It {0}\r\n", idIten);
                }
            }

            Totais = new SortedList<int, decimal>();
            Totais.Add(10, 10);
            Totais.Add(20, 15);
            Totais.Add(30, 26);
            Totais.Add(40, 70);
            Totais.Add(50, 300);
            Itens = new SortedList<int, decimal>();
            Itens.Add(10, 5);
            Itens.Add(20, 5);
            Itens.Add(30, 10);
            Itens.Add(40, 10);
            Itens.Add(50, 10);
            Itens.Add(60, 100);
            Itens.Add(90, 100);
            Itens.Add(80, 100);
            Itens.Add(70, 7);
            Itens.Add(71, 7);
            Itens.Add(72, 7);
            Itens.Add(73, 7);
            Itens.Add(74, 7);
            Itens.Add(75, 7);
            Itens.Add(76, 7);
            Itens.Add(77, 7);
            Itens.Add(78, 7);
            Itens.Add(79, 7);
            Itens.Add(11, 11);
            Ag = new dllC12.Agrupador();
            Ag.IniciaLog(@"D:\testeagrupa2.log");
            //Ag.TempoMaximo = 15;
            Resposta = Ag.Agrupar(Totais, Itens);
            if (Resposta == null)
                textBox1.Text += "\r\nNão encontrado";
            else
            {
                textBox1.Text += "\r\nRetorno ok\r\n\r\n";
                foreach (int idGrupo in Resposta.Keys)
                {
                    textBox1.Text += string.Format("   Grupo {0}\r\n", idGrupo);
                    foreach (int idIten in Resposta[idGrupo])
                        textBox1.Text += string.Format("       It {0}\r\n", idIten);
                }
            }
        }
    }
}
