using System;
using VirDB;
using VirDB.Bancovirtual;
using System.Data;
using FirebirdSql.Data.FirebirdClient;

namespace VirFB
{
    /// <summary>
    /// TableAdapter para FB
    /// </summary>
    public class TableAdapter : VirtualTableAdapter
    {
        /// <summary>
        /// Data e hora do servidor
        /// </summary>
        /// <returns></returns>
        public override DateTime DataServidor()
        {
            DateTime Retorno;
            if (BuscaEscalar("select CURRENT_TIMESTAMP from rdb$database", out Retorno))
                return Retorno;
            else
                return DateTime.MinValue;
        }

        public override int IncluirAutoInc(string comando, params object[] Parametros)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        protected override VirtualTableAdapter STTableAdapterEspecifico
        {
            get { return STTableAdapter; }
        }

        protected override bool EstaEmTransST
        {
            get { return (GuardaConeLocal != null); }
        }

        /// <summary>
        /// Transa��o est�tica
        /// </summary>
        public FbTransaction Tran;
        /// <summary>
        /// Conex�o est�tica
        /// </summary>
        private FbConnection _Cone;

        private FbConnection Cone {
            get {
                if (_Cone == null)
                {
                    System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                    if (PropCon != null)
                        _Cone = (FbConnection)PropCon.GetValue(this, null);
                    else
                        _Cone = new FbConnection(BancoVirtual.StringConeFinal(Tipo));
                };
                return _Cone;
            }
            set
            {
                _Cone = value;

                System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                if (PropCon != null)
                    PropCon.SetValue(this, value, null);


            }
        }

        private static TableAdapter stTableAdapter;

        /// <summary>
        /// Instanica est�tica
        /// </summary>
        public static TableAdapter STTableAdapter
        {
            get
            {
                if (stTableAdapter == null)
                {
                    stTableAdapter = new TableAdapter();
                };
                return stTableAdapter;
            }

        }

        /// <summary>
        /// Cria uma nova conexao
        /// </summary>
        /// <param name="StringDeConexao">"string de conexao"</param>
        public override void CriaCone(string StringDeConexao)
        {
            _Cone = new FbConnection(StringDeConexao);
        }

        /// <summary>
        /// Fecha Conexao
        /// </summary>
        /// <returns></returns>
        protected override bool FechaCone()
        {
            if (_Cone != null)
            {
                if (Cone.State != ConnectionState.Closed)
                    Cone.Close();
            }
            return true;
        }

        /// <summary>
        /// Abre conexao
        /// </summary>
        /// <returns></returns>
        protected override bool AbreCone()
        {            
            if (Cone.State == ConnectionState.Closed)
                Cone.Open();
            return true;
        }

        

        /// <summary>
        /// Inicia Transacao
        /// </summary>
        /// <param name="Nivel">Nivel de isolamento</param>
        /// <returns>Se a trasacao foi iniciada</returns>       
        protected override bool IniciaTrasacaoReal(IsolationLevel Nivel)
        {
            if (Tran != null)
                throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de apertura de transa��o com outra j� em andamento", SubTrans));
            else
            {
                if (!AbreCone())
                    return false;
                Tran = Cone.BeginTransaction(Nivel);
                ImplantaTrans(true);
                return true;
            }
        }


        /// <summary>
        ///Coloca o componente na transacao de _Dono_Trans que pode ser STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>       
        protected override bool EntrarEmTrans(VirtualTableAdapter _Dono_Trans)
        {
            TableAdapter Dono_Trans = (TableAdapter)_Dono_Trans;
            if (GuardaConeLocal != null)
                return false;
            if (Tran != null)
                throw new Exception("Objeto em trasa��o local");
            else
            {
                //EstaEmTransST = true;
                if (Dono_Trans.Tran == null)
                    throw new Exception("STTableAdapter n�o est� em transacao");
                Tran = Dono_Trans.Tran;
                GuardaConeLocal = Cone;
                Cone = Dono_Trans.Cone;
                ImplantaTrans(true);
                return true;
            }
        }

        /*
        /// <summary>
        /// Inicia uma trasacao Estatica. Na verdade coloca o componente na transacao de STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>       
        protected override bool IniciaTrasacaoST()
        {
            if (GuardaConeLocal != null)
                return false;
            if (Tran != null)
                throw new Exception("Objeto em trasa��o local");
            else
            {
                EstaEmTransST = true;
                if (STTableAdapter.Tran == null)
                    throw new Exception("STTableAdapter n�o est� em transacao");
                Tran = STTableAdapter.Tran;
                GuardaConeLocal = Cone;
                Cone = STTableAdapter.Cone;
                ImplantaTrans(true);
                return true;
            }
        }
        */

        /// <summary>
        /// Retorna se a transa�ao est�tica esta ativa
        /// </summary>
        /// <returns></returns>
        public override bool STTransAtiva()
        {
            return (VirFB.TableAdapter.STTableAdapter.Tran != null);
        }

        /// <summary>
        /// Adapter do objeto derivado
        /// </summary>        
        /// <remarks>Para os objetos staticos retorna null</remarks>
        public FbDataAdapter VirAdapter
        {
            get
            {
                System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
                System.Reflection.PropertyInfo PropAdap = GetType().GetProperty("Adapter", BF);
                if (PropAdap != null)
                    return (FbDataAdapter)PropAdap.GetValue(this, null);
                else
                    return null;
            }
        }

        /// <summary>
        /// Coloca o adapter sob a transacao Tran
        /// </summary>
        /// <param name="Inicia">Inicio ou termino</param>
        protected override void ImplantaTrans(bool Inicia)
        {
            if (VirAdapter == null)
                return;
            FbTransaction Tranx = Inicia ? Tran : null;
            if (VirAdapter.InsertCommand != null)
                VirAdapter.InsertCommand.Transaction = Tranx;

            if (VirAdapter.DeleteCommand != null)
                VirAdapter.DeleteCommand.Transaction = Tranx;

            if (VirAdapter.UpdateCommand != null)
                VirAdapter.UpdateCommand.Transaction = Tranx;

            if (VirAdapter.SelectCommand != null)
                VirAdapter.SelectCommand.Transaction = Tranx;
            System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
            System.Reflection.PropertyInfo PropCom = GetType().GetProperty("CommandCollection", BF);
            if (PropCom != null)
            {
                FbCommand[] Comandos = (FbCommand[])PropCom.GetValue(this, null);
                for (int i = 0; (i < Comandos.Length); i = (i + 1))
                    if ((Comandos[i] != null))
                        Comandos[i].Transaction = Tranx;
            }
        }

        /// <summary>
        /// Quarda a conexao real do componente para incorpor�lo na transaco STD
        /// </summary>
        private FbConnection GuardaConeLocal;

        /// <summary>
        /// Termina trasacao
        /// </summary>
        /// <param name="Commit">True para commit e false para rollback</param>
        /// <returns>Esta retornando sempre true</returns>
        protected override bool FimTrasacaoReal(bool Commit)
        {
            ImplantaTrans(false);
            if (GuardaConeLocal != null)
            {
                Cone = GuardaConeLocal;
                GuardaConeLocal = null;
                Tran = null;
            }
            else
            {
                if (Tran == null)
                    throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de termino de transa��o com Trans = null", SubTrans));
                if (Commit)
                    Tran.Commit();
                else
                    Tran.Rollback();
                Tran = null;
                FechaCone();
            };
            return true;
        }

        
        /// <summary>
        /// TableAdapter para FireBird
        /// </summary> 
        public TableAdapter() {
            Tipo = TiposDeBanco.FB;
        }

        /// <summary>
        /// Troca o string de conexao
        /// </summary>
        /// <param name="novo">Novo String de conexao</param>
        public override void TrocarStringDeConexao(string novo)
        {
            System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");            
            if (PropCon == null)
                Cone = new FbConnection();
            else
            {
                Cone = (FbConnection)PropCon.GetValue(this, null);                
            };
            Cone.ConnectionString = novo;
            if (Tabela == null)
            {                
                string Name = GetType().Name;
                if (Name.Contains("TableAdapter"))
                    Tabela = Name.Replace("TableAdapter", "");
            }
        }

        /// <summary>
        /// Executa um comando SQL.
        /// </summary>
        /// <remarks>Os paramentros devem obrigatoriamentes ser nomeados como @P1, @P2 ...</remarks>
        /// <param name="comando">Comando SQL: select, update ou delete</param>
        /// <param name="TipoComando">
        /// Tipo de a��o/retorno
        /// pode retornar um unico registro em forma de ArrayList,
        /// escalar,
        /// DataTable
        /// ou ArrayList com os valores do primeiro registro
        /// </param>
        /// <param name="Paramentros">
        /// Paramentros do comando SQL usar sintaxe:        
        /// </param>
        public override object ExecutarSQL(string comando, VirtualTableAdapter.ComandosBusca TipoComando, params object[] Paramentros)
        {
            object oRetorno;
            if (!AbreCone())
                return null;



            using (FbCommand Comando = new FbCommand(comando, Cone, Tran))
            {
                if (Paramentros != null)
                {
                    int i = 1;
                    foreach (object Parametro in Paramentros)
                    {
                        Comando.Parameters.AddWithValue("@P" + i.ToString(), Parametro);
                        i++;
                    }
                }
                switch (TipoComando)
                {
                    case ComandosBusca.ArrayList:
                        System.Collections.ArrayList retorno = new System.Collections.ArrayList();
                        FbDataReader RespostaSQL = Comando.ExecuteReader();
                        if (RespostaSQL.Read())
                            for (int i = 0; i < RespostaSQL.FieldCount; i++)
                                retorno.Add(RespostaSQL[i]);
                        RespostaSQL.Close();
                        oRetorno = retorno;
                        break;
                    case ComandosBusca.DataRow:
                        DataTable TabelaRetorno2 = new DataTable();
                        int registros = (new FbDataAdapter(Comando).Fill(TabelaRetorno2));
                        if (registros > 0)
                            oRetorno = TabelaRetorno2.Rows[0];
                        else
                            oRetorno = null;
                        break;
                    case ComandosBusca.DataTable:
                        DataTable TabelaRetorno = new DataTable();
                        using (FbDataAdapter fbDataAdapter = new FbDataAdapter(Comando))
                        {
                            fbDataAdapter.Fill(TabelaRetorno);
                        }
                        oRetorno = TabelaRetorno;
                        break;
                    case ComandosBusca.Escalar:
                        oRetorno = Comando.ExecuteScalar();
                        break;
                    case ComandosBusca.Reader:
                        oRetorno = Comando.ExecuteReader();
                        break;
                    case ComandosBusca.NonQuery:
                        oRetorno = Comando.ExecuteNonQuery();
                        break;
                    default:
                        oRetorno = null;
                        break;
                }
            }
            if(Tran == null)
               FechaCone();
            return oRetorno;
            
        }

        

        /// <summary>
        /// Nome da tabela no banco de dados
        /// </summary>
        /// <remarks>Usado para auto incremeto em bancos FB</remarks>
        public string Tabela;

        public bool ComAutoInc;

        /// <summary>
        /// Pega numeros de auto incremento em bancos FB
        /// </summary>
        /// <remarks>O campo Tabela deve estar preenchido, caso contr�rio uma excess�o sera gerada</remarks>
        /// <returns>Novo n�mero</returns>
        public Int32 PegaNumeroFB()
        {
            if ((Tabela == "") || (Tabela == null))
                throw new Exception("Tabela n�o defina para o VirtualTableAdapter");
            return PegaNumeroFB(Tabela);
        }

        /// <summary>
        /// Pega numeros de auto incremento em bancos FB
        /// </summary>
        /// <param name="Tabela">Nome da tabela no banco de dados</param>
        /// <returns>Auto incremento</returns>
        public Int32 PegaNumeroFB(string Tabela)
        {
            return (Int32)(Int64)ExecutarSQL(String.Format("select GEN_ID(gen_{0}_id,1) from rdb$database", Tabela), ComandosBusca.Escalar);
        }

        protected override DateTime DataBanco()
        {
            DateTime retorno;
            if (BuscaEscalar("select current_timestamp from rdb$database", out retorno))
                return retorno;
            else
                return base.DataBanco();
        }

        public override int IniciaLinha(DataRow rowNova)
        {
            if(!ComAutoInc)
                return base.IniciaLinha(rowNova);
            
            if (Tabela == null)                           
                Tabela = rowNova.Table.TableName;
            
            int ID = PegaNumeroFB();
            rowNova[0] = ID;
            return ID;
                            
        }

    }
}
