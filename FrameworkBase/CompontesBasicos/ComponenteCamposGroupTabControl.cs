using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using CompontesBasicos.Classes_de_interface;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteCamposGroupTabControl : CompontesBasicos.ComponenteCamposBase
    {
        #region Variaveis

        /// <summary>
        /// 
        /// </summary>
        public PaginaTabInfoCollection _PaginaTabInfoCollection = new PaginaTabInfoCollection();

        /// <summary>
        /// 
        /// </summary>
        public System.Collections.SortedList ListaDosComponentesTab;
        //private ModuleInfo _ModuleInfo = new ModuleInfo();
        #endregion

        #region Propriendades

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Registro da P�ginas")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor(typeof(System.ComponentModel.Design.CollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public PaginaTabInfoCollection PaginaTabInfoCollection
        {
            get
            {
                return _PaginaTabInfoCollection;
            }

        }

        #endregion        

        

        #region Metodos

        /// <summary>
        /// Fun��o para ser adcionada ao delegate do resize do tabs criados para sediar os componentes
        /// </summary>
        private void ResizeCentralizador(object sender, EventArgs e)
        {
            Control Pai = (Control)sender;
            Control Filho = Pai.Controls[0];
            Filho.Left = (Pai.Width - Filho.Width) / 2;
            Filho.Top = (Pai.Height - Filho.Height) / 2;
        }

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public ComponenteCamposGroupTabControl()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        protected bool TarefaParaTodos(object Tarefa)
        {
            bool retorno = true;
            foreach (System.Collections.DictionaryEntry DE in ListaDosComponentesTab)
            {
                ComponenteTabCampos ComponenteTab = (ComponenteTabCampos)DE.Value;
                if(!ComponenteTab.TarefaParaTodos(Tarefa))
                    retorno = false;
            }
            return retorno;
        }

        /// <summary>
        /// 
        /// </summary>
        protected bool PaginasCriadas = false;

        /// <summary>
        /// Cria as paginas listadas em PaginaTabInfoCollection
        /// </summary>
        /// <param name="somenteleitura">Somente leitura</param>
        public void CriaPaginas(bool somenteleitura)
        {
            ListaDosComponentesTab = new System.Collections.SortedList();
            foreach (PaginaTabInfo TInfo in _PaginaTabInfoCollection)
            {
                ComponenteTabCampos NovaPagina = TInfo.CriaPagina(this);
                if (somenteleitura)
                    FormBase.SomenteLeitura(NovaPagina);
                DevExpress.XtraTab.XtraTabPage NovaTab = TabControl_F.TabPages.Add(TInfo.Name);
                ListaDosComponentesTab.Add(NovaTab.GetHashCode(), NovaPagina);
                NovaPagina.Parent = NovaTab;                                    
                if (NovaPagina.Dock == DockStyle.None){
                    NovaPagina.Parent.Resize += ResizeCentralizador;
                };
                BindingFlags bf = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
                
                foreach (FieldInfo fi in (NovaPagina.GetType()).GetFields(bf))
                {
                    if (fi.FieldType.BaseType != null)
                      if (fi.FieldType == typeof(BindingSource)) {                          
                          BindingSource xBindingSource = (BindingSource)fi.GetValue(NovaPagina);
                          if (xBindingSource.DataSource is Type)
                           if ((Type)xBindingSource.DataSource == BindingSourcePrincipal.DataSource.GetType())
                           {
                              if (xBindingSource.DataMember != BindingSourcePrincipal.DataMember)
                                  xBindingSource.DataSource = BindingSourcePrincipal.DataSource;
                              else
                                  xBindingSource.DataSource = BindingSourcePrincipal;
                           }
                      }
                }
                
            }
            PaginasCriadas = true;
        } 
        #endregion

        /// <summary>
        /// Fecha o formulario conteiner
        /// </summary>
        /// <param name="Resultado">DialogResult utilizado</param>
        /// <returns>DialogResult</returns>
        /// <remarks>Se o resultado for ok chama OnTrocaPagina para cada p�gina</remarks>
        protected override void FechaTela(DialogResult Resultado)
        {
            if(Resultado == DialogResult.OK)
                foreach (System.Collections.DictionaryEntry Dic in ListaDosComponentesTab){
                    ComponenteTabCampos Pagina = (ComponenteTabCampos)Dic.Value;
                    Pagina.OnTrocaPagina(false);
                };
            base.FechaTela(Resultado);
        }

        private void TabControl_F_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            //string geraerro = "";
            //geraerro = geraerro.Substring(4, 3);
            if (e.PrevPage != null)
            {
                ComponenteTabCampos PaginaAntiga = (ComponenteTabCampos)ListaDosComponentesTab[e.PrevPage.GetHashCode()];
                if (PaginaAntiga != null)
                    PaginaAntiga.OnTrocaPagina(false);
            };
            if (e.Page != null)
            {
                ComponenteTabCampos PaginaNova = (ComponenteTabCampos)ListaDosComponentesTab[e.Page.GetHashCode()];
                if (PaginaNova != null)
                    PaginaNova.OnTrocaPagina(true);
            };
        }
        
    }
}

