namespace CompontesBasicos
{
    partial class FEspera
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.Motivo = new DevExpress.XtraEditors.LabelControl();
            this.progressBarControl1 = new DevExpress.XtraEditors.ProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(191, 64);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(90, 23);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Aguarde";
            this.labelControl1.UseWaitCursor = true;
            // 
            // Motivo
            // 
            this.Motivo.Appearance.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Motivo.Appearance.ForeColor = System.Drawing.Color.Black;
            this.Motivo.Appearance.Options.UseFont = true;
            this.Motivo.Appearance.Options.UseForeColor = true;
            this.Motivo.Appearance.Options.UseTextOptions = true;
            this.Motivo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Motivo.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.Motivo.LineLocation = DevExpress.XtraEditors.LineLocation.Top;
            this.Motivo.Location = new System.Drawing.Point(12, 130);
            this.Motivo.Name = "Motivo";
            this.Motivo.Size = new System.Drawing.Size(454, 23);
            this.Motivo.TabIndex = 1;
            this.Motivo.Text = "Aguarde";
            this.Motivo.UseWaitCursor = true;
            // 
            // progressBarControl1
            // 
            this.progressBarControl1.Location = new System.Drawing.Point(13, 173);
            this.progressBarControl1.Name = "progressBarControl1";
            this.progressBarControl1.Size = new System.Drawing.Size(453, 18);
            this.progressBarControl1.TabIndex = 2;
            this.progressBarControl1.UseWaitCursor = true;
            this.progressBarControl1.Visible = false;
            // 
            // FEspera
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(478, 203);
            this.ControlBox = false;
            this.Controls.Add(this.progressBarControl1);
            this.Controls.Add(this.Motivo);
            this.Controls.Add(this.labelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FEspera";
            this.Opacity = 0.7;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Processando";
            this.TopMost = true;
            this.UseWaitCursor = true;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FEspera_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.LabelControl Motivo;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraEditors.ProgressBarControl progressBarControl1;
    }
}
