using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraExport;
using DevExpress.XtraGrid.Export;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteGradeNavegador : CompontesBasicos.ComponenteGradeBase
   {
        private ArrayList FilhotesRegistrados;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filhote"></param>
        public void RegistraFilhote(ComponenteCamposBase Filhote)
        {
            if (!FilhotesRegistrados.Contains(Filhote))
                FilhotesRegistrados.Add(Filhote);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Filhote"></param>
        public void DesRegistraFilhote(ComponenteCamposBase Filhote)
        {
            if (FilhotesRegistrados.Contains(Filhote))
                FilhotesRegistrados.Remove(Filhote);
        }

        
        private void ExportTo(IExportProvider provider) 
        {
			Cursor currentCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;            
			BaseExportLink link = GridView_F.CreateExportLink(provider);
            link.ExportAll = false;			
			link.ExportTo(true);
			provider.Dispose();

			Cursor.Current = currentCursor;
		}

        private static string ShowSaveFileDialog(string title, string filter)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                string name = Application.ProductName;
                int n = name.LastIndexOf(".") + 1;
                if (n > 0)
                    name = name.Substring(n, name.Length - n);
                dlg.Title = "Exportar para " + title;
                dlg.FileName = name;
                dlg.Filter = filter;
                if (dlg.ShowDialog() == DialogResult.OK)
                    return dlg.FileName;
            }
            return "";
        }

        private void OpenFile(string fileName)
        {
            if (MessageBox.Show("Voc� gostaria de abrir o arquivo?", "Exportar Para...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                    {
                        process.StartInfo.FileName = fileName;
                        process.StartInfo.Verb = "Open";
                        process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized;
                        process.Start();
                    }
                }
                catch
                {
                    MessageBox.Show(this, "N�o foi encontrado em seu sistema operacional um aplicativo apropriado para abrir o arquivo com os dados exportados.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool EmEdicao 
        {
            get 
            {
                return !barNavegacao_F.Visible;
            }
            set
            {
                ShowToolBarByRowState(value);
            }
        }

        /// <summary>
        /// Evendo para mostrar ou n�o a barra de edicao (confirma/cancela)
        /// </summary>
        /// <remarks>Dispara os eventos 'editando' e 'n�o editando'</remarks>
        /// <param name="Editing">Indica de edita ou n�o</param>
        protected virtual void ShowToolBarByRowState(bool Editing)
        {
            barDataBase_F.Visible = Editing;
            barNavegacao_F.Visible = (!Editing);
            GridView_F.OptionsBehavior.Editable = Editing;
            if (Editing)
                Oneditando(new EventArgs());
            else
                Onnaoeditando(new EventArgs());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual bool ConfirmarRegistro_F()
        {
            return Update_F();
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void ExcluirRegistro_F()
        {
            throw new NotImplementedException("O m�todo ExcluirRegistro_F() n�o foi implementado.");
        }


        private void DoException()
        {
            GridControl_F.MainView.HideEditor();

            if (GridControl_F.MainView.DataSource is BindingSource)
            {
                if ((GridControl_F.MainView.DataSource as BindingSource).DataSource is DataSet)
                    (((GridControl_F.MainView.DataSource as BindingSource).DataSource) as DataSet).RejectChanges();
            }
            else if (GridControl_F.MainView.DataSource is DataSet)
                (GridControl_F.MainView.DataSource as DataSet).RejectChanges();
        }

        /// <summary>
        /// 
        /// </summary>
        public ComponenteGradeNavegador()
        {
            InitializeComponent();
            FilhotesRegistrados = new ArrayList();
            barDataBase_F.Visible = false;            

        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void Imprimir_F(){
            GridControl_F.ShowPrintPreview();
        }

        private void BtnImprimirGrade_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Imprimir_F();
        }

        private void BtnExportarHTML_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
			string fileName = ShowSaveFileDialog("Documento HTML", "Documentos HTML|*.html");
            if (fileName != "")
            {
                GridView_F.ExportToHtml(fileName);
                OpenFile(fileName);
            }
        }

        private void BtnExportarXML_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = ShowSaveFileDialog("Documento XML", "Documentos XML|*.xml");
            if (fileName != "")
            {
                ExportTo(new ExportXmlProvider(fileName));
                OpenFile(fileName);
            }
        }

        private void BtnExportarXLS_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = ShowSaveFileDialog("Documento do Microsoft Excel", "Microsoft Excel|*.xls");
            if (fileName != "")
            {
                //ExportTo(new ExportXlsProvider(fileName));                
                GridControl_F.MainView.ExportToXls(fileName, new DevExpress.XtraPrinting.XlsExportOptions(DevExpress.XtraPrinting.TextExportMode.Value,false));
                OpenFile(fileName);
            }
        }

        private void BtnExportarTXT_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string fileName = ShowSaveFileDialog("Documento de Texto", "Documentos de Texto|*.xls");
            if (fileName != "")
            {
                GridView_F.ExportToText(fileName);
                OpenFile(fileName);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void GridView_F_RowCountChanged(object sender, EventArgs e)
        {
            try
            {
                bool bEnabled = (GridControl_F.MainView.DataRowCount > 0);
                if (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade(funcionalidade) >= nivelgravacao)
                {
                    BtnAlterar_F.Enabled = bEnabled;
                    BtnExcluir_F.Enabled = bEnabled;
                };
            }
            catch
            {
                BtnAlterar_F.Enabled = false;
                BtnExcluir_F.Enabled = false;
            }
           
        }

        /// <summary>
        /// Fun��o chamada pelo bot�o. De preferenica a sobrescrever o Incluir_F e se necessario chame o Incluir_F(campo,valor)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Incluir_F();            
        }

        /// <summary>
        /// Metodo de inclusao. � chamado pelo botao e chama funcao Incluir_F(campo,valor) com paramentros nulos
        /// </summary>
        /// <returns></returns>
        protected virtual DataRow Incluir_F()
        {
            return Incluir_F(null, null);
        }

        /// <summary>
        /// Faz a inclusao de uma nova linha, mostra os editores e o menu (ok/cancel).
        /// </summary>
        /// <param name="Campo">Da um valor inicial ao campo. pode ser null</param>
        /// <param name="Valor">Valor do campo. pode ser null</param>
        /// <returns></returns>
        protected DataRow Incluir_F(string Campo,object Valor)
        {
            
                DataRow DR = null;
                object orow = BindingSourcePrincipal.AddNew();

                if (orow is DataRow)
                    DR = (DataRow)orow;
                else
                    if (orow is DataRowView)
                        DR = ((DataRowView)orow).Row;


                if ((TableAdapterPrincipal != null) && (DR != null))
                    TableAdapterPrincipal.IniciaLinha(DR);

                if (Campo != null)
                    DR[Campo] = Valor;

                ShowToolBarByRowState(true);

                GridControl_F.MainView.ShowEditor();

                return DR;
            
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void Alterar() {
            
                ShowToolBarByRowState(true);
                GridControl_F.MainView.ShowEditor();

            
        }

        private void BtnAlterar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Alterar();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void BtnExcluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.GridView_F.DataRowCount > 0)
                if (DialogResult.Yes == MessageBox.Show("Confirma a exclus�o do registro selecionado?", "Excluir", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                {
                    try
                    {
                        this.ExcluirRegistro_F();
                        //this.GridView_F.DeleteRow(this.GridView_F.FocusedRowHandle);

                        //if (this.tabela.DataSet is DataSet)
                          //  this.tabela.AcceptChanges();
                        //else
                          //  this.tabela.AcceptChanges();

                        //this.GridControl_F.RefreshDataSource();
                    }
                    catch (Exception Erro)
                    {
                        if (Erro.Message.IndexOf("The DELETE statement conflicted with the REFERENCE constraint") >= 0)
                            MessageBox.Show("Este registro n�o pode ser exclu�do porque existe um outro registro ligado a ele.", "Excluir", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        else
                            MessageBox.Show("Ocorreu um erro ao tentar excluir o registro selecionado.\r\n\r\n Erro: " + Erro.Message.ToUpper() + 
                                            "\r\n\r\nEntre em contato com o suporte da Virtual Software e informe sobre o problema.", "Excluir", 
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);


                        DoException();
                    }
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (this.ConfirmarRegistro_F())
                {
                    ShowToolBarByRowState(false);

                    if ((estado == EstadosDosComponentes.PopUp) && (tipoDeCarga == TipoDeCarga.novo))
                    {
                        Form formulario = (Form)this.Parent;
                        formulario.DialogResult = DialogResult.OK;
                    }
                }
            }
            catch (NoNullAllowedException)
            {
                MessageBox.Show("Existe campo obrigat�rio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

/*
            catch (Exception Erro)
            {
                MessageBox.Show("Ocorreu um erro ao tentar confirmar o registro.\r\n\r\n Erro: " + Erro.Message.ToUpper(), "Confirmar", MessageBoxButtons.OK, MessageBoxIcon.Error);

                
            }
*/
        
        }

        /// <summary>
        /// Cancela altera��es
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
                this.GridView_F.BeginDataUpdate();
                this.GridView_F.CancelUpdateCurrentRow();
                this.BindingSource_F.CancelEdit();
                this.GridView_F.EndDataUpdate();

                ShowToolBarByRowState(false);

                if ((estado == EstadosDosComponentes.PopUp) && (tipoDeCarga == TipoDeCarga.novo))
                {
                    Form formulario = (Form)this.Parent;
                    formulario.DialogResult = DialogResult.Cancel;

                };
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void GridView_F_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DataTable dataTable = null;

            if (this.GridControl_F.MainView.DataSource is BindingSource)
            {
                if ((this.GridControl_F.MainView.DataSource as BindingSource).DataSource is DataSet)
                    dataTable = ((this.GridControl_F.MainView.DataSource as BindingSource).DataSource as DataSet).Tables[(this.GridControl_F.MainView.DataSource as BindingSource).DataMember];
            }
            else
                if (this.GridControl_F.DataSource is DataSet)
                    dataTable = (this.GridControl_F.DataSource as DataSet).Tables[this.GridControl_F.DataMember];

            if (dataTable != null)
            {
                this.GridView_F.ClearColumnErrors();

           //     if (GridView_F.FocusedRowHandle < 0){
           //         e.Valid = true;
           //         return;
           //     };

                DataRow dataRow = ((DataRowView)(this.GridView_F.GetRow(GridView_F.FocusedRowHandle))).Row;
                
                foreach (DataColumn dataColumn in dataTable.Columns)
                {
                    DevExpress.XtraGrid.Columns.GridColumn gridColumn = this.GridView_F.Columns.ColumnByFieldName(dataColumn.ToString());

                    if (gridColumn != null)
                    {
                        object cellValue = dataRow[dataColumn];

                        if ((!dataColumn.AllowDBNull) && ((cellValue == null) || (cellValue.ToString().Trim() == "")))
                        {
                            this.GridView_F.SetColumnError(gridColumn, "Campo obrigat�rio sem um valor informado.");
                        }
                        else
                            if ((dataColumn.Unique) && (dataRow.RowState == DataRowState.Detached))
                            {
                                string select = dataColumn.ColumnName + " = ''";

                                if (cellValue != null)
                                {
                                    if (dataColumn.DataType == System.Type.GetType("System.DateTime"))
                                        select = String.Format("{0} = #{1:MM/dd/yyyy}#", dataColumn.ColumnName, Convert.ToDateTime(cellValue));
                                    else if (dataColumn.DataType == System.Type.GetType("System.String"))
                                        select = dataColumn.ColumnName + " = '" + cellValue.ToString() + "'";
                                    else
                                        select = dataColumn.ColumnName + " = " + cellValue.ToString();
                                }

                                DataRow[] foundRows;

                                foundRows = dataTable.Select(select);

                                if (foundRows.Length > 0)
                                    this.GridView_F.SetColumnError(gridColumn, "Este campo n�o permite valor duplicado.");
                            }
                    }
                }
            }
            e.Valid = (!this.GridView_F.HasColumnErrors);
        }

        private void GridView_F_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            //Suspende a exibi��o da mensagem de erro
            e.ExceptionMode = ExceptionMode.NoAction;
        }

        private void BindingSource_F_AddingNew(object sender, AddingNewEventArgs e)
        {
            ShowToolBarByRowState(true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            return !barDataBase_F.Visible;
        }

        private System.DateTime DataLayout = System.DateTime.MinValue;

        /// <summary>
        /// Data do layout obrigatorio
        /// </summary>
        [Category("Virtual Software")]
        [Description("Data Layout")]
        public System.DateTime dataLayout
        {
            get { return DataLayout; }
            set { DataLayout = value; }
        }
        
        private string funcionalidade;


        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Funcionalidade")]
        public string Funcionalidade
        {
            get { return funcionalidade; }
            set { funcionalidade = value; }
        }
        
        private int nivelgravacao;
        /// <summary>
        /// 
        /// </summary>
        public bool podeimprimir=true;

        /// <summary>
        /// 
        /// </summary>
        public bool podeexportar=true;
        
        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("N�vel m�nimo para grava��o")]
        public int NivelLeitura
        {
            get { return nivelgravacao; }
            set { nivelgravacao = value; }
        }

        /// <summary>
        /// Botao Excluir
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Excluir")]        
        public bool BotaoExcluir
        {
            get { return BtnExcluir_F.Visibility == DevExpress.XtraBars.BarItemVisibility.Always; }
            set 
            {
                BtnExcluir_F.Visibility = value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            }
        }
         
        /// <summary>
        /// Botao Incluir
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Incluir")]
        public bool BotaoIncluir
        {
            get { return BtnIncluir_F.Visibility == DevExpress.XtraBars.BarItemVisibility.Always; }
            set
            {
                BtnIncluir_F.Visibility = value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            }
        }

        /// <summary>
        /// Botao Alterar
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Alterar")]
        public bool BotaoAlterar
        {
            get { return BtnAlterar_F.Visibility == DevExpress.XtraBars.BarItemVisibility.Always; }
            set
            {
                BtnAlterar_F.Visibility = value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never;
            }
        }

        /// <summary>
        /// Ocorre quando o componente entra em edi��o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento ao entrar em edicao")]
        public event EventHandler editando;

        /// <summary>
        /// Ocorre quando o componente sai de edi��o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento ao sair de edicao")]
        public event EventHandler naoeditando;


        /// <summary>
        /// Evento ao entrar em edicao
        /// </summary>        
        private void Oneditando(EventArgs e)
        {
            if (editando != null)
                editando(this, e);
        }

        /// <summary>
        /// Evento ao sair de edicao
        /// </summary>        
        private void Onnaoeditando(EventArgs e)
        {
            if (naoeditando != null)
                naoeditando(this, e);
        }

        

        private void ComponenteGradeNavegador_Load(object sender, EventArgs e)
        {
            if(!VirDB.Bancovirtual.BancoVirtual.Configurado(VirDB.Bancovirtual.TiposDeBanco.SQL))
                return;
            if (funcionalidade != null)
            {
                BtnAlterar_F.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade(funcionalidade) >= nivelgravacao);
                BtnIncluir_F.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade(funcionalidade) >= nivelgravacao);
                BtnExcluir_F.Enabled = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade(funcionalidade) >= nivelgravacao);
            }
            podeimprimir = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("IMPRIMIR") >= 1);
            podeexportar = (CompontesBasicos.Bancovirtual.UsuarioLogado.UsuarioLogadoSt.VerificaFuncionalidade("EXPORTAR") >= 1);
            BtnImprimir_F.Enabled = podeimprimir;
            BtnExportar_F.Enabled = podeexportar;
            string StrArq = NomeArquivo(0);
            string StrDir = System.IO.Path.GetDirectoryName(StrArq);
            try
            {
                if (!System.IO.Directory.Exists(StrDir))
                    System.IO.Directory.CreateDirectory(StrDir);
                GridControl_F.MainView.SaveLayoutToXml(StrArq);
                string ArquivoXML = NomeArquivo(1);
                if (System.IO.File.Exists(ArquivoXML))
                {
                    if (System.IO.File.GetCreationTime(ArquivoXML) < dataLayout)
                        System.IO.File.Delete(ArquivoXML);
                    else
                        GridControl_F.MainView.RestoreLayoutFromXml(ArquivoXML);
                }
            }
            catch { };
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Ativado = true;
            RefreshDadosManual();
        }

        /// <summary>
        /// 
        /// </summary>
        public enum CriterioRefresh
        {
            /// <summary>
            /// 
            /// </summary>
            sempre,
            /// <summary>
            /// 
            /// </summary>
            nunca,
            /// <summary>
            /// 
            /// </summary>
            semFilhotes
        }

        private CriterioRefresh refreshDadosLiberado = CriterioRefresh.sempre;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Alterar")]
        public CriterioRefresh RefreshDadosLiberado
        {
            get { return refreshDadosLiberado; }
            set { refreshDadosLiberado = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override void RefreshDados()
        {
            if (refreshDadosLiberado == CriterioRefresh.nunca)
                return;
            if ((refreshDadosLiberado == CriterioRefresh.semFilhotes) && (FilhotesRegistrados.Count != 0))
                return;
            base.RefreshDados();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GridControl_F.MainView.SaveLayoutToXml(NomeArquivo(1));
        }

        private void barButtonItem3_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            System.IO.File.Copy(NomeArquivo(0), NomeArquivo(1), true);
            GridControl_F.MainView.RestoreLayoutFromXml(NomeArquivo(1));
        }

        private string NomeArquivo(int modelo)
        {
            return String.Format("{0}\\XML\\{1}[{2}].XML", Application.StartupPath, Name, modelo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void GridControl_F_DoubleClick(object sender, EventArgs e)
        {
            if(BtnAlterar_F.Enabled)
                 Alterar();
        }

        private void btnVisualizar_FPai_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Visualizar();
        }

        /// <summary>
        /// Abre somente leitura
        /// </summary>
        protected virtual void Visualizar()
        {
            
        }
    }
}

