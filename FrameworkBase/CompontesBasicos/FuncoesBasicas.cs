/*
LH - 21/02/2015 08:20  14.2.4.30- Inclus�o das fun��es "Extension"
*/

using System;
using System.Windows.Forms;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public static class FuncoesBasicas
    {
        /// <summary>
        /// Metodo recursivo para verificar se o enter deve agir como TAB
        /// </summary>
        /// <remarks>Evita o salto em Memos (-) e componentes de grid</remarks>
        /// <param name="CTRL">Controle a ser testado</param>
        /// <returns>Retorna se o foco pode passar para o pr�ximo controle</returns>
        public static bool PularComEnterRc(Control CTRL)
        {
            if (
                (CTRL is TextBoxBase) && (((TextBoxBase)CTRL).Multiline)
                ||
                (CTRL is DevExpress.XtraGrid.GridControl)
               )
                return false;
            else
                if (CTRL.Parent == null)
                    return true;
                else
                    return PularComEnterRc(CTRL.Parent);
        }        
    }    
}
