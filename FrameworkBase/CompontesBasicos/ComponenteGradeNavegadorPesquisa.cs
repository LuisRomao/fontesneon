using System;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraLayout;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteGradeNavegadorPesquisa : CompontesBasicos.ComponenteGradeNavegadorP
    {
     

        
        
        private string selectOriginal;
        private PropertyInfo PropertyInfoDoComandoSQL;
        private Object Comando;

        /// <summary>
        /// Complemento do comando SQL
        /// </summary>
        public string complementoWhere = "";

        /// <summary>
        /// Coluna da Grid Selecionada
        /// </summary>
        private DevExpress.XtraGrid.Columns.GridColumn ColunaSel;
        
        //private Type componenteCampos;

        

        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteGradeNavegadorPesquisa()
        {
            
            InitializeComponent();
            
        }

        /* 
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (this.estado == EstadosDosComponentes.Registrado)
            {
                ForPai().ShowModuloCamposNovo(ComponenteCampos, "Novo",this);
                //CCB.BindingSource_F.AddNew();
            }
        }
        */ 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GridControl_F_DoubleClick(object sender, EventArgs e)
        {
            if (BtnAlterar_F.Enabled)
                Alterar();
            else
                Visualizar();                
        }

        /// <summary>
        /// M�todo de altera��o
        /// </summary>
        protected override void Alterar()
        {
            AbreCampos(false, "Altera", "");
        }

        
        

        

        private void ComponenteGradeNavegadorPesquisa_Load(object sender, EventArgs e)
        {            


            foreach (DevExpress.XtraGrid.Columns.GridColumn GC in GridView_F.Columns)
                cbxCampo_F.Properties.Items.Add(new ColunaDaGrid(GC));
//            if (cbxCampo_F.Properties.Items.Count != 0)
  //              cbxCampo_F.SelectedIndex = 0;


            

            BindingFlags bf;

            if (tableAdapter == null)
                return;

            bf = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            
            PropertyInfo pi = tableAdapter.GetType().GetProperty("CommandCollection", bf);
            
            Object[] CommandCollection = (Object[])pi.GetValue(tableAdapter, null);
            
            if (CommandCollection == null)
                throw new MissingFieldException("CommandCollection n�o encontrado");
            
            Comando = CommandCollection[0];
            
            if (Comando == null)
                throw new MissingFieldException("CommandCollection[0] n�o encontrado");
            
            Type tipo = Comando.GetType();
            
            PropertyInfoDoComandoSQL = tipo.GetProperty("CommandText");
            
            selectOriginal = (string)PropertyInfoDoComandoSQL.GetValue(Comando, null);

            
        }

        private void SetaComandoSQL(string novoComando) 
        {
            PropertyInfoDoComandoSQL.SetValue(Comando, novoComando, null);            
        }

        private bool ajustaAdapter() 
        {
            String condicaoSQL = "";


            // DESLIGADO - Fun��o de filtragem
            /*
            if ((ColunaSel != null) && (ColunaSel.ColumnType == typeof(System.String)))
            {
                string valor = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;
                
                switch (cbxCondicao_F.SelectedIndex)
                {
                    case 0: //Comece com
                        {
                            condicaoSQL = ColunaSel.FieldName + " Like '" + valor + "%'";
                            break;
                        }
                    case 1: //Termine com
                        {
                            condicaoSQL = ColunaSel.FieldName + " Like '%" + valor + "'";
                            break;
                        }
                    case 2: //Contenha
                        {
                            condicaoSQL = ColunaSel.FieldName + " Like '%" + valor + "%'";
                            break;
                        }
                }
            }
            else if ((ColunaSel.ColumnType == typeof(System.Int16))
                       ||
                     (ColunaSel.ColumnType == typeof(System.Int32))
                       ||
                     (ColunaSel.ColumnType == typeof(System.Int64))
                       ||
                     (ColunaSel.ColumnType == typeof(System.Decimal)))
            {
                switch (cbxCondicao_F.SelectedIndex)
                {
                    case 0: //Maior
                        {
                            string valor = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;

                            condicaoSQL = ColunaSel.FieldName + " > " + valor.ToString();

                            break;
                        }
                    case 1: //Maior ou Igual
                        {
                            string valor = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;

                            condicaoSQL = ColunaSel.FieldName + " >= " + valor.ToString();
                            
                            break;
                        }
                    case 2: //Menor
                        {
                            string valor = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;

                            condicaoSQL = ColunaSel.FieldName + " < " + valor.ToString();

                            break;
                        }
                    case 3: //Menor ou Igual
                        {
                            string valor = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;

                            condicaoSQL = ColunaSel.FieldName + " <= " + valor.ToString();
                            
                            break;
                        }
                    case 4: //Entre
                        {
                            string valor1 = ((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text;

                            string valor2 = ((LayoutControlItem)(LayoutValor_F.Root.Items[1])).Control.Text;

                            condicaoSQL = ColunaSel.FieldName + " Between " + valor1.ToString() + " and " + valor2.ToString();
                            
                            break;
                        }
                }
            }
            else if (ColunaSel.ColumnType == typeof(System.DateTime))
            {
                switch (cbxCondicao_F.SelectedIndex)
                {
                    case 0: //Maior
                        {
                            string valor = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text).ToString("MM/dd/yyyy");

                            condicaoSQL = ColunaSel.FieldName + " > '" + valor.ToString() + "'";
                            
                            break;
                        }
                    case 1: //Maior ou Igual
                        {
                            string valor = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text).ToString("MM/dd/yyyy");

                            condicaoSQL = ColunaSel.FieldName + " >= '" + valor.ToString() + "'";

                            break;
                        }
                    case 2: //Menor
                        {
                            string valor = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text).ToString("MM/dd/yyyy");

                            condicaoSQL = ColunaSel.FieldName + " < '" + valor.ToString() + "'";

                            break;
                        }
                    case 3: //Menor ou Igual
                        {
                            string valor = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text).ToString("MM/dd/yyyy");

                            condicaoSQL = ColunaSel.FieldName + " <= '" + valor.ToString() + "'";

                            break;
                        }
                    case 4: //Entre
                        {
                            string valor1 = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[0])).Control.Text).ToString("MM/dd/yyyy");

                            string valor2 = Convert.ToDateTime(((LayoutControlItem)(LayoutValor_F.Root.Items[1])).Control.Text).ToString("MM/dd/yyyy");

                            condicaoSQL = ColunaSel.FieldName + " Between '" + valor1.ToString() + "' and '" + valor2.ToString() + "'";

                            break;
                        }
                }
            }*/
            string Novocomando = selectOriginal;

            if (condicaoSQL == "")
                condicaoSQL = complementoWhere;
            else {
                if (complementoWhere != "")
                    condicaoSQL += (" AND " + complementoWhere);
            };




            if (condicaoSQL != "")
            {
                if (selectOriginal.ToUpper().IndexOf("WHERE") > 0)
                    Novocomando += (" And " + condicaoSQL);
                else
                    Novocomando += (" Where " + condicaoSQL);
            };
            

            
            if (Novocomando != "")
                SetaComandoSQL(Novocomando);
            return true;
        }

        /*
        /// <summary>
        /// Tipo do componente Campos a ser aberto
        /// </summary>
        [Category("Virtual Software")]
        [Description("Componente a ser chamado para editar o registro")]
        [Editor(typeof(SelControleList), typeof(UITypeEditor))]
        public Type ComponenteCampos
        {
            get { return componenteCampos; }
            set { componenteCampos = value; }
        }
        */

        

        

        /// <summary>
        /// Campo a mostrar na tab
        /// </summary>
        [Category("Virtual Software")]
        [Editor(typeof(SelTableAdapterList), typeof(UITypeEditor))]
        public Component TableAdapter
        {
            get { return tableAdapter; }
            set { tableAdapter = value; }
        }

        private void RemoveItemFromLayoutValor_F(int LayoutItemIndex)
        {
            if (LayoutValor_F.Root.Items.Count > 0)
            {
                LayoutValor_F.BeginUpdate();

                if (LayoutItemIndex == 0) //0 = Remove todos os itens.
                {
                    for (int i = (LayoutValor_F.Root.Items.Count - 1); i >= 0; i--)
                    {
                        if (!LayoutValor_F.Root.Items[i].IsGroup)
                        {
                            LayoutControlItem LCI = (LayoutControlItem)LayoutValor_F.Root.Items[i];

                            LayoutValor_F.Root.Remove(LCI);

                            LCI.Control.Dispose();
                        }
                    }
                }
                else //Remove somente o item passado no parametro
                {
                    LayoutControlItem LCI = (LayoutControlItem)LayoutValor_F.Root.Items[LayoutItemIndex];                    
                    LayoutValor_F.Root.Remove(LCI);
                    LCI.Control.Dispose();
                }

                LayoutValor_F.EndUpdate();
            }
        }

        private void btnLimpar_F_Click(object sender, EventArgs e)
        {
            tabela.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Pesquisar() {
           // if ((cbxCampo_F.Text.Trim() != "") && (cbxCondicao_F.Text.Trim() != ""))
           // {
                ajustaAdapter();
                object[] Parametros = new object[1];
                Parametros[0] = tabela;

                tableAdapter.GetType().GetMethod("Fill").Invoke(tableAdapter, Parametros);
                
            //}
        }

        private void btnPesquisar_F_Click(object sender, EventArgs e)
        {
            Pesquisar();            
        }

        private string[] cString = new string[] { "Comece com", "Termine com", "Contenha" };

        private string[] cNumber = new string[] { "Maior", "Maior ou Igual", "Menor", "Menor ou Igual", "Entre" };

        private string[] cDateTime = new string[] { "Maior", "Maior ou Igual", "Menor", "Menor ou Igual", "Entre" };

        private void cbxCampo_F_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxCampo_F.Text.Trim() != "")
            {
                if ((ColunaSel != null)
                      &&
                    (ColunaSel.ColumnType != ((ColunaDaGrid)cbxCampo_F.EditValue).Coluna.ColumnType))
                {
                    RemoveItemFromLayoutValor_F(0);
                }

                cbxCondicao_F.Properties.Items.Clear();

                cbxCondicao_F.Text = "";

                ColunaSel = ((ColunaDaGrid)cbxCampo_F.EditValue).Coluna;

                if (ColunaSel.ColumnType == typeof(System.String))
                {
                    cbxCondicao_F.Properties.Items.AddRange(cString);
                }
                else if ((ColunaSel.ColumnType == typeof(System.Int16))
                          ||
                         (ColunaSel.ColumnType == typeof(System.Int32))
                          ||
                         (ColunaSel.ColumnType == typeof(System.Int64))
                          ||
                         (ColunaSel.ColumnType == typeof(System.Decimal)))
                {
                    cbxCondicao_F.Properties.Items.AddRange(cNumber);
                }
                else if (ColunaSel.ColumnType == typeof(System.DateTime))
                {
                    cbxCondicao_F.Properties.Items.AddRange(cDateTime);
                }                
                cbxCondicao_F.SelectedIndex = 0;
                
                
            }    

        }

        private void cbxCondicao_F_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxCondicao_F.Text.Trim() != "")
            {
                LayoutValor_F.BeginUpdate();

                LayoutValor_F.OptionsItemText.TextToControlDistance = 0;

                if (ColunaSel.ColumnType == typeof(System.String))
                {
                    if (LayoutValor_F.Root.Items.Count == 0)
                    {
                        LayoutControlItem LCI = LayoutValor_F.Root.AddItem("Valor", new DevExpress.XtraEditors.TextEdit());

                        LCI.TextLocation = DevExpress.Utils.Locations.Top;
                    }
                }
                else if ((ColunaSel.ColumnType == typeof(System.Int16))
                           ||
                          (ColunaSel.ColumnType == typeof(System.Int32))
                           ||
                          (ColunaSel.ColumnType == typeof(System.Int64))
                           ||
                          (ColunaSel.ColumnType == typeof(System.Decimal)))
                {
                    if ((cbxCondicao_F.SelectedIndex == 0) //Maior
                         ||
                        (cbxCondicao_F.SelectedIndex == 1) //Maior ou Igual
                         ||
                        (cbxCondicao_F.SelectedIndex == 2) //Menor
                         ||
                        (cbxCondicao_F.SelectedIndex == 3)) //Menor ou Igual
                    {
                        if (LayoutValor_F.Root.Items.Count == 0)
                        {
                            LayoutControlItem LCI = null;

                            if (ColunaSel.ColumnType == typeof(System.Decimal))
                                LCI = LayoutValor_F.Root.AddItem("Valor", new DevExpress.XtraEditors.CalcEdit());
                            else
                            {
                                DevExpress.XtraEditors.SpinEdit sp1 = new DevExpress.XtraEditors.SpinEdit();

                                sp1.Properties.IsFloatValue = false;

                                LCI = LayoutValor_F.Root.AddItem("Valor", sp1);
                            }

                            LCI.TextLocation = DevExpress.Utils.Locations.Top;
                        }
                        else if (LayoutValor_F.Root.Items.Count == 2)
                        {
                            RemoveItemFromLayoutValor_F(1);

                            LayoutControlItem LCI = ((LayoutControlItem)LayoutValor_F.Root[0]);
                            LCI.TextLocation = DevExpress.Utils.Locations.Top;
                            LCI.Text = "Valor";
                        }
                    }
                    else //Entre
                    {
                        LayoutValor_F.OptionsItemText.TextToControlDistance = 5;

                        if (LayoutValor_F.Root.Items.Count == 0)
                        {
                            LayoutControlItem LCI_1 = null;
                            LayoutControlItem LCI_2 = null;

                            if (ColunaSel.ColumnType == typeof(System.Decimal))
                            {
                                LCI_1 = LayoutValor_F.Root.AddItem("Valor Inicial", new DevExpress.XtraEditors.CalcEdit());
                                LCI_2 = LayoutValor_F.Root.AddItem("Valor Final", new DevExpress.XtraEditors.CalcEdit());
                            }
                            else
                            {
                                DevExpress.XtraEditors.SpinEdit sp1 = new DevExpress.XtraEditors.SpinEdit();
                                DevExpress.XtraEditors.SpinEdit sp2 = new DevExpress.XtraEditors.SpinEdit();

                                sp1.Properties.IsFloatValue = false;
                                sp2.Properties.IsFloatValue = false;

                                LCI_1 = LayoutValor_F.Root.AddItem("Valor Inicial", sp1);
                                LCI_2 = LayoutValor_F.Root.AddItem("Valor Final", sp2);
                            }

                            LCI_1.TextLocation = DevExpress.Utils.Locations.Left;
                            LCI_2.TextLocation = DevExpress.Utils.Locations.Left;
                        }
                        else if (LayoutValor_F.Root.Items.Count == 1)
                        {
                            // Item 1
                            LayoutControlItem LCI_1 = ((LayoutControlItem)LayoutValor_F.Root[0]);
                            LCI_1.TextLocation = DevExpress.Utils.Locations.Left;
                            LCI_1.Text = "Valor Inicial";

                            //Item 2
                            LayoutControlItem LCI_2 = null;
                            if (ColunaSel.ColumnType == typeof(System.Decimal))
                            {
                                LCI_2 = LayoutValor_F.Root.AddItem("Valor Final", new DevExpress.XtraEditors.CalcEdit());
                            }
                            else
                            {
                                DevExpress.XtraEditors.SpinEdit sp2 = new DevExpress.XtraEditors.SpinEdit();
                                sp2.Properties.IsFloatValue = false;
                                LCI_2 = LayoutValor_F.Root.AddItem("Valor Final", sp2);
                            }
                            LCI_2.TextLocation = DevExpress.Utils.Locations.Left;
                        }
                    }
                }
                else if (ColunaSel.ColumnType == typeof(System.DateTime))
                {
                    if ((cbxCondicao_F.SelectedIndex == 0) //Maior
                         ||
                        (cbxCondicao_F.SelectedIndex == 1) //Maior ou Igual
                         ||
                        (cbxCondicao_F.SelectedIndex == 2) //Menor
                         ||
                        (cbxCondicao_F.SelectedIndex == 3)) //Menor ou Igual
                    {
                        if (LayoutValor_F.Root.Items.Count == 0)
                        {
                            LayoutControlItem LCI = LayoutValor_F.Root.AddItem("Data", new DevExpress.XtraEditors.DateEdit());
                            LCI.TextLocation = DevExpress.Utils.Locations.Top;
                        }
                        else if (LayoutValor_F.Root.Items.Count == 2)
                        {
                            RemoveItemFromLayoutValor_F(1);

                            LayoutControlItem LCI = ((LayoutControlItem)LayoutValor_F.Root[0]);
                            LCI.TextLocation = DevExpress.Utils.Locations.Top;
                            LCI.Text = "Data";
                        }
                    }
                    else //Entre
                    {
                        LayoutValor_F.OptionsItemText.TextToControlDistance = 5;

                        if (LayoutValor_F.Root.Items.Count == 0)
                        {
                            LayoutControlItem LCI_1 = LayoutValor_F.Root.AddItem("Data Inicial", new DevExpress.XtraEditors.DateEdit());
                            LayoutControlItem LCI_2 = LayoutValor_F.Root.AddItem("Data Final", new DevExpress.XtraEditors.DateEdit());

                            LCI_1.TextLocation = DevExpress.Utils.Locations.Left;
                            LCI_2.TextLocation = DevExpress.Utils.Locations.Left;
                        }
                        else if (LayoutValor_F.Root.Items.Count == 1)
                        {
                            // Item 1
                            LayoutControlItem LCI_1 = ((LayoutControlItem)LayoutValor_F.Root[0]);
                            LCI_1.TextLocation = DevExpress.Utils.Locations.Left;
                            LCI_1.Text = "Data Inicial";

                            //Item 2
                            LayoutControlItem LCI_2 = LayoutValor_F.Root.AddItem("Data Final", new DevExpress.XtraEditors.DateEdit());
                            LCI_2.TextLocation = DevExpress.Utils.Locations.Left;
                        }
                    }
                }
                LayoutValor_F.EndUpdate();
            }
        }

        private void btnVisualizar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Visualizar();
        }

        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="SomenteLeitura"></param>
        /// <param name="Titulo"></param>
        /// <param name="CampoChave"></param>
        /// <returns></returns>
        public bool AbreCampos(bool SomenteLeitura,string Titulo,string CampoChave) 
        {
            using (Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Motivo.Text = "Abrindo";
                Application.DoEvents();
                DataRowView DRV;
                if (BindingSourcePrincipal.Current is DataRowView)
                    DRV = (DataRowView)BindingSourcePrincipal.Current;
                else
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GridView = (DevExpress.XtraGrid.Views.Grid.GridView)GridControl_F.MainView;
                    DRV = (DataRowView)GridView.GetRow(GridView.FocusedRowHandle);
                };



                if ((campoTab != null) && (DRV.Row.Table.Columns[campoTab] != null))
                {
                    DataColumn Coluna = DRV.Row.Table.Columns[campoTab];
                    DRV.Row[Coluna].ToString();
                    Titulo = prefixoTab + " " + DRV.Row[Coluna].ToString();
                };
                if (DRV != null)
                {
                    int pk;
                    if (CampoChave != "")
                    {
                        pk = (Int32)(DRV.Row[CampoChave]);
                    }
                    else
                        pk = (Int32)(DRV.Row.ItemArray[0]);
                    ComponenteCamposBase Comp = (ComponenteCamposBase)ForPai().ShowMoludoCampos(ComponenteCampos, Titulo, pk, SomenteLeitura, this);

                    return true;
                }
                else
                    return false;
            }
        }

        */
        
        
        /*private FormPrincipalBase ForPai() {
            Control CompPai = this.Parent;
            while (!(CompPai is FormPrincipalBase))
                CompPai = CompPai.Parent;
            return (FormPrincipalBase)CompPai;
        }*/

        private void GridControl_F_Load(object sender, EventArgs e)
        {
            if (cbxCampo_F.Properties.Items.Count != 0)
                cbxCampo_F.SelectedIndex = 0;
          
        }

        /*
        /// <summary>
        /// Prefixo do t�tulo da tab
        /// </summary>
        private string prefixoTab;

        /// <summary>
        /// Prefixo a mostrar na tab
        /// </summary>
        /// <remarks>exemplo (Cond. XXXXXXX)</remarks>
        [Category("Virtual Software")]
        [Description("Prefixo a ser mostrado na TAB")]
        public string PrefixoTab
        {
            get { return prefixoTab; }
            set { prefixoTab = value; }
        }


        /// <summary>
        /// Campo a mostrar na tab
        /// </summary>
        private string campoTab;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Campo a ser mostrado na TAB")]
        public string CampoTab
        {
            get { return campoTab; }
            set { campoTab = value; }
        }*/

    }

    class ColunaDaGrid:Object
    { 
        private DevExpress.XtraGrid.Columns.GridColumn coluna;

        public DevExpress.XtraGrid.Columns.GridColumn Coluna
        {
         get { return coluna; }
         set { coluna = value; }
        }

        public ColunaDaGrid(DevExpress.XtraGrid.Columns.GridColumn Coluna){
            this.Coluna = Coluna;
        }

        public override string ToString(){
            return coluna.Caption;
        }
    }
}

