using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteGradeBase : CompontesBasicos.ComponenteBaseBindingSource
    {
        /// <summary>
        /// 
        /// </summary>
        public override DataRow LinhaMae_F
        {
            get
            {
                try
                {
                    if (GridControl_F.MainView is DevExpress.XtraGrid.Views.Grid.GridView)
                    {
                        DevExpress.XtraGrid.Views.Grid.GridView Grid = (DevExpress.XtraGrid.Views.Grid.GridView)GridControl_F.MainView;
                        return Grid.GetFocusedDataRow();
                    }
                    else if (GridControl_F.MainView is DevExpress.XtraGrid.Views.BandedGrid.BandedGridView)
                    {
                        DevExpress.XtraGrid.Views.BandedGrid.BandedGridView BGrid = (DevExpress.XtraGrid.Views.BandedGrid.BandedGridView)GridControl_F.MainView;
                        return BGrid.GetFocusedDataRow();
                    }
                    else
                        return null;

                    /*
                    object oDRV = GridView_F.GetRow(GridView_F.FocusedRowHandle);
                    if (oDRV != null)
                    {
                        DataRowView DRV = (DataRowView)oDRV;
                        return DRV.Row;
                    }
                    else
                        return null;
                    */ 
                }
                catch {
                    return null;
                }
            }            
        } 

        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteGradeBase()
        {
            podeincluirlinha = false;
            InitializeComponent();
            this.Dock = DockStyle.Fill;
        }

        private void GridControl_F_Load(object sender, EventArgs e)
        {
            if (tipoDeCarga == TipoDeCarga.novo)
            {
                DataRowView Novalinha = ((DataRowView)BindingSource_F.AddNew());
                //if ((campoNovo != null) && (valorNovo != null) && (campoNovo.Trim() != ""))
                //    Novalinha[campoNovo] = valorNovo;
                if (camposNovos != null)
                    foreach (System.Collections.DictionaryEntry Entrada in camposNovos)
                        Novalinha[Entrada.Key.ToString()] = Entrada.Value;
                    
                GridControl_F.DefaultView.ShowEditor();
            }
        }

        private void GridView_F_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //LinhaMae_F = GridView_F.GetDataRow(e.FocusedRowHandle);
        }
    }
}

