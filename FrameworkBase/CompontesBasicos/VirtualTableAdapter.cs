using System;
using System.Collections.Generic;
using System.Text;
using CompontesBasicos.Bancovirtual;

namespace CompontesBasicos
{
    public class VirtualTableAdapter : System.ComponentModel.Component
    {
        public TiposDeBanco Tipo = TiposDeBanco.SQL;
        public void TrocarStringDeConexao(string novo)
        {
            System.Reflection.PropertyInfo PropCon = this.GetType().GetProperty("Connection");
            if (PropCon == null)
                System.Windows.Forms.MessageBox.Show("Connection n�o encontrado.(" + this.ToString() + ")");
            else
            {
                switch (Tipo)
                {
                    case TiposDeBanco.SQL:
                        System.Data.SqlClient.SqlConnection cone = (System.Data.SqlClient.SqlConnection)PropCon.GetValue(this, null);
                        cone.ConnectionString = novo;
                        break;
                    case TiposDeBanco.Internet:
                        System.Data.SqlClient.SqlConnection cone1 = (System.Data.SqlClient.SqlConnection)PropCon.GetValue(this, null);
                        cone1.ConnectionString = novo;
                        break;
                    case TiposDeBanco.Access:
                        System.Data.OleDb.OleDbConnection coneA = (System.Data.OleDb.OleDbConnection)PropCon.GetValue(this, null);
                        coneA.ConnectionString = novo;
                        break;
                };
                
                
            };                  
        }

        public void TrocarStringDeConexao()
        {
            TrocarStringDeConexao(CompontesBasicos.Bancovirtual.BancoVirtual.StringConeFinal(Tipo));                        
        }

        public void TrocarStringDeConexao(TiposDeBanco TB)
        {
            Tipo = TB;
            TrocarStringDeConexao();            
        }
    }
}
