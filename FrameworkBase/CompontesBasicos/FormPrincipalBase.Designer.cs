namespace CompontesBasicos
{
    partial class FormPrincipalBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary spellCheckerOpenOfficeDictionary5 = new DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary();
            DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary spellCheckerOpenOfficeDictionary6 = new DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary();
            DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary spellCheckerOpenOfficeDictionary7 = new DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary();
            DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary spellCheckerOpenOfficeDictionary8 = new DevExpress.XtraSpellChecker.SpellCheckerOpenOfficeDictionary();
            this.DockManager_F = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.BarManager_F = new DevExpress.XtraBars.BarManager(this.components);
            this.barStatus_F = new DevExpress.XtraBars.Bar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BarAndDockingController_F = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.ToolTipController_F = new DevExpress.Utils.ToolTipController(this.components);
            this.DockPanel_F = new DevExpress.XtraBars.Docking.DockPanel();
            this.DockPanel_FContainer = new DevExpress.XtraBars.Docking.ControlContainer();
            this.NavBarControl_F = new DevExpress.XtraNavBar.NavBarControl();
            this.PanelControl_F = new DevExpress.XtraEditors.PanelControl();
            this.PictureEdit_F = new DevExpress.XtraEditors.PictureEdit();
            this.DefaultLookAndFeel_F = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.DefaultToolTipController_F = new DevExpress.Utils.DefaultToolTipController(this.components);
            this.PanelTESTE = new DevExpress.XtraEditors.PanelControl();
            this.LabelTeste = new DevExpress.XtraEditors.LabelControl();
            this.TabControl_F = new DevExpress.XtraTab.XtraTabControl();
            this.sharedDictionaryStorage1 = new DevExpress.XtraSpellChecker.SharedDictionaryStorage(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DockManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.DockPanel_F.SuspendLayout();
            this.DockPanel_FContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl_F)).BeginInit();
            this.PanelControl_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTESTE)).BeginInit();
            this.PanelTESTE.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.SuspendLayout();
            // 
            // DockManager_F
            // 
            this.DockManager_F.Form = this;
            this.DockManager_F.MenuManager = this.BarManager_F;
            this.DockManager_F.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.DockPanel_F});
            this.DockManager_F.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar"});
            // 
            // BarManager_F
            // 
            this.BarManager_F.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus_F});
            this.BarManager_F.Controller = this.BarAndDockingController_F;
            this.BarManager_F.DockControls.Add(this.barDockControlTop);
            this.BarManager_F.DockControls.Add(this.barDockControlBottom);
            this.BarManager_F.DockControls.Add(this.barDockControlLeft);
            this.BarManager_F.DockControls.Add(this.barDockControlRight);
            this.BarManager_F.DockManager = this.DockManager_F;
            this.BarManager_F.Form = this;
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItem1});
            this.BarManager_F.MaxItemId = 1;
            this.BarManager_F.StatusBar = this.barStatus_F;
            this.BarManager_F.ToolTipController = this.ToolTipController_F;
            // 
            // barStatus_F
            // 
            this.barStatus_F.BarName = "StatusBar_F";
            this.barStatus_F.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus_F.DockCol = 0;
            this.barStatus_F.DockRow = 0;
            this.barStatus_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Text = "StatusBar";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "USU�RIO";
            this.barButtonItem1.Id = 0;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(782, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 534);
            this.barDockControlBottom.Size = new System.Drawing.Size(782, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 534);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(782, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 534);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Rounded = true;
            // 
            // DockPanel_F
            // 
            this.DockPanel_F.Appearance.BackColor = System.Drawing.Color.White;
            this.DockPanel_F.Appearance.Options.UseBackColor = true;
            this.DockPanel_F.Controls.Add(this.DockPanel_FContainer);
            this.DockPanel_F.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.DockPanel_F.FloatSize = new System.Drawing.Size(210, 203);
            this.DockPanel_F.ID = new System.Guid("84da3a75-5c78-4e4e-ab17-97d91fbebb45");
            this.DockPanel_F.Location = new System.Drawing.Point(0, 0);
            this.DockPanel_F.Name = "DockPanel_F";
            this.DockPanel_F.Options.AllowDockFill = false;
            this.DockPanel_F.Options.ShowCloseButton = false;
            this.DockPanel_F.Options.ShowMaximizeButton = false;
            this.DockPanel_F.OriginalSize = new System.Drawing.Size(200, 200);
            this.DockPanel_F.Size = new System.Drawing.Size(200, 534);
            this.DockPanel_F.Text = "Menu Principal";
            // 
            // DockPanel_FContainer
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.DockPanel_FContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.DockPanel_FContainer.Controls.Add(this.NavBarControl_F);
            this.DockPanel_FContainer.Controls.Add(this.PanelControl_F);
            this.DockPanel_FContainer.Location = new System.Drawing.Point(3, 27);
            this.DockPanel_FContainer.Name = "DockPanel_FContainer";
            this.DockPanel_FContainer.Size = new System.Drawing.Size(194, 504);
            this.DockPanel_FContainer.TabIndex = 0;
            // 
            // NavBarControl_F
            // 
            this.NavBarControl_F.ActiveGroup = null;
            this.NavBarControl_F.ContentButtonHint = null;
            this.NavBarControl_F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NavBarControl_F.Location = new System.Drawing.Point(0, 81);
            this.NavBarControl_F.Name = "NavBarControl_F";
            this.NavBarControl_F.OptionsNavPane.ExpandedWidth = 194;
            this.NavBarControl_F.Padding = new System.Windows.Forms.Padding(3);
            this.NavBarControl_F.Size = new System.Drawing.Size(194, 423);
            this.NavBarControl_F.TabIndex = 0;
            this.NavBarControl_F.ToolTipController = this.ToolTipController_F;
            this.NavBarControl_F.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBar_LinkClicked);
            // 
            // PanelControl_F
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.PanelControl_F, DevExpress.Utils.DefaultBoolean.Default);
            this.PanelControl_F.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.PanelControl_F.Controls.Add(this.PictureEdit_F);
            this.PanelControl_F.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelControl_F.Location = new System.Drawing.Point(0, 0);
            this.PanelControl_F.Margin = new System.Windows.Forms.Padding(0);
            this.PanelControl_F.Name = "PanelControl_F";
            this.PanelControl_F.Size = new System.Drawing.Size(194, 81);
            this.PanelControl_F.TabIndex = 2;
            // 
            // PictureEdit_F
            // 
            this.PictureEdit_F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureEdit_F.Location = new System.Drawing.Point(0, 0);
            this.PictureEdit_F.Name = "PictureEdit_F";
            this.PictureEdit_F.Size = new System.Drawing.Size(194, 81);
            this.PictureEdit_F.TabIndex = 1;
            this.PictureEdit_F.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormPrincipalBase_MouseDown);
            this.PictureEdit_F.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureEdit_F_MouseUp);
            // 
            // DefaultLookAndFeel_F
            // 
            this.DefaultLookAndFeel_F.LookAndFeel.SkinName = "The Asphalt World";
            this.DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.DefaultLookAndFeel_F.LookAndFeel.UseWindowsXPTheme = true;
            // 
            // DefaultToolTipController_F
            // 
            // 
            // 
            // 
            this.DefaultToolTipController_F.DefaultController.Rounded = true;
            // 
            // PanelTESTE
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this.PanelTESTE, DevExpress.Utils.DefaultBoolean.Default);
            this.PanelTESTE.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.PanelTESTE.Appearance.Options.UseBackColor = true;
            this.PanelTESTE.Controls.Add(this.LabelTeste);
            this.PanelTESTE.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTESTE.Location = new System.Drawing.Point(200, 0);
            this.PanelTESTE.Name = "PanelTESTE";
            this.PanelTESTE.Size = new System.Drawing.Size(582, 70);
            this.PanelTESTE.TabIndex = 18;
            this.PanelTESTE.Visible = false;
            // 
            // LabelTeste
            // 
            this.LabelTeste.Appearance.Font = new System.Drawing.Font("Tahoma", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelTeste.Appearance.ForeColor = System.Drawing.Color.Red;
            this.LabelTeste.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LabelTeste.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.LabelTeste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelTeste.Location = new System.Drawing.Point(0, 0);
            this.LabelTeste.Name = "LabelTeste";
            this.LabelTeste.Size = new System.Drawing.Size(582, 70);
            this.LabelTeste.TabIndex = 17;
            this.LabelTeste.Text = "Ambiente de TESTES";
            // 
            // TabControl_F
            // 
            this.TabControl_F.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.TabControl_F.Location = new System.Drawing.Point(291, 121);
            this.TabControl_F.Name = "TabControl_F";
            this.TabControl_F.Size = new System.Drawing.Size(385, 354);
            this.TabControl_F.TabIndex = 10;
            this.TabControl_F.ToolTipController = this.ToolTipController_F;
            this.TabControl_F.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabControl_F_SelectedPageChanged);
            this.TabControl_F.CloseButtonClick += new System.EventHandler(this.TabControl_F_CloseButtonClick);
            // 
            // sharedDictionaryStorage1
            // 
            spellCheckerOpenOfficeDictionary5.AlphabetPath = "";
            spellCheckerOpenOfficeDictionary5.CacheKey = null;
            spellCheckerOpenOfficeDictionary5.Culture = new System.Globalization.CultureInfo("pt-BR");
            spellCheckerOpenOfficeDictionary5.DictionaryPath = "pt_BR.dic";
            spellCheckerOpenOfficeDictionary5.Encoding = null;
            spellCheckerOpenOfficeDictionary5.GrammarPath = "pt_BR.aff";
            spellCheckerOpenOfficeDictionary6.AlphabetPath = "";
            spellCheckerOpenOfficeDictionary6.CacheKey = null;
            spellCheckerOpenOfficeDictionary6.Culture = new System.Globalization.CultureInfo("pt-BR");
            spellCheckerOpenOfficeDictionary6.DictionaryPath = "pt_BR.dic";
            spellCheckerOpenOfficeDictionary6.Encoding = null;
            spellCheckerOpenOfficeDictionary6.GrammarPath = "pt_BR.aff";
            spellCheckerOpenOfficeDictionary7.AlphabetPath = "";
            spellCheckerOpenOfficeDictionary7.CacheKey = null;
            spellCheckerOpenOfficeDictionary7.Culture = new System.Globalization.CultureInfo("pt-BR");
            spellCheckerOpenOfficeDictionary7.DictionaryPath = "pt_BR.dic";
            spellCheckerOpenOfficeDictionary7.Encoding = null;
            spellCheckerOpenOfficeDictionary7.GrammarPath = "pt_BR.aff";
            spellCheckerOpenOfficeDictionary8.AlphabetPath = "";
            spellCheckerOpenOfficeDictionary8.CacheKey = null;
            spellCheckerOpenOfficeDictionary8.Culture = new System.Globalization.CultureInfo("pt-BR");
            spellCheckerOpenOfficeDictionary8.DictionaryPath = "pt_BR.dic";
            spellCheckerOpenOfficeDictionary8.Encoding = null;
            spellCheckerOpenOfficeDictionary8.GrammarPath = "pt_BR.aff";
            this.sharedDictionaryStorage1.Dictionaries.Add(spellCheckerOpenOfficeDictionary5);
            this.sharedDictionaryStorage1.Dictionaries.Add(spellCheckerOpenOfficeDictionary6);
            this.sharedDictionaryStorage1.Dictionaries.Add(spellCheckerOpenOfficeDictionary7);
            this.sharedDictionaryStorage1.Dictionaries.Add(spellCheckerOpenOfficeDictionary8);
            // 
            // FormPrincipalBase
            // 
            this.DefaultToolTipController_F.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 556);
            this.Controls.Add(this.TabControl_F);
            this.Controls.Add(this.PanelTESTE);
            this.Controls.Add(this.DockPanel_F);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "FormPrincipalBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormPrincipalBase_FormClosing);
            this.Load += new System.EventHandler(this.FormPrincipalBase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DockManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.DockPanel_F.ResumeLayout(false);
            this.DockPanel_FContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.NavBarControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelControl_F)).EndInit();
            this.PanelControl_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureEdit_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PanelTESTE)).EndInit();
            this.PanelTESTE.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl PanelControl_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraEditors.PictureEdit PictureEdit_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.Utils.ToolTipController ToolTipController_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraNavBar.NavBarControl NavBarControl_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarAndDockingController BarAndDockingController_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.LookAndFeel.DefaultLookAndFeel DefaultLookAndFeel_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.Utils.DefaultToolTipController DefaultToolTipController_F;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.Bar barStatus_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.Docking.ControlContainer DockPanel_FContainer;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraTab.XtraTabControl TabControl_F;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraBars.BarManager BarManager_F;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraBars.Docking.DockPanel DockPanel_F;
        private DevExpress.XtraBars.Docking.DockManager DockManager_F;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraSpellChecker.SharedDictionaryStorage sharedDictionaryStorage1;
        private DevExpress.XtraEditors.PanelControl PanelTESTE;
        private DevExpress.XtraEditors.LabelControl LabelTeste;
    }
}