using System;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using VirDB;
using VirDB.Bancovirtual;


namespace CompontesBasicos
{
    
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteBaseBindingSource : CompontesBasicos.ComponenteBase
    {
        #region Campos

        /// <summary>
        /// podeincluirlinha
        /// </summary>
        protected bool podeincluirlinha = true;
        //public string campoNovo = "";
        //public object valorNovo = null;
        /// <summary>
        /// Valores dos campos ao iniciar um novo registro
        /// </summary>
        public System.Collections.SortedList camposNovos = null; 
        /// <summary>
        /// Linha em foco na grid
        /// </summary>
        /// <remarks>Nos objetos filhos criar uma vari�vel "LinhaMae" tipando o DataRow</remarks>
        public virtual DataRow LinhaMae_F {
            get { return _LinhaMae_F; }
            set { _LinhaMae_F = value; }
        }

        /// <summary>
        /// valor real de LinhaMae_F
        /// </summary>
        protected DataRow _LinhaMae_F;

        private bool autofill = true;

        /// <summary>
        /// Tipo de carga
        /// </summary>
        public TipoDeCarga tipoDeCarga = TipoDeCarga.navegacao;
        /// <summary>
        /// Id do registro em foco
        /// </summary>
        /// <remarks>N�o est� trabalhando em sincronia com LinhaMae_F, seu uso se restrigem a dataset com um �nico registro ou que nao se move (exemplo camposbase)</remarks>
        public Int32 pk;

        

        private BindingSource bindingSourcePrincipal;

        /// <summary>
        /// Indica que os dados j� forma carregados
        /// </summary>
        /// <remarks>Permite RefreshDados</remarks>
        public bool Ativado = false;


        /// <summary>
        /// TableAdapter
        /// </summary>
        /// <remarks>� localizado no evento Load</remarks>
        protected Component tableAdapter;

        /// <summary>
        /// TableAdapter para o select
        /// </summary>
        protected Component tableAdapterSelect;
        /// <summary>
        /// Tabela pricipal
        /// </summary>
        protected DataTable tabela;
        /// <summary>
        /// Usuario de inclusao
        /// </summary>
        protected DataColumn colunaInclusao = null;
        /// <summary>
        /// Usu�rio de altera��o
        /// </summary>
        protected DataColumn colunaAlteracao = null;
        /// <summary>
        /// Data de inclus�o
        /// </summary>
        protected DataColumn colunaDataInclusao = null;
        /// <summary>
        /// Data de altera��o
        /// </summary>
        protected DataColumn colunaDataAlteracao = null;
        /// <summary>
        /// m�doto utilizado para o fill autom�tico
        /// </summary>
        protected MethodInfo metodoFill_F;
        /// <summary>
        /// m�doto utilizado para o fill autom�tico com pk
        /// </summary>
        protected MethodInfo metodoFillBy;
        /// <summary>
        /// m�todo utilizado para update autom�tico
        /// </summary>
        protected MethodInfo metodoupdate; 
        #endregion


        #region Propriedades

        /// <summary>
        /// Define o TableAdapter a ser usado para Select/Update autom�tico
        /// </summary>
        /// <remarks>Ajuste o bindingSourcePrincipal antes de chamar este m�todo</remarks>
        [Category("Virtual Software")]
        [Description("TableAdapter Pricipal")]
        public VirDB.VirtualTableAdapter TableAdapterPrincipal 
        {
            set 
            {
                tableAdapter = value;

                if (bindingSourcePrincipal == null)
                    bindingSourcePrincipal = BindingSource_F;

                if((tabela == null)
                   &&             
                   (bindingSourcePrincipal != null)
                   &&
                   (bindingSourcePrincipal.DataSource != null)
                   )
                {
                    if(bindingSourcePrincipal.DataSource is DataTable)
                        tabela = (DataTable)bindingSourcePrincipal.DataSource;
                    else if (bindingSourcePrincipal.DataSource is DataSet){
                        DataSet DS = (DataSet)bindingSourcePrincipal.DataSource;
                        tabela = DS.Tables[bindingSourcePrincipal.DataMember];    
                    }
                };


                if ((tabela != null) && (tableAdapter != null))
                {
                    metodoupdate = tableAdapter.GetType().GetMethod("Update", new Type[1] { tabela.GetType() });
                    metodoFill_F = tableAdapter.GetType().GetMethod("Fill", new Type[1] { tabela.GetType() });                    
                    metodoFillBy = tableAdapter.GetType().GetMethod("FillBy",new Type[2] { tabela.GetType(),typeof(int) });
                }
            }
            get {
                if ((tableAdapter != null) && (tableAdapter.GetType().IsSubclassOf(typeof(VirDB.VirtualTableAdapter))))
                    return (VirDB.VirtualTableAdapter)tableAdapter;
                else
                    return null;
            }
        }

        /// <summary>
        /// BindingSourcePrincipal
        /// </summary>
        /// <remarks>Se nao for explicitamente definido utiliza o bindingSource_F</remarks>
        [Category("Virtual Software")]
        [Description("BindingSourcePrincipal")]
        public BindingSource BindingSourcePrincipal
        {
            get { return bindingSourcePrincipal; }
            set { bindingSourcePrincipal = value; }
        }

        /// <summary>
        /// Evento que ocorre antes da carga da tabela pricipal
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento para a carga das tabelas antes da principal")]
        public event EventHandler cargaInicial;

        /// <summary>
        /// Evento para carga da tabela principal
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento para a carga da tabela principal")]
        public event EventHandler cargaPrincipal;

        /// <summary>
        /// Evento que ocorre ap�s a carga da tabela principal
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento para a carga das tabelas ap�s da principal")]
        public event EventHandler cargaFinal;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Fill autom�tico nas tabela")]
        public bool Autofill
        {
            get { return autofill; }
            set { autofill = value; }
        }

        /*
        [Category("Virtual Software")]
        [Description("Binding Principal")]
        public BindingSource PBindingSource_F
        {
            get { return BindingSource_F; }
            set { BindingSource_F = value; }
        }
        */

        #endregion

        /// <summary>
        /// carga da tabela pricipal
        /// </summary>
        /// <remarks>
        /// O evento load dispara a carga das tabelas (Fill) em 3 partes:
        /// 1) Carga inicial: as tabela que servir�o de suporte para a tabela principal (lookUp)
        /// 2) Carga principal: carga da tabela pricipal. Observe que esta evento pode ser dispararado autmaticamante por chamadas como ShowAdd, ShowKey etc. 
        /// 3) carga das tabelas que dependam da tabela principal (detalhes)
        /// </remarks>
        private void OncargaInicial(EventArgs e)
        {            
            if (cargaInicial != null)                            
                cargaInicial(this, e);         
        }

        /// <summary>
        /// Evento para a carga das tabelas que dependam da tabela principal
        /// </summary>
        /// <remarks>
        /// O evento load dispara a carga das tabelas (Fill) em 3 partes:
        /// 1) Carga inicial: as tabela que servir�o de suporte para a tabela principal (lookUp)
        /// 2) Carga principal: carga da tabela pricipal. Observe que esta evento pode ser dispararado autmaticamante por chamadas como ShowAdd, ShowKey etc. 
        /// 3) carga das tabelas que dependam da tabela principal (detalhes)
        /// </remarks>
        private void OncargaFinal(EventArgs e)
        {
            if (cargaFinal != null)                        
                cargaFinal(this, e);            
        }

        /// <summary>
        /// carga das tabelas que dependam da tabela principal
        /// </summary>
        /// <remarks>
        /// O evento load dispara a carga das tabelas (Fill) em 3 partes:
        /// 1) Carga inicial: as tabela que servir�o de suporte para a tabela principal (lookUp)
        /// 2) Carga principal: carga da tabela pricipal. Observe que esta evento pode ser dispararado autmaticamante por chamadas como ShowAdd, ShowKey etc. 
        /// 3) carga das tabelas que dependam da tabela principal (detalhes)
        /// </remarks>
        private void OncargaPrincipal(EventArgs e)
        {            
            if (cargaPrincipal != null)
                cargaPrincipal(this, e);         
        }

        
        /// <summary>
        /// 
        /// </summary>
        public ComponenteBaseBindingSource()
        {
            InitializeComponent();
            
        }

        

        private bool FillInicialFeito = false;

        /// <summary>
        /// 
        /// </summary>
        public void Fill() {
            if (FillInicialFeito)
                return;
            FillInicialFeito = true;
            this.Cursor = Cursors.WaitCursor;
            ConfiguracaoInicial();
            if (tipoDeCarga != TipoDeCarga.limpo)
            {
                OncargaInicial(new EventArgs());

                switch (tipoDeCarga)
                {
                    case TipoDeCarga.navegacao:
                        OncargaPrincipal(new EventArgs());
                        FillAutomatico();
                        if ((tabela != null) && (tabela.Rows.Count == 1))
                            LinhaMae_F = tabela.Rows[0];                        
                        break;
                    case TipoDeCarga.novo:
                        FillAutomatico();
                        if (podeincluirlinha)
                        {
                            LinhaMae_F = ((DataRowView)bindingSourcePrincipal.AddNew()).Row;
                            //if ((campoNovo != null) && (valorNovo != null) && (campoNovo.Trim() != ""))
                            //    LinhaMae_F[campoNovo] = valorNovo;
                            if(camposNovos != null)
                                foreach(System.Collections.DictionaryEntry Entrada in camposNovos)
                                    LinhaMae_F[Entrada.Key.ToString()] = Entrada.Value;
                        };

                        break;
                    case TipoDeCarga.pk:
                        Ativado = false;
                        FillAutomatico();
                        /*
                        if (xautofill)
                        {
                            bindingSourcePrincipal.Position = bindingSourcePrincipal.Find(tabela.Columns[0].ColumnName, pk);
                        }
                        else
                            if ((tableAdapterSelect != null) && (metodoFillBy != null))
                            {
                                object[] Parametros = new Object[2];
                                Parametros[0] = tabela;
                                Parametros[1] = pk;
                                metodoFillBy.Invoke(tableAdapterSelect, Parametros);
                            }
                        */




                        if(!Ativado)
                          if ((tableAdapterSelect != null) && (metodoFillBy != null))
                          {
                            object[] Parametros = new Object[2];
                            Parametros[0] = tabela;
                            Parametros[1] = pk;
                            metodoFillBy.Invoke(tableAdapterSelect, Parametros);
                          }
                          else                          
                              Ativado = Find();                                                        
                        break;
                };
                //OncargaPrincipal(new EventArgs());
                OncargaFinal(new EventArgs());
                this.Cursor = Cursors.Default;
            };
        }

        /// <summary>
        /// Localiza o registro pk
        /// </summary>
        /// <returns></returns>
        public bool Find() {
            if ((bindingSourcePrincipal == null) || (tabela == null))
                return false;
            if (tabela.Columns.Count == 0)
                return false;   
            int retorno = bindingSourcePrincipal.Find(tabela.Columns[0].ColumnName, pk);

            if (retorno == -1)
            {                
                LinhaMae_F = null;
                return false;
            };
            bindingSourcePrincipal.Position = retorno;
            DataRowView DRV = (DataRowView)bindingSourcePrincipal.Current;
            if ((DRV != null) && (DRV.Row != null))
            {
                LinhaMae_F = DRV.Row;
                return true;                
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TipoDeCarga"></param>
        /// <param name="pk"></param>
        /// <param name="SomenteLeitura"></param>
        public void Fill(TipoDeCarga TipoDeCarga, int pk, bool SomenteLeitura)
        {
            this.tipoDeCarga = TipoDeCarga;
            this.pk = pk;
            this.somenteleitura = SomenteLeitura;
            Fill();
        }

        /// <summary>
        /// 
        /// </summary>
        public bool TravadoParaEvitarBug = true;

        private bool Configurado = false;

        /// <summary>
        /// Busca dos adpters
        /// </summary>
        /// <remarks>Melhorar</remarks>
        private void ConfiguracaoInicial() {
            if (this.DesignMode)
                return;                       
            if (Configurado)
                return;
            Configurado = true;
            BindingFlags bf;
            //Bancovirtual.BancoVirtual.StringConeFinal();
            bf = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            foreach (FieldInfo fi in (this.GetType()).GetFields(bf))
            {
                if (fi.FieldType.IsSubclassOf(typeof(VirDB.VirtualTableAdapter)))
                {
                    VirDB.VirtualTableAdapter Vtableadapter = (VirDB.VirtualTableAdapter)fi.GetValue(this);
                    if (Vtableadapter != null)
                      if (BancoVirtual.EscolherString() != -1)
                        Vtableadapter.TrocarStringDeConexao();
                }
                if (fi.FieldType.BaseType == typeof(DataSet))
                {
                    DataSet VDataSet = (DataSet)fi.GetValue(this);
                    if (VDataSet != null)
                        VirDB.VirtualTableAdapter.AjustaAutoInc(VDataSet);
                    //if (VDataColumn.AutoIncrement)
                    //    VDataColumn.AutoIncrementStep = 10000;
  //                  VirDB.VirtualTableAdapter Vtableadapter = (VirtualTableAdapter)fi.GetValue(this);
    //                if (Vtableadapter != null)
      //                  if (Bancovirtual.BancoVirtual.EscolherString() != -1)
        //                    Vtableadapter.TrocarStringDeConexao();
                }
            };

            if (bindingSourcePrincipal == null)
                bindingSourcePrincipal = BindingSource_F;

            if ((bindingSourcePrincipal.DataSource != null) && (bindingSourcePrincipal.DataSource is DataSet))
            {
                DataSet DS = (DataSet)bindingSourcePrincipal.DataSource;

                if (DS == null)
                    return;

                tabela = DS.Tables[bindingSourcePrincipal.DataMember];

                if (tabela == null)
                    return;

                if (TableAdapterPrincipal == null)
                {
                    if (!TravadoParaEvitarBug)
                    {
                        PropertyInfo infoTA = DS.GetType().GetProperty(string.Format("{0}TableAdapter", tabela));
                        if (infoTA != null)
                            TableAdapterPrincipal = (VirtualTableAdapter)infoTA.GetValue(DS, null);
                    }
                }

                string Prefixo = tabela.Columns[0].ColumnName;
                string CampoIncluisao = Prefixo + "I_USU";
                string CampoAlteracao = Prefixo + "A_USU";
                string CampoDataAlteracao = Prefixo + "DATAA";
                string CampoDataInclusao = Prefixo + "DATAI";
                if (colunaInclusao == null)
                    colunaInclusao = tabela.Columns[CampoIncluisao];
                if (colunaAlteracao == null)
                    colunaAlteracao = tabela.Columns[CampoAlteracao];
                if (colunaDataAlteracao == null)
                    colunaDataAlteracao = tabela.Columns[CampoDataAlteracao];
                if (colunaDataInclusao == null)
                    colunaDataInclusao = tabela.Columns[CampoDataInclusao];

                if (colunaInclusao == null)
                {
                    CampoIncluisao = Prefixo + "INCLUSAO_USU";
                    colunaInclusao = tabela.Columns[CampoIncluisao];
                };
                if (colunaAlteracao == null)
                {
                    CampoAlteracao = Prefixo + "ATUALIZACAO_USU";
                    colunaAlteracao = tabela.Columns[CampoAlteracao];
                };
                if (colunaAlteracao == null)
                {
                    CampoAlteracao = Prefixo + "ALTERACAO_USU";
                    colunaAlteracao = tabela.Columns[CampoAlteracao];
                };
                if (colunaDataAlteracao == null)
                {
                    CampoDataAlteracao = Prefixo + "DATAATUALIZACAO";
                    colunaDataAlteracao = tabela.Columns[CampoDataAlteracao];
                };
                if (colunaDataAlteracao == null)
                {
                    CampoDataAlteracao = Prefixo + "DATAALTERACAO";
                    colunaDataAlteracao = tabela.Columns[CampoDataAlteracao];
                };
                if (colunaDataInclusao == null)
                {
                    CampoDataInclusao = Prefixo + "DATAINCLUSAO";
                    colunaDataInclusao = tabela.Columns[CampoDataInclusao];
                };

                if (tableAdapter == null)
                {
                    bf = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

                    //bool Duplicado = false;

                    foreach (FieldInfo fi in (this.GetType()).GetFields(bf))
                    {
                        if ((fi.FieldType.BaseType == typeof(Component))
                            ||
                            (fi.FieldType.IsSubclassOf(typeof(VirDB.VirtualTableAdapter))))
                        {

                            MethodInfo metodoFill = fi.FieldType.GetMethod("Fill");
                            if (metodoFill != null)
                            {

                                Component candidato = (Component)(fi.GetValue(this));

                                string nome = fi.Name.ToUpper();

                                if ((tableAdapter == null) || (nome.IndexOf(bindingSourcePrincipal.DataMember.ToUpper() + "TABLEADAPTER") == 0))
                                {
                                    tableAdapter = (Component)(fi.GetValue(this));

                                    //this.Cursor = Cursors.AppStarting;


                                    ParameterInfo[] pars1 = metodoFill.GetParameters();
                                    if ((pars1.GetLength(0) == 2) && (pars1[1].ParameterType == typeof(Int32)))
                                    {

                                        if ((tableAdapterSelect == null) || (nome.IndexOf(bindingSourcePrincipal.DataMember.ToUpper() + "TABLEADAPTER") == 0))
                                        {
                                            tableAdapterSelect = (Component)(fi.GetValue(this));
                                            metodoFillBy = metodoFill;
                                        }
                                    }
                                    else
                                        if (pars1.GetLength(0) == 1)
                                            metodoFill_F = metodoFill;

                                };


                                if (tableAdapter == null)
                                    tableAdapter = (Component)(fi.GetValue(this));
                                
                                Type[] Tipos = new Type[1];

                                Tipos[0] = tabela.GetType();
                                
                                //Pocotizado - Verificar
                                if (metodoupdate == null)
                                    metodoupdate = fi.FieldType.GetMethod("Update", Tipos);


                            };


                            metodoFill = fi.FieldType.GetMethod("FillBy");
                            if (metodoFill != null)
                            {

                                ParameterInfo[] pars = metodoFill.GetParameters();
                                if ((pars.GetLength(0) == 2) && (pars[1].ParameterType == typeof(Int32)))
                                {
                                    metodoFillBy = metodoFill;
                                    tableAdapterSelect = (Component)(fi.GetValue(this));
                                };

                                if (tableAdapter == null)
                                    tableAdapter = (Component)(fi.GetValue(this));

                            };

                        };
                    };
                }
                else
                {

                    if ((metodoFillBy == null) && !TravadoParaEvitarBug)
                    {
                        
                        foreach (string Tentativas in new string[] { "Fill", "FillBy" })
                        {
                            if (metodoFillBy == null)
                            {
                                MethodInfo metodoFill = tableAdapter.GetType().GetMethod(Tentativas);

                                if (metodoFill != null)
                                {

                                    string nome = tableAdapter.GetType().Name.ToUpper();

                                    ParameterInfo[] pars1 = metodoFill.GetParameters();
                                    if ((pars1.GetLength(0) == 2) && (pars1[1].ParameterType == typeof(Int32)))
                                    {

                                        if ((tableAdapterSelect == null) || (nome.IndexOf(bindingSourcePrincipal.DataMember.ToUpper() + "TABLEADAPTER") == 0))
                                        {
                                            tableAdapterSelect = tableAdapter;
                                            metodoFillBy = metodoFill;
                                        }
                                    }
                                    else
                                        if (pars1.GetLength(0) == 1)
                                            metodoFill_F = metodoFill;



                                    Type[] Tipos = new Type[] { tabela.GetType() };

                                    //Pocotizado - Verificar
                                    if (metodoupdate == null)
                                        metodoupdate = tableAdapter.GetType().GetMethod("Update", Tipos);
                                };
                            };
                        };
                    };
                    
                };
            };
        }


        /// <summary>
        /// Verifica se todos os campos obrigat�rios est�o preenchidos
        /// </summary>
        /// <param name="BS">BindingSource a verificar</param>
        /// <returns>Retorna se os campos obrigat�rios est�o preenchidos</returns>
        public bool CamposObrigatoriosOk(BindingSource BS)
        {
            if ((BS == null) || (BS.Current == null))
                return true;
            DataRow DR;
            if (BS.Current is DataRowView)
                DR = ((DataRowView)BS.Current).Row;
            else if (BS.Current is DataRow)
                DR = (DataRow)BS.Current;
            else
                throw new InvalidCastException("O BindingSource.Current � do tipo " + BS.Current.GetType().ToString());
                //DataRowView DR = (DataRowView)BS.Current;
            foreach (DataColumn Coluna in CamposObrigatorios)
                {
                    
                    {
                        if (DR[Coluna.ColumnName] == null)
                            return true;
                        if ((DR[Coluna.ColumnName] == DBNull.Value)
                            ||
                            (DR[Coluna.ColumnName].ToString() == "")
                           )
                        {                            
                            return false;
                        }
                    }
                }
            
            return true;
        }
                

                
            /*
            if (BS == null)
                BS = BindingSource_F;
            while (BS.DataSource is BindingSource)
                BS = (BindingSource)(BS.DataSource);
            if ((BS == null) || !(BS.DataSource is DataSet))
                return true;
            DataSet DS = (DataSet)BS.DataSource;

            DataTable Tabela = DS.Tables[BS.DataMember];
            DataRowView DR = (DataRowView)BS.Current;
            foreach (DataColumn Coluna in Tabela.Columns)
            {
                if (
                    (!Coluna.AutoIncrement)
                     &&
                    (!Coluna.AllowDBNull)
                   )
                {
                    if ((DR[Coluna.ColumnName] == DBNull.Value)
                        ||
                        (DR[Coluna.ColumnName].ToString() == "")
                       )
                        return false;
                }
                
            };  */
        //    return true;
        /// <summary>
        /// 
        /// </summary>
        public object ParametroMetodoFieldBy = null;

        /// <summary>
        /// Efetua a carga autom�tica dos campos
        /// </summary>
        /// <remarks>S� ir� operar no caso a booleana fillautomatico estja setada</remarks>
        protected virtual void FillAutomatico()
        {


            
            if (autofill && (metodoFill_F != null))
            {
                    object[] Parametros = new Object[1];
                    Parametros[0] = tabela;
                    metodoFill_F.Invoke(tableAdapter, Parametros);
                    Ativado = true;
            }
            else{
                if (autofill && (metodoFillBy != null) && (ParametroMetodoFieldBy != null))
                {
                    object[] Parametros = new Object[2];
                    Parametros[0] = tabela;
                    Parametros[1] = ParametroMetodoFieldBy;
                    metodoFillBy.Invoke(tableAdapter, Parametros);
                    Ativado = true;
                }
            };
        }

        /// <summary>
        /// Load do Formul�rio
        /// </summary>
        /// <remarks>Este evento dispara o configura��o inicial e os 3 eventos de carga dos dados nesta ordem</remarks>
        private void ComponenteBaseBindingSource_Load(object sender, EventArgs e)
        {
            ConfiguracaoInicial();
        }

        /// <summary>
        /// 
        /// </summary>
        public void AjustaInclusaiAlteracao() {
            if ((colunaInclusao != null) && (colunaAlteracao != null) && (colunaDataInclusao != null) && (colunaDataAlteracao != null))
            {
                foreach (DataRow row in tabela.Rows)
                    switch (row.RowState)
                    {
                        case DataRowState.Added:
                            row[colunaInclusao] = Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                            row[colunaAlteracao] = Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                            row[colunaDataInclusao] = DateTime.Now;
                            row[colunaDataAlteracao] = DateTime.Now;
                            break;
                        case DataRowState.Modified:
                            row[colunaAlteracao] = Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
                            row[colunaDataAlteracao] = DateTime.Now;
                            break;
                    };

            };
        }

        /// <summary>
        /// Procedimento basico de cancelamente - Fecha o formul�rio
        /// </summary>
        /// <param name="TesteCanClose">Define se a funcao CanClose deve ser consultada</param>
        /// <returns>se fechou</returns>
        public virtual bool Cancela_F(bool TesteCanClose) {
            if (TesteCanClose && !CanClose())
                return false;
            if (BindingSourcePrincipal.DataSource is DataRow)
            {
                BindingSourcePrincipal.EndEdit();
                if (!VirDB.VirtualTableAdapter.EfetivamenteAlterados(BindingSourcePrincipal.DataSource as DataRow))
                {
                    FechaTela(DialogResult.Cancel);
                    return true;
                }
            }
            if (DialogResult.Yes == MessageBox.Show("Confirma o cancelamento?", "Cancelar", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                BindingSourcePrincipal.CancelEdit();

                if (BindingSourcePrincipal.DataSource is DataSet)
                    (BindingSourcePrincipal.DataSource as DataSet).RejectChanges();
                if (BindingSourcePrincipal.DataSource is DataRow)
                    (BindingSourcePrincipal.DataSource as DataRow).RejectChanges();

                FechaTela(DialogResult.Cancel);
            }



            
            //FechaTela(DialogResult.Cancel);
            return true;            
        }

        /// <summary>
        /// Esta fun��o faz a grava��o efetiva no banco
        /// </summary>
        /// <remarks>Pode ser regravada (overhide)</remarks>
        public virtual bool Update_F()
        {
            if (!this.Validate(true))
                return false;

            
            if (!CamposObrigatoriosOk(this.bindingSourcePrincipal))
            {
                MessageBox.Show("Campo obrigat�rio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            };
            

            if (DialogResult.Yes == MessageBox.Show("Confirma a grava��o?.", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                try
                {
                    if (!this.Validate(true))
                        return false;
                    this.BindingSourcePrincipal.EndEdit();

                    if (this.BindingSourcePrincipal.DataSource is DataSet)
                    {
                        DataSet DS = (DataSet)this.BindingSourcePrincipal.DataSource;
                        if (DS.HasChanges())
                        {
                            AjustaInclusaiAlteracao(); 
                                                                                

                            if ((tableAdapter != null) && (metodoupdate != null))
                            {
                                Object[] Obj = new Object[1];
                                Obj[0] = tabela;
                                //          try
                                //          {
                                metodoupdate.Invoke(tableAdapter, Obj);
                                tabela.AcceptChanges();
                                if (pk == 0)
                                {
                                    if (LinhaMae_F != null)
                                        try
                                        {
                                            pk = (int)LinhaMae_F[0];
                                        }
                                        catch { }

                                    else

                                        if ((tabela != null) && (tabela.Rows.Count == 1))
                                            try
                                            {
                                                pk = (int)tabela.Rows[0][0];
                                            }
                                            catch { }
                                };
                                
                                //(this.BindingSourcePrincipal.DataSource as DataSet).AcceptChanges();
                                //          }
                                //          catch (Exception Erro)
                                //          {
                                //              MessageBox.Show(Erro.InnerException.Message);
                                //          }
                            };


                        }
                    };
                    return true;
                }

                catch (TargetInvocationException Erro)
                {
                    if (Erro.InnerException is NoNullAllowedException)

                        MessageBox.Show("Existe campo obrig�torio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);


                    else
                    {
                        throw Erro;
                        /*
                        string men = Erro.Message;
                        if (Erro.InnerException != null)
                            men += " - " + Erro.InnerException.Message;
                        MessageBox.Show("Ocorreu uma excess�o desconhecida ao tentar efetuar a opera��o:\r\n\r\n" + Erro.Message.ToUpper() + "\r\n\r\n" +
                                 "Entre em contato com o suporte da Virtual Software e informe sobre o problema.\r\n\r\n"+men, "Excess�o Desconhecida", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        */ 
                    }

                    return false;
                }
                    
                catch (Exception Erro)
                {
                    if (Erro is NoNullAllowedException)
                        MessageBox.Show("Existe campo obrig�torio sem um valor informado.", "Campo Obrigat�rio", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    else
                        if ((Erro is ConstraintException) && (Erro.Message.ToUpper().IndexOf("UNIQUE") >= 0))
                            MessageBox.Show("Existe campo de valor �nico com valor duplicado.", "Campo de Valor �nico", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                        else
                            MessageBox.Show("Ocorreu uma excess�o desconhecida ao tentar efetuar a opera��o:\r\n\r\n" + Erro.Message.ToUpper() + "\r\n\r\n" +
                                            "Entre em contato com o suporte da Virtual Software e informe sobre o problema.", "Excess�o Desconhecida", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    return false;
                }
                
            }
            else
                return false;
        }

        /// <summary>
        /// Substitui todas as referencias do DataSet local (apontado pelo BindingSource_F) pelo externo
        /// </summary>
        public void SubstituaDataSet(DataSet DataSetExterno)
        {
            BindingFlags bf;
            bf = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            foreach (FieldInfo fi in (this.GetType()).GetFields(bf))
            {
                if (fi.FieldType == DataSetExterno.GetType())
                {
                    DataSet DSasersubstituido = (DataSet)fi.GetValue(this);
                    foreach (FieldInfo fi1 in (this.GetType()).GetFields(bf))
                        if (fi1.FieldType == typeof(BindingSource))
                        {
                            BindingSource Bin = (BindingSource)fi1.GetValue(this);
                            if (Bin.DataSource == DSasersubstituido) {
                                Bin.DataSource = DataSetExterno;
                            }
                        };
                };
                            
            }
        }



        /// <summary>
        /// Re-faz a carga dos dados
        /// </summary>        
        public override void RefreshDados()
        {
            object opk = null;
            if (!(this is ComponenteGradeBase))
                return;
            if(Ativado && (tabela != null)){                
                DataRowView DRV = (DataRowView)bindingSourcePrincipal.Current;
                if (DRV != null){
                    if(DRV.Row.RowState != DataRowState.Unchanged)
                        return;
                    opk = DRV[0];
                    if (opk is int)
                        pk = (int)opk;
                };
                tabela.Clear();
                FillAutomatico();
                if (opk != null)
                    bindingSourcePrincipal.Position = bindingSourcePrincipal.Find(tabela.Columns[0].ColumnName, opk);
            }
        }

        
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TipoDeCarga { 
        /// <summary>
        /// 
        /// </summary>
        navegacao,
        /// <summary>
        /// 
        /// </summary>
        novo,
        /// <summary>
        /// 
        /// </summary>
        limpo,
        /// <summary>
        /// 
        /// </summary>
        pk
    }

}

