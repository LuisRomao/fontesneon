namespace CompontesBasicos
{
    partial class ComponenteBaseDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComponenteBaseDialog));
            this.BarManager_F = new DevExpress.XtraBars.BarManager(this.components);
            this.barComponente_F = new DevExpress.XtraBars.Bar();
            this.btnConfirmar_F = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelar_F = new DevExpress.XtraBars.BarButtonItem();
            this.btnFechar_F = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.stxUsuarioInclusao_F = new DevExpress.XtraBars.BarStaticItem();
            this.stxUsuarioAtualizacao_F = new DevExpress.XtraBars.BarStaticItem();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.AllowCustomization = false;
            this.BarManager_F.AllowMoveBarOnToolbar = false;
            this.BarManager_F.AllowQuickCustomization = false;
            this.BarManager_F.AllowShowToolbarsPopup = false;
            this.BarManager_F.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barComponente_F});
            this.BarManager_F.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Status", new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36")),
            new DevExpress.XtraBars.BarManagerCategory("Componente", new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5"))});
            this.BarManager_F.DockControls.Add(this.barDockControlTop);
            this.BarManager_F.DockControls.Add(this.barDockControlBottom);
            this.BarManager_F.DockControls.Add(this.barDockControlLeft);
            this.BarManager_F.DockControls.Add(this.barDockControlRight);
            this.BarManager_F.Form = this;
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.stxUsuarioInclusao_F,
            this.stxUsuarioAtualizacao_F,
            this.btnConfirmar_F,
            this.btnCancelar_F,
            this.btnFechar_F});
            this.BarManager_F.MaxItemId = 9;
            // 
            // barComponente_F
            // 
            this.barComponente_F.BarName = "Componente";
            this.barComponente_F.DockCol = 0;
            this.barComponente_F.DockRow = 0;
            this.barComponente_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barComponente_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConfirmar_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCancelar_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnFechar_F)});
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            this.barComponente_F.Text = "Componente";
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.Caption = "C&onfirmar";
            this.btnConfirmar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnConfirmar_F.Hint = "Confirma as altera��es feitas no registro para o banco de dados.";
            this.btnConfirmar_F.Id = 6;
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            this.btnConfirmar_F.Name = "btnConfirmar_F";
            this.btnConfirmar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnConfirmar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfirmar_F_ItemClick);
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.Caption = "C&ancelar";
            this.btnCancelar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnCancelar_F.Hint = "Cancela as altera��es feitas no registro.";
            this.btnCancelar_F.Id = 7;
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            this.btnCancelar_F.Name = "btnCancelar_F";
            this.btnCancelar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCancelar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_F_ItemClick);
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Caption = "&Fechar";
            this.btnFechar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnFechar_F.Hint = "Fechar";
            this.btnFechar_F.Id = 8;
            this.btnFechar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.ImageOptions.Image")));
            this.btnFechar_F.Name = "btnFechar_F";
            this.btnFechar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnFechar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFechar_F_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.BarManager_F;
            this.barDockControlTop.Size = new System.Drawing.Size(570, 35);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 291);
            this.barDockControlBottom.Manager = this.BarManager_F;
            this.barDockControlBottom.Size = new System.Drawing.Size(570, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 35);
            this.barDockControlLeft.Manager = this.BarManager_F;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 256);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(570, 35);
            this.barDockControlRight.Manager = this.BarManager_F;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 256);
            // 
            // stxUsuarioInclusao_F
            // 
            this.stxUsuarioInclusao_F.Caption = "Data/Usu�rio Inclus�o";
            this.stxUsuarioInclusao_F.CategoryGuid = new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36");
            this.stxUsuarioInclusao_F.Id = 0;
            this.stxUsuarioInclusao_F.Name = "stxUsuarioInclusao_F";
            this.stxUsuarioInclusao_F.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // stxUsuarioAtualizacao_F
            // 
            this.stxUsuarioAtualizacao_F.Caption = "Data/Usu�rio Atualiza��o";
            this.stxUsuarioAtualizacao_F.CategoryGuid = new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36");
            this.stxUsuarioAtualizacao_F.Id = 2;
            this.stxUsuarioAtualizacao_F.Name = "stxUsuarioAtualizacao_F";
            this.stxUsuarioAtualizacao_F.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ComponenteBaseDialog
            // 
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteBaseDialog";
            this.Load += new System.EventHandler(this.ComponenteBaseDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarManager BarManager_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarStaticItem stxUsuarioInclusao_F;
        private DevExpress.XtraBars.BarStaticItem stxUsuarioAtualizacao_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.Bar barComponente_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnConfirmar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnCancelar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnFechar_F;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
    }
}
