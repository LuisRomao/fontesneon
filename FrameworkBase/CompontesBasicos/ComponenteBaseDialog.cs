using DevExpress.XtraBars;
using System.Windows.Forms;


namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponenteBaseDialog : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteBaseDialog()
        {
            InitializeComponent();
            Left0 = Top0 = 0;
        }

        private void btnCancelar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }

        private void btnConfirmar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (CanClose())
                FechaTela(DialogResult.OK);
        }

        private void ComponenteBaseDialog_Load(object sender, System.EventArgs e)
        {
            btnCancelar_F.Visibility = btnConfirmar_F.Visibility = (somenteleitura ? BarItemVisibility.Never : BarItemVisibility.Always);
            btnFechar_F.Visibility = (somenteleitura ? BarItemVisibility.Always : BarItemVisibility.Never);
        }

        /// <summary>
        /// Oculta/Mostra os bot�es na parte superior da tela
        /// </summary>
        /// <param name="visivel"></param>
        public void BotoesOKCancel(bool visivel)
        {
            barComponente_F.Visible = visivel;
        }

        private void btnFechar_F_ItemClick(object sender, ItemClickEventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }
    }
}

