using System;
using System.Drawing;
using System.Windows.Forms;

namespace CompontesBasicos.Espera
{
    /// <summary>
    /// Tela amarela indicando espera
    /// </summary>
    public partial class cEspera : CompontesBasicos.ComponenteBase
    {
        private DateTime ProximaMostra;

        /// <summary>
        /// Avan�a o gauge somente se deu a carencia de 3 segungos
        /// </summary>
        /// <param name="Posicao"></param>
        /// <returns>Retorna se � para cancelar</returns>
        public bool GaugeTempo(int? Posicao=null)
        {
            if (ProximaMostra < DateTime.Now)
            {
                if (Posicao.HasValue)
                    Gauge(Posicao.Value);
                else
                    Gauge();
                ProximaMostra = DateTime.Now.AddSeconds(3);             
            }
            return Abortar;
        }

        /// <summary>
        /// Solicita��o de Abortagem
        /// </summary>
        public bool Abortar = false;

        /// <summary>
        /// 
        /// </summary>
        public bool GerarException = false;

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public cEspera()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="Pai">Janela chamadora</param>
        /// <param name="Modelo2"></param>
        public cEspera(System.Windows.Forms.Control Pai, bool Modelo2 = false)
        {
            InitializeComponent();
            if (Pai != null)
            {
                Parent = Pai;
                this.BringToFront();
                // this.Parent = this.Parent;
                int bordaW = (Pai.Width - this.Width) / 2;
                int bordaH = (Pai.Height - this.Height) / 2;
                this.Location = new Point(bordaW, bordaH);
                Parent.Invalidate(true);

            }
            TrocaModelo(Modelo2);            
        }

        /// <summary>
        /// Troca o modelo
        /// </summary>
        /// <param name="Modelo2">Modelo 2 � para WS</param>
        public void TrocaModelo(bool Modelo2)
        {
            pictureEdit1.Visible = !Modelo2;
            pictureEdit2.Visible = Modelo2;
            BackColor = Modelo2 ? Color.LavenderBlush : Color.Gold;
        }

        /// <summary>
        /// Inicializa a tela de espera
        /// </summary>
        /// <param name="_Motivo">Titulo</param>
        public void Espere(string _Motivo) {
            Motivo.Text=_Motivo;
            Visible = true;
            progressBarControl1.Visible = false;
            Abortar = false;
            Application.DoEvents();
        }

        /// <summary>
        /// Mostra o gauge
        /// </summary>
        /// <param name="max">m�ximo do gauge</param>
        public void AtivaGauge(Int32 max)
        {
                progressBarControl1.EditValue = 0;
                progressBarControl1.Properties.Maximum = max;
                progressBarControl1.EditValue = 0;
                progressBarControl1.Visible = true;
                Abortar = false;    
                Application.DoEvents();
        }

        /// <summary>
        /// Ajusta progress�o do gauge
        /// </summary>
        /// <param name="Posicao">nova posicao</param>
        public void Gauge(Int32 Posicao)
        {
                progressBarControl1.EditValue = Posicao;
                progressBarControl1.Visible = true;
                Application.DoEvents();
        }

        /// <summary>
        /// Avan�a o gauge
        /// </summary>
        public void Gauge()
        {
                progressBarControl1.EditValue = 1 + (Int32)progressBarControl1.EditValue;
                Application.DoEvents();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o  cancelamento", "CANCELAR", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Abortar = true;
                if (GerarException)
                    throw new EsperaException();
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EsperaException : Exception
    {
        /// <summary>
        /// 
        /// </summary>
        public EsperaException()
            : base("CANCELADO")
        { 
        }
    }
}

