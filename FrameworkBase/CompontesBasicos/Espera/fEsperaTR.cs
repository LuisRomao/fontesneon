using System;

namespace CompontesBasicos.Espera
{
    /// <summary>
    /// 
    /// </summary>
    public partial class fEsperaTR : CompontesBasicos.FormBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected int posicao;
        /// <summary>
        /// 
        /// </summary>
        protected string Obs = "";
        /// <summary>
        /// 
        /// </summary>
        public fEsperaTR()
        {
            InitializeComponent();
            labelControl1.Text = "";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (posicao == -1)
                this.Close();
            else
            {
                progressBarControl1.Position = posicao;
                labelControl1.Text = Obs;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="max"></param>
        public void Ativa(int max)
        {
            progressBarControl1.Position = posicao  = 0;
            progressBarControl1.Properties.Maximum = max;            
            System.Threading.Thread ThreadCarga = new System.Threading.Thread(new System.Threading.ThreadStart(ProcedimentoPrincipal));
            ThreadCarga.Start();
            Show();
            timer1.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void ProcedimentoPrincipal(){
            throw new Exception("Chamada de classe abstrata: fEsperaTR");
        }
    }
}

