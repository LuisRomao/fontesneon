using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace CompontesBasicos.Bancovirtual
{
    /// <summary>
    /// Tipos de Banco de dados
    /// </summary>
    public enum TiposDeBanco
    {
        Access,
        SQL,
        Internet        
    }



    /// <summary>
    /// Objeto que representa os bancos de dados dispon�ves
    /// </summary>
    /// <remarks>� usado na classe virtual 'BancoVirtual'</remarks>
    public class Bancos : Object
    {
        public string Nome;
        public string StringDeConexao;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Nome">Nome do banco na lista de escolha</param>
        /// <param name="StringDeConexao">String de conexao</param>
        public Bancos(string Nome, string StringDeConexao)
        {
            this.Nome = Nome;
            this.StringDeConexao = StringDeConexao;
        }

        /// <summary>
        /// Sobregava o m�todo
        /// </summary>
        /// <remarks>Necess�rio para ser usado para popular listas e combos</remarks>
        /// <returns>Nome do banco</returns>
        public override string ToString()
        {
            return Nome;
        }
    }


    /// <summary>
    /// Represenata qual o banco de dados ser� usado
    /// </summary>
    public static class BancoVirtual
    {
        /// <summary>
        /// Lista dos possiveis bancos
        /// </summary>
        public static ArrayList Strings;

        /// <summary>
        /// Lista dos possiveis bancos Access
        /// </summary>
        public static ArrayList StringsAccess;

        /// <summary>
        /// Lista dos possiveis bancos Internet
        /// </summary>
        public static ArrayList StringsInternet;

        /// <summary>
        /// String de conexao escolhido
        /// </summary>
        public static int StringEscolhido=-1;

        /// <summary>
        /// String de conexao escolhido Access
        /// </summary>
        public static int StringEscolhidoAccess = -1;

        /// <summary>
        /// String de conexao escolhido Internet
        /// </summary>
        public static int StringEscolhidoInternet = -1;

        
        
        /// <summary>
        /// Retorna o string escolhido
        /// </summary>
        public static string StringConeFinal(TiposDeBanco Tipo)
        {
            String Retorno = "";
            switch (Tipo)
            {
                case TiposDeBanco.SQL:
                    if (StringEscolhido == -1)
                        EscolherString();
                    if (StringEscolhido != -1)
                        Retorno = ((Bancos)Strings[StringEscolhido]).StringDeConexao;
                    break;
                case TiposDeBanco.Internet:
                    if (StringEscolhidoInternet == -1)
                        EscolherStringInternet();
                    if (StringEscolhidoInternet != -1)
                        Retorno = ((Bancos)StringsInternet[StringEscolhidoInternet]).StringDeConexao;
                    break;
                case TiposDeBanco.Access:
                    if (StringEscolhidoAccess == -1)
                        EscolherStringAccess();
                    if (StringEscolhidoAccess != -1)
                        Retorno = ((Bancos)StringsAccess[StringEscolhidoAccess]).StringDeConexao;
                    break;
            };
            return Retorno;
        }

        

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco) - gen�rico
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig</returns>
        private static int EscolherString(ArrayList Lista)
        {
            if (Lista == null)
                return -1;
            else                
                    if (Lista.Count == 1)
                        return 0;
                    else
                        using (SelStringDeConexao form = new SelStringDeConexao())
                        {
                            form.Selecao.Properties.Items.AddRange(Lista);
                            form.ShowDialog();
                            return form.Selecao.SelectedIndex;
                        };            

        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig</returns>
        public static int EscolherString()
        {
            if (StringEscolhido == -1){
                StringEscolhido = EscolherString(Strings);
            };             
            return StringEscolhido;                        
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco Access)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig Access</returns>
        public static int EscolherStringAccess()
        {
            if (StringEscolhidoAccess == -1)
            {
                StringEscolhidoAccess = EscolherString(StringsAccess);
            };
            return StringEscolhidoAccess;                        
        }

        /// <summary>
        /// Metodo chamado para que o usu�rio escolha um string (banco Internet)
        /// </summary>
        /// <remarks>Se s� houver um na lista a janela nao � aberta. (Caso do uso em prodo��o)</remarks>
        /// <returns>indice do strig Internet</returns>
        public static int EscolherStringInternet()
        {
            if (StringEscolhidoInternet == -1)
            {
                StringEscolhidoInternet = EscolherString(StringsInternet);
            };
            return StringEscolhidoInternet;
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos)
        /// </summary>
        public static void Popular(string Nome, string StringDeConexao)
        {
            if (Strings == null)
            {
                Strings = new ArrayList();
                StringEscolhido = -1;
            };
            Strings.Add(new Bancos(Nome,StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos Access)
        /// </summary>
        public static void PopularAcces(string Nome, string StringDeConexao)
        {
            if (StringsAccess == null)
            {
                StringsAccess = new ArrayList();
                StringEscolhidoAccess = -1;
            };
            StringsAccess.Add(new Bancos(Nome, StringDeConexao));
        }

        /// <summary>
        /// Prepara a lista dos possiveis strings (Bancos Access)
        /// </summary>
        public static void PopularInternet(string Nome, string StringDeConexao)
        {
            if (StringsInternet == null)
            {
                StringsInternet = new ArrayList();
                StringEscolhidoInternet = -1;
            };
            StringsInternet.Add(new Bancos(Nome, StringDeConexao));
        }
    }

    


}
