using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using VirDB.Bancovirtual;

namespace CompontesBasicos.Bancovirtual {
    /// <summary>
    /// Dados do usu�rio logado
    /// </summary>
    /// <remarks>Classe abstrata que deve ser derivada para buscar as informa��es do cadastro de usu�rios</remarks>
    public abstract class UsuarioLogado
    {
        /// <summary>
        /// Construtor
        /// </summary>
        protected UsuarioLogado()
        {
            HoraLogin = DateTime.Now;
        }

        /// <summary>
        /// Instancia est�tica
        /// </summary>
        /// <remarks>Substitui as vari�veis est�ticas</remarks>
        private static UsuarioLogado _UsuarioLogadoSt;

        /// <summary>
        /// Instancia est�tica
        /// </summary>
        /// <remarks>Substitui as vari�veis est�ticas</remarks>
        public static UsuarioLogado UsuarioLogadoSt {
            get { return _UsuarioLogadoSt; }
            set { _UsuarioLogadoSt = value; }
        }

        /// <summary>
        /// Impressora que este usu�rio usa para boletos
        /// </summary>
        public static string ImpressoraBoletos;

        /// <summary>
        /// Usu em programa��o
        /// </summary>
        protected static bool desenvolvimento = false;

        /// <summary>
        /// Seta variavel de desenvolvimento
        /// </summary>
        public static string Desenvolvimento
        {            
            set { 
                if (value == "AZUL FLORESTA")
                    UsuarioLogado.desenvolvimento = true; 
            }
        }


        #region Propriedades e m�todos Abstratos

        /// <summary>
        /// Codigo do usu�rio logado
        /// </summary>
        public abstract int USU { get;}

        /// <summary>
        /// Nome do usu�rio logado
        /// </summary>
        public abstract string USUNome { get;}

        /// <summary>
        /// Verifica se o usuario logado possui acesso � funcionalidade
        /// </summary>
        /// <param name="Funcionalidade">Funcionalidade a pesquisar</param>
        /// <returns>Nivel de acesso</returns>
        public abstract int VerificaFuncionalidade(string Funcionalidade);

        /// <summary>
        /// Servidor SMTP
        /// </summary>
        public abstract string ServidorSMTP { get; }

        /// <summary>
        /// Remetente
        /// </summary>
        public abstract string EmailRemetente { get; }

        /// <summary>
        /// Credenciais para e.mail (pode ser null)
        /// </summary>
        public abstract System.Net.NetworkCredential EmailNetCredenciais { get; }

        /// <summary>
        /// Proxy (pode ser null)
        /// </summary>
        public abstract System.Net.WebProxy Proxy { get; }

        /// <summary>
        /// Fun��o que fornece o nome do Usu�rio
        /// </summary>
        /// <param name="USU">C�digo USU do usu�rio</param>
        public abstract string BuscaUSUNome(object USU);

        #endregion
        


        #region Campos de Estancia
        /// <summary>
        /// Hora do login
        /// </summary>
        public DateTime HoraLogin; 
        #endregion

                
        
        
    }
}