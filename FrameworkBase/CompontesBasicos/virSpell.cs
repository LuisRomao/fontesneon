﻿using System;
using System.Windows.Forms;

namespace CompontesBasicos
{
    /// <summary>
    /// Spell padrão
    /// </summary>
    public partial class virSpell : Control
    {
        private static virSpell stvirSpell;        

        /// <summary>
        /// stspellChecker estático
        /// </summary>
        public static DevExpress.XtraSpellChecker.SpellChecker StstspellChecker
        {
            get
            {
                if (stvirSpell == null)                
                    stvirSpell = new virSpell();                                    
                return stvirSpell.spellChecker1;
            }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public virSpell()
        {
            InitializeComponent();
        }

        private CompontesBasicos.Espera.cEspera cespera = null;

        private CompontesBasicos.Espera.cEspera Cespera
        {
            get
            {
                if (cespera == null)
                    cespera = new CompontesBasicos.Espera.cEspera();
                return cespera;
            }
        }

        private delegate void del_spellChecker1_BeforeLoadDictionaries(object sender, EventArgs e);

        private void spellChecker1_BeforeLoadDictionaries(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                del_spellChecker1_BeforeLoadDictionaries d = new del_spellChecker1_BeforeLoadDictionaries(spellChecker1_BeforeLoadDictionaries);
                Invoke(d, new object[] { sender, e });
            }
            else
            {
                //Cespera.Motivo.Text = "Carregando dicionário";
                Cespera.Espere("Carregando dicionário");
                //Application.DoEvents();
            }
        }

        private delegate void del_spellChecker1_AfterLoadDictionaries(object sender, EventArgs e);

        private void spellChecker1_AfterLoadDictionaries(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                del_spellChecker1_AfterLoadDictionaries d = new del_spellChecker1_AfterLoadDictionaries(spellChecker1_AfterLoadDictionaries);
                Invoke(d, new object[] { sender, e });
            }
            else
                if (cespera != null)
                {
                    cespera.Visible = false;
                    cespera.Dispose();
                    cespera = null;
                }
        }
    }




}
