namespace CompontesBasicos
{
    partial class ComponenteGradeNavegador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComponenteGradeNavegador));
            this.BarManager_F = new DevExpress.XtraBars.BarManager(this.components);
            this.barNavegacao_F = new DevExpress.XtraBars.Bar();
            this.BtnIncluir_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnAlterar_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnImprimir_F = new DevExpress.XtraBars.BarButtonItem();
            this.PopImprimir_F = new DevExpress.XtraBars.PopupMenu(this.components);
            this.BtnImprimirGrade_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnExportar_F = new DevExpress.XtraBars.BarButtonItem();
            this.PopExportar_F = new DevExpress.XtraBars.PopupMenu(this.components);
            this.BtnExportarHTML_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnExportarXML_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnExportarXLS_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnExportarTXT_F = new DevExpress.XtraBars.BarButtonItem();
            this.BtnExcluir_F = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonAtualizar = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.btnVisualizar_FPai = new DevExpress.XtraBars.BarButtonItem();
            this.barDataBase_F = new DevExpress.XtraBars.Bar();
            this.btnConfirmar_F = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelar_F = new DevExpress.XtraBars.BarButtonItem();
            this.BarAndDockingController_F = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.btnVisualizar_F = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Size = new System.Drawing.Size(1511, 747);
            this.GridControl_F.DoubleClick += new System.EventHandler(this.GridControl_F_DoubleClick);
            // 
            // GridView_F
            // 
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowFooter = true;
            this.GridView_F.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.GridView_F_InvalidRowException);
            this.GridView_F.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.GridView_F_ValidateRow);
            this.GridView_F.RowCountChanged += new System.EventHandler(this.GridView_F_RowCountChanged);
            // 
            // BindingSource_F
            // 
            this.BindingSource_F.AddingNew += new System.ComponentModel.AddingNewEventHandler(this.BindingSource_F_AddingNew);
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.AllowMoveBarOnToolbar = false;
            this.BarManager_F.AutoSaveInRegistry = true;
            this.BarManager_F.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barNavegacao_F,
            this.barDataBase_F});
            this.BarManager_F.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Grade_F", new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05")),
            new DevExpress.XtraBars.BarManagerCategory("Imprimir_F", new System.Guid("74469e40-4a35-4534-a884-d06c1ef3a9d3")),
            new DevExpress.XtraBars.BarManagerCategory("Exportar_F", new System.Guid("680ca792-cfeb-41e8-9650-426d8eabfc8d")),
            new DevExpress.XtraBars.BarManagerCategory("Database_F", new System.Guid("8bd9c9ec-4ea7-4ec8-b15c-a110bacdb4b5"))});
            this.BarManager_F.Controller = this.BarAndDockingController_F;
            this.BarManager_F.DockControls.Add(this.barDockControlTop);
            this.BarManager_F.DockControls.Add(this.barDockControlBottom);
            this.BarManager_F.DockControls.Add(this.barDockControlLeft);
            this.BarManager_F.DockControls.Add(this.barDockControlRight);
            this.BarManager_F.Form = this;
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.BtnIncluir_F,
            this.BtnAlterar_F,
            this.BtnExcluir_F,
            this.BtnImprimir_F,
            this.BtnImprimirGrade_F,
            this.BtnExportar_F,
            this.BtnExportarHTML_F,
            this.BtnExportarXML_F,
            this.BtnExportarXLS_F,
            this.BtnExportarTXT_F,
            this.btnConfirmar_F,
            this.btnCancelar_F,
            this.barButtonAtualizar,
            this.barButtonItem1,
            this.barSubItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.btnVisualizar_FPai});
            this.BarManager_F.MaxItemId = 24;
            this.BarManager_F.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.BarManager_F.StatusBar = this.barNavegacao_F;
            this.BarManager_F.ToolTipController = this.ToolTipController_F;
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.BarName = "Navegacao_F";
            this.barNavegacao_F.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barNavegacao_F.DockCol = 0;
            this.barNavegacao_F.DockRow = 0;
            this.barNavegacao_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnIncluir_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnAlterar_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnImprimir_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExportar_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExcluir_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonAtualizar),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnVisualizar_FPai)});
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            this.barNavegacao_F.Text = "Navega��o";
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.Caption = "Incluir";
            this.BtnIncluir_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.BtnIncluir_F.Hint = "Incluir";
            this.BtnIncluir_F.Id = 3;
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            this.BtnIncluir_F.Name = "BtnIncluir_F";
            this.BtnIncluir_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.BtnIncluir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnIncluir_F_ItemClick);
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.Caption = "Alterar";
            this.BtnAlterar_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.BtnAlterar_F.Hint = "Alterar";
            this.BtnAlterar_F.Id = 4;
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            this.BtnAlterar_F.Name = "BtnAlterar_F";
            this.BtnAlterar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.BtnAlterar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnAlterar_F_ItemClick);
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.BtnImprimir_F.Caption = "Imprimir";
            this.BtnImprimir_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.BtnImprimir_F.DropDownControl = this.PopImprimir_F;
            this.BtnImprimir_F.Hint = "Imprimir";
            this.BtnImprimir_F.Id = 6;
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            this.BtnImprimir_F.Name = "BtnImprimir_F";
            this.BtnImprimir_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.BtnImprimir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnImprimirGrade_F_ItemClick);
            // 
            // PopImprimir_F
            // 
            this.PopImprimir_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnImprimirGrade_F)});
            this.PopImprimir_F.Manager = this.BarManager_F;
            this.PopImprimir_F.Name = "PopImprimir_F";
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.Caption = "Grade";
            this.BtnImprimirGrade_F.CategoryGuid = new System.Guid("74469e40-4a35-4534-a884-d06c1ef3a9d3");
            this.BtnImprimirGrade_F.DropDownControl = this.PopImprimir_F;
            this.BtnImprimirGrade_F.Hint = "Imprime a Grade";
            this.BtnImprimirGrade_F.Id = 9;
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            this.BtnImprimirGrade_F.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7);
            this.BtnImprimirGrade_F.Name = "BtnImprimirGrade_F";
            this.BtnImprimirGrade_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnImprimirGrade_F_ItemClick);
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.BtnExportar_F.Caption = "Exportar";
            this.BtnExportar_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.BtnExportar_F.DropDownControl = this.PopExportar_F;
            this.BtnExportar_F.Hint = "Exportar para Arquivo";
            this.BtnExportar_F.Id = 10;
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            this.BtnExportar_F.Name = "BtnExportar_F";
            this.BtnExportar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.BtnExportar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportarHTML_F_ItemClick);
            // 
            // PopExportar_F
            // 
            this.PopExportar_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExportarHTML_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExportarXML_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExportarXLS_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.BtnExportarTXT_F)});
            this.PopExportar_F.Manager = this.BarManager_F;
            this.PopExportar_F.Name = "PopExportar_F";
            // 
            // BtnExportarHTML_F
            // 
            this.BtnExportarHTML_F.Caption = "Exportar para HTML...";
            this.BtnExportarHTML_F.CategoryGuid = new System.Guid("680ca792-cfeb-41e8-9650-426d8eabfc8d");
            this.BtnExportarHTML_F.Hint = "Gera um arquivo HTML com os registros da grade.";
            this.BtnExportarHTML_F.Id = 11;
            this.BtnExportarHTML_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportarHTML_F.ImageOptions.Image")));
            this.BtnExportarHTML_F.Name = "BtnExportarHTML_F";
            this.BtnExportarHTML_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportarHTML_F_ItemClick);
            // 
            // BtnExportarXML_F
            // 
            this.BtnExportarXML_F.Caption = "Exportar para XML...";
            this.BtnExportarXML_F.CategoryGuid = new System.Guid("680ca792-cfeb-41e8-9650-426d8eabfc8d");
            this.BtnExportarXML_F.Hint = "Gera um arquivo XML com os registros da grade.";
            this.BtnExportarXML_F.Id = 12;
            this.BtnExportarXML_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportarXML_F.ImageOptions.Image")));
            this.BtnExportarXML_F.Name = "BtnExportarXML_F";
            this.BtnExportarXML_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportarXML_F_ItemClick);
            // 
            // BtnExportarXLS_F
            // 
            this.BtnExportarXLS_F.Caption = "Exportar para XLS...";
            this.BtnExportarXLS_F.CategoryGuid = new System.Guid("680ca792-cfeb-41e8-9650-426d8eabfc8d");
            this.BtnExportarXLS_F.Hint = "Gera um arquivo XLS com os registros da grade.";
            this.BtnExportarXLS_F.Id = 13;
            this.BtnExportarXLS_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportarXLS_F.ImageOptions.Image")));
            this.BtnExportarXLS_F.Name = "BtnExportarXLS_F";
            this.BtnExportarXLS_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportarXLS_F_ItemClick);
            // 
            // BtnExportarTXT_F
            // 
            this.BtnExportarTXT_F.Caption = "Exportar para TXT...";
            this.BtnExportarTXT_F.CategoryGuid = new System.Guid("680ca792-cfeb-41e8-9650-426d8eabfc8d");
            this.BtnExportarTXT_F.Hint = "Gera um arquivo TXT com os registros da grade.";
            this.BtnExportarTXT_F.Id = 14;
            this.BtnExportarTXT_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportarTXT_F.ImageOptions.Image")));
            this.BtnExportarTXT_F.Name = "BtnExportarTXT_F";
            this.BtnExportarTXT_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExportarTXT_F_ItemClick);
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.Caption = "Excluir";
            this.BtnExcluir_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.BtnExcluir_F.Hint = "Excluir";
            this.BtnExcluir_F.Id = 5;
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            this.BtnExcluir_F.Name = "BtnExcluir_F";
            this.BtnExcluir_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.BtnExcluir_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnExcluir_F_ItemClick);
            // 
            // barButtonAtualizar
            // 
            this.barButtonAtualizar.Caption = "Atualizar";
            this.barButtonAtualizar.Id = 18;
            this.barButtonAtualizar.Name = "barButtonAtualizar";
            this.barButtonAtualizar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonItem1.Caption = "layout";
            this.barButtonItem1.DropDownControl = this.popupMenu1;
            this.barButtonItem1.Id = 19;
            this.barButtonItem1.ImageOptions.Image = global::CompontesBasicos.Properties.Resources.layout;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.popupMenu1.Manager = this.BarManager_F;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Salvar";
            this.barButtonItem2.Id = 21;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Restaurar original";
            this.barButtonItem3.Id = 22;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // btnVisualizar_FPai
            // 
            this.btnVisualizar_FPai.Caption = "&Visualizar";
            this.btnVisualizar_FPai.Id = 23;
            this.btnVisualizar_FPai.ImageOptions.Image = global::CompontesBasicos.Properties.Resources.btnVisualizar_F_Glyph;
            this.btnVisualizar_FPai.Name = "btnVisualizar_FPai";
            this.btnVisualizar_FPai.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnVisualizar_FPai.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnVisualizar_FPai_ItemClick);
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.BarName = "Database_F";
            this.barDataBase_F.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barDataBase_F.DockCol = 0;
            this.barDataBase_F.DockRow = 1;
            this.barDataBase_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barDataBase_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConfirmar_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCancelar_F)});
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            this.barDataBase_F.Text = "Banco de Dados";
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.Caption = "C&onfirmar";
            this.btnConfirmar_F.CategoryGuid = new System.Guid("8bd9c9ec-4ea7-4ec8-b15c-a110bacdb4b5");
            this.btnConfirmar_F.Id = 16;
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            this.btnConfirmar_F.Name = "btnConfirmar_F";
            this.btnConfirmar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnConfirmar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfirmar_F_ItemClick);
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.Caption = "C&ancelar";
            this.btnCancelar_F.CategoryGuid = new System.Guid("8bd9c9ec-4ea7-4ec8-b15c-a110bacdb4b5");
            this.btnCancelar_F.Id = 17;
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            this.btnCancelar_F.Name = "btnCancelar_F";
            this.btnCancelar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCancelar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_F_ItemClick);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            this.BarAndDockingController_F.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this.BarAndDockingController_F.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.BarManager_F;
            this.barDockControlTop.Size = new System.Drawing.Size(1511, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 747);
            this.barDockControlBottom.Manager = this.BarManager_F;
            this.barDockControlBottom.Size = new System.Drawing.Size(1511, 60);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Manager = this.BarManager_F;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 747);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1511, 0);
            this.barDockControlRight.Manager = this.BarManager_F;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 747);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "layout";
            this.barSubItem1.Id = 20;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // btnVisualizar_F
            // 
            this.btnVisualizar_F.Caption = "&Visualizar (F9)";
            this.btnVisualizar_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.btnVisualizar_F.Id = 18;
            this.btnVisualizar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizar_F.ImageOptions.Image")));
            this.btnVisualizar_F.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F9);
            this.btnVisualizar_F.Name = "btnVisualizar_F";
            // 
            // ComponenteGradeNavegador
            // 
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteGradeNavegador";
            this.Size = new System.Drawing.Size(1511, 807);
            this.Load += new System.EventHandler(this.ComponenteGradeNavegador_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.Bar barNavegacao_F;
        private DevExpress.XtraBars.BarAndDockingController BarAndDockingController_F;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu PopImprimir_F;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraBars.BarManager BarManager_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnIncluir_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnAlterar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnExcluir_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnImprimir_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnImprimirGrade_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.PopupMenu PopExportar_F;
        private DevExpress.XtraBars.BarButtonItem BtnExportarHTML_F;
        private DevExpress.XtraBars.BarButtonItem BtnExportarXML_F;
        private DevExpress.XtraBars.BarButtonItem BtnExportarXLS_F;
        private DevExpress.XtraBars.BarButtonItem BtnExportarTXT_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem BtnExportar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnConfirmar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnCancelar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.Bar barDataBase_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem barButtonAtualizar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem btnVisualizar_F;
        private DevExpress.XtraBars.BarButtonItem btnVisualizar_FPai;
    }
}
