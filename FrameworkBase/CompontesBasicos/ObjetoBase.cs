﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompontesBasicos
{
    /// <summary>
    /// Classe para derivar os objetos com tratamento customizado
    /// </summary>
    public class ObjetoBase
    {
        /// <summary>
        /// Chamada genérica para uso em objetos nao listados no uses
        /// </summary>
        /// <param name="args">entradas</param>
        /// <returns>retornos</returns>
        public virtual object[] ChamadaGenerica(params object[] args)
        {
            return null;
        }
    }
}
