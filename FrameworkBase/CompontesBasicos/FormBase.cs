using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FormBase : Form
    {
        

        //public bool somenteLeitura;
        /// <summary>
        /// For�a Caixa Alta Geral
        /// </summary>
        protected bool caixaAltaGeral=true;

        private bool desabilitarAllowDBNull = true;

        /// <summary>
        /// Desabilita obrigatoriedade de allowDBNull durante a edi��o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Desabilitar AllowDBNull")]
        public bool DesabilitarAllowDBNull
        {
            get { return desabilitarAllowDBNull; }
            set { desabilitarAllowDBNull = value; }
        }



        /// <summary>
        /// For�a Caixa Alta Geral
        /// </summary>
        /// <remarks>O componetebase tem uma propriedade com o mesmo que sobrepoem esta</remarks>
        [Category("Virtual Software")]
        [Description("For�a Caixa Alta Geral")]
        public bool CaixaAltaGeral
        {
            get { return caixaAltaGeral; }
            set { caixaAltaGeral = value; }
        }
    
        /// <summary>
        /// 
        /// </summary>
        public FormBase()
        {
            InitializeComponent();
        }

        private void FormBase_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (caixaAltaGeral)
                e.KeyChar = e.KeyChar.ToString().ToUpper()[0];
        }        

        /// <summary>
        /// Marca de vermelho o controle se o conte�do for nulo
        /// </summary>
        /// <remarks>
        /// � chamado no change dos controles que cont�m campos obrigat�rios
        /// Tem tratamento especial para componentes DEV
        /// </remarks>
        /// <param name="Sender">Controle chamador (a ser marcado)</param>
        /// <param name="e"></param>
        private void Ev_ChecaControleObrigatorio(object Sender, EventArgs e)
        {
            if (Sender is BaseEdit)
                Ev_ChecaControleObrigatorioDEV(Sender, null);
            else
            {
                Control controle = (Control)Sender;
                if (controle.Text.Trim() == "")
                    controle.BackColor = Color.FromArgb(237, 124, 124);
                else
                    controle.BackColor = Color.White;
            };


        }

        /// <summary>
        /// Marca icone de erro no controle se o conte�do for nulo
        /// </summary>
        /// <remarks>
        /// � chamado no validate dos controles que cont�m campos obrigat�rios
        /// Somente componentes DEV
        /// </remarks>
        /// <param name="sender">Controle chamador (a ser marcado)</param>
        /// <param name="e"></param>
        private void Ev_ChecaControleObrigatorioDEV(object sender, CancelEventArgs e)
        {
            BaseEdit Edbase = (BaseEdit)sender;
            if (Edbase is LookUpEditBase) {
                if ((Edbase.EditValue == null) || (Edbase.EditValue == DBNull.Value))
                    Edbase.ErrorText = "Campo Obrigat�rio";
                else
                    Edbase.ErrorText = "";
            }
            else
            {
                if (Edbase.Text.Trim() == "")
                    Edbase.ErrorText = "Campo Obrigat�rio";
                else
                    if (Edbase.ErrorText == "Campo Obrigat�rio")
                        Edbase.ErrorText = "";
            };
        }



        /// <summary>
        /// Metodo que instala os eventos
        /// associar o Ev_ChecaControleObrigatorio aos controles ligados a campos
        /// </summary>
        /// <remarks>
        /// Instala os eventos que marcam os campos obrigat�rio
        /// Desabilita controle no caso de somente leitura
        /// Chama o m�todo recursivo RC_InstalaEventos
        /// </remarks>
        /// <param name="Controlebase">Controle base da recurss�o</param>
        /// <param name="AjustarEnabled">Controla se a propriedade enabled deve ser ajusta para o controle base e seus filhos</param>
        /// <param name="CamposObrigatorios"></param>
        /// <param name="somenteLeitura"></param>
        public void InstalaEventos(Control Controlebase, bool AjustarEnabled, bool somenteLeitura,ArrayList CamposObrigatorios)
        {
            //O Rc_InstalaEventos n�o funciona bem para a colo��o das views em somente leitura
            // portanto foi feito um tratamento separado
            if (somenteLeitura)  //coloca as views como somenteleitura
            {
                SomenteLeitura(Controlebase);
                            };
            //if (somenteLeitura)
              //  Rc_SomenteLeitura(Controlebase);

            Rc_InstalaEventos(Controlebase, AjustarEnabled, CamposObrigatorios);

            bool DesabilitarAllowDBNull = desabilitarAllowDBNull;

            if (Controlebase is ComponenteCamposBase)
            {
                ComponenteCamposBase ControleBaseCampos = (ComponenteCamposBase)Controlebase;
                if (ControleBaseCampos.DesabilitarAllowDBNull == false)
                    DesabilitarAllowDBNull = false;
            };
            if (DesabilitarAllowDBNull)
               foreach (DataColumn Campo in CamposObrigatorios) {
                   Campo.AllowDBNull = true;                
            }
        }

        /// <summary>
        /// Metodo recursivo que instala os eventos
        /// associar o Ev_ChecaControleObrigatorio aos controles ligados a campos
        /// </summary>
        /// <remarks>
        /// Instala os eventos que marcam os campos obrigat�rio
        /// Desabilita controle no caso de somente leitura
        /// </remarks>
        /// <param name="Controlebase">Controle base da recurss�o</param>  
        /// <param name="AjustarEnabled"></param>
        /// <param name="CamposObrigatorios"></param>
        private void Rc_InstalaEventos(Control Controlebase, bool AjustarEnabled, ArrayList CamposObrigatorios)
        {

            foreach (Control CTRL in Controlebase.Controls)
            {
                
                if (CTRL is DevExpress.XtraEditors.TextEdit ) {
                    DevExpress.XtraEditors.TextEdit Edi = (DevExpress.XtraEditors.TextEdit)CTRL;
                    if (Edi is DevExpress.XtraEditors.MemoEdit) {                        
                        Edi.EnterMoveNextControl = !((DevExpress.XtraEditors.MemoEdit)Edi).Properties.AcceptsReturn;
                    }
                    else
                        Edi.EnterMoveNextControl = true;
                }

                //if (CTRL is DevExpress.XtraBars.BarDockControl)
                //    somenteLeitura = false;

                if (CTRL is DevExpress.XtraEditors.LookUpEdit) {
                    DevExpress.XtraEditors.LookUpEdit LU = (DevExpress.XtraEditors.LookUpEdit)CTRL;
                    LU.Properties.NullText="";                    
                };
                
                
                  
                


                if (CTRL is DevExpress.XtraEditors.GridLookUpEdit)
                {
                    ((DevExpress.XtraEditors.GridLookUpEdit)CTRL).Properties.NullText = "";
                }

                // primeiro instalamos recursivamente para os filhotes do CTRL
                if (CTRL is DevExpress.XtraGrid.GridControl)
                    Rc_InstalaEventos(CTRL, AjustarEnabled,CamposObrigatorios);
                else
                    Rc_InstalaEventos(CTRL, AjustarEnabled,CamposObrigatorios);

                // Depois para o CTRL efetivamente

                    

                
                Binding binding = CTRL.DataBindings["EditValue"];
                if (binding == null)
                    binding = CTRL.DataBindings["Text"];
                String DataMember = "";                
                DataSet dataset=null;
                BindingSource BS = null;
                //nulltext

                if (binding != null)
                {
                    //Todo: Permitir que o binding possa a ser uma row
                    if ((binding.DataSource != null) && (binding.DataSource is DataSet))
                    {
                        dataset = (DataSet)binding.DataSource;                                                
                        DataMember = binding.BindingMemberInfo.BindingPath;
                    };

                    

                    if ((binding.DataSource != null) && (binding.DataSource is BindingSource))
                    {
                            BS = (BindingSource)binding.DataSource;
                            while (BS.DataSource is BindingSource)
                                BS = (BindingSource)BS.DataSource;

                            if ((BS.DataSource != null) && (BS.DataSource is DataSet))
                            {
                                dataset = (DataSet)BS.DataSource;
                                DataMember = BS.DataMember;
                            };
                    };

                     

                    if ((dataset != null) && (DataMember != ""))
                        {

                            DataColumn Campo = dataset.Tables[DataMember].Columns[binding.BindingMemberInfo.BindingField];



                            if ((Campo != null) && (!Campo.AllowDBNull) && (!Campo.AutoIncrement) && (CTRL.Enabled))
                            {
                                if(!CamposObrigatorios.Contains(Campo))
                                 CamposObrigatorios.Add(Campo);
                                Ev_ChecaControleObrigatorio(CTRL, null);
                                if (CTRL is DevExpress.XtraEditors.RadioGroup)
                                    ((DevExpress.XtraEditors.RadioGroup)CTRL).SelectedIndexChanged += new EventHandler(this.Ev_ChecaControleObrigatorio);
                                else
                                {
                                    CTRL.TextChanged += new System.EventHandler(this.Ev_ChecaControleObrigatorio);
                                    if (CTRL is BaseEdit)
                                        ((BaseEdit)CTRL).Validating += new System.ComponentModel.CancelEventHandler(this.Ev_ChecaControleObrigatorioDEV);
                                }
                            };

                            if ((CTRL is TextEdit) && (Campo != null) && (Campo.MaxLength > 0))
                                ((TextEdit)CTRL).Properties.MaxLength = Campo.MaxLength;

                        }

                    

                }
            }
        }

        /// <summary>
        /// Metodo recursivo que coloca os elementos em somenteleitura        
        /// </summary>
        /// <remarks>        
        /// Desabilita os controles
        /// </remarks>
        /// <param name="Controlebase">Controle base da recurss�o</param>        
        private static void Rc_SomenteLeitura(Control Controlebase)
        {

            foreach (Control CTRL in Controlebase.Controls)
            {
                

                // primeiro instalamos recursivamente para os filhotes do CTRL
                if ((!(CTRL is DevExpress.XtraGrid.GridControl))
                    &&
                    (!(CTRL is DevExpress.XtraBars.BarDockControl))
                   )
                    if (!(CTRL is BaseEdit))
                      Rc_SomenteLeitura(CTRL);

                // Depois para o CTRL efetivamente

                if (CTRL is BaseEdit)
                {
                    BaseEdit CTRLDev = (BaseEdit)CTRL;
                    if (!(CTRLDev.Properties.ReadOnly))
                    {

                        if (
                            (CTRLDev.Controls.Count == 0)
                            ||
                            (CTRLDev is SpinEdit)
                            ||
                            (CTRLDev is LookUpEdit)
                            ||
                            (CTRLDev is MemoEdit)
                            ||
                            (CTRLDev is CalcEdit)
                            ||
                            (CTRLDev is TextEdit)
                           )
                        {
                            CTRLDev.Properties.ReadOnly = true;
                            if (CTRLDev is ButtonEdit){
                                ButtonEdit CTRLDevBE = (ButtonEdit)CTRLDev;
                                foreach (DevExpress.XtraEditors.Controls.EditorButton Botao in CTRLDevBE.Properties.Buttons)
                                    Botao.Visible = false;
                               }
                        }
                    };
                }
                else
                    if (CTRL.Enabled)
                    {
                    if (
                        (CTRL.Controls.Count == 0)
                        ||
                        (CTRL is SpinEdit)
                        ||
                        (CTRL is LookUpEdit)
                       )
                        if ((!(CTRL is Label))
                            &&
                            (!(CTRL is WebBrowser))
                            &&
                            (!(CTRL is DevExpress.XtraBars.BarDockControl))
                            &&
                            (!(CTRL is DevExpress.XtraLayout.Helpers.FakeFocusContainer))
                           )
                        {
                            CTRL.Enabled = false;
                            //Console.Write("2 ******************* T�tulo: {0} Tipo: {1}\r\n", CTRL.Name, CTRL.GetType()); // MessageBox.Show(CTRL.GetType().ToString());
                        }
                                
                        if (CTRL is DevExpress.XtraEditors.ButtonEdit)
                        {
                            DevExpress.XtraEditors.ButtonEdit BE = (DevExpress.XtraEditors.ButtonEdit)CTRL;
                            foreach (DevExpress.XtraEditors.Controls.EditorButton Bot in BE.Properties.Buttons)
                            {
                                Bot.Enabled = false;
                                //MessageBox.Show(Bot.Caption);
                            }
                        };
                        if (CTRL is DevExpress.XtraGrid.GridControl)
                        {
                            DevExpress.XtraGrid.GridControl XG = (DevExpress.XtraGrid.GridControl)CTRL;                
                        };
                    };
                }
            
        }

        private void FormBase_KeyDown(object sender, KeyEventArgs e)
        {
            //this.GetNextControl(ActiveControl,true);
            //this.SelectNextControl(ActiveControl,true,true,true,false);
            //this.ProcessTabKey(true);
            /*
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (FuncoesBasicas.PularComEnterRc(ActiveControl))
                        this.ProcessTabKey(true);
                    
                    break;
            }*/
        }

        /// <summary>
        /// Metodo que coloca os elementos em somenteleitura
        /// </summary>
        public static void SomenteLeitura(Control Controlebase)
        {
            BindingFlags bf = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;

            foreach (FieldInfo fi in (Controlebase.GetType()).GetFields(bf))
            {
                if (fi.FieldType == typeof(DevExpress.XtraGrid.Views.Grid.GridView))
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GV = (DevExpress.XtraGrid.Views.Grid.GridView)fi.GetValue(Controlebase);
                    foreach (DevExpress.XtraGrid.Columns.GridColumn Col in GV.Columns)
                        Col.OptionsColumn.ReadOnly = true;
                    //GV.OptionsBehavior.Editable = false;
                }
            };

            Rc_SomenteLeitura(Controlebase);
        }
    }
}