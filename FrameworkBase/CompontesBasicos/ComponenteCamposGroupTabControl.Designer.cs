namespace CompontesBasicos
{
    partial class ComponenteCamposGroupTabControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComponenteCamposGroupTabControl));
            this.GroupControl_F = new DevExpress.XtraEditors.GroupControl();
            this.TabControl_F = new DevExpress.XtraTab.XtraTabControl();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).BeginInit();
            this.SuspendLayout();
            // 
            // barStatus_F
            // 
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            // 
            // barComponente_F
            // 
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.AppearanceFocused.Options.UseFont = true;
            // 
            // GroupControl_F
            // 
            this.GroupControl_F.Dock = System.Windows.Forms.DockStyle.Top;
            this.GroupControl_F.Location = new System.Drawing.Point(0, 30);
            this.GroupControl_F.LookAndFeel.SkinName = "Lilian";
            this.GroupControl_F.Name = "GroupControl_F";
            this.GroupControl_F.ShowCaption = false;
            this.GroupControl_F.Size = new System.Drawing.Size(750, 64);
            this.ToolTipController_F.SetSuperTip(this.GroupControl_F, null);
            this.GroupControl_F.TabIndex = 0;
            // 
            // TabControl_F
            // 
            this.TabControl_F.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl_F.Location = new System.Drawing.Point(0, 94);
            this.TabControl_F.Name = "TabControl_F";
            this.TabControl_F.Size = new System.Drawing.Size(750, 363);
            this.TabControl_F.TabIndex = 4;
            this.TabControl_F.Text = "xtraTabControl1";
            this.TabControl_F.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.TabControl_F_SelectedPageChanged);
            // 
            // ComponenteCamposGroupTabControl
            // 
            this.Controls.Add(this.TabControl_F);
            this.Controls.Add(this.GroupControl_F);
            
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteCamposGroupTabControl";
            this.Size = new System.Drawing.Size(750, 480);
            this.ToolTipController_F.SetSuperTip(this, null);            
            this.Controls.SetChildIndex(this.GroupControl_F, 0);
            this.Controls.SetChildIndex(this.TabControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TabControl_F)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraEditors.GroupControl GroupControl_F;
        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraTab.XtraTabControl TabControl_F;
    }
}
