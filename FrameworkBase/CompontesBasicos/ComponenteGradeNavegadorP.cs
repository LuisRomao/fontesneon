using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Design;

namespace CompontesBasicos
{
    /// <summary>
    /// Coponete abstrato de pesquisa
    /// </summary>
    public partial class ComponenteGradeNavegadorP : ComponenteGradeNavegador
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteGradeNavegadorP()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tipo do componente Campos a ser aberto
        /// </summary>
        private Type componenteCampos;

        /// <summary>
        /// Tipo do componente Campos a ser aberto
        /// </summary>
        [Category("Virtual Software")]
        [Description("Componente a ser chamado para editar o registro")]
        [Editor(typeof(SelControleList), typeof(UITypeEditor))]
        public Type ComponenteCampos
        {
            get { return componenteCampos; }
            set { componenteCampos = value; }
        }

        private EstadosDosComponentes estadoComponenteCampos = EstadosDosComponentes.JanelasAtivas;

        /// <summary>
        /// Estado do Componente a ser chamado para editar o registro
        /// </summary>
        [Category("Virtual Software")]
        [Description("Estado do Componente a ser chamado para editar o registro")]
        public EstadosDosComponentes EstadoComponenteCampos
        {
            get { return estadoComponenteCampos; }
            set { estadoComponenteCampos = value; }
        }

        /// <summary>
        /// M�todo para incluir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void BtnIncluir_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if ((this.estado == EstadosDosComponentes.Registrado) && (ComponenteCampos != null))
            {
                FormPrincipalBase.FormPrincipal.ShowMoludoCampos(ComponenteCampos,"Novo",-1,false,this,EstadoComponenteCampos);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SomenteLeitura"></param>
        /// <param name="Titulo"></param>
        /// <param name="CampoChave"></param>
        /// <returns></returns>
        public bool AbreCampos(bool SomenteLeitura, string Titulo, string CampoChave)
        {
            using (Espera.cEspera Esp = new CompontesBasicos.Espera.cEspera(this))
            {
                Esp.Motivo.Text = "Abrindo";
                Application.DoEvents();
                DataRowView DRV;
                if (BindingSourcePrincipal.Current is DataRowView)
                    DRV = (DataRowView)BindingSourcePrincipal.Current;
                else
                {
                    DevExpress.XtraGrid.Views.Grid.GridView GridView = (DevExpress.XtraGrid.Views.Grid.GridView)GridControl_F.MainView;
                    DRV = (DataRowView)GridView.GetRow(GridView.FocusedRowHandle);
                };



                if ((campoTab != null) && (DRV.Row.Table.Columns[campoTab] != null))
                {
                    DataColumn Coluna = DRV.Row.Table.Columns[campoTab];
                    DRV.Row[Coluna].ToString();
                    Titulo = prefixoTab + " " + DRV.Row[Coluna].ToString();
                };
                if (DRV != null)
                {
                    int pk;
                    if (CampoChave != "")
                    {
                        pk = (Int32)(DRV.Row[CampoChave]);
                    }
                    else
                        pk = (Int32)(DRV.Row.ItemArray[0]);
                    ComponenteCamposBase Comp = (ComponenteCamposBase)ForPai().ShowMoludoCampos(ComponenteCampos, Titulo, pk, SomenteLeitura, this);

                    return true;
                }
                else
                    return false;
            }
        }

        /// <summary>
        /// Prefixo do t�tulo da tab
        /// </summary>
        private string prefixoTab;

        /// <summary>
        /// Prefixo a mostrar na tab
        /// </summary>
        /// <remarks>exemplo (Cond. XXXXXXX)</remarks>
        [Category("Virtual Software")]
        [Description("Prefixo a ser mostrado na TAB")]
        public string PrefixoTab
        {
            get { return prefixoTab; }
            set { prefixoTab = value; }
        }


        /// <summary>
        /// Campo a mostrar na tab
        /// </summary>
        private string campoTab;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Campo a ser mostrado na TAB")]
        public string CampoTab
        {
            get { return campoTab; }
            set { campoTab = value; }
        }


        private bool edicaoExterna;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Edita na tela campos")]
        public bool EdicaoExterna
        {
            get { return edicaoExterna; }
            set { edicaoExterna = value; }
        }

        /// <summary>
        /// A��o do bot�o Alterar
        /// </summary>
        protected override void Alterar()
        {
            if((EdicaoExterna) && (ComponenteCampos != null))
                AbreCampos(false, "Altera", "");
            else
                base.Alterar();
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void Visualizar()
        {
            AbreCampos(true, "Altera", "");
        }
    }
}

