using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraNavBar;
using System.Reflection;
using DevExpress.XtraTab;

namespace CompontesBasicos
{
    /// <summary>
    /// Formulario base para os Form principal
    /// </summary>
    /// <remarks>
    /// Inclui o registro dos componentes do programa
    /// Este tipo base � reconhecido pelos componentes o que nao ocorre com o formulario principal derivado
    /// </remarks>
    public partial class FormPrincipalBase : FormBase
    {
        private static string _TituloPrincipal = "";

        /// <summary>
        /// Titulo do aplicativo. Ser� acompanhado da vers�o.
        /// </summary>
        public static string TituloPrincipal
        {
            set 
            {            
                _TituloPrincipal = value; 
            }
            get 
            {
                if (_TituloPrincipal == "")
                    return "";
                else
                    return string.Format("{0} - {1}{2}", 
                                        _TituloPrincipal, 
                                        System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.ExecutablePath).FileVersion,
                                        CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao ? "" : " ** Em TESTE **");                                    
            }
        }

        /*
        /// <summary>
        /// Coloca o banner de TESTE
        /// </summary>
        /// <param name="Titulo">T�tulo</param>
        public void ColocaEmTeste(string Titulo)
        {
            PanelTESTE.Visible = true;
            LabelTeste.Text = Titulo;
        }*/

        /// <summary>
        /// Ponte para o formulario pricipal
        /// </summary>
        public static FormPrincipalBase FormPrincipal = null;

        /// <summary>
        /// Usuario logado
        /// </summary>
        public static int USULogado
        {
            get 
            {
                return Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USU;
            }
        }

        /// <summary>
        /// Usuario logado
        /// </summary>
        public static string USULogadoNome
        {
            get
            {
                return Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome;
            }
        }

        /// <summary>
        /// Ocorre ao cadastrar cada m�dulo
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento ao cadastrar m�dulo")]
        public event ModuleInfoEventHandler OnCadastraModulo;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        public void RemoveModudoRegistrado(Type Tipo)
        {
            
            foreach (ModuleInfo modulo in ModuleInfoCollection)
            {
                if ((modulo.ModuleTypestr == Tipo.FullName) || (modulo.ModuleType == Tipo))
                    modulo.Dell();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        public void RemoveModudoRegistrado(string Tipo)
        {

            foreach (ModuleInfo modulo in ModuleInfoCollection)
            {
                if (modulo.ModuleTypestr == Tipo)
                    modulo.Dell();
            }
        }

        #region Campos
        private ModuleInfoCollection _ModuleInfoCollection = new ModuleInfoCollection();
        private ModuleInfo _ModuleInfo = new ModuleInfo();

        /*
        /// <summary>
        /// Indica se o programa est� em produ��o
        /// </summary>
        /// <remarks>
        /// Deve ser setado no formul�rio principal
        /// Nos programas de teste dos m�dulos N�O deve ser setado
        /// </remarks>
        public static bool CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao => CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao;
        */

        /// <summary>
        /// Mostra a descri��o dos tipos na tab para facilitar a manuten��o com a r�pida localiza��o dos fontes.
        /// </summary>
        public static bool MostrarTiposNaTab = false;

        private cHelp _cHep;

        private cHelp cHelp 
        {
            get 
            {
                if (_cHep == null)
                {
                    _cHep = new cHelp(HELPURL);
                    _cHep.GoHome();
                    _cHep.VirShowModulo(EstadosDosComponentes.JanelasAtivas);
                    ((XtraTabPage)_cHep.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                }
                return _cHep;
            }
        }

        #endregion

        #region Propriendades

        private string _HELPURL;

        /// <summary>
        /// URL HELP
        /// </summary>
        [Category("Virtual Software")]
        [Description("URL HELP")]
        public string HELPURL 
        {
            get { return _HELPURL; }
            set { _HELPURL = value; }
        }

        /// <summary>
        /// Conjunto de componentes
        /// </summary>
        [Category("Virtual Software")]
        [Description("Registro dos m�dulos")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [Editor(typeof(System.ComponentModel.Design.CollectionEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ModuleInfoCollection ModuleInfoCollection
        {
            get
            {
                return _ModuleInfoCollection;
            }

        }

        private bool ocultarTabs = true;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Oculta as tab n�o usadas")]
        public bool OcultarTabs 
        {
            get 
            {
                return ocultarTabs;
            }
            set 
            {
                ocultarTabs = value;
                if (ocultarTabs)
                    TabControl_F.ClosePageButtonShowMode = ClosePageButtonShowMode.Default;
                else
                    TabControl_F.ClosePageButtonShowMode = ClosePageButtonShowMode.InActiveTabPageHeader;
            }
        }

        #endregion        
    
        #region metodos

        private void SalvarConfig(Int32 Numero) {
            using (System.IO.StreamWriter Configuracao = new System.IO.StreamWriter(Application.StartupPath + "\\XML\\Config" + Numero.ToString() + ".dat")){
                Configuracao.WriteLine(DefaultLookAndFeel_F.LookAndFeel.Style.ToString());
            };
        }

        private void LeDefault(Int32 Numero)
        {
            //MessageBox.Show("Cancelado");
            //return;
            string ArqConfigDir = Application.StartupPath + "\\XML\\";
            string ArqConfig = Application.StartupPath + "\\XML\\Config" + Numero.ToString() + ".dat";
            if (!System.IO.Directory.Exists(ArqConfigDir))
                System.IO.Directory.CreateDirectory(ArqConfigDir);
            if(!System.IO.File.Exists(ArqConfig)){
                DefaultLookAndFeel_F.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
                SalvarConfig(0);
            }
            else
            {
                using (System.IO.StreamReader Configuracao = new System.IO.StreamReader(ArqConfig, System.Text.Encoding.Default))
                {
                    string Estilo = Configuracao.ReadLine();
                    if (Estilo != null)
                    {
                        object o = System.Enum.Parse(typeof(DevExpress.LookAndFeel.LookAndFeelStyle), Estilo);
                        if ((o != null) && (o is DevExpress.LookAndFeel.LookAndFeelStyle))
                            DefaultLookAndFeel_F.LookAndFeel.Style = (DevExpress.LookAndFeel.LookAndFeelStyle)o;
                    }
                }
            }
        }
        
        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public FormPrincipalBase()
        {
            
            InitializeComponent();
            //if (!this.DesignMode)
              //  LeDefault(0);
            FormPrincipalBase.FormPrincipal = this;

            

           // DockManager_F.Form = this;            
        }

        /// <summary>
        /// Abre o help na pagina inicial
        /// </summary>
        public void AbreHelp() 
        {            
            ((XtraTabPage)cHelp.Parent).PageVisible = true;
            TabControl_F.SelectedTabPage = (XtraTabPage)cHelp.Parent;
            
        }

        /// <summary>
        /// Fun��o para ser adcionada ao delegate do resize do tabs criados para sediar os componentes
        /// </summary>
        private void ResizeCentralizador(object sender, EventArgs e)
        {
            Control Pai = (Control)sender;
            Control Filho = Pai.Controls[0];
            Filho.Left = (Pai.Width - Filho.Width) / 2;
            Filho.Top = (Pai.Height - Filho.Height) / 2;
        }


        private void RegisterModule(ModuleInfo mInfo)
        {

            // Create NavBar item
            NavBarItem item = NavBarControl_F.Items.Add();
            // Set up the NavBar item caption
            item.Caption = mInfo.Name;            
            item.SmallImageIndex = mInfo.ImageIndexP;
            item.LargeImageIndex = mInfo.ImageIndexG;
            item.SmallImage = mInfo.ImagemP;
            item.LargeImage = mInfo.ImagemG;

            // Use the item tag property for module identification
            item.Tag = mInfo;
            // Add the NavBar item into the appropriate Navbar group            
            NavBarControl_F.Groups[mInfo.Grupo].ItemLinks.Add(item);

            // Add a new item into bar list item
            //this.barListItemModules.Strings.Add(mInfo.Name);
        }

        /// <summary>
        /// Abre um m�dulo - Equivale a clicar no link
        /// </summary>
        /// <param name="Tipo">Tipo do m�duto - usar TypeOf</param>
        /// <remarks>Equivale a clicar no link</remarks>
        public void ShowModulo(Type Tipo) {
            CompontesBasicos.ModuleInfo Abrir = ModuleInfoCollection[Tipo];
            if (Abrir != null)
            {
                ModuleInfoCollection.ShowModule(Abrir, this);
                //this.Activate();
                //this.BringToFront();
                //this.Focus();
                //this.SetTopLevel(true);
                //this.WindowState = FormWindowState.Maximized;
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public ComponenteBase GetModuloRegistrado(Type Tipo)
        {
            ModuleInfo minfo = ModuleInfoCollection[Tipo];
            if (minfo == null)
                return null;
            else
            {
                return minfo.GetModulo();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ModuleTypestr"></param>
        /// <returns></returns>
        public ComponenteBase GetModuloRegistrado(string ModuleTypestr)
        {
            ModuleInfo info = ModuleInfoCollection[ModuleTypestr];
            if (info != null)
                return info.GetModulo();
            else
                return null;
            //foreach (ModuleInfo info in ModuleInfoCollection)
              //  if (info.ModuleTypestr == ModuleTypestr)
                //    return info.GetModulo();
            //return null;            
        }


        /// <summary>
        /// Abre um m�dulo - Equivale a clicar no link
        /// </summary>
        /// <param name="Tipostr">Tipo do m�duto - usar nome do tipo completo (com dll)</param>
        /// <remarks>Equivale a clicar no link</remarks>
        public void ShowModulo(string Tipostr)
        {
            ModuleInfoCollection.ShowModule(Tipostr, this);                
        }

        /// <summary>
        /// Coloca os m�dulos no menu
        /// </summary>
        private void RegisterModules()
        {
            foreach (ModuleInfo mInfo in _ModuleInfoCollection)
            {
                bool Abrir = (Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null) && mInfo.IniciarAberto;
                bool Cadastrar = true;
                if (OnCadastraModulo != null)
                    OnCadastraModulo(this, mInfo, ref Abrir, ref Cadastrar);
                if (Cadastrar)
                {
                    RegisterModule(mInfo);
                    if (Abrir)
                        ModuleInfoCollection.ShowModule(mInfo, this);
                }
            }

        }

        /// <summary>
        /// 
        /// </summary>
        protected void EscondeMenu()
        {
            if (DockPanel_F.Visibility != DevExpress.XtraBars.Docking.DockVisibility.AutoHide)
                DockPanel_F.Visibility = DevExpress.XtraBars.Docking.DockVisibility.AutoHide;
            DockPanel_F.HideSliding();
        }

        private void navBar_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            if (e.Link.Item.Tag is ModuleInfo)
            {
                EscondeMenu();                
                ModuleInfoCollection.ShowModule(e.Link.Item.Tag as ModuleInfo, this);
            }
        }

        private void FormPrincipalBase_FormClosing(object sender, FormClosingEventArgs e){
            bool CanClose = true;
            System.Collections.ArrayList ComponentesAbertos = new ArrayList();
            foreach(XtraTabPage pagina in TabControl_F.TabPages){
                try
                {
                    if (pagina.Controls.Count >= 0)
                        if (pagina.Controls[0] is ComponenteBase)
                        {
                            CanClose = ((ComponenteBase)pagina.Controls[0]).CanClose();
                            ComponentesAbertos.Add(pagina.Controls[0]);
                        }
                    if (!CanClose)
                    {
                        pagina.PageVisible = true;
                        TabControl_F.SelectedTabPage = pagina;                       
                        break;
                    }
                }
                catch { };
            };
            e.Cancel = !CanClose;

            //Dispara o evento OnClose
            if (CanClose)
                foreach (ComponenteBase Componente in ComponentesAbertos)
                    Componente.DipararOnClose(this,new EventArgs());
        }

        private void FormPrincipalBase_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (Bancovirtual.UsuarioLogado.UsuarioLogadoSt != null)
                {
                    barButtonItem1.Caption = String.Format("{0} - {1}", Bancovirtual.UsuarioLogado.UsuarioLogadoSt.USUNome, Bancovirtual.UsuarioLogado.UsuarioLogadoSt.HoraLogin);
                };
                RegisterModules();
                TabControl_F.Dock = DockStyle.Fill;
                LeDefault(0);
                if (_TituloPrincipal != "")
                    Text = TituloPrincipal;
                if(!CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    PanelTESTE.Visible = true;             
            }
        }

        /// <summary>
        /// Mostra um m�dulo na Tab
        /// </summary>
        /// <remarks>Cria uma nova inst�cia</remarks>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="tipoDeCarga"></param>
        /// <param name="Titulo"></param>
        public Object ShowMoludo(Type Tipo, string Titulo,TipoDeCarga tipoDeCarga)
        {
            return ShowMoludo(Tipo, Titulo, tipoDeCarga, 0, false);
        }

        /// <summary>
        /// Mostra um m�dulo na Tab
        /// </summary>
        /// <remarks>Cria uma nova inst�cia</remarks>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="Titulo"></param>
        public object ShowMoludo(Type Tipo, string Titulo) {
            return ShowMoludo(Tipo, Titulo, TipoDeCarga.navegacao,0,false);
        }

        //teste
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="somenteleitura"></param>
        /// <returns></returns>
        public Object ShowModulo(Type Tipo, bool somenteleitura) {
            return ShowMoludo(Tipo, "Titulo", TipoDeCarga.navegacao, 0, false); ; 
        }        

        /// <summary>
        /// Mostra um m�dulo na Tab vinculando com a chamadora
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="Titulo"></param>
        /// <param name="pk"></param>
        /// <param name="somenteleitura"></param>
        /// <param name="_GradeChamadora"></param>
        /// <remarks>Cria uma nova inst�cia</remarks>
        /// <returns></returns>
        public object ShowMoludoCampos(Type Tipo, string Titulo, Int32 pk, bool somenteleitura, ComponenteGradeNavegador _GradeChamadora) {
            return ShowMoludoCampos(Tipo, Titulo, pk, somenteleitura, _GradeChamadora, EstadosDosComponentes.JanelasAtivas);
        }

        /// <summary>
        /// Mostra um m�dulo na Tab vinculando com a chamadora, ou em popup
        /// </summary>
        /// <param name="nomeTipo">Nome do tipo. Chamar com o Assembly exemplo "ContaPagar.cNotasCampos"</param>
        /// <param name="Titulo"></param>
        /// <param name="pk"></param>
        /// <param name="somenteleitura"></param>
        /// <param name="_GradeChamadora"></param>
        /// <param name="Estado"></param>
        /// <returns></returns>
        public object ShowMoludoCampos(string nomeTipo, string Titulo, Int32 pk, bool somenteleitura, ComponenteGradeNavegador _GradeChamadora, EstadosDosComponentes Estado)
        {
            return ShowMoludoCampos(ModuleInfo.BuscaType(nomeTipo), Titulo, pk, somenteleitura, _GradeChamadora, Estado);
        }


        /// <summary>
        /// Mostra um m�dulo na Tab vinculando com a chamadora, ou em popup
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="Titulo"></param>
        /// <param name="pk"></param>
        /// <param name="somenteleitura"></param>
        /// <param name="_GradeChamadora"></param>
        /// �<param name="Estado"></param>
        /// <remarks>Cria uma nova inst�cia</remarks>        
        public object ShowMoludoCampos(Type Tipo, string Titulo, Int32 pk, bool somenteleitura, ComponenteGradeNavegador _GradeChamadora, EstadosDosComponentes Estado)
        {
            if (!Tipo.IsSubclassOf(typeof(ComponenteCamposBase)))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteCamposBase'");
            ComponenteBase novoModulo = null;
            ConstructorInfo constructorInfoObj;
            constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);
            novoModulo = constructorInfoObj.Invoke(null) as ComponenteBase;
            novoModulo.titulo = Titulo;
            ComponenteCamposBase CBB = (ComponenteCamposBase)novoModulo;
            CBB.GradeChamadora = _GradeChamadora;
            if(pk > 0)
                CBB.Fill(TipoDeCarga.pk, pk, somenteleitura);
            else
                CBB.Fill(TipoDeCarga.novo, 0, false);            
            return VirShowMoludo(novoModulo, Estado);
            


        }

        /// <summary>
        /// Mostra um m�dulo na Tab
        /// </summary>
        /// <param name="Tipo"></param>
        /// <param name="Titulo"></param>
        /// <param name="tipoDeCarga"></param>
        /// <param name="pk"></param>
        /// <param name="somenteleitura"></param>
        /// <returns></returns>
        public object ShowMoludo(Type Tipo, string Titulo, TipoDeCarga tipoDeCarga,Int32 pk, bool somenteleitura)
        {
            if (!Tipo.IsSubclassOf(typeof(ComponenteBase)))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteBase'");
            ComponenteBase novoModulo = null;
            ConstructorInfo constructorInfoObj;
    //        novoModulo.BringToFront();
            try
            {
                constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);
                novoModulo = constructorInfoObj.Invoke(null) as ComponenteBase; 
                if (novoModulo is ComponenteBaseBindingSource)
                {
                    ComponenteBaseBindingSource CBB = (ComponenteBaseBindingSource)novoModulo;
                    CBB.Fill(tipoDeCarga, pk, somenteleitura);                    
                }
                
                return ShowMoludointerno(novoModulo, Titulo, EstadosDosComponentes.JanelasAtivas, somenteleitura);
            }
            catch (Exception Erro)
                    {
                        if (Erro.InnerException != null)
                            MessageBox.Show(Erro.InnerException.Message);
                        else
                            MessageBox.Show(Erro.Message);

                        return null;
                    };

            
        }

        /// <summary>
        /// Mostra um compomente
        /// </summary>
        /// <param name="novoModulo">Componente instanciado</param>
        /// <param name="estadosDosComponentes">Estado do componente</param>       
        public object VirShowMoludo(ComponenteBase novoModulo, EstadosDosComponentes estadosDosComponentes)
        {
            novoModulo.Estado = estadosDosComponentes;
            
            novoModulo.Visible = false;
            if (novoModulo is ComponenteCamposGroupTabControl)
                ((ComponenteCamposGroupTabControl)novoModulo).CriaPaginas(novoModulo.somenteleitura);
            switch (estadosDosComponentes)
            {
                case EstadosDosComponentes.Registrado:
                    if (novoModulo.Parent == null)
                    {
                        TabControl_F.TabPages.Insert(0);
                        novoModulo.Parent = TabControl_F.TabPages[0];
                        TabControl_F.TabPages[0].Text = novoModulo.Titulo;
                        TabControl_F.TabPages[0].ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                    }
                    else
                        ((XtraTabPage)novoModulo.Parent).PageVisible = true;
                    novoModulo.AtivarAtualizarTabelas(true);
                    break;
                case EstadosDosComponentes.PopUp:

                    return novoModulo.ShowEmPopUp();
                                        
                case EstadosDosComponentes.JanelasAtivas:                    
                    novoModulo.Parent = TabControl_F.TabPages.Add(novoModulo.Titulo);
                    ((XtraTabPage)novoModulo.Parent).ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
                    break;
            }
            
            if (novoModulo.Dock != DockStyle.Fill)
                novoModulo.Parent.Resize += ResizeCentralizador;
            TabControl_F.SelectedTabPage = (XtraTabPage)novoModulo.Parent;
            //if (novoModulo is ComponenteCamposGroupTabControl)
            //    ((ComponenteCamposGroupTabControl)novoModulo).CriaPaginas(novoModulo.somenteleitura);
            InstalaEventos(novoModulo, true, novoModulo.somenteleitura, novoModulo.CamposObrigatorios);
            if (novoModulo is CompontesBasicos.ComponenteBaseBindingSource)
                ((CompontesBasicos.ComponenteBaseBindingSource)novoModulo).Fill();
            try
            {
                novoModulo.Visible = true;
            }
            catch { }
            return novoModulo;
        }

        /// <summary>
        /// Obsoleto usar VirShowModulo
        /// </summary>
        /// <param name="novoModulo"></param>
        /// <param name="Titulo"></param>
        /// <param name="estadosDosComponentes"></param>
        /// <param name="somenteleitura"></param>
        /// <returns></returns>
        public object ShowMoludointerno(ComponenteBase novoModulo, string Titulo, EstadosDosComponentes estadosDosComponentes,bool somenteleitura)
        {            
            novoModulo.Estado = estadosDosComponentes;
            novoModulo.Visible = false;
            if (Titulo == "")
                Titulo = novoModulo.titulo;
            switch (estadosDosComponentes)
            {
                case EstadosDosComponentes.Registrado:
                    if (novoModulo.Parent == null)
                    {
                        TabControl_F.TabPages.Insert(0);
                        novoModulo.Parent = TabControl_F.TabPages[0];
                        TabControl_F.TabPages[0].Text = Titulo;
                        TabControl_F.TabPages[0].ShowCloseButton = DevExpress.Utils.DefaultBoolean.True;
                    }
                    else
                        ((XtraTabPage)novoModulo.Parent).PageVisible = true;
                    novoModulo.AtivarAtualizarTabelas(true);                    
                    break;
                case EstadosDosComponentes.PopUp:
                    ///////////
                    FormBase Formulario = new FormBase();
                    Formulario.Parent = FormPrincipalBase.FormPrincipal.Parent;
                    novoModulo.Estado = EstadosDosComponentes.PopUp;
                    Formulario.Controls.Add(novoModulo);
                    //novoModulo.Left = novoModulo.Left0;
                    //novoModulo.Top = novoModulo.Top0;

                    Formulario.FormBorderStyle = FormBorderStyle.FixedDialog;
                    Formulario.ControlBox = true;
                    Formulario.MaximizeBox = false;
                    Formulario.MinimizeBox = false;
                    if (novoModulo.Doc == DockStyle.Fill)
                    {
                        Formulario.Width = Convert.ToInt32(0.9 * Screen.PrimaryScreen.WorkingArea.Width);
                        Formulario.Height = Convert.ToInt32(0.9 * Screen.PrimaryScreen.WorkingArea.Height);
                        Formulario.Left = Convert.ToInt32(0.05 * (Screen.PrimaryScreen.WorkingArea.Width));
                        Formulario.Top = Convert.ToInt32(0.05 * (Screen.PrimaryScreen.WorkingArea.Height));
                    }
                    else
                        Formulario.AutoSize = true;
                    Formulario.StartPosition = FormStartPosition.CenterParent;
                    Formulario.Text = novoModulo.titulo;
                    Formulario.InstalaEventos(novoModulo, true, false, novoModulo.CamposObrigatorios);
                    Formulario.ShowDialog();                    
                    ///////////
                    return null;
                    
                case EstadosDosComponentes.JanelasAtivas:
                    novoModulo.Parent = TabControl_F.TabPages.Add(Titulo);
                    TabControl_F.SelectedTabPage.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
                    break;
            }
            
            if (novoModulo.Dock != DockStyle.Fill)
             novoModulo.Parent.Resize += ResizeCentralizador;
            TabControl_F.SelectedTabPage = (XtraTabPage)novoModulo.Parent;
            if (novoModulo is ComponenteCamposGroupTabControl)
                ((ComponenteCamposGroupTabControl)novoModulo).CriaPaginas(somenteleitura);
            InstalaEventos(novoModulo, true,somenteleitura,novoModulo.CamposObrigatorios);
            if (novoModulo is CompontesBasicos.ComponenteBaseBindingSource)
                ((CompontesBasicos.ComponenteBaseBindingSource)novoModulo).Fill();
            novoModulo.Visible = true;
            return novoModulo;
        }

        #endregion

        private void TabControl_F_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            try
            {
                ComponenteBase Ativando = null;
                ComponenteBase Desativando = null;

                if (e.PrevPage != null)
                    if (e.PrevPage.Controls.Count == 1)
                        if (e.PrevPage.Controls[0] is ComponenteBase)
                            Desativando = (ComponenteBase)e.PrevPage.Controls[0];
                if (e.Page != null)
                    if (e.Page.Controls.Count == 1)
                        if (e.Page.Controls[0] is ComponenteBase)
                            Ativando = (ComponenteBase)e.Page.Controls[0];

                if (Desativando != null)
                    Desativando.AtivarAtualizarTabelas(false);
                if (Ativando != null)
                {
                    Ativando.RefreshDados();
                    Ativando.AtivarAtualizarTabelas(true);
                }
            }
            catch
            { 
            }                
        }

        private void TabControl_F_CloseButtonClick(object sender, EventArgs e)
        {            
            TabControl_F.SelectedTabPage.PageVisible = false;
            foreach (DevExpress.XtraTab.XtraTabPage TabTeste in TabControl_F.TabPages)
                if (TabTeste.Visible)
                    return;
            DockPanel_F.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Visible;
        }

        private bool Direito = false;

        private bool Esquerdo = false;

        private void FormPrincipalBase_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                Direito = true;
            if (e.Button == MouseButtons.Left)
                Esquerdo = true;

            if (Direito && Esquerdo)
            {
                String AssRelat = "";
                System.Reflection.Assembly[] Ass = AppDomain.CurrentDomain.GetAssemblies();

                if (Ass != null)
                    foreach (System.Reflection.Assembly As in Ass)
                        AssRelat += As.FullName + Environment.NewLine;
                Clipboard.SetText(AssRelat);
            }
        }

        private void PictureEdit_F_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                Direito = false;
            if (e.Button == MouseButtons.Left)
                Esquerdo = false;
        }
           
    }
   
}