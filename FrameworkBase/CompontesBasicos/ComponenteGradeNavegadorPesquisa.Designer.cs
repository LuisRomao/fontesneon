namespace CompontesBasicos
{
    partial class ComponenteGradeNavegadorPesquisa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComponenteGradeNavegadorPesquisa));
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.LayoutPesquisa_F = new DevExpress.XtraLayout.LayoutControl();
            this.btnPesquisar_F = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutValor_F = new DevExpress.XtraLayout.LayoutControl();
            this.LayoutRootValor_F = new DevExpress.XtraLayout.LayoutControlGroup();
            this.cbxCondicao_F = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbxCampo_F = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnLimpar_F = new DevExpress.XtraEditors.SimpleButton();
            this.LayoutRootPesquisa_F = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItemImagem_F = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemCampo_F = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItemCondicao_F = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutItemValor_F = new DevExpress.XtraLayout.LayoutControlItem();
            this.EmptySpaceItem_F = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnVisualizar_F = new DevExpress.XtraBars.BarButtonItem();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).BeginInit();
            this.LayoutPesquisa_F.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutValor_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRootValor_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCondicao_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCampo_F.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRootPesquisa_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItemImagem_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCampo_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItemCondicao_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemValor_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // barNavegacao_F
            // 
            this.barNavegacao_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnVisualizar_F, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barNavegacao_F.OptionsBar.AllowQuickCustomization = false;
            this.barNavegacao_F.OptionsBar.DisableClose = true;
            this.barNavegacao_F.OptionsBar.DisableCustomization = true;
            this.barNavegacao_F.OptionsBar.DrawDragBorder = false;
            this.barNavegacao_F.OptionsBar.DrawSizeGrip = true;
            this.barNavegacao_F.OptionsBar.RotateWhenVertical = false;
            this.barNavegacao_F.OptionsBar.UseWholeRow = true;
            // 
            // BarManager_F
            // 
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnVisualizar_F});
            // 
            // BtnIncluir_F
            // 
            this.BtnIncluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnIncluir_F.ImageOptions.Image")));
            // 
            // BtnAlterar_F
            // 
            this.BtnAlterar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnAlterar_F.ImageOptions.Image")));
            this.BtnAlterar_F.ImageOptions.ImageIndex = 0;
            // 
            // BtnExcluir_F
            // 
            this.BtnExcluir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExcluir_F.ImageOptions.Image")));
            // 
            // BtnImprimir_F
            // 
            this.BtnImprimir_F.Enabled = false;
            this.BtnImprimir_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimir_F.ImageOptions.Image")));
            // 
            // BtnImprimirGrade_F
            // 
            this.BtnImprimirGrade_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnImprimirGrade_F.ImageOptions.Image")));
            // 
            // BtnExportar_F
            // 
            this.BtnExportar_F.Enabled = false;
            this.BtnExportar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("BtnExportar_F.ImageOptions.Image")));
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.ImageOptions.Image")));
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.ImageOptions.Image")));
            // 
            // barDataBase_F
            // 
            this.barDataBase_F.OptionsBar.AllowQuickCustomization = false;
            this.barDataBase_F.OptionsBar.DisableClose = true;
            this.barDataBase_F.OptionsBar.DisableCustomization = true;
            this.barDataBase_F.OptionsBar.DrawDragBorder = false;
            this.barDataBase_F.OptionsBar.UseWholeRow = true;
            // 
            // GridControl_F
            // 
            this.GridControl_F.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.GridControl_F.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.GridControl_F.Location = new System.Drawing.Point(0, 84);
            this.GridControl_F.Size = new System.Drawing.Size(1490, 346);
            this.GridControl_F.TabIndex = 1;
            this.GridControl_F.Load += new System.EventHandler(this.GridControl_F_Load);
            // 
            // GridView_F
            // 
            this.GridView_F.OptionsBehavior.AllowIncrementalSearch = true;
            this.GridView_F.OptionsBehavior.Editable = false;
            this.GridView_F.OptionsLayout.Columns.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.Columns.StoreAppearance = true;
            this.GridView_F.OptionsLayout.StoreAllOptions = true;
            this.GridView_F.OptionsLayout.StoreAppearance = true;
            this.GridView_F.OptionsNavigation.EnterMoveNextColumn = true;
            this.GridView_F.OptionsPrint.EnableAppearanceEvenRow = true;
            this.GridView_F.OptionsPrint.EnableAppearanceOddRow = true;
            this.GridView_F.OptionsPrint.ExpandAllDetails = true;
            this.GridView_F.OptionsSelection.InvertSelection = true;
            this.GridView_F.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways;
            this.GridView_F.OptionsView.ShowFooter = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            this.StyleController_F.LookAndFeel.SkinName = "Lilian";
            this.StyleController_F.LookAndFeel.UseDefaultLookAndFeel = false;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(12, 30);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(42, 60);
            this.pictureEdit1.StyleController = this.LayoutPesquisa_F;
            this.pictureEdit1.TabIndex = 0;
            // 
            // LayoutPesquisa_F
            // 
            this.LayoutPesquisa_F.AllowCustomization = false;
            this.LayoutPesquisa_F.Controls.Add(this.btnPesquisar_F);
            this.LayoutPesquisa_F.Controls.Add(this.LayoutValor_F);
            this.LayoutPesquisa_F.Controls.Add(this.cbxCondicao_F);
            this.LayoutPesquisa_F.Controls.Add(this.cbxCampo_F);
            this.LayoutPesquisa_F.Controls.Add(this.pictureEdit1);
            this.LayoutPesquisa_F.Controls.Add(this.btnLimpar_F);
            this.LayoutPesquisa_F.Dock = System.Windows.Forms.DockStyle.Top;
            this.LayoutPesquisa_F.Location = new System.Drawing.Point(2, 2);
            this.LayoutPesquisa_F.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutPesquisa_F.Name = "LayoutPesquisa_F";
            this.LayoutPesquisa_F.Root = this.LayoutRootPesquisa_F;
            this.LayoutPesquisa_F.Size = new System.Drawing.Size(1486, 84);
            this.LayoutPesquisa_F.TabIndex = 0;
            // 
            // btnPesquisar_F
            // 
            this.btnPesquisar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar_F.ImageOptions.Image")));
            this.btnPesquisar_F.Location = new System.Drawing.Point(472, 30);
            this.btnPesquisar_F.Name = "btnPesquisar_F";
            this.btnPesquisar_F.Size = new System.Drawing.Size(78, 29);
            this.btnPesquisar_F.StyleController = this.LayoutPesquisa_F;
            this.btnPesquisar_F.TabIndex = 4;
            this.btnPesquisar_F.Text = "&Pesquisar";
            this.btnPesquisar_F.Click += new System.EventHandler(this.btnPesquisar_F_Click);
            // 
            // LayoutValor_F
            // 
            this.LayoutValor_F.AllowCustomization = false;
            this.LayoutValor_F.Location = new System.Drawing.Point(306, 28);
            this.LayoutValor_F.Name = "LayoutValor_F";
            this.LayoutValor_F.Root = this.LayoutRootValor_F;
            this.LayoutValor_F.Size = new System.Drawing.Size(164, 64);
            this.LayoutValor_F.TabIndex = 3;
            // 
            // LayoutRootValor_F
            // 
            this.LayoutRootValor_F.CustomizationFormText = "LayoutRootValor_F";
            this.LayoutRootValor_F.GroupBordersVisible = false;
            this.LayoutRootValor_F.Location = new System.Drawing.Point(0, 0);
            this.LayoutRootValor_F.Name = "LayoutRootValor_F";
            this.LayoutRootValor_F.Size = new System.Drawing.Size(164, 64);
            this.LayoutRootValor_F.TextVisible = false;
            // 
            // cbxCondicao_F
            // 
            this.cbxCondicao_F.Location = new System.Drawing.Point(188, 46);
            this.cbxCondicao_F.Name = "cbxCondicao_F";
            this.cbxCondicao_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxCondicao_F.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxCondicao_F.Size = new System.Drawing.Size(116, 20);
            this.cbxCondicao_F.StyleController = this.LayoutPesquisa_F;
            this.cbxCondicao_F.TabIndex = 2;
            this.cbxCondicao_F.SelectedIndexChanged += new System.EventHandler(this.cbxCondicao_F_SelectedIndexChanged);
            // 
            // cbxCampo_F
            // 
            this.cbxCampo_F.Location = new System.Drawing.Point(58, 46);
            this.cbxCampo_F.Name = "cbxCampo_F";
            this.cbxCampo_F.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbxCampo_F.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cbxCampo_F.Size = new System.Drawing.Size(126, 20);
            this.cbxCampo_F.StyleController = this.LayoutPesquisa_F;
            this.cbxCampo_F.TabIndex = 1;
            this.cbxCampo_F.SelectedIndexChanged += new System.EventHandler(this.cbxCampo_F_SelectedIndexChanged);
            // 
            // btnLimpar_F
            // 
            this.btnLimpar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnLimpar_F.ImageOptions.Image")));
            this.btnLimpar_F.Location = new System.Drawing.Point(472, 63);
            this.btnLimpar_F.Name = "btnLimpar_F";
            this.btnLimpar_F.Size = new System.Drawing.Size(78, 27);
            this.btnLimpar_F.StyleController = this.LayoutPesquisa_F;
            this.btnLimpar_F.TabIndex = 5;
            this.btnLimpar_F.Text = "&Limpar";
            this.btnLimpar_F.Click += new System.EventHandler(this.btnLimpar_F_Click);
            // 
            // LayoutRootPesquisa_F
            // 
            this.LayoutRootPesquisa_F.CustomizationFormText = ":: Pesquisar ::";
            this.LayoutRootPesquisa_F.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.LayoutRootPesquisa_F.Enabled = false;
            this.LayoutRootPesquisa_F.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItemImagem_F,
            this.layoutControlItemCampo_F,
            this.LayoutControlItemCondicao_F,
            this.layoutItemValor_F,
            this.EmptySpaceItem_F,
            this.layoutControlGroup1});
            this.LayoutRootPesquisa_F.Location = new System.Drawing.Point(0, 0);
            this.LayoutRootPesquisa_F.Name = "LayoutRootPesquisa_F";
            this.LayoutRootPesquisa_F.Size = new System.Drawing.Size(1469, 102);
            this.LayoutRootPesquisa_F.Text = ":: Pesquisar ::";
            // 
            // LayoutControlItemImagem_F
            // 
            this.LayoutControlItemImagem_F.Control = this.pictureEdit1;
            this.LayoutControlItemImagem_F.CustomizationFormText = "Imagem";
            this.LayoutControlItemImagem_F.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItemImagem_F.MaxSize = new System.Drawing.Size(46, 64);
            this.LayoutControlItemImagem_F.MinSize = new System.Drawing.Size(46, 64);
            this.LayoutControlItemImagem_F.Name = "LayoutControlItemImagem_F";
            this.LayoutControlItemImagem_F.Size = new System.Drawing.Size(46, 64);
            this.LayoutControlItemImagem_F.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItemImagem_F.Text = "Imagem";
            this.LayoutControlItemImagem_F.TextLocation = DevExpress.Utils.Locations.Left;
            this.LayoutControlItemImagem_F.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItemImagem_F.TextVisible = false;
            // 
            // layoutControlItemCampo_F
            // 
            this.layoutControlItemCampo_F.Control = this.cbxCampo_F;
            this.layoutControlItemCampo_F.CustomizationFormText = "Campo";
            this.layoutControlItemCampo_F.Location = new System.Drawing.Point(46, 0);
            this.layoutControlItemCampo_F.MaxSize = new System.Drawing.Size(130, 64);
            this.layoutControlItemCampo_F.MinSize = new System.Drawing.Size(130, 64);
            this.layoutControlItemCampo_F.Name = "layoutControlItemCampo_F";
            this.layoutControlItemCampo_F.Size = new System.Drawing.Size(130, 64);
            this.layoutControlItemCampo_F.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemCampo_F.Text = "Campo";
            this.layoutControlItemCampo_F.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItemCampo_F.TextSize = new System.Drawing.Size(44, 13);
            // 
            // LayoutControlItemCondicao_F
            // 
            this.LayoutControlItemCondicao_F.Control = this.cbxCondicao_F;
            this.LayoutControlItemCondicao_F.CustomizationFormText = "Condi��o";
            this.LayoutControlItemCondicao_F.Location = new System.Drawing.Point(176, 0);
            this.LayoutControlItemCondicao_F.MaxSize = new System.Drawing.Size(120, 64);
            this.LayoutControlItemCondicao_F.MinSize = new System.Drawing.Size(120, 64);
            this.LayoutControlItemCondicao_F.Name = "LayoutControlItemCondicao_F";
            this.LayoutControlItemCondicao_F.Size = new System.Drawing.Size(120, 64);
            this.LayoutControlItemCondicao_F.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.LayoutControlItemCondicao_F.Text = "Condi��o";
            this.LayoutControlItemCondicao_F.TextLocation = DevExpress.Utils.Locations.Top;
            this.LayoutControlItemCondicao_F.TextSize = new System.Drawing.Size(44, 13);
            // 
            // layoutItemValor_F
            // 
            this.layoutItemValor_F.Control = this.LayoutValor_F;
            this.layoutItemValor_F.CustomizationFormText = "Reservado para campos de valor";
            this.layoutItemValor_F.Location = new System.Drawing.Point(296, 0);
            this.layoutItemValor_F.MaxSize = new System.Drawing.Size(164, 64);
            this.layoutItemValor_F.MinSize = new System.Drawing.Size(164, 64);
            this.layoutItemValor_F.Name = "layoutItemValor_F";
            this.layoutItemValor_F.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutItemValor_F.Size = new System.Drawing.Size(164, 64);
            this.layoutItemValor_F.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutItemValor_F.Text = "Reservado para campos de valor";
            this.layoutItemValor_F.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutItemValor_F.TextSize = new System.Drawing.Size(0, 0);
            this.layoutItemValor_F.TextVisible = false;
            // 
            // EmptySpaceItem_F
            // 
            this.EmptySpaceItem_F.AllowHotTrack = false;
            this.EmptySpaceItem_F.CustomizationFormText = "EmptySpaceItem_F";
            this.EmptySpaceItem_F.Location = new System.Drawing.Point(542, 0);
            this.EmptySpaceItem_F.Name = "EmptySpaceItem_F";
            this.EmptySpaceItem_F.Size = new System.Drawing.Size(907, 64);
            this.EmptySpaceItem_F.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3});
            this.layoutControlGroup1.Location = new System.Drawing.Point(460, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(82, 64);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnLimpar_F;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(82, 31);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(82, 31);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(82, 31);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnPesquisar_F;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(82, 33);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(82, 33);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(82, 33);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // btnVisualizar_F
            // 
            this.btnVisualizar_F.Caption = "&Visualizar (F9)";
            this.btnVisualizar_F.CategoryGuid = new System.Guid("70b162a1-0e6a-4dea-87b4-fc5b71c53e05");
            this.btnVisualizar_F.Id = 18;
            this.btnVisualizar_F.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizar_F.ImageOptions.Image")));
            this.btnVisualizar_F.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F9);
            this.btnVisualizar_F.Name = "btnVisualizar_F";
            this.btnVisualizar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnVisualizar_F_ItemClick);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.LayoutPesquisa_F);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.ShowCaption = false;
            this.groupControl1.Size = new System.Drawing.Size(1490, 84);
            this.groupControl1.TabIndex = 5;
            this.groupControl1.Text = "groupControl1";
            this.groupControl1.Visible = false;
            // 
            // ComponenteGradeNavegadorPesquisa
            // 
            this.Controls.Add(this.groupControl1);
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteGradeNavegadorPesquisa";
            this.Size = new System.Drawing.Size(1490, 490);
            this.Load += new System.EventHandler(this.ComponenteGradeNavegadorPesquisa_Load);
            this.Controls.SetChildIndex(this.groupControl1, 0);
            this.Controls.SetChildIndex(this.GridControl_F, 0);
            ((System.ComponentModel.ISupportInitialize)(this.PopImprimir_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PopExportar_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridControl_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutPesquisa_F)).EndInit();
            this.LayoutPesquisa_F.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutValor_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRootValor_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCondicao_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbxCampo_F.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutRootPesquisa_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItemImagem_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemCampo_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItemCondicao_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutItemValor_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EmptySpaceItem_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.ComboBoxEdit cbxCondicao_F;
        private DevExpress.XtraEditors.ComboBoxEdit cbxCampo_F;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutRootPesquisa_F;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItemImagem_F;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemCampo_F;
        private DevExpress.XtraLayout.LayoutControlItem LayoutControlItemCondicao_F;
        private DevExpress.XtraLayout.LayoutControl LayoutValor_F;
        private DevExpress.XtraLayout.LayoutControlGroup LayoutRootValor_F;
        private DevExpress.XtraLayout.LayoutControlItem layoutItemValor_F;
        private DevExpress.XtraLayout.EmptySpaceItem EmptySpaceItem_F;
        private DevExpress.XtraEditors.SimpleButton btnLimpar_F;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton btnPesquisar_F;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.BarButtonItem btnVisualizar_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraEditors.GroupControl groupControl1;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraLayout.LayoutControl LayoutPesquisa_F;
    }
}
