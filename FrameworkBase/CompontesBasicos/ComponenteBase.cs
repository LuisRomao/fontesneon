using DevExpress.XtraTab;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;

namespace CompontesBasicos
{
    /// <summary>
    /// Componente b�sico de todo o projeto.
    /// </summary>
    /// <remarks>Todos os componentes s�o derivados deste (Os componetes e n�o os Forms)</remarks>
    public partial class ComponenteBase : DevExpress.XtraEditors.XtraUserControl
    {
        private bool caixaAltaGeralParaRestaurar = false;

        /// <summary>
        /// Etras maiusculas
        /// </summary>
        protected bool caixaAltaGeral = true;

        /// <summary>
        /// Fun��o chamada no retorno do objeto
        /// </summary>
        public del_retornodechamada retornodechamada;

        /// <summary>
        /// delegate para o retorno da chamada
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="dados"></param>
        /// <param name="Modo"></param>
        /// <returns></returns>
        public delegate void del_retornodechamada(ComponenteBase Sender, DialogResult Modo, params object[] dados);

        /// <summary>
        /// Evento que ocorre antes de fechar o programa
        /// </summary>
        /// <remarks>� chamado pelo formul�rio pricipal ao fechar</remarks>
        [Category("Virtual Software")]
        [Description("Close do formul�rio principal")]
        public event EventHandler OnClose;


        private void ComponenteBase_Enter(object sender, EventArgs e)
        {
            if ((FormPrincipalBase.FormPrincipal != null) && (caixaAltaGeral == false))
            {
                caixaAltaGeralParaRestaurar = FormPrincipalBase.FormPrincipal.CaixaAltaGeral;
                FormPrincipalBase.FormPrincipal.CaixaAltaGeral = false;
            }
        }

        private void ComponenteBase_Leave(object sender, EventArgs e)
        {
            if ((FormPrincipalBase.FormPrincipal != null) && (caixaAltaGeral == false))
                FormPrincipalBase.FormPrincipal.CaixaAltaGeral = caixaAltaGeralParaRestaurar;
        }

        /// <summary>
        /// Faz o retorno da chamada se a callback foi gestrada;
        /// </summary>
        /// <param name="Modo"></param>
        /// <param name="dados"></param>
        protected virtual void RetornaChamada(DialogResult Modo, params object[] dados)
        {
            if (retornodechamada != null)
                retornodechamada(this, Modo, dados);
        }

        /// <summary>
        /// Chamada gen�rica para uso em objetos nao listados no uses
        /// </summary>
        /// <param name="args">entradas</param>
        /// <returns>retornos</returns>
        public virtual object[] ChamadaGenerica(params object[] args)
        {
            return null;
        }

        /// <summary>
        /// Dispara o evento que ocorre antes de fechar o programa
        /// </summary>
        /// <remarks>� chamado pelo formul�rio pricipal ao fechar</remarks>
        public void DipararOnClose(object sender, EventArgs e)
        {
            if (OnClose != null)
                OnClose(sender, e);
        }

        /// <summary>
        /// For�a Caixa Alta Geral
        /// </summary>
        /// <remarks>Sobrepoem a propriedade do FormBase</remarks>
        [Category("Virtual Software")]
        [Description("For�a Caixa Alta Geral")]
        public bool CaixaAltaGeral
        {
            get { return caixaAltaGeral; }
            set { caixaAltaGeral = value; }
        }

        #region Campos
        /// <summary>
        /// T�tulo
        /// </summary>
        public String titulo;

        /// <summary>
        /// Indica que houve um erro na cria��o do objeto n�o deve ser aberto.
        /// </summary>
        public bool Abortar_Por_Erro = false;

        /// <summary>
        /// Lista dos campos obrigat�rios
        /// </summary>
        public ArrayList CamposObrigatorios = new ArrayList();

        /// <summary>
        /// Indica o estado de apresenta�ao do componente
        /// </summary>
        /// <value>
        /// Registrado (na tab)
        /// PopUp
        /// Como Janela ativa
        /// </value>
        protected EstadosDosComponentes estado;

        /// <summary>
        /// Conjunto de tableAdapter para mater atualizados quando o componete estiver visivel
        /// </summary>
        protected VirDB.VirtualTableAdapter[] AtualizarTabelas;

        #endregion

        #region Propriedades

        /// <summary>
        /// Somente leitura
        /// </summary>
        private bool _somenteleitura;

        /// <summary>
        /// Somente leitura
        /// </summary>
        [Category("Virtual Software"), Description("Somente Leitura")]
        public virtual bool somenteleitura
        {
            get
            {
                return _somenteleitura;
            }
            set
            {
                bool disparar = (_somenteleitura != value);
                _somenteleitura = value;
                if ((disparar) && (somenteleituraChange != null))
                    somenteleituraChange(this, new EventArgs());
            }
        }

        /// <summary>
        /// Evento que ocorre ao mudar o valor de somenteleitura
        /// </summary>
        [Category("Virtual Software")]
        [Description("Altera��o de somenteleitura")]
        protected event EventHandler somenteleituraChange;


        /// <summary>
        /// T�tulo
        /// </summary>
        [Category("Virtual Software"), Description("T�tulo do componente no TAB")]
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]

        public string Titulo
        {
            get
            {
                if (CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao)
                    return titulo;
                else
                    if (FormPrincipalBase.MostrarTiposNaTab)
                    return string.Format("{0} - {1}", GetType().FullName, titulo);
                else
                    return titulo;
            }
            set { titulo = value; }
        }

        /// <summary>
        /// Visibilidade da tab que cont�m o controle
        /// </summary>
        /// <param name="Visivel"></param>
        /// <returns>retorna como ficou</returns>
        public bool TabVisivel(bool Visivel)
        {
            if ((Parent == null) || !(Parent is XtraTabPage))
                return false;
            else
            {
                XtraTabPage TabConteiner = (XtraTabPage)Parent;
                TabConteiner.PageVisible = Visivel;
                return TabConteiner.PageVisible;
            }
        }

        /// <summary>
        /// Visibilidade da tab que cont�m o controle
        /// </summary>        
        /// <returns>retorna como ficou</returns>
        public bool TabVisivel()
        {
            if ((Parent == null) || !(Parent is XtraTabPage))
                return false;
            else
            {
                XtraTabPage TabConteiner = (XtraTabPage)Parent;
                return TabConteiner.PageVisible = TabConteiner.PageVisible;
            }
        }
        

        /// <summary>
        /// Define se o componente deve ser mostrado em tela cheia
        /// </summary>
        /// <remarks>grava na propriedade DOCK</remarks>
        /// <value>DockStyle</value>
        [Category("Virtual Software"), Description("T�tulo do componente no TAB")]
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public DockStyle Doc
        {
            get
            {
                return this.Dock;
            }
            set
            {
                this.Dock = value;
            }
        }        

        /// <summary>
        /// Indica o estado de apresenta�ao do componente
        /// </summary>
        /// <value>
        /// Registrado (na tab)
        /// PopUp
        /// Como Janela ativa
        /// </value>
        [Category("Virtual Software"), Description("Indica o estado de apresenta��o do componente")]
        public EstadosDosComponentes Estado
        {
            get { return estado; }
            set { estado = value; }
        }

        #endregion

        #region M�todos

        /// <summary>
        /// Liga/Desliga a atualiza��o das tabela com timer
        /// </summary>
        /// <param name="Ativa"></param>
        //public void AtivarAtualizarTabelas(bool Ativa){
        internal void AtivarAtualizarTabelas(bool Ativa)
        {
            //desligado
            //if (AtualizarTabelas != null)
            //  foreach (VirDB.VirtualTableAdapter virtTA in AtualizarTabelas)
            //    virtTA.AtivarAtualizacao(Ativa, this.GetHashCode());
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteBase()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Left padrao do componente no form
        /// </summary>
        protected int Left0 = 3;
        /// <summary>
        /// Top padrao do componente no form
        /// </summary>
        protected int Top0 = 4;

        /// <summary>
        /// Abre o componete em formulario pr�prio
        /// </summary>
        /// <returns></returns>
        public DialogResult ShowEmPopUp()
        {
            FormBase Formulario = new FormBase();
            Formulario.CaixaAltaGeral = CaixaAltaGeral;
            if (FormPrincipalBase.FormPrincipal != null)
                Formulario.Parent = FormPrincipalBase.FormPrincipal.Parent;
            this.Estado = EstadosDosComponentes.PopUp;
            Formulario.Controls.Add(this);
            this.Left = this.Left0;
            this.Top = this.Top0;
            Formulario.Width = Formulario.Height = 100;

            Formulario.FormBorderStyle = FormBorderStyle.FixedDialog;
            Formulario.ControlBox = true;
            Formulario.MaximizeBox = false;
            Formulario.MinimizeBox = false;
            if (this.Doc == DockStyle.Fill)
            {
                Formulario.Width = Convert.ToInt32(0.9 * Screen.PrimaryScreen.WorkingArea.Width);
                Formulario.Height = Convert.ToInt32(0.9 * Screen.PrimaryScreen.WorkingArea.Height);
                Formulario.Left = Convert.ToInt32(0.05 * (Screen.PrimaryScreen.WorkingArea.Width));
                Formulario.Top = Convert.ToInt32(0.05 * (Screen.PrimaryScreen.WorkingArea.Height));
            }
            else
                Formulario.AutoSize = true;
            //Formulario.StartPosition = FormStartPosition.CenterParent;
            Formulario.StartPosition = FormStartPosition.CenterScreen;
            Formulario.Text = this.titulo;
            Formulario.InstalaEventos(this, true, false, this.CamposObrigatorios);
            Visible = true;
            return Formulario.ShowDialog();
        }

        /// <summary>
        /// Abre outro m�dulo
        /// </summary>        
        /// <param name="CB">Componente pre-instanciado</param>
        /// <param name="Estado">Como o componente ser� mostratrado:JanelaAtiva,PopUp ou Registrado</param>        
        /// <returns>Como o di�logo foi fechado</returns>
        public static DialogResult ShowModuloSTA(EstadosDosComponentes Estado, ComponenteBase CB)
        {
            switch (Estado)
            {
                case EstadosDosComponentes.JanelasAtivas:
                    if (FormPrincipalBase.FormPrincipal != null)
                        FormPrincipalBase.FormPrincipal.VirShowMoludo(CB, Estado);
                    break;
                case EstadosDosComponentes.PopUp:
                    return CB.ShowEmPopUp();
                case EstadosDosComponentes.Registrado:
                    break;

            };
            return DialogResult.None;

        }

        /// <summary>
        /// Abre o m�dulo
        /// </summary>                                
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult VirShowModulo()
        {
            return VirShowModulo(Estado);
        }

        /// <summary>
        /// Abre o m�dulo
        /// </summary>
        /// <param name="_Estado">Estado do m�dulo</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult VirShowModulo(EstadosDosComponentes _Estado)
        {
            return ComponenteBase.ShowModuloSTA(_Estado, this);
        }

        /*
        /// <summary>
        /// Abre o m�dulo - NAO USAR substitu�do por VirShowModulo
        /// </summary>
        /// <param name="_Estado">Estado do m�dulo</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(EstadosDosComponentes _Estado)
        {
            return ComponenteBase.ShowModuloSTA(_Estado, this);
        }

         */

        /// <summary>
        /// FUN��O DESCONTINUALDA (USAR SHOWMODULO)
        /// </summary>
        private DialogResult ShowInForm()
        {
            using (FormBase Formulario = new FormBase())
            {
                Formulario.AutoSize = true;

                this.Parent = Formulario;

                return Formulario.ShowDialog();
            }
        }

        /// <summary>
        /// Formulario que contem o componente
        /// </summary>
        /// <remarks>Se formul�rio pai n�o for do tipo FormPrincipalBase ent�o a fun��o retorna null</remarks>
        /// <returns>Formulario que contem o componente</returns>
        protected FormPrincipalBase ForPai()
        {
            Control CompPai = this.Parent;
            while ((!(CompPai is FormPrincipalBase)) && (CompPai.Parent != null))
                CompPai = CompPai.Parent;
            if (CompPai is FormPrincipalBase)
                return (FormPrincipalBase)CompPai;
            else
                return null;
        }


        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <remarks>DataSet e binding pr�prio</remarks>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="somenteleitura">Somente leitura</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(Type Tipo, bool somenteleitura)
        //public Object ShowModulo(Type Tipo, bool somenteleitura)
        {
            return ShowModulo(Tipo, (BindingSource)null, somenteleitura);
        }

        /// <summary>
        /// M�todo para atualizar os dados
        /// </summary>
        /// <remarks>
        /// � abstrato e deve ser derivado.
        /// J� esta derivado no componentebasebindingsourse
        /// </remarks>
        public virtual void RefreshDados() { }

        /// <summary>
        /// M�todo para atualizar os dados de forma for�ada. � chamado pelo usur�rio. se nao derivado vai chamar o RefreshDados
        /// </summary>
        /// <remarks>
        /// � abstrato e deve ser derivado.
        /// J� esta derivado no componentebasebindingsourse
        /// </remarks>
        public virtual void RefreshDadosManual()
        {
            RefreshDados();
        }

        /// <summary>
        /// Fecha o formulario conteiner
        /// </summary>
        /// <param name="Resultado">DialogResult utilizado</param>
        /// <returns>DialogResult</returns>
        protected virtual void FechaTela(DialogResult Resultado)
        {
            RetornaChamada(Resultado);
            switch (estado)
            {
                case EstadosDosComponentes.PopUp:
                    ((Form)this.Parent).DialogResult = Resultado;
                    ((Form)this.Parent).Close();
                    break;
                case EstadosDosComponentes.JanelasAtivas:
                    ((XtraTabControl)this.Parent.Parent).TabPages.Remove((XtraTabPage)this.Parent);
                    this.Parent.Dispose();
                    this.Dispose();
                    break;
                //case EstadosDosComponentes.Registrado:
                //    TabVisivel(false);
                //    break;
                case EstadosDosComponentes.Registrado:
                default:
                    throw new NotImplementedException("N�o implementado para estado = '" + Estado.ToString() + "'");
            };

        }


        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="DS">DataSet a ser usado</param>
        /// <param name="somenteleitura">Somente leitura</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(Type Tipo, DataSet DS, bool somenteleitura)
        {
            if (!Tipo.IsSubclassOf(typeof(ComponenteBase)))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteBase'");
            if ((DS != null) && (!Tipo.IsSubclassOf(typeof(ComponenteCamposBase))))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteCamposBase'");

            ComponenteBase CB = (ComponenteBase)(Tipo.GetConstructor(Type.EmptyTypes).Invoke(null));
            if ((DS != null) && (CB is ComponenteBaseBindingSource))
            {
                ((ComponenteBaseBindingSource)CB).somenteleitura = somenteleitura;
                ((ComponenteBaseBindingSource)CB).SubstituaDataSet(DS);
            };

            return ShowModulo(CB, null);
        }


        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="Binding">Usar o binding do indicado</param>
        /// <param name="somenteleitura">Somente leitura</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(Type Tipo, BindingSource Binding, bool somenteleitura)
        {
            if (!Tipo.IsSubclassOf(typeof(ComponenteBase)))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteBase'");
            if ((Binding != null) && (!Tipo.IsSubclassOf(typeof(ComponenteCamposBase))))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteCamposBase'");

            ComponenteBase CB = (ComponenteBase)(Tipo.GetConstructor(Type.EmptyTypes).Invoke(null));
            if (CB is ComponenteBaseBindingSource)
                ((ComponenteBaseBindingSource)CB).somenteleitura = somenteleitura;

            return ShowModulo(CB, Binding);
        }

        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>        
        /// <param name="CB">Componente pre-instanciado</param>
        /// <param name="Binding">Usar o binding do indicado</param>
        /// <returns>Como o di�logo foi fechado</returns>
        static public DialogResult ShowModulo(ComponenteBase CB, BindingSource Binding)
        {

            FormBase Formulario = new FormBase();
            //Formulario.Parent = this.ParentForm.Parent;
            Formulario.Parent = FormPrincipalBase.FormPrincipal.Parent;
            CB.Estado = EstadosDosComponentes.PopUp;
            Formulario.Controls.Add(CB);
            CB.Left = 3;
            CB.Top = 4;

            Formulario.FormBorderStyle = FormBorderStyle.FixedDialog;
            Formulario.ControlBox = true;
            Formulario.MaximizeBox = false;
            Formulario.MinimizeBox = false;
            if ((CB is ComponenteGradeBase) || (CB.Doc == DockStyle.Fill))
            {
                Formulario.Width = Convert.ToInt32(0.8 * Screen.PrimaryScreen.WorkingArea.Width);
                Formulario.Height = Convert.ToInt32(0.8 * Screen.PrimaryScreen.WorkingArea.Height);
                Formulario.Left = Convert.ToInt32(0.1 * (Screen.PrimaryScreen.WorkingArea.Width));
                Formulario.Top = Convert.ToInt32(0.1 * (Screen.PrimaryScreen.WorkingArea.Height));
            }
            else
                Formulario.AutoSize = true;

            Formulario.StartPosition = FormStartPosition.CenterParent;
            Formulario.Text = CB.titulo;

            if (Binding != null)
            {
                ((ComponenteCamposBase)CB).BindingSource_F.DataSource = Binding;
                ((BindingSource)(((ComponenteCamposBase)CB).BindingSource_F.DataSource)).Position = Binding.Position;
                ((ComponenteCamposBase)CB).BindingSource_F.Position = Binding.Position;
            };
            bool somenteleitura = false;
            if (CB is ComponenteBaseBindingSource)
                somenteleitura = ((ComponenteBaseBindingSource)CB).somenteleitura;
            Formulario.InstalaEventos(CB, true, somenteleitura, CB.CamposObrigatorios);
            return Formulario.ShowDialog();

        }

        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>        
        /// <param name="CB">Componente previamente istanciado</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(ComponenteBase CB)
        {
            return ShowModulo(CB, null);
        }

        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="DR">Registro a ser editado</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(Type Tipo, DataRow DR)
        {
            return ShowModulo(Tipo, (object)DR);
        }

        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <remarks>Chama a fun��o privada</remarks>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="DR">Registro a ser editado</param>
        /// <returns>Como o di�logo foi fechado</returns>
        public DialogResult ShowModulo(Type Tipo, DataRowView DR)
        {
            return ShowModulo(Tipo, (object)DR);
        }

        /// <summary>
        /// Abre outro m�dulo como PopUp
        /// </summary>
        /// <remarks>Esta � a fun��o real onde esta a programa��o. Ela � chamadas por outras fun��es externa que definem os parametros no lugar do 'object'</remarks>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="DR">Registro a ser editdo, pode ser DataRow ou DataRowViel</param>
        /// <returns>Como o di�logo foi fechado</returns>
        private DialogResult ShowModulo(Type Tipo, object DR)
        {

            if (!Tipo.IsSubclassOf(typeof(ComponenteBase)))
                throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteBase'");

            ComponenteBase CB = (ComponenteBase)(Tipo.GetConstructor(Type.EmptyTypes).Invoke(null));
            //if (CB is ComponenteBaseBindingSource)
            //    ((ComponenteBaseBindingSource)CB).somenteleitura = somenteleitura;

            FormBase Formulario = new FormBase();
            Formulario.Parent = this.ParentForm.Parent;
            CB.Estado = EstadosDosComponentes.PopUp;
            Formulario.Controls.Add(CB);
            CB.Left = 3;
            CB.Top = 4;

            Formulario.FormBorderStyle = FormBorderStyle.FixedDialog;
            Formulario.ControlBox = true;
            Formulario.MaximizeBox = false;
            Formulario.MinimizeBox = false;
            if (CB is ComponenteGradeBase)
            {
                Formulario.Width = Convert.ToInt32(0.8 * Screen.PrimaryScreen.WorkingArea.Width);
                Formulario.Height = Convert.ToInt32(0.8 * Screen.PrimaryScreen.WorkingArea.Height);
                Formulario.Left = Convert.ToInt32(0.1 * (Screen.PrimaryScreen.WorkingArea.Width));
                Formulario.Top = Convert.ToInt32(0.1 * (Screen.PrimaryScreen.WorkingArea.Height));
            }
            else
                Formulario.AutoSize = true;

            Formulario.StartPosition = FormStartPosition.CenterParent;
            Formulario.Text = CB.titulo;

            if (DR != null)
                ((ComponenteCamposBase)CB).BindingSource_F.DataSource = DR;
            ;
            bool somenteleitura = false;
            if (CB is ComponenteBaseBindingSource)
                somenteleitura = ((ComponenteBaseBindingSource)CB).somenteleitura;
            Formulario.InstalaEventos(CB, true, somenteleitura, CB.CamposObrigatorios);
            return Formulario.ShowDialog();

        }

        /// <summary>
        /// Abre a janela como PopUp com a includao de um registro
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <returns>PK do novo registro - 0 se cancelado e -1 se deu erro</returns>
        public Int32 ShowModuloAdd(Type Tipo)
        {
            return ShowModuloAdd(Tipo, string.Empty, null);
        }



        /// <summary>
        /// Abre a janela como PopUp com a includao de um registro
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="campo">Campo a ser iniciado</param>
        /// <param name="valor">Valor inicial do campo</param>
        /// <returns>PK do novo registro - 0 se cancelado e -1 se deu erro</returns>
        public Int32 ShowModuloAdd(Type Tipo, string campo, object valor)
        {
            System.Collections.SortedList campos = new SortedList();
            campos.Add(campo, valor);
            return ShowModuloAdd(Tipo, campos);
        }

        /// <summary>
        /// Abre a janela como PopUp com a includao de um registro
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="campos">Campos a serem iniciados (com seus valores)</param>        
        /// <returns>PK do novo registro - 0 se cancelado e -1 se deu erro</returns>
        static public Int32 ShowModuloAdd(Type Tipo, System.Collections.SortedList campos)
        {
            try
            {
                if (!Tipo.IsSubclassOf(typeof(ComponenteBaseBindingSource)))
                    throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteCamposBase'");
                ComponenteBaseBindingSource CB = (ComponenteBaseBindingSource)(Tipo.GetConstructor(Type.EmptyTypes).Invoke(null));
                CB.camposNovos = campos;
                CB.tipoDeCarga = TipoDeCarga.novo;
                CB.Fill();
                switch (ShowModulo(CB, null))
                {
                    case DialogResult.Cancel:
                        return 0;

                    case DialogResult.OK:
                        return (Int32)(((DataRowView)(CB.BindingSource_F.Current)).Row[0]);

                    default: return -1;
                };
            }
            catch (Exception Erro)
            {
                MessageBox.Show(Erro.Message);
                return -1;
            };
        }

        /// <summary>
        /// Abre a janela como PopUp com a includao de um registro
        /// </summary>
        /// <param name="Tipo">Type do m�dudo - exemplo  typeof(CompomentesBase)</param>
        /// <param name="DS">DataSet a utilizar</param>
        /// <returns>PK do novo registro - 0 se cancelado e -1 se deu erro</returns>
        public Int32 ShowModuloAdd(Type Tipo, DataSet DS)
        {
            try
            {
                if (!Tipo.IsSubclassOf(typeof(ComponenteBaseBindingSource)))
                    throw new ArgumentOutOfRangeException("'" + Tipo.Name + "' n�o � derivado de 'ComponenteCamposBase'");
                ComponenteBaseBindingSource CB = (ComponenteBaseBindingSource)(Tipo.GetConstructor(Type.EmptyTypes).Invoke(null));
                //object teste = CB.BindingSource_F.AddNew();
                //DataRowView Novalinha = (DataRowView)CB.BindingSource_F.AddNew();
                //if ((campo != null) && (campo.Trim() != ""))
                //    Novalinha[campo] = valor;
                CB.tipoDeCarga = TipoDeCarga.novo;
                CB.SubstituaDataSet(DS);
                switch (ShowModulo(CB, null))
                {
                    case DialogResult.Cancel:
                        return 0;

                    case DialogResult.OK:
                        return (Int32)(((DataRowView)(CB.BindingSource_F.Current)).Row[0]);

                    default: return -1;
                };
            }
            catch (Exception Erro)
            {
                MessageBox.Show(Erro.Message);
                return -1;
            };
        }

        /// <summary>
        /// Reponde se o formulario pode ser fechado
        /// </summary>
        /// <remarks>Evita sair sem salvar</remarks>
        public virtual bool CanClose()
        {
            return true;
        }
        #endregion

    }
}
