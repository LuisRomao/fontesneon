using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompontesBasicos
{
    internal partial class cHelp : CompontesBasicos.ComponenteBase
    {
        public cHelp()
        {
            InitializeComponent();
            Iniciar();
        }

        public cHelp(string _HomeURL)
        {
            InitializeComponent();
            if(_HomeURL != "")
                HomeURL = _HomeURL;
            Iniciar();            
        }

        private void Iniciar() 
        {
            componenteWEB1.webBrowser1.WebBrowserShortcutsEnabled = false;
            componenteWEB1.webBrowser1.IsWebBrowserContextMenuEnabled = false;            
        }

        public string HomeURL="http://virweb.com.br";

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            GoHome();
        }

        /// <summary>
        /// Navega para HomeURL
        /// </summary>
        public void GoHome() 
        {
            componenteWEB1.Navigate(HomeURL);
        }

        private void ClickBack(object sender, EventArgs e)
        {
            componenteWEB1.webBrowser1.GoBack();
        }

        private void ClickForward(object sender, EventArgs e)
        {
            componenteWEB1.webBrowser1.GoForward();
        }

    }
}

