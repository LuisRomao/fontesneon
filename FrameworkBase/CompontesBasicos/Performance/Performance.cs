using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CompontesBasicos.Performance
{
    /// <summary>
    /// Classe para avaliar performance em processos lentos localizando os gargalos
    /// </summary>
    [Obsolete("usar o PROC")]
    public class Performance
    {
        private static Performance _PerformanceST;

        /// <summary>
        /// Objeto est�tico
        /// </summary>
        public static Performance PerformanceST {
            get {
                if (_PerformanceST == null) {
                    _PerformanceST = new Performance();
                };
                return _PerformanceST;
            }
        }

        /// <summary>
        /// Ativa o recurso
        /// </summary>
        public static bool Ativado = false;

        /// <summary>
        /// 
        /// </summary>
        public SortedList<string,TimeSpan> Tempos;

        private DateTime Ref;

        /// <summary>
        /// Chave em uso
        /// </summary>
        public string ChaveAnterior;

        /// <summary>
        /// Cosntrutor padr�o
        /// </summary>
        public Performance() 
        {
            Tempos = new System.Collections.Generic.SortedList<string, TimeSpan>();
        }

        /// <summary>
        /// Reset da estat�stica
        /// </summary>
        public void Zerar() {
            if (!Ativado)
                return;
            Tempos.Clear();
            ChaveAnterior = null;
        }

        private Stack<string> stackChaves;

        private Stack<string> StackChaves
        {
            get 
            {
                return stackChaves ?? (stackChaves = new Stack<string>());
            }
        }        

        /// <summary>
        /// Marca o tempo deste ponto at� a proxima chamada
        /// </summary>
        /// <param name="Chave">Nome dado para o acumulador</param>
        /// <param name="Stack">Usa stack - retomar com StackPop</param>
        public void Registra(string Chave, bool Stack = false)
        {
            if (!Ativado)
                return;
            DateTime NovaRef = DateTime.Now;
            if (ChaveAnterior != null)
            {
                TimeSpan Delta = NovaRef - Ref;
                //TimeSpan Anterior = Tempos[ChaveAnterior];                
                Tempos[ChaveAnterior] += Delta;
            };
            if (Stack)
                StackChaves.Push(ChaveAnterior ?? "Geral");
            ChaveAnterior = Chave;
            Ref = NovaRef;
            if (Chave == null)            
                Ref = DateTime.Now;            
            else 
                if (!Tempos.ContainsKey(Chave))
                    Tempos.Add(Chave, new TimeSpan());            
        }

        /// <summary>
        /// Retorno do Stack - usar como para do Registra("",true)
        /// </summary>
        public void StackPop()
        {
            string Chave = StackChaves.Count == 0 ? "Geral" : StackChaves.Pop();
            Registra(Chave, false);
        }        

        /// <summary>
        /// Resultado
        /// </summary>
        /// <param name="SalvaClip"></param>
        /// <param name="AbreMessageBox"></param>
        /// <returns></returns>
        public string Relatorio(bool SalvaClip = false,bool AbreMessageBox = false) {
            if (!Ativado)
                return "";
            string Rel = "";
            double total = 0;            
            foreach (string Chave in Tempos.Keys)
            {
                Rel += string.Format("{0,-10} - {1:D2}:{2:D2}:{3:D4}\r\n", Chave, Tempos[Chave].Minutes, +Tempos[Chave].Seconds, Tempos[Chave].Milliseconds);
                total += Tempos[Chave].TotalSeconds;
            }
            if (total > 0) 
            {
                Rel += "\r\n";
                foreach (string Chave in Tempos.Keys)
                    Rel += string.Format("{0,-10} - {1:n2} %\r\n", Chave, 100 * Tempos[Chave].TotalSeconds / total);                                     
            }
            if (SalvaClip)
                Clipboard.SetText(Rel);
            if (AbreMessageBox)
                MessageBox.Show(Rel);
            return Rel;
        }
        
    }
}
