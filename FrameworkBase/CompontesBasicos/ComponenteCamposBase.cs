using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace CompontesBasicos
{
    /// <summary>
    /// Componente para editar/visualizar um �nico registro (com seus campos)
    /// </summary>
    public partial class ComponenteCamposBase : CompontesBasicos.ComponenteBaseBindingSource
    {

        private ComponenteGradeNavegador gradeChamadora;
        
        /// <summary>
        /// Indica qual componente ComponenteGradeNavegador efetuou a chamada
        /// </summary>
        public ComponenteGradeNavegador GradeChamadora
        {
            get { return gradeChamadora; }
            set 
            { 
                gradeChamadora = value;
                Registra();
            }
        }

        /// <summary>
        /// Efetua a carga autom�tica do registro
        /// </summary>
        protected override void FillAutomatico() {
            
            switch (tipoBuscaPk)
            {
                case ETipoBuscaPk.Padrao:
                    if ((metodoFillBy != null) && (tipoDeCarga == TipoDeCarga.pk))
                    {
                        BuscaRegistro(pk);
                        Ativado = true;
                    }
                    else
                        base.FillAutomatico();
                    break;
                case ETipoBuscaPk.Nenhum:
                    break;
                case ETipoBuscaPk.fill:
                    break;
                case ETipoBuscaPk.find:
                    Ativado = Find();
                    break;
                default:
                    break;
            }
            
        
        }

        private ETipoBuscaPk tipoBuscaPk = ETipoBuscaPk.Padrao;

        /// <summary>
        /// M�todo para localizar o registro
        /// </summary>
        [Category("Virtual Software")]
        [Description("M�todo para localizar o registro")]
        public ETipoBuscaPk TipoBuscaPk {
            get { return tipoBuscaPk;}
            set { tipoBuscaPk = value;}
        }

        private bool desabilitarAllowDBNull = true;

        /// <summary>
        /// Desabilita obrigatoriedade de allowDBNull durante a edi��o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Desabilitar AllowDBNull")]
        public bool DesabilitarAllowDBNull
        {
            get { return desabilitarAllowDBNull; }
            set { desabilitarAllowDBNull = value; }
        }

        /// <summary>
        /// Posiciona o editor no registro (PK)
        /// </summary>
        /// <param name="pk">Chave prim�ria</param>
        /// <returns>Retorna falso se o registro n�o for encontrado</returns>
        public bool BuscaRegistro(Int32 pk)
        {            
            if ((tableAdapterSelect != null) && (metodoFillBy != null)) {
                object[] Parametros = new Object[2];
                Parametros[0] = tabela;
                Parametros[1] = pk;
                metodoFillBy.Invoke(tableAdapterSelect,Parametros);
                if (tabela.Rows.Count == 1)
                {
                    LinhaMae_F = tabela.Rows[0];
                    return true;
                }
                else
                    return false;
            }
            else
                return false;

        }

        /// <summary>
        /// Construtor padr�o
        /// </summary>
        public ComponenteCamposBase()
        {
            InitializeComponent();
        }

        

        /// <summary>
        /// Clique do botao fechar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnFechar_F_ItemClick(object sender, ItemClickEventArgs e)
        {
            FechaTela(DialogResult.Cancel);
        }

        /// <summary>
        /// Clieque do bot�o confirma
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnConfirmar_F_ItemClick(object sender, ItemClickEventArgs e)
        {            
            if (Update_F())
                FechaTela(DialogResult.OK);             
        }

        /// <summary>
        /// Clique do botao cancelar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void btnCancelar_F_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cancela_F(false);
            /*
            if (DialogResult.Yes == MessageBox.Show("Confirma o cancelamento?", "Cancelar", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                BindingSource_F.CancelEdit();

                if (BindingSource_F.DataSource is DataSet)
                    (BindingSource_F.DataSource as DataSet).RejectChanges();
             
                FechaTela(DialogResult.Cancel);
            }
            */
        }

        private bool editavel = true;

        /// <summary>
        /// Indica se o componete permite edicao
        /// </summary>
        [Category("Virtual Software")]
        [Description("Indica se o componete permite edicao")]
        public bool Editavel {
            get { return editavel; }
            set { editavel = value; }
        }

        private bool semBotoes;

        /// <summary>
        /// Elimina a barra com os botoes (ideal para quando o componente for colocado detro de outro)
        /// </summary>
        [Category("Virtual Software")]
        [Description("Elimina a barra com os botoes (ideal para quando o componente for colocado dentro de outro)")]
        public bool SemBotoes
        {
            get { return semBotoes; }
            set {                
                semBotoes = value;
                barComponente_F.Visible = !semBotoes;
            }
        }

        private void ComponenteCamposBase_Load(object sender, EventArgs e)
        {
            if (!editavel) {
                btnCancelar_F.Visibility = btnConfirmar_F.Visibility = BarItemVisibility.Never;
                btnFechar_F.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnCancelar_F.Visibility = btnConfirmar_F.Visibility = (somenteleitura ? BarItemVisibility.Never : BarItemVisibility.Always);
                btnFechar_F.Visibility = (somenteleitura ? BarItemVisibility.Always : BarItemVisibility.Never);
            }
            if (!DesignMode)
            {
                //BindingSourcePrincipal.DataSource
                
                if ((colunaInclusao != null) && (colunaAlteracao != null) && (colunaDataInclusao != null) && (colunaDataAlteracao != null) && (BindingSourcePrincipal != null))
                {
                    DataRow row=null;
                    if ((BindingSourcePrincipal != null) && (BindingSourcePrincipal.Current != null))
                    {
                        if (BindingSourcePrincipal.Current is DataRowView)
                        {
                            DataRowView DRV = (DataRowView)BindingSourcePrincipal.Current;
                            if (DRV != null)
                                row = DRV.Row;
                        };
                        if (BindingSourcePrincipal.Current.GetType().IsSubclassOf(typeof(DataRow)) || (BindingSourcePrincipal.Current is DataRow))
                            row = (DataRow)BindingSourcePrincipal.Current;
                        if (row != null)
                        {
                            stxUsuarioInclusao_F.Caption = String.Format("Inclus�o:{0}/{1}", row[colunaDataInclusao], Bancovirtual.UsuarioLogado.UsuarioLogadoSt.BuscaUSUNome(row[colunaInclusao]));
                            stxUsuarioAtualizacao_F.Caption = String.Format("Altera��o:{0}/{1}", row[colunaDataAlteracao], Bancovirtual.UsuarioLogado.UsuarioLogadoSt.BuscaUSUNome(row[colunaAlteracao]));
                        }
                    };
                }
                else{
                    if ((colunaDataInclusao != null) && (colunaDataAlteracao != null) && (BindingSourcePrincipal != null))
                    {
                        DataRow row = null;
                        if ((BindingSourcePrincipal != null) && (BindingSourcePrincipal.Current != null))
                        {
                            if (BindingSourcePrincipal.Current is DataRowView)
                            {
                                DataRowView DRV = (DataRowView)BindingSourcePrincipal.Current;
                                if (DRV != null)
                                    row = DRV.Row;
                            };
                            if (BindingSourcePrincipal.Current.GetType().IsSubclassOf(typeof(DataRow)) || (BindingSourcePrincipal.Current is DataRow))
                                row = (DataRow)BindingSourcePrincipal.Current;
                            if (row != null)
                            {
                                stxUsuarioInclusao_F.Caption = "Inclus�o:" + row[colunaDataInclusao];
                                stxUsuarioAtualizacao_F.Caption = "Altera��o:" + row[colunaDataAlteracao];
                            }
                        };
                    };
                }
            };
        }

        /// <summary>
        /// Responde se o programa pode ser fechado ou existe alguma pend�ncia com este componente (confirma/cancela)
        /// </summary>
        /// <returns></returns>
        public override bool CanClose()
        {
            if (!editavel)
                return true;
            if (somenteleitura)
                return true;
            else
            {
                return false;
            };
        }

        private void Registra()
        {
            if (gradeChamadora != null)
                GradeChamadora.RegistraFilhote(this);
        }

        private void DesRegistra()
        {
            if (gradeChamadora != null)
                GradeChamadora.DesRegistraFilhote(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Resultado"></param>
        protected override void FechaTela(DialogResult Resultado)
        {
            DesRegistra();
            base.FechaTela(Resultado);
        } 


    }

    /// <summary>
    /// M�todo para localizar o registro
    /// </summary>
    public enum ETipoBuscaPk { 
        /// <summary>
        /// Procura autom�tica
        /// </summary>
        Padrao,
        /// <summary>
        /// N�o faz busca. usar eventos.
        /// </summary>
        Nenhum,
        /// <summary>
        /// Chama um comando Fill
        /// </summary>        
        fill,
        /// <summary>
        /// Localiza o registro na tabela j� populada
        /// </summary>
        find
    }
}

