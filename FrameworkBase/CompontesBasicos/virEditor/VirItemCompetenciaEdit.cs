﻿using System;
using System.ComponentModel;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.Registrator;

namespace CompontesBasicos
{
    /// <summary>
    /// Componente para grid competência.
    /// </summary>
    [UserRepositoryItem("Register")]
    public class RepositoryItemCompetenciaEdit : RepositoryItemButtonEdit
    {

        /// <summary>
        /// Construtor estático
        /// </summary>
        static RepositoryItemCompetenciaEdit()
        {
            Register();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public RepositoryItemCompetenciaEdit() 
        {
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this.AutoHeight = false;            
            this.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] 
                            {
                               new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Left, 
                                                                                "", -1, true, true, true, DevExpress.XtraEditors.ImageLocation.MiddleCenter,
                                                                                null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), 
                                                                                serializableAppearanceObject1, "", null, null, true),
                               new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Right)
                            });
            this.Buttons[0].Visible = false;
            this.Mask.EditMask = "00/0000";
            this.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;            
            this.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(_ButtonClick);
            this.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(_ParseEditValue);
            this.FormatEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(_FormatEditValue);
            this.Enter += new System.EventHandler(_Enter);
        }

        internal const string EditorName = "CompetenciaEdit";

        /// <summary>
        /// Método necessário para o registro
        /// </summary>
        public static void Register()
        {
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(EditorName,
                                                                           typeof(CompetenciaEdit),
                                                                           typeof(RepositoryItemCompetenciaEdit),
                                                                           typeof(DevExpress.XtraEditors.ViewInfo.ButtonEditViewInfo),
                                                                           new DevExpress.XtraEditors.Drawing.ButtonEditPainter(),
                                                                           true,
                                                                           null,
                                                                           typeof(DevExpress.Accessibility.ButtonEditAccessible)));
        }

        /// <summary>
        /// Método necessário para o registro
        /// </summary>
        public override string EditorTypeName
        {
            get { return EditorName; }
        }

        private string Format(int Original)
        {
            return string.Format("{0:00}/{1:0000}", Original % 100, Original / 100);
        }

        private void _FormatEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if ((e.Value == null) || (!(e.Value is int)))
                return;            
            e.Value = Format((int)e.Value);
            e.Handled = true;
        }

        private void _ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (e.Value == null)
            {                
                e.Value = DBNull.Value;
                e.Handled = true;
                return;
            }
            if (!(e.Value is string))
            {                
                e.Handled = true;
                return;
            }
            string Original = (string)e.Value;
            if (Original.Length != 7)
            {             
                e.Value = DBNull.Value;
                e.Handled = true;
                return;
            }
            int mes;
            int ano;
            if (int.TryParse(Original.Substring(0, 2), out mes) && int.TryParse(Original.Substring(3, 4), out ano))
            {
                int Formatado = ano * 100 + mes;             
                e.Value = Formatado;
                e.Handled = true;
            }
        }

        private void _ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            if (ReadOnly)
                return;
            int mes;
            int ano;
            DevExpress.XtraEditors.ButtonEdit Editor = (DevExpress.XtraEditors.ButtonEdit)sender;
            if (!Editor.Enabled)
                return;            
            if (Editor.EditValue is int)
            {
                int Original = (int)Editor.EditValue;
                mes = Original % 100;
                ano = Original / 100;
            }
            else if (Editor.EditValue is string)
            {
                string Original = (string)Editor.EditValue;
                if ((Original.Length != 7) || !int.TryParse(Original.Substring(0, 2), out mes) || !int.TryParse(Original.Substring(3, 4), out ano))
                    return;
            }
            else
                return;
            switch (e.Button.Kind)
            {
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Left:
                    mes--;
                    if (mes == 0)
                    {
                        mes = 12;
                        ano--;
                    }
                    break;
                case DevExpress.XtraEditors.Controls.ButtonPredefines.Right:
                    mes++;
                    if (mes == 13)
                    {
                        mes = 1;
                        ano++;
                    }
                    break;
            }
            int Valor = mes + ano * 100;
            Editor.EditValue = Valor;
            Editor.Text = Format(Valor);
        }

        private void _Enter(object sender, EventArgs e)
        {
            for (int i = 0; i < Buttons.Count; i++)
                Buttons[i].Enabled = !ReadOnly;
        }
    }
}