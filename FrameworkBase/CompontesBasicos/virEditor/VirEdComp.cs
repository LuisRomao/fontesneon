﻿using System.ComponentModel;
using DevExpress.XtraEditors;

namespace CompontesBasicos
{    
    /// <summary>
    /// Editor para competência
    /// </summary>
    public class CompetenciaEdit : ButtonEdit {

        /// <summary>
        /// Construtor estático
        /// </summary>
        static CompetenciaEdit() {
            RepositoryItemCompetenciaEdit.Register();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public CompetenciaEdit() {}

        /// <summary>
        /// Nome
        /// </summary>
        public override string EditorTypeName {
            get { return RepositoryItemCompetenciaEdit.EditorName; } 
        }


        /// <summary>
        /// Properties
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]        
        public new RepositoryItemCompetenciaEdit Properties
        {
            get { return base.Properties as RepositoryItemCompetenciaEdit; } 
        }

        /*
        protected override void OnClickButton(DevExpress.XtraEditors.Drawing.EditorButtonObjectInfoArgs buttonInfo) {
            ShowPopupForm();
            base.OnClickButton(buttonInfo);
        }

        protected virtual void ShowPopupForm() {
            using(Form form = new Form()) {

                form.StartPosition = FormStartPosition.Manual;

                form.Location = this.PointToScreen(new Point(0, Height));

                form.ShowDialog();

            }

        }
        */
    }
}