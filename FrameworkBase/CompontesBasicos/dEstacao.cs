﻿namespace CompontesBasicos {

    /// <summary>
    /// Classe para armazenar os dados da estação
    /// </summary>
    partial class dEstacao
    {
        partial class DadosDataTable
        {
        }
    
        private static dEstacao _dEstacaoSt;

        /// <summary>
        /// dataset estático:dEstacao
        /// </summary>
        public static dEstacao dEstacaoSt
        {
            get
            {
                if (_dEstacaoSt == null)
                {
                    _dEstacaoSt = new dEstacao();
                    if (!System.IO.Directory.Exists(System.Windows.Forms.Application.StartupPath + "\\XML"))
                        System.IO.Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + "\\XML");
                    if (System.IO.File.Exists(System.Windows.Forms.Application.StartupPath + "\\XML\\Estacao.xml"))
                        _dEstacaoSt.Dados.ReadXml(System.Windows.Forms.Application.StartupPath + "\\XML\\Estacao.xml");
                };
                return _dEstacaoSt;
            }
        }

        /// <summary>
        /// Le dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <returns></returns>
        public string GetValor(string Chave)
        {
            DadosRow row = Dados.FindByChave(Chave);
            return ((row == null) || (row.IsValorNull())) ? "" : row.Valor;
        }

        /// <summary>
        /// Le dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        public string GetValor(string Chave,string Padrao)
        {            
            DadosRow row = Dados.FindByChave(Chave);
            if ((row == null) || (row.IsValorNull()))
            {
                SetValor(Chave, Padrao);
                return Padrao;
            }
            else
                return row.Valor;
        }

        /// <summary>
        /// Le dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        public int GetValor(string Chave,int Padrao)
        {
            int Retornar;
            string Lido = GetValor(Chave);
            if (!int.TryParse(Lido,out Retornar)) 
            {
                Retornar = Padrao;
                SetValor(Chave, Retornar.ToString());
            }
            return Retornar;
        }

        /// <summary>
        /// Le dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Padrao"></param>
        /// <returns></returns>
        public bool GetValor(string Chave, bool Padrao)
        {
            bool Retornar;
            string Lido = GetValor(Chave);
            int intRetornar;
            if (!int.TryParse(Lido, out intRetornar))
            {
                Retornar = Padrao;
                SetValor(Chave, Retornar.ToString());
            }
            else 
                Retornar = intRetornar == 1;
            return Retornar;
        }

        /*
        /// <summary>
        /// Grava dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Valor"></param>
        public void SetValor(string Chave,string Valor)
        {
            DadosRow row = Dados.FindByChave(Chave);
            if (row == null)
                Dados.AddDadosRow(Chave, Valor);
            else
                row.Valor = Valor;
            _dEstacaoSt.Dados.WriteXml(System.Windows.Forms.Application.StartupPath + "\\XML\\Estacao.xml");
                
        }

        /// <summary>
        /// Grava dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Valor"></param>
        public void SetValor(string Chave, bool Valor)
        {
            SetValor(Chave, Valor ? "1" : "0");
        }
        */

        /// <summary>
        /// Grava dados da estação
        /// </summary>
        /// <param name="Chave"></param>
        /// <param name="Valor"></param>
        public void SetValor(string Chave, object Valor)
        {
            if (Valor is bool)
                SetValor(Chave, (bool)Valor ? "1" : "0");
            else
            {
                string strValor = Valor.ToString();
                DadosRow row = Dados.FindByChave(Chave);
                if (row == null)
                    Dados.AddDadosRow(Chave, strValor);
                else
                    row.Valor = strValor;
                _dEstacaoSt.Dados.WriteXml(System.Windows.Forms.Application.StartupPath + "\\XML\\Estacao.xml");
            }
            //SetValor(Chave, Valor.ToString());
        }
    }
}
