using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompontesBasicos
{
    public partial class ComponenteTabCampos : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Componente ComponenteCamposGroupTabControl conteiner
        /// </summary>
        public ComponenteCamposGroupTabControl TabMae;

        /// <summary>
        /// Construtor padrao
        /// </summary>
        public ComponenteTabCampos()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_TabMae">Componente ComponenteCamposGroupTabControl conteiner</param>
        public ComponenteTabCampos(ComponenteCamposGroupTabControl _TabMae)
        {
            InitializeComponent();
            TabMae = _TabMae;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tarefa"></param>
        /// <returns></returns>
        public virtual bool TarefaParaTodos(object Tarefa)
        {
            return true;
        }

        /// <summary>
        /// M�todo virtual para aviso de troca de p�gina
        /// </summary>
        /// <param name="Entrando">Entrando ou saido</param>
        public virtual void OnTrocaPagina(bool Entrando){
            //M�todo abstrato para ser derivado
        }
    }
}

