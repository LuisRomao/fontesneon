using System;
using System.Windows.Forms;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class FEspera : CompontesBasicos.FormBase
    {
        /// <summary>
        /// 
        /// </summary>
        public static FEspera instanciaFEspera;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Motivo"></param>
        public static void Espere(string Motivo) {
            if (instanciaFEspera == null)
                instanciaFEspera = new FEspera();
            instanciaFEspera.Motivo.Text=Motivo;
            instanciaFEspera.progressBarControl1.Visible = false;
            instanciaFEspera.Show();            
            Application.DoEvents();
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="max"></param>
        public static void AtivaGauge(Int32 max) { 
          if(instanciaFEspera != null){
              instanciaFEspera.progressBarControl1.Properties.Maximum = max;
              instanciaFEspera.progressBarControl1.EditValue = 0;
              instanciaFEspera.progressBarControl1.Visible = true;
          }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Posicao"></param>
        public static void Gauge(Int32 Posicao)
        {
            if (instanciaFEspera != null)
            {
                instanciaFEspera.progressBarControl1.EditValue = Posicao;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Gauge()
        {
            if (instanciaFEspera != null)
            {
                instanciaFEspera.progressBarControl1.EditValue = 1 + (Int32)instanciaFEspera.progressBarControl1.EditValue;
                Application.DoEvents();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Espere() {
            if(instanciaFEspera != null)
                 instanciaFEspera.Visible = false;
        }
        
        
        /// <summary>
        /// 
        /// </summary>
        public FEspera()
        {
            InitializeComponent();
        }

        private void FEspera_FormClosed(object sender, FormClosedEventArgs e)
        {
            instanciaFEspera = null;
        }
    }
}

