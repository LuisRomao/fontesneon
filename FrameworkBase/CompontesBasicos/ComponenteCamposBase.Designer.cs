namespace CompontesBasicos
{
    partial class ComponenteCamposBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ComponenteCamposBase));
            this.BarManager_F = new DevExpress.XtraBars.BarManager(this.components);
            this.barStatus_F = new DevExpress.XtraBars.Bar();
            this.stxUsuarioInclusao_F = new DevExpress.XtraBars.BarStaticItem();
            this.stxUsuarioAtualizacao_F = new DevExpress.XtraBars.BarStaticItem();
            this.barComponente_F = new DevExpress.XtraBars.Bar();
            this.btnConfirmar_F = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelar_F = new DevExpress.XtraBars.BarButtonItem();
            this.btnFechar_F = new DevExpress.XtraBars.BarButtonItem();
            this.BarAndDockingController_F = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).BeginInit();
            this.SuspendLayout();
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(237)))), ((int)(((byte)(250)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(69)))), ((int)(((byte)(141)))));
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(253)))), ((int)(((byte)(181)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            
            // 
            // BarManager_F
            // 
            this.BarManager_F.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus_F,
            this.barComponente_F});
            this.BarManager_F.Categories.AddRange(new DevExpress.XtraBars.BarManagerCategory[] {
            new DevExpress.XtraBars.BarManagerCategory("Status", new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36")),
            new DevExpress.XtraBars.BarManagerCategory("Componente", new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5"))});
            this.BarManager_F.Controller = this.BarAndDockingController_F;
            this.BarManager_F.DockControls.Add(this.barDockControlTop);
            this.BarManager_F.DockControls.Add(this.barDockControlBottom);
            this.BarManager_F.DockControls.Add(this.barDockControlLeft);
            this.BarManager_F.DockControls.Add(this.barDockControlRight);
            this.BarManager_F.Form = this;
            this.BarManager_F.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.stxUsuarioInclusao_F,
            this.stxUsuarioAtualizacao_F,
            this.btnConfirmar_F,
            this.btnCancelar_F,
            this.btnFechar_F});
            this.BarManager_F.MaxItemId = 9;
            this.BarManager_F.StatusBar = this.barStatus_F;
            // 
            // barStatus_F
            // 
            this.barStatus_F.BarName = "Status";
            this.barStatus_F.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus_F.DockCol = 0;
            this.barStatus_F.DockRow = 0;
            this.barStatus_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.stxUsuarioInclusao_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.stxUsuarioAtualizacao_F, true)});
            this.barStatus_F.OptionsBar.AllowQuickCustomization = false;
            this.barStatus_F.OptionsBar.DisableClose = true;
            this.barStatus_F.OptionsBar.DisableCustomization = true;
            this.barStatus_F.OptionsBar.DrawDragBorder = false;
            this.barStatus_F.OptionsBar.DrawSizeGrip = true;
            this.barStatus_F.OptionsBar.UseWholeRow = true;
            this.barStatus_F.Text = "Status";
            // 
            // stxUsuarioInclusao_F
            // 
            this.stxUsuarioInclusao_F.Caption = "Data/Usu�rio Inclus�o";
            this.stxUsuarioInclusao_F.CategoryGuid = new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36");
            this.stxUsuarioInclusao_F.Id = 0;
            this.stxUsuarioInclusao_F.Name = "stxUsuarioInclusao_F";
            this.stxUsuarioInclusao_F.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // stxUsuarioAtualizacao_F
            // 
            this.stxUsuarioAtualizacao_F.Caption = "Data/Usu�rio Atualiza��o";
            this.stxUsuarioAtualizacao_F.CategoryGuid = new System.Guid("692b8328-a3c6-410d-b260-81999a0a8d36");
            this.stxUsuarioAtualizacao_F.Id = 2;
            this.stxUsuarioAtualizacao_F.Name = "stxUsuarioAtualizacao_F";
            this.stxUsuarioAtualizacao_F.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barComponente_F
            // 
            this.barComponente_F.BarName = "Componente";
            this.barComponente_F.DockCol = 0;
            this.barComponente_F.DockRow = 0;
            this.barComponente_F.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barComponente_F.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnConfirmar_F, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCancelar_F),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnFechar_F)});
            this.barComponente_F.OptionsBar.AllowQuickCustomization = false;
            this.barComponente_F.OptionsBar.DisableClose = true;
            this.barComponente_F.OptionsBar.DisableCustomization = true;
            this.barComponente_F.OptionsBar.DrawDragBorder = false;
            this.barComponente_F.OptionsBar.UseWholeRow = true;
            this.barComponente_F.Text = "Componente";
            // 
            // btnConfirmar_F
            // 
            this.btnConfirmar_F.Caption = "C&onfirmar";
            this.btnConfirmar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnConfirmar_F.Glyph = ((System.Drawing.Image)(resources.GetObject("btnConfirmar_F.Glyph")));
            this.btnConfirmar_F.Hint = "Confirma as altera��es feitas no registro para o banco de dados.";
            this.btnConfirmar_F.Id = 6;
            this.btnConfirmar_F.Name = "btnConfirmar_F";
            this.btnConfirmar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnConfirmar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnConfirmar_F_ItemClick);
            // 
            // btnCancelar_F
            // 
            this.btnCancelar_F.Caption = "C&ancelar";
            this.btnCancelar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnCancelar_F.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCancelar_F.Glyph")));
            this.btnCancelar_F.Hint = "Cancela as altera��es feitas no registro.";
            this.btnCancelar_F.Id = 7;
            this.btnCancelar_F.Name = "btnCancelar_F";
            this.btnCancelar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCancelar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelar_F_ItemClick);
            // 
            // btnFechar_F
            // 
            this.btnFechar_F.Caption = "&Fechar";
            this.btnFechar_F.CategoryGuid = new System.Guid("c8d368e9-eb8d-4f73-b816-b76364b18eb5");
            this.btnFechar_F.Glyph = ((System.Drawing.Image)(resources.GetObject("btnFechar_F.Glyph")));
            this.btnFechar_F.Hint = "Fechar";
            this.btnFechar_F.Id = 8;
            this.btnFechar_F.Name = "btnFechar_F";
            this.btnFechar_F.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnFechar_F.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnFechar_F_ItemClick);
            // 
            // BarAndDockingController_F
            // 
            this.BarAndDockingController_F.PropertiesBar.AllowLinkLighting = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(519, 35);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 266);
            this.barDockControlBottom.Size = new System.Drawing.Size(519, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 35);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 231);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(519, 35);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 231);
            // 
            // ComponenteCamposBase
            // 
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteCamposBase";
            this.Load += new System.EventHandler(this.ComponenteCamposBase_Load);
            ((System.ComponentModel.ISupportInitialize)(this.BindingSource_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarManager_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BarAndDockingController_F)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        /// <summary>
        /// statusBar
        /// </summary>
        protected DevExpress.XtraBars.Bar barStatus_F;
        /// <summary>
        /// statusbar
        /// </summary>
        protected DevExpress.XtraBars.Bar barComponente_F;
        private DevExpress.XtraBars.BarStaticItem stxUsuarioAtualizacao_F;
        /// <summary>
        /// Botao confirmar
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnConfirmar_F;
        /// <summary>
        /// botao cancela
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnCancelar_F;
        /// <summary>
        /// botao fechar
        /// </summary>
        protected DevExpress.XtraBars.BarButtonItem btnFechar_F;
        /// <summary>
        /// usuario de inclusao
        /// </summary>
        protected DevExpress.XtraBars.BarStaticItem stxUsuarioInclusao_F;
        /// <summary>
        /// barmanager
        /// </summary>
        protected DevExpress.XtraBars.BarManager BarManager_F;
        /// <summary>
        /// BarAndDockingController_F
        /// </summary>
        protected DevExpress.XtraBars.BarAndDockingController BarAndDockingController_F;
        

    }
}
