namespace CompontesBasicos
{
    partial class ComponenteBase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.StyleController_F = new DevExpress.XtraEditors.StyleController(this.components);
            this.ToolTipController_F = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).BeginInit();
            this.SuspendLayout();
            // 
            // StyleController_F
            // 
            this.StyleController_F.AppearanceDisabled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceDisabled.Options.UseBackColor = true;
            this.StyleController_F.AppearanceDisabled.Options.UseForeColor = true;
            this.StyleController_F.AppearanceFocused.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.StyleController_F.AppearanceFocused.Options.UseBackColor = true;
            // 
            // ToolTipController_F
            // 
            this.ToolTipController_F.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ToolTipController_F.Appearance.ForeColor = System.Drawing.Color.Black;
            this.ToolTipController_F.Appearance.Options.UseBackColor = true;
            this.ToolTipController_F.Appearance.Options.UseFont = true;
            this.ToolTipController_F.Appearance.Options.UseForeColor = true;
            this.ToolTipController_F.Appearance.Options.UseTextOptions = true;
            this.ToolTipController_F.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.ToolTipController_F.Rounded = true;
            this.ToolTipController_F.ShowBeak = true;
            this.ToolTipController_F.ShowShadow = false;
            // 
            // ComponenteBase
            // 
            this.LookAndFeel.UseDefaultLookAndFeel = false;
            this.Name = "ComponenteBase";
            this.Size = new System.Drawing.Size(570, 291);
            this.Enter += new System.EventHandler(this.ComponenteBase_Enter);
            this.Leave += new System.EventHandler(this.ComponenteBase_Leave);
            ((System.ComponentModel.ISupportInitialize)(this.StyleController_F)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.Utils.ToolTipController ToolTipController_F;
        /// <summary>
        /// 
        /// </summary>
        protected DevExpress.XtraEditors.StyleController StyleController_F;
    }
}
