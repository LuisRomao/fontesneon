using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CompontesBasicos
{
    
    /// <summary>
    /// Navegador Virtual
    /// </summary>
    public partial class ComponenteWEB : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public ComponenteWEB()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Mostra ou n�o o quadro com a URL
        /// </summary>
        [Category("Virtual Software"), Description("Mostra ou n�o o quadro com a URL")]
        [DefaultValue(typeof(bool), "true")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool MostraEndereco {
            get {
                return panel1.Visible;
            }
            set {
                panel1.Visible = value;
                //webBrowser1.AllowNavigation = value;
            }
        }

        /// <summary>
        /// Navega
        /// </summary>
        /// <param name="URL">Destino</param>
        public void Navigate(string URL)
        {
            
            TURL.Text = URL;
            Navigate();
        }

        private void Navigate() {                        
            webBrowser1.Navigate(TURL.Text);
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            Navigate();
            
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            webBrowser1.Print();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Permite"></param>
        public void PermirtirImpressao(bool Permite)
        {
            panel2.Visible = Permite;
            webBrowser1.IsWebBrowserContextMenuEnabled = Permite;                                   
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            webBrowser1.ShowPrintPreviewDialog();
        }
    }
}

