using System;
using System.Drawing;
using System.Drawing.Design;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace CompontesBasicos
{    
    /// <summary>
    /// 
    /// </summary>
    public class ItenLista : Object{
        /// <summary>
        /// 
        /// </summary>
        public Type Tipo;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Tipo"></param>
        public ItenLista(Type Tipo) {
            this.Tipo = Tipo;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {            
            return Tipo.Name;
        }
    }

    class SelControleList : UITypeEditor
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            //use IWindowsFormsEditorService object to display a control in the dropdown area
            IWindowsFormsEditorService frmsvr = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (frmsvr == null)
                return null;
            ListBox Cb = new ListBox();
            Cb.Size = new Size(60, 120);
            Assembly[] Ass = AppDomain.CurrentDomain.GetAssemblies();
            if (Ass != null)
                foreach (Assembly As in Ass)
                {
                    Module[] Modulos = As.GetModules(false);
                    if (Modulos != null)
                    {
                        foreach (Module Modulo in Modulos)
                        {
                            if (Modulo != null)
                            {
                                try
                                {
                                    Type[] Tipos = Modulo.GetTypes();
                                    if (Tipos != null)
                                        foreach (Type Tipo in Tipos)
                                        {
                                            if (Tipo.IsSubclassOf(typeof(ComponenteBase)))
                                                Cb.Items.Add(new ItenLista(Tipo));
                                        };
                                }
                                catch { };
                            };
                        }
                    }
                };
            Cb.Sorted = true;
            frmsvr.DropDownControl(Cb);
            return ((ItenLista)Cb.SelectedItem).Tipo;
        }
    }

    class SelTableAdapterList : UITypeEditor
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override UITypeEditorEditStyle GetEditStyle(System.ComponentModel.ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.DropDown;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="provider"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object EditValue(System.ComponentModel.ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            //use IWindowsFormsEditorService object to display a control in the dropdown area
            IWindowsFormsEditorService frmsvr = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (frmsvr == null)
                return null;
            ListBox Cb = new ListBox();
            Cb.Size = new Size(60, 120);
            Cb.Items.Add("context.GetType() " + context.GetType());
            Cb.Items.Add("context.Tostring() " + context.ToString());
            Cb.Items.Add("this " + this.GetType());
            Cb.Items.Add("Instancia " + context.Instance.ToString());
            Cb.Items.Add("Instancia nome " + ((Control)context.Instance).Name.ToString());
            Cb.Items.Add("Classe "+context.Instance.GetType().ToString());
            BindingFlags bf = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            foreach(FieldInfo fi in context.Instance.GetType().GetFields(bf))
                Cb.Items.Add("Campo:" + fi.Name );
            /*foreach (Component Comp in ((ComponenteBase)context.Instance).Controls) {
                //if (Comp.GetType().GetMethod("Fill") != null)
                {
                    Cb.Items.Add(Comp);
                }
            };*/            
            frmsvr.DropDownControl(Cb);
            return Cb.SelectedItem;
        }
    }
}
