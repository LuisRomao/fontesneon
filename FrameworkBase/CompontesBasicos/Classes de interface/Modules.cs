using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraTab;

namespace CompontesBasicos {

    /// <summary>
    /// Chamada quando cadastra o m�dulo
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="Modulo">m�dulo</param>
    /// <param name="Abrir">Inicar aberto?</param>
    /// <param name="Cadastrar"></param>
    public delegate void ModuleInfoEventHandler(Object sender,ModuleInfo Modulo,ref bool Abrir,ref bool Cadastrar);
	
    /// <summary>
    /// Contains information about a module
    /// </summary>
	public class ModuleInfo {
        #region Campos
        private String name;
        private String moduleTypestr;
        private Type moduleType;
        private Image imagemP;
        private Image imagemG;
        private int imageIndexP;
        private int imageIndexG;
        private ComponenteBase module;
        private int grupo;
        private bool iniciarAberto;
        #endregion

        

        #region Metodos Estaticos
        /// <summary>
        /// Localiza um tipo pelo nome
        /// </summary>
        /// <param name="NomeComponente"></param>
        /// <returns></returns>
        public static Type BuscaType(string NomeComponente)
        {

            int PosPonto = NomeComponente.IndexOf('.');
            if (PosPonto <= 0)
                return null;
            System.Reflection.Assembly Ass;
            try
            {
                Ass = System.Reflection.Assembly.Load(NomeComponente.Substring(0, PosPonto));
            }
            catch
            {
                MessageBox.Show("Tentar em: " + @"C:\FontesVS\Geral Releases\DLLS\" + NomeComponente.Substring(0, PosPonto) + ".dll");
                Ass = System.Reflection.Assembly.LoadFrom(@"C:\FontesVS\Geral Releases\DLLS\" + NomeComponente.Substring(0, PosPonto) + ".dll");
            }
            return Ass.GetType(NomeComponente);

        }

        /// <summary>
        /// Constroi um objeto a partir de seu nome. Isso evita a carga da dll desnecessariamente
        /// </summary>
        /// <param name="Nome"></param>
        /// <param name="parametros"></param>
        /// <returns></returns>
        public static object ContruaObjeto(string Nome, params object[] parametros)
        {
            if (Nome != null)
            {
                Type Tipo = BuscaType(Nome);
                if (Tipo == null)
                    return null;
                if (parametros.Length == 0)
                {
                    System.Reflection.ConstructorInfo constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);
                    if (constructorInfoObj == null)
                        return null;
                    return constructorInfoObj.Invoke(null);
                }
                else
                {
                    Type[] Tipos = new Type[parametros.Length];
                    for (int i = 0; i < parametros.Length; i++)
                        Tipos[i] = parametros[i].GetType();
                    System.Reflection.ConstructorInfo constructorInfoObjP = Tipo.GetConstructor(Tipos);
                    if (constructorInfoObjP == null)
                        return null;
                    return constructorInfoObjP.Invoke(parametros);
                }
            }
            else
                return null;
        }

        #endregion

        #region Propriedades

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")] 
        [DefaultValue(typeof(bool), "false")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]        
        public bool IniciarAberto
        {
            get { return this.iniciarAberto; }
            set { this.iniciarAberto = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")] // Take this out, and you will soon have problems with serialization;
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Editor(typeof(SelControleList), typeof(UITypeEditor))]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Type ModuleType
        {
            get { return moduleType; }
            set { 
                moduleType = value;
                if (value != null)
                {
                    Type Tipo = (Type)value;
                    moduleTypestr = Tipo.FullName;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Image ImagemP
        {
            get { return imagemP; }
            set { imagemP = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int Grupo
        {
            get { return grupo; }
            set { grupo = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public Image ImagemG
        {
            get { return imagemG; }
            set { imagemG = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DefaultValue(typeof(String), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public String ModuleTypestr
        {
            get { return moduleTypestr; }
            set { 
                moduleTypestr = value;
                ModuleType = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DefaultValue(typeof(int), "0")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int ImageIndexP
        {
            get { return imageIndexP; }
            set { imageIndexP = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DefaultValue(typeof(int), "0")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public int ImageIndexG
        {
            get { return imageIndexG; }
            set { imageIndexG = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ComponenteBase Module { get { return this.module; } }

        #endregion

        #region Construtores
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="moduleType"></param>
        /// <param name="Grupo"></param>
        /// <param name="ImageIndexP"></param>
        /// <param name="ImageIndexG"></param>
        public ModuleInfo(string name, Type moduleType, int Grupo, int ImageIndexP, int ImageIndexG)
        {
            // throw exception if the module is not inherited from ComponeteBase class
            if (!moduleType.IsSubclassOf(typeof(ComponenteBase)))
                throw new ArgumentException("moduleClass has to be inherited from ComponeteBase");
            // if there is no any category yet, create the default one
            grupo = Grupo;
            this.name = name;
            this.imageIndexP = ImageIndexP;
            this.imageIndexG = ImageIndexG;
            this.moduleType = moduleType;
            this.module = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public ModuleInfo()
        {
        } 
        #endregion

        #region Metodos        
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="formprincipal"></param>
        public void Show(FormPrincipalBase formprincipal)
        {            
            CreateModule();
            formprincipal.ShowMoludointerno(module, name, EstadosDosComponentes.Registrado, false);
            /*
            if (FormPrincipalBase.strEmProducao)
                formprincipal.ShowMoludointerno(module, name, EstadosDosComponentes.Registrado, false);
            else
                formprincipal.ShowMoludointerno(module, string.Format("{0} - {1}", Module.Titulo, name), EstadosDosComponentes.Registrado, false);
            */
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ComponenteBase GetModulo()
        {
            CreateModule();
            return Module;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Hide()
        {
            if (module != null)
                ((XtraTabPage)(module.Parent)).PageVisible = false;
                                               
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dell()
        {
            if (module != null)
            {
                if ((module.Parent != null) && (module.Parent is XtraTabPage))
                {
                    XtraTabPage Page = (XtraTabPage)(module.Parent);
                    Page.TabControl.TabPages.Remove(Page);
                    module.Parent = null;
                }
                module = null;
            }

        }

        /// <summary>
        /// Cria um modulo
        /// </summary>
        protected void CreateModule()
        {
            if (this.module == null)
            {
                if (ModuleTypestr != null) {                    
                    Type Tipo = BuscaType(ModuleTypestr);
                    if(Tipo == null)
                        return;
                    //Type[] Tipos = NovoAss.GetTypes(ModuleTypestr);
                    System.Reflection.ConstructorInfo constructorInfoObj = Tipo.GetConstructor(Type.EmptyTypes);


                    if (constructorInfoObj == null)
                        return;
                    this.module = constructorInfoObj.Invoke(null) as ComponenteBase;
                    return;
                }                
            }
        } 

        #endregion
		
        
	}



	
    /// <summary>
    /// The list of modules registered in the system
    /// </summary>
	[ListBindable(false)]
	public class ModuleInfoCollection : CollectionBase {

        private ModuleInfo currentModule;

        #region Construtores

        /// <summary>
        /// 
        /// </summary>
        public ModuleInfoCollection()
        {
            this.currentModule = null;
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vModuleInfo"></param>
        /// <returns></returns>
        public ModuleInfo Add(ModuleInfo vModuleInfo)
        {
            this.InnerList.Add(vModuleInfo);
            return vModuleInfo;
        }
               
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vModuleInfo"></param>
        public void Remove(ModuleInfo vModuleInfo)
        {
            this.InnerList.Remove(vModuleInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vModuleInfo"></param>
        /// <returns></returns>
        public bool Contains(ModuleInfo vModuleInfo)
        {
            return this.InnerList.Contains(vModuleInfo);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tipo"></param>
        /// <returns></returns>
        public ModuleInfo this[Type tipo]
        {
            get {
                foreach (ModuleInfo info in this)
                    if (info.ModuleType != null)
                    {
                        if (info.ModuleType.Equals(tipo))
                            return info;
                    }
                    else
                    {
                        if (info.ModuleTypestr == tipo.FullName)
                            return info;
                    }
                return null;
            }            
        }

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <param name="index"></param>
	    /// <returns></returns>
		public ModuleInfo this[int index] {
            get { return (ModuleInfo)this.InnerList[index]; }
            set { this.InnerList[index] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
		public ModuleInfo this[string name] {
			get {
				foreach(ModuleInfo info in this) 
					if(info.ModuleTypestr == name) 
						return info;
				return null;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vModuleInfo"></param>
        public void AddRange(ModuleInfo[] vModuleInfo)
        {
            this.InnerList.AddRange(vModuleInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ModuleInfo[] GetValues()
        {
            ModuleInfo[] vModuleInfos = new ModuleInfo[this.InnerList.Count];
            this.InnerList.CopyTo(0, vModuleInfos, 0, this.InnerList.Count);
            return vModuleInfos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ModuleTypestr"></param>
        /// <param name="formprincipal"></param>
        public void ShowModule(string ModuleTypestr, FormPrincipalBase formprincipal)
        {
            ModuleInfo MI = this[ModuleTypestr];
            if (MI != null)
                ShowModule(this[ModuleTypestr], formprincipal);
            //foreach (ModuleInfo info in this)
              //  if (info.ModuleTypestr == ModuleTypestr)
                //    ShowModule(info, formprincipal);
        }
		

        /// <summary>
        /// Show the module on the particular control
        /// </summary>
        /// <param name="item"></param>
        /// <param name="formprincipal"></param>
        public void ShowModule(ModuleInfo item, FormPrincipalBase formprincipal)
        {			
            if (currentModule != null)
                if (formprincipal.OcultarTabs)
                    currentModule.Hide();
			item.Show(formprincipal);
			currentModule = item;			
		}


		/// <summary>
        /// return the currently showing module
		/// </summary>
		public ModuleInfo CurrentModuleInfo { get { return currentModule; } }
		
	}

    #region Enumera�oes
    /// <summary>
    /// Estados dos componentes
    /// </summary>
    public enum EstadosDosComponentes
    {
        /// <summary>
        /// Componete base chamador
        /// </summary>
        Registrado,
        /// <summary>
        /// Em formularios
        /// </summary>
        PopUp,
        /// <summary>
        /// instancia em nova tab
        /// </summary>
        JanelasAtivas
    } 
    #endregion
}