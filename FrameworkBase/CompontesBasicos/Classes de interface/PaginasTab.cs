using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Reflection;

namespace CompontesBasicos.Classes_de_interface
{
    /// <summary>
    /// 
    /// </summary>
    public class PaginaTabInfo
    {
        #region Campos
        private String name;        
        private Type paginaTabType;
        //private ComponenteTabCampos Pagina;        
        #endregion

        #region Propriedades

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")] // Take this out, and you will soon have problems with serialization;
        [DefaultValue(typeof(string), "")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Editor(typeof(SelControleList), typeof(UITypeEditor))]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Type PaginaTabType
        {
            get { return paginaTabType; }
            set { paginaTabType = value; }
        }

        
        //[Category("Virtual Software")]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public ComponenteBase Module { get { return this.module; } }

        #endregion

        #region Construtores

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="PaginaTabType"></param>
        public PaginaTabInfo(string name, Type PaginaTabType)
        {
            // throw exception if the module is not inherited from ComponeteBase class
            if (!PaginaTabType.IsSubclassOf(typeof(ComponenteTabCampos)))
                throw new ArgumentException("moduleClass has to be inherited from ComponenteTabCampos");                        
            this.name = name;
            this.PaginaTabType = PaginaTabType;
            //this.module = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public PaginaTabInfo()
        {
        } 
        #endregion

        #region Metodos        
        
    /*
        public void Show(FormPrincipalBase formprincipal)
        {            
            CreateModule();
            formprincipal.ShowMoludointerno(module, name,EstadosDosComponentes.Registrado,false);
        }
        
        public void Hide()
        {
            if (module != null)
                ((XtraTabPage)(module.Parent)).PageVisible = false;
        }
        */

        /// <summary>
        /// Cria umas instancia do objeto definido
        /// </summary>
        /// <param name="TabMae">Componente criador</param>
        /// <returns></returns>
        public ComponenteTabCampos CriaPagina(ComponenteCamposGroupTabControl TabMae)
        {
            if (paginaTabType == null)
                throw new MissingFieldException("N�o foi informado nenhum valor no campo paginaTabType da propriedade PaginaTabInfoCollection do camponente conteiner.");
            else
                if (!paginaTabType.IsSubclassOf(typeof(ComponenteTabCampos)))
                    throw new ArgumentException("paginaTabType nao � do tipo ComponenteTabCampos");
                else
                {
                    //ConstructorInfo constructorInfoObj = paginaTabType.GetConstructor(Type.EmptyTypes);
                    ComponenteTabCampos retorno;
                    ConstructorInfo constructorInfoObj = paginaTabType.GetConstructor(new Type[] { typeof(ComponenteCamposGroupTabControl) });
                    if (constructorInfoObj == null)
                    {
                        constructorInfoObj = paginaTabType.GetConstructor(Type.EmptyTypes);
                        if (constructorInfoObj == null)
                            throw new ApplicationException(paginaTabType.FullName + " n�o possui um construtor publico sem parametros");
                        else
                        {
                            retorno = (ComponenteTabCampos)constructorInfoObj.Invoke(null);
                            retorno.TabMae = TabMae;
                        }
                    }
                    else
                    {
                        retorno = (ComponenteTabCampos)constructorInfoObj.Invoke(new object[] { TabMae });
                    }
                    
                    //return constructorInfoObj.Invoke(new object[1] { TabMae }) as ComponenteTabCampos;
                    //
                    return retorno;
                }            
        } 
        
        #endregion
		
        
	}

    // Lista das paginas registradas
    /// <summary>
    /// Conjunto de componetes tab
    /// </summary>
    [ListBindable(false)]
    public class PaginaTabInfoCollection : CollectionBase
    {

        //private ModuleInfo currentModule;

        #region Construtores
        /// <summary>
        /// Construtor
        /// </summary>
        public PaginaTabInfoCollection()
        {
            //this.currentModule = null;
        }

        #endregion

        /// <summary>
        /// Inclui na lista
        /// </summary>
        /// <param name="vPaginaTabInfo">Definicao de tab</param>
        /// <returns></returns>
        public PaginaTabInfo Add(PaginaTabInfo vPaginaTabInfo)
        {
            this.InnerList.Add(vPaginaTabInfo);
            return vPaginaTabInfo;
        }

        /// <summary>
        /// Remove tab
        /// </summary>
        /// <param name="vPaginaTabInfo">tab a ser removida</param>
        public void Remove(PaginaTabInfo vPaginaTabInfo)
        {
            this.InnerList.Remove(vPaginaTabInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vPaginaTabInfo"></param>
        /// <returns></returns>
        public bool Contains(PaginaTabInfo vPaginaTabInfo)
        {
            return this.InnerList.Contains(vPaginaTabInfo);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public PaginaTabInfo this[int index]
        {
            get { return (PaginaTabInfo)this.InnerList[index]; }
            set { this.InnerList[index] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public PaginaTabInfo this[string name]
        {
            get
            {
                foreach (PaginaTabInfo info in this)
                    if (info.Name.Equals(name))
                        return info;
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vPaginaTabInfo"></param>
        public void AddRange(PaginaTabInfo[] vPaginaTabInfo)
        {
            this.InnerList.AddRange(vPaginaTabInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public PaginaTabInfo[] GetValues()
        {
            PaginaTabInfo[] vPaginaTabInfos = new PaginaTabInfo[this.InnerList.Count];
            this.InnerList.CopyTo(0, vPaginaTabInfos, 0, this.InnerList.Count);
            return vPaginaTabInfos;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        protected override void OnInsert(int index, object value)
        {
            base.OnInsert(index, value);
        }


        /*
        //Show the module on the particular control
        public void ShowModule(ModuleInfo item, FormPrincipalBase formprincipal)
        {
            if (item == currentModule) return;
            if (currentModule != null)
                currentModule.Hide();
            item.Show(formprincipal);
            currentModule = item;
            // update UI action objects
            //			item.Module.Actions.UpdateVisibility();
            //			item.Module.UpdateActions();
        }
        // return the currently showing module
        public ModuleInfo CurrentModuleInfo { get { return currentModule; } }
        */
    }
}
