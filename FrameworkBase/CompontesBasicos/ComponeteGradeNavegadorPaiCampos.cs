using System;
using System.ComponentModel;
using System.Data;
using System.Drawing.Design;

namespace CompontesBasicos
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ComponeteGradeNavegadorPaiCampos : CompontesBasicos.ComponenteGradeNavegadorP
    {
        

        /// <summary>
        /// Campo a mostrar na tab
        /// </summary>
        [Category("Virtual Software")]
        [Editor(typeof(SelTableAdapterList), typeof(UITypeEditor))]
        public Component TableAdapter
        {
            get { return tableAdapter; }
            set { tableAdapter = value; }
        }

        /// <summary>
        /// Construtor
        /// </summary>
        public ComponeteGradeNavegadorPaiCampos()
        {
            InitializeComponent();
        }

        

        /// <summary>
        /// Duplo click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void GridControl_F_DoubleClick(object sender, EventArgs e)
        {
            if (BtnAlterar_F.Enabled)
                Alterar();
            else
                Visualizar();
        }

        /// <summary>
        /// M�todo de altera��o
        /// </summary>
        protected override void Alterar()
        {
            AbreCampos(false, "Altera");
        }
        
        

        /// <summary>
        /// Abrer somente leitura
        /// </summary>
        protected override void Visualizar()
        {
            AbreCampos(true, "Altera");
        }

        private void btnVisualizar_F_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Visualizar();
        }

        /// <summary>
        /// Abrir o formul�rio campos
        /// </summary>
        /// <param name="SomenteLeitura">somente leitura</param>
        /// <param name="Titulo">T�tulo</param>        
        /// <returns></returns>
        public bool AbreCampos(bool SomenteLeitura, string Titulo)
        {
            if (ComponenteCampos == null)
                return false;
            if (LinhaMae_F == null)
                return false;

            if ((CampoTab != null) && (LinhaMae_F.Table.Columns[CampoTab] != null))
            {
                DataColumn Coluna = tabela.Columns[CampoTab];
                //DRV.Row[Coluna].ToString();
                Titulo = PrefixoTab + " " + LinhaMae_F[Coluna].ToString();
            };
            int pk = (Int32)LinhaMae_F[0];
                
            FormPrincipalBase.FormPrincipal.ShowMoludoCampos(ComponenteCampos, Titulo, pk, SomenteLeitura, this,EstadoComponenteCampos);
                return true;
            
        }

        /*
        /// <summary>
        /// Prefixo do t�tulo da tab
        /// </summary>
        private string prefixoTab;

        /// <summary>
        /// Prefixo a mostrar na tab
        /// </summary>
        /// <remarks>exemplo (Cond. XXXXXXX)</remarks>
        [Category("Virtual Software")]
        [Description("Prefixo a ser mostrado na TAB")]
        public string PrefixoTab
        {
            get { return prefixoTab; }
            set { prefixoTab = value; }
        }*/

            /*
        /// <summary>
        /// Campo a mostrar na tab
        /// </summary>
        private string campoTab;

        /// <summary>
        /// Campo a ser mostrado na TAB
        /// </summary>
        [Category("Virtual Software")]
        [Description("Campo a ser mostrado na TAB")]
        public string CampoTab
        {
            get { return campoTab; }
            set { campoTab = value; }
        }*/

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Visualizar();
        }
    }

}

