using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CompontesBasicos.ObjetosEstaticos
{
    /// <summary>
    /// Editor de strings
    /// </summary>
    public class StringEdit
    {
        /// <summary>
        /// Tipo
        /// </summary>
        public enum TiposLimpesa
        { 
            /// <summary>
            /// 
            /// </summary>
            RemoveBrancosDuplicados,
            /// <summary>
            /// 
            /// </summary>
            RemoveQuebrasLinha,
            /// <summary>
            /// 
            /// </summary>
            SoNumeros,
            /// <summary>
            /// 
            /// </summary>
            RemoveAcentos,
            /// <summary>
            /// 
            /// </summary>
            Maiusculas,
            /// <summary>
            /// 
            /// </summary>
            PrimeiroCaracMaiusculo,
            /// <summary>
            /// 
            /// </summary>
            NomeArquivo
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        [Obsolete("usar extens�o do string")]
        static public string Limpa(string Entrada, TiposLimpesa Tipo)
        {            
            return Limpa(Entrada, new TiposLimpesa[] { Tipo });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        [Obsolete("usar extens�o do Stream")]      
        static public string Limpa(Stream Str, TiposLimpesa Tipo)
        {
            return Limpa(Str, new TiposLimpesa[] { Tipo });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Str"></param>
        /// <param name="Tipos"></param>
        /// <returns></returns>
        [Obsolete("usar extens�o do Stream")]      
        static public string Limpa(Stream Str, TiposLimpesa[] Tipos)
        {
            Str.Position = 0;
            string strLido = new StreamReader(Str).ReadToEnd();
            return Limpa(strLido, Tipos);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entrada"></param>
        /// <param name="Tipos"></param>
        /// <returns></returns>
        [Obsolete("usar extens�o do string")]      
        static public string Limpa(string Entrada, TiposLimpesa[] Tipos)
        {
            bool RemoveBrancosDuplicados = false;
            bool RemoveQuebrasLinha = false;
            bool SoNumeros = false;
            bool RemoveAcentos = false;
            bool Maiusculas = false;
            bool PrimeiroCaracMaiusculo = false;
            bool NomeArquivo = false;
            foreach (TiposLimpesa Tipo in Tipos)
                if (Tipo == TiposLimpesa.RemoveBrancosDuplicados)
                    RemoveBrancosDuplicados = true;
                else if (Tipo == TiposLimpesa.RemoveQuebrasLinha)
                    RemoveQuebrasLinha = true;
                else if (Tipo == TiposLimpesa.SoNumeros)
                    SoNumeros = true;
                else if (Tipo == TiposLimpesa.RemoveAcentos)
                    RemoveAcentos = true;
                else if (Tipo == TiposLimpesa.Maiusculas)
                    Maiusculas = true;
                else if (Tipo == TiposLimpesa.PrimeiroCaracMaiusculo)
                    PrimeiroCaracMaiusculo = true;
                else if (Tipo == TiposLimpesa.NomeArquivo)
                {
                    NomeArquivo = true;
                    RemoveBrancosDuplicados = true;
                }
            StringBuilder SB = new StringBuilder();
            bool eBranco = false;
            bool InicioPal = true;
            char novoc;
            foreach (char c in Entrada)
            {
                if (RemoveAcentos)
                    switch (c)
                    {
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'a';
                            break;
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'e';
                            break;
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'i';
                            break;
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'o';
                            break;
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'u';
                            break;
                        case '�':
                            novoc = 'c';
                            break;
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'A';
                            break;
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'E';
                            break;
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'I';
                            break;
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'O';
                            break;
                        case '�':
                        case '�':
                        case '�':
                        case '�':
                            novoc = 'U';
                            break;
                        case '�':
                            novoc = 'C';
                            break;
                        default:
                            novoc = c;
                            break;
                    }
                else
                    novoc = c;
                if (PrimeiroCaracMaiusculo)
                {
                    if (c == ' ')
                        InicioPal = true;
                    else
                        if (InicioPal)
                        {
                            novoc = char.ToUpper(novoc);
                            InicioPal = false;
                        }
                }
                if (SoNumeros)
                    if (!char.IsDigit(novoc))
                        continue;
                if (NomeArquivo)
                    if ((c == '\\') || (c == '/') || (c == '.'))
                        novoc = '_';
                if (RemoveBrancosDuplicados)
                {
                    if (novoc == ' ')
                    {
                        if (eBranco)
                            continue;
                        else
                            eBranco = true;
                    }
                    else
                        eBranco = false;
                }
                if (RemoveQuebrasLinha)
                    if ((novoc == '\r') || (novoc == '\n'))
                        continue;
                SB.Append(novoc);
            }
            string Retorno = SB.ToString();
            if (RemoveBrancosDuplicados)
                Retorno = Retorno.Trim();
            if (Maiusculas)
                return Retorno.ToUpper();
            return Retorno;
        }
    }
}
