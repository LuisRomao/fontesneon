﻿

namespace VirPDFModelo
{
    /// <summary>
    /// Classe para troca de textos no PDF - Em produção de preferencia a utilizar gabaritos gerados no Virtual.exe
    /// </summary>
    public class Troca
    {
        string modelo;
        string valor;
        float tamanho;
        int indFont;
        bool centralizado;
        float ajusteFino;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Modelo"></param>
        /// <param name="_Valor"></param>
        /// <param name="_Tamanho"></param>
        /// <param name="_IndFont"></param>
        /// <param name="_Centralizado"></param>
        /// <param name="_AjusteFino"></param>
        public Troca(string _Modelo, string _Valor, float _Tamanho, int _IndFont, bool _Centralizado,float _AjusteFino)
        {
            Modelo = _Modelo;
            Valor = _Valor;
            Tamanho = _Tamanho;
            IndFont = _IndFont;
            Centralizado = _Centralizado;
            AjusteFino = _AjusteFino;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="row"></param>
        public Troca(dMapeamento.TrocaRow row):this(row.Modelo,row.Valor,row.TamanhoFonte,row.Fonte,row.Centralizado,(float)row.AjusteFino)
        {
        }

        /// <summary>
        /// Modelo a procurar
        /// </summary>
        public string Modelo { get => modelo; set => modelo = value; }
        /// <summary>
        /// Novo conteúdo
        /// </summary>
        public string Valor { get => valor; set => valor = value; }
        /// <summary>
        /// Tamanho do fonte
        /// </summary>
        public float Tamanho { get => tamanho; set => tamanho = value; }
        /// <summary>
        /// indice do fonte no PDF
        /// </summary>
        public int IndFont { get => indFont; set => indFont = value; }
        /// <summary>
        /// Centralizar
        /// </summary>
        public bool Centralizado { get => centralizado; set => centralizado = value; }
        /// <summary>
        /// Juste fino na posição vertical
        /// </summary>
        public float AjusteFino { get => ajusteFino; set => ajusteFino = value; }
    }
}
