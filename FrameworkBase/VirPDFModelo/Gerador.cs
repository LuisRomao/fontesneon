﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.IO;

// ************************************************************************************************************
// *                                             ATENÇÃO                                                      *
// * O programa Virtual.exe utiliza este componente para geração dos gabaritos a serem utilizados em produção *
// ************************************************************************************************************

namespace VirPDFModelo
{
    /// <summary>
    /// Classe para gerar PDF a partir de modelos
    /// </summary>
    public class Gerador
    {
        private PdfStamper stamper = null;
        private PdfReader pReader;

        #region Uso em tempo de projeto
        /// <summary>
        /// Modo para ajuste de fontes e tamanho
        /// </summary>
        public bool Calibracao = false;
        /// <summary>
        /// Pinta um quadro verde no local da substituição
        /// </summary>
        public bool PintarDeVerde = false;
        /// <summary>
        /// Sobrepõe
        /// </summary>
        public bool Sobrepor = false;
        /// <summary>
        /// Faz a impressão com letrar branca para verificar a sobreposição
        /// </summary>
        public bool Branco = false;

        /// <summary>
        /// Mostra o conteúdo do pdf (em texto)
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public string ConteudoPDF(string arquivo)
        {
            string retorno = "";
            if (!File.Exists(arquivo))
                return string.Format("arquivo {0} não encontrado",arquivo);
            PdfReader pReader = new PdfReader(arquivo);


            for (int page = 1; page <= pReader.NumberOfPages; page++)
            {
                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                retorno += PdfTextExtractor.GetTextFromPage(pReader, page, strategy);
            }
            return retorno;
        }

        /// <summary>
        /// Lista os fontes do PDF
        /// </summary>
        /// <param name="arquivo"></param>
        /// <returns></returns>
        public string ListaFontes(string arquivo)
        {
            string retorno = "";
            if (!File.Exists(arquivo))
                return string.Format("arquivo {0} não encontrado", arquivo);
            PdfReader pReader = new PdfReader(arquivo);
            List<object[]> strFonts = BaseFont.GetDocumentFonts(pReader);

            int pos = 0;

            foreach (object[] conjunto in strFonts)
            {
                PRIndirectReference Fonte = (PRIndirectReference)conjunto[1];
                retorno += string.Format("{0} = {1} {2}\r\n", pos++, Fonte, conjunto[0]);
            }
            return retorno;
        }
        #endregion

        Exception UltimoErro = null;

        /// <summary>
        /// Gera o PDF
        /// </summary>
        /// <param name="NomeArquivoModelo"></param>
        /// <param name="NomeArquivoGerar"></param>
        /// <param name="NomeXML"></param>
        /// <returns></returns>
        public bool GeraPDF(string NomeArquivoModelo, string NomeArquivoGerar, string NomeXML="")
        {
            if (NomeXML == "")
                NomeXML = string.Format("{0}.xml", System.IO.Path.GetFileNameWithoutExtension(NomeArquivoModelo));
            if (!File.Exists(NomeXML))
                return false;
            dGabarito DGabarito = new dGabarito();
            DGabarito.ReadXml(NomeXML);
            return GeraPDF(NomeArquivoModelo, NomeArquivoGerar, DGabarito);
        }

        /// <summary>
        /// Gera o PDF
        /// </summary>
        /// <param name="NomeArquivoModelo"></param>
        /// <param name="NomeArquivoGerar"></param>
        /// <param name="DGabarito"></param>
        /// <returns></returns>
        public bool GeraPDF(string NomeArquivoModelo, string NomeArquivoGerar, dGabarito DGabarito)
        {
            try
            {
                if (!File.Exists(NomeArquivoModelo))
                    return false;
                using (pReader = new PdfReader(NomeArquivoModelo))
                {
                    using (stamper = new PdfStamper(pReader, new FileStream(NomeArquivoGerar, FileMode.Create)))
                    {
                        EfeturInclusoes(DGabarito);
                        stamper.Close();
                        pReader.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return false;
            }
        }

        private bool EfeturInclusoes(dGabarito DGabarito)
        {
            PdfContentByte cb2;
            BaseFont bf;
            List<object[]> strFonts = BaseFont.GetDocumentFonts(pReader);
            for (int page = 1; page <= pReader.NumberOfPages; page++)
            {
                cb2 = stamper.GetOverContent(page);

                foreach (dGabarito.GabaritoRow rowG in DGabarito.Gabarito)
                {
                    BaseColor CorAlternativa = Branco ? BaseColor.WHITE : BaseColor.RED;
                    cb2.SetColorFill(!Sobrepor ? BaseColor.BLACK : CorAlternativa);
                    bf = BaseFont.CreateFont((PRIndirectReference)strFonts[rowG.Fonte][1]);
                    cb2.SetFontAndSize(bf, rowG.TamanhoFonte);                    
                    EscreveUm(cb2, bf, rowG.Conteudo, rowG.Centralizado, (float)rowG.X, (float)rowG.Y, (float)rowG.DelataLinha);
                }
            }
            return true;
        }

        /// <summary>
        /// Gera o PDF
        /// </summary>
        /// <param name="NomeArquivoModelo"></param>
        /// <param name="NomeArquivoGerar"></param>
        /// <param name="DMapeamento"></param>
        /// <returns></returns>
        public bool GeraPDF(string NomeArquivoModelo, string NomeArquivoGerar, dMapeamento DMapeamento)
        {
            List<Troca> Trocas = new List<Troca>();
            foreach (dMapeamento.TrocaRow row in DMapeamento.Troca)
                Trocas.Add(new Troca(row));
            return GeraPDF(NomeArquivoModelo, NomeArquivoGerar, Trocas.ToArray());
        }

        /// <summary>
        /// Gera o PDF
        /// </summary>
        /// <param name="NomeArquivoModelo"></param>
        /// <param name="NomeArquivoGerar"></param>
        /// <param name="Trocas"></param>
        /// <returns></returns>
        public bool GeraPDF(string NomeArquivoModelo, string NomeArquivoGerar, params Troca[] Trocas)
        {
            try
            {
                if (!File.Exists(NomeArquivoModelo))
                    return false;
                using (pReader = new PdfReader(NomeArquivoModelo))
                {
                    using (stamper = new PdfStamper(pReader, new FileStream(NomeArquivoGerar, FileMode.Create)))
                    {
                        EfetuaTrocas(Trocas);
                        stamper.Close();
                        pReader.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return false;
            }
        }
        
        private void EscreveUm(PdfContentByte cb2, BaseFont bf, string Valor, bool Centralizado, float X, float Y, float DeltaLinha = 0)
        {
            string[] Linhas = Valor.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            int nLinha = 0;
            foreach (string Linha in Linhas)
            {                
                float DeltaLinhas = nLinha * 1.3F * DeltaLinha;
                cb2.BeginText();
                cb2.ShowTextAligned(Centralizado ? Element.ALIGN_CENTER : Element.ALIGN_LEFT, Linha, X, Y - DeltaLinhas, 0);                                                 
                cb2.EndText();
                cb2.Fill();
                nLinha++;
            }
        }

        /// <summary>
        /// Busca ocorrências de um modelo
        /// </summary>
        /// <param name="NomeArquivoModelo"></param>
        /// <param name="Modelo"></param>
        /// <returns></returns>
        public List<Rectangle> BuscaOcorrencias(string NomeArquivoModelo, string Modelo)
        {
            List<Rectangle> Retorno = new List<Rectangle>();
            try
            {
                using (pReader = new PdfReader(NomeArquivoModelo))
                {
                    for (int page = 1; page <= pReader.NumberOfPages; page++)
                    {
                        myLocationTextExtractionStrategy strategy = new myLocationTextExtractionStrategy();
                        PdfContentByte cb = stamper.GetOverContent(page);
                        strategy.UndercontentCharacterSpacing = (int)cb.CharacterSpacing;
                        strategy.UndercontentHorizontalScaling = (int)cb.HorizontalScaling;
                        //NÃO REMOVER a chamada do GetTextFromPage porque ele dispara triggers do  strategy 
                        string currentText = PdfTextExtractor.GetTextFromPage(pReader, page, strategy);
                        Retorno.AddRange(strategy.GetTextLocations(Modelo, StringComparison.CurrentCultureIgnoreCase));
                    }

                    pReader.Close();

                }
                return Retorno;
            }
            catch (Exception ex)
            {
                UltimoErro = ex;
                return null;
            }
        }

        private void EfetuaTrocas(Troca[] Trocas)
        {
            StringComparison SC = StringComparison.CurrentCultureIgnoreCase;

            PdfContentByte cb = null;
            PdfContentByte cb2 = null;
            BaseFont bf = null;
            List<object[]> strFonts = BaseFont.GetDocumentFonts(pReader);
            for (int page = 1; page <= pReader.NumberOfPages; page++)
            {
                myLocationTextExtractionStrategy strategy = new myLocationTextExtractionStrategy();
                cb = stamper.GetOverContent(page);
                cb2 = stamper.GetOverContent(page);
                strategy.UndercontentCharacterSpacing = (int)cb.CharacterSpacing;
                strategy.UndercontentHorizontalScaling = (int)cb.HorizontalScaling;

                //NÃO REMOVER a chamada do GetTextFromPage porque ele dispara triggers do  strategy 
                string currentText = PdfTextExtractor.GetTextFromPage(pReader, page, strategy);

                foreach (Troca TrocaX in Trocas)
                {
                    List<Rectangle> MatchesFound = strategy.GetTextLocations(TrocaX.Modelo, SC);

                    //cb.SetColorFill(PintarDeVerde ? BaseColor.GREEN : BaseColor.WHITE);

                    foreach (Rectangle rect in MatchesFound)
                    {
                        if (float.IsNaN(rect.Left))
                            continue;
                        if (!Sobrepor && (!Calibracao || PintarDeVerde))
                        {
                            cb.SetColorFill(PintarDeVerde ? BaseColor.GREEN : BaseColor.WHITE);
                            cb.Rectangle(rect.Left, rect.Bottom, rect.Width, rect.Height+1);
                            cb.Fill();
                        }
                        BaseColor CorAlternativa = Branco ? BaseColor.WHITE : BaseColor.RED;
                        cb2.SetColorFill(!Sobrepor ? BaseColor.BLACK : CorAlternativa);
                        bf = BaseFont.CreateFont((PRIndirectReference)strFonts[TrocaX.IndFont][1]);
                        cb2.SetFontAndSize(bf, TrocaX.Tamanho);
                        string NovoValor = TrocaX.Valor;
                        if (Calibracao || Sobrepor)
                            NovoValor = TrocaX.Modelo;
                        if (Calibracao)
                        {
                            EscreveUm(cb2, bf, NovoValor, TrocaX.Centralizado, TrocaX.Centralizado ? (rect.Left + rect.Right) / 2 : rect.Left, rect.Bottom + TrocaX.AjusteFino - rect.Height);
                            EscreveUm(cb2, bf, NovoValor, TrocaX.Centralizado, (TrocaX.Centralizado ? (rect.Left + rect.Right) / 2 : rect.Left) + rect.Width, rect.Bottom + TrocaX.AjusteFino);
                        }
                        else
                            EscreveUm(cb2, bf, NovoValor, TrocaX.Centralizado, TrocaX.Centralizado ? (rect.Left + rect.Right) / 2 : rect.Left, rect.Bottom + TrocaX.AjusteFino);                        
                    }
                }

            }

        }
    }
}
