namespace TesteRTF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtfEditor1 = new RtfEditor.RtfEditor();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tMPERROPRODBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataSet1 = new TesteRTF.DataSet1();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNOTA = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPRODUTO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCOMISSAO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.tMPERROPRODTableAdapter = new TesteRTF.DataSet1TableAdapters.TMPERROPRODTableAdapter();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tMPERROPRODBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // rtfEditor1
            // 
            this.rtfEditor1.Location = new System.Drawing.Point(13, 13);
            this.rtfEditor1.Name = "rtfEditor1";
            this.rtfEditor1.Size = new System.Drawing.Size(692, 270);
            this.rtfEditor1.TabIndex = 0;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(13, 320);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(116, 23);
            this.simpleButton1.TabIndex = 1;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tMPERROPRODBindingSource;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(13, 349);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(746, 300);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // tMPERROPRODBindingSource
            // 
            this.tMPERROPRODBindingSource.DataMember = "TMPERROPROD";
            this.tMPERROPRODBindingSource.DataSource = this.dataSet1;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "DataSet1";
            this.dataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNOTA,
            this.colPRODUTO,
            this.colCOMISSAO});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // colNOTA
            // 
            this.colNOTA.Caption = "NOTA";
            this.colNOTA.FieldName = "NOTA";
            this.colNOTA.Name = "colNOTA";
            this.colNOTA.Visible = true;
            this.colNOTA.VisibleIndex = 0;
            // 
            // colPRODUTO
            // 
            this.colPRODUTO.Caption = "PRODUTO";
            this.colPRODUTO.FieldName = "PRODUTO";
            this.colPRODUTO.Name = "colPRODUTO";
            this.colPRODUTO.Visible = true;
            this.colPRODUTO.VisibleIndex = 1;
            // 
            // colCOMISSAO
            // 
            this.colCOMISSAO.Caption = "COMISSAO";
            this.colCOMISSAO.FieldName = "COMISSAO";
            this.colCOMISSAO.Name = "colCOMISSAO";
            this.colCOMISSAO.Visible = true;
            this.colCOMISSAO.VisibleIndex = 2;
            // 
            // tMPERROPRODTableAdapter
            // 
            this.tMPERROPRODTableAdapter.ClearBeforeFill = true;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(135, 320);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(116, 23);
            this.simpleButton2.TabIndex = 3;
            this.simpleButton2.Text = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 661);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.rtfEditor1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tMPERROPRODBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private RtfEditor.RtfEditor rtfEditor1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DataSet1 dataSet1;
        private System.Windows.Forms.BindingSource tMPERROPRODBindingSource;
        private TesteRTF.DataSet1TableAdapters.TMPERROPRODTableAdapter tMPERROPRODTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colNOTA;
        private DevExpress.XtraGrid.Columns.GridColumn colPRODUTO;
        private DevExpress.XtraGrid.Columns.GridColumn colCOMISSAO;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
    }
}

