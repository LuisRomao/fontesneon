using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace TesteRTF
{
    public partial class Form1 : CompontesBasicos.FormBase
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            
            ArquivosTXT.cTabelaTXT Arquivo = new ArquivosTXT.cTabelaTXT(dataSet1.TMPERROPROD, ArquivosTXT.ModelosTXT.delimitado);
            Arquivo.RegistraMapa("Nota", 16, 0);
            Arquivo.RegistraMapa("Produto", 5, 0);
            Arquivo.RegistraMapa("Comissao", 13, 0);
            Arquivo.LeCabecalho("c:\\prov");
            Arquivo.Separador = new char[1]{(char)9};
            Arquivo.DivDecimal = 1;
            Arquivo.Carrega();
            tMPERROPRODTableAdapter.Update(dataSet1.TMPERROPROD);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tMPERROPRODTableAdapter.Fill(dataSet1.TMPERROPROD);

        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            VirFB.TableAdapter TA = new VirFB.TableAdapter();
            TA.CriaCone(tMPERROPRODTableAdapter.Connection.ConnectionString);
            /*
            string comando = "select t.nota,t.produto from tmperroprod t\r\n"+
                             "left join itenscomiss i on (i.nota = t.nota) and (i.produto = t.produto)\r\n" +
                             "where i.comiss = t.comissao";
            string comandodel = "delete from tmperroprod t where t.nota = @P1 and t.produto = @P2";
            
            DataTable DT = TA.BuscaSQLTabela(comando);
            foreach (DataRow DR in DT.Rows) {
                TA.ExecutarSQLNonQuery(comandodel, DR["NOTA"], DR["PRODUTO"]);
            }
            */
            /*
            int ID=1;
            string comando = "UPDATE TMPERROPROD SET ID = @P1 where NOTA = @P2 and PRODUTO = @P3";
            foreach (DataSet1.TMPERROPRODRow row in dataSet1.TMPERROPROD) {
                row.ID = ID;
                ID++;
                TA.ExecutarSQLNonQuery(comando, ID, row.NOTA, row.PRODUTO);
            };
            */

            string comandoBusca = "select a.comissao,a.lote,a.ai from ar a where a.numero = @P1";
            string comandoinsert = "INSERT INTO TMPDET(AR, TMP, VALOR, VALORORIG, LOTE) VALUES (@P1,@P2,@P3,@P4,@P5)";
            foreach (DataSet1.TMPERROPRODRow row in dataSet1.TMPERROPROD)
            {
                DataTable DT = TA.BuscaSQLTabela(comandoBusca, row.NOTA);
                decimal Total = 0;
                foreach (DataRow DR in DT.Rows)
                {                    
                    Total += (decimal)DR["comissao"];
                };
                decimal subtot = 0;
                int parcela = 1;
                foreach (DataRow DR in DT.Rows)
                {
                    decimal valor;
                    if (parcela == DT.Rows.Count)
                        valor = Math.Round(row.COMISSAO - subtot,2);
                    else
                    {
                        if (Total != 0)
                            valor = (row.COMISSAO / Total) * (decimal)DR["comissao"];
                        else
                            valor = (decimal)DR["comissao"];
                        valor = Math.Round(valor, 2);
                        subtot += valor;
                    };
                    TA.ExecutarSQLNonQuery(comandoinsert, DR["ai"], row.ID, valor, DR["comissao"], DR["lote"]);
                    parcela++;
                }
            };
        }
    }
}