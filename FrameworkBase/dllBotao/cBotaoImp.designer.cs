namespace dllBotao
{
    partial class cBotaoImp
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dropDownButton1 = new DevExpress.XtraEditors.DropDownButton();
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu();
            this.barButtonItemIMP = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemTela = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemPDF = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemEmail = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonplanilha = new DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // dropDownButton1
            // 
            this.dropDownButton1.AllowDrop = true;
            this.dropDownButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dropDownButton1.DropDownControl = this.popupMenu1;
            this.dropDownButton1.Location = new System.Drawing.Point(0, 0);
            this.dropDownButton1.Margin = new System.Windows.Forms.Padding(0);
            this.dropDownButton1.Name = "dropDownButton1";
            this.barManager1.SetPopupContextMenu(this.dropDownButton1, this.popupMenu1);
            this.dropDownButton1.Size = new System.Drawing.Size(131, 29);
            this.dropDownButton1.TabIndex = 6;
            this.dropDownButton1.Text = "Imprimir";
            
            this.dropDownButton1.Click += new System.EventHandler(this.dropDownButton1_Click);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemIMP),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemTela),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemPDF),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemEmail),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonplanilha, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // barButtonItemIMP
            // 
            this.barButtonItemIMP.Caption = "Imprimir";
            this.barButtonItemIMP.Glyph = global::dllBotao.Properties.Resources.imprimir;
            this.barButtonItemIMP.Id = 8;
            this.barButtonItemIMP.Name = "barButtonItemIMP";
            this.barButtonItemIMP.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemX_ItemClick);
            // 
            // barButtonItemTela
            // 
            this.barButtonItemTela.Caption = "Tela";
            this.barButtonItemTela.Glyph = global::dllBotao.Properties.Resources.tela;
            this.barButtonItemTela.Id = 9;
            this.barButtonItemTela.Name = "barButtonItemTela";
            this.barButtonItemTela.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemX_ItemClick);
            // 
            // barButtonItemPDF
            // 
            this.barButtonItemPDF.Caption = "Gerar PDF";
            this.barButtonItemPDF.Glyph = global::dllBotao.Properties.Resources.pdf;
            this.barButtonItemPDF.Id = 2;
            this.barButtonItemPDF.Name = "barButtonItemPDF";
            this.barButtonItemPDF.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemX_ItemClick);
            // 
            // barButtonItemEmail
            // 
            this.barButtonItemEmail.Caption = "Enviar por e.mail";
            this.barButtonItemEmail.Glyph = global::dllBotao.Properties.Resources.E_mail;
            this.barButtonItemEmail.Id = 3;
            this.barButtonItemEmail.Name = "barButtonItemEmail";
            this.barButtonItemEmail.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemX_ItemClick);
            // 
            // barButtonplanilha
            // 
            this.barButtonplanilha.Caption = "Planilha";
            this.barButtonplanilha.Glyph = global::dllBotao.Properties.Resources.excel3;
            this.barButtonplanilha.Id = 10;
            this.barButtonplanilha.Name = "barButtonplanilha";
            this.barButtonplanilha.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemX_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barButtonItemIMP,
            this.barButtonItemTela,
            this.barButtonItemPDF,
            this.barButtonItemEmail,
            this.barButtonplanilha});
            this.barManager1.MaxItemId = 11;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(131, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 29);
            this.barDockControlBottom.Size = new System.Drawing.Size(131, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 29);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(131, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 29);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "pdf|*.pdf|Excel|*.xls|Excel|*xlsx|Texto|*.txt|Texto virgulas|*.CSV|HTML|*.html|BM" +
    "P|*.bmp|RTF|*.rtf|outros|*.*";
            this.saveFileDialog1.RestoreDirectory = true;
            this.saveFileDialog1.Title = "Salvar";
            // 
            // cBotaoImp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dropDownButton1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "cBotaoImp";
            this.Size = new System.Drawing.Size(131, 29);
            this.EnabledChanged += new System.EventHandler(this.cBotaoImp_EnabledChanged);
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.DropDownButton dropDownButton1;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraBars.PopupMenu popupMenu1;
        private DevExpress.XtraBars.BarButtonItem barButtonItemIMP;
        private DevExpress.XtraBars.BarButtonItem barButtonItemTela;
        private DevExpress.XtraBars.BarButtonItem barButtonItemPDF;
        private DevExpress.XtraBars.BarButtonItem barButtonItemEmail;

        /// <summary>
        /// 
        /// </summary>
        public DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem barButtonplanilha;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}
