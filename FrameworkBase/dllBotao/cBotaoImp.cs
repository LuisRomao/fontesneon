using System;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting;

namespace dllBotao
{
    /// <summary>
    /// Bot�o para impressos
    /// </summary>
    public partial class cBotaoImp : DevExpress.XtraEditors.XtraUserControl
    {
        /// <summary>
        /// Construtor
        /// </summary>
        public cBotaoImp()
        {
            InitializeComponent();
        }
        

        #region Propriedades
        /// <summary>
        /// T�tulo do bot�o
        /// </summary>
        [Category("Virtual Software")]
        [Description("Titulo")]
        public string Titulo
        {
            get
            {
                return dropDownButton1.Text;
            }
            set
            {
                dropDownButton1.Text = value;
            }
        }

        /// <summary>
        /// Bot�o imprimir visivel
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Imprimir")]
        public bool BotaoImprimir
        {
            get
            {
                return barButtonItemIMP.Visibility == DevExpress.XtraBars.BarItemVisibility.Always;
            }
            set
            {
                barButtonItemIMP.Visibility = (value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
            }
        }

        /// <summary>
        /// Bot�o tela visivel
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao Tela")]
        public bool BotaoTela
        {
            get
            {
                return barButtonItemTela.Visibility == DevExpress.XtraBars.BarItemVisibility.Always;
            }
            set
            {
                barButtonItemTela.Visibility = (value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
            }
        }

        /// <summary>
        /// Bot�o PDF visivel
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao PDF")]
        public bool BotaoPDF
        {
            get
            {
                return barButtonItemPDF.Visibility == DevExpress.XtraBars.BarItemVisibility.Always;
            }
            set
            {
                barButtonItemPDF.Visibility = (value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
            }
        }

        /// <summary>
        /// Bot�o tela visivel
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao E.mail")]
        public bool BotaoEmail
        {
            get
            {
                return barButtonItemEmail.Visibility == DevExpress.XtraBars.BarItemVisibility.Always;
            }
            set
            {
                barButtonItemEmail.Visibility = (value ? DevExpress.XtraBars.BarItemVisibility.Always : DevExpress.XtraBars.BarItemVisibility.Never);
            }
        }

        private bool _CreateDocAutomatico = true;

        /// <summary>
        /// O m�dodo CreateDocument � chamado autom�ticamente
        /// </summary>
        [Category("Virtual Software")]
        [Description("O m�dodo CreateDocument � chamado autom�ticamente")]
        public bool CreateDocAutomatico
        {
            get { return _CreateDocAutomatico; }
            set { _CreateDocAutomatico = value; }
        }

        private XtraReport _Impresso;

        /// <summary>
        /// Impresso
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao E.mail")]
        public XtraReport Impresso
        {
            get { return _Impresso; }
            set { _Impresso = value; }
        }


        private PrintableComponentLink _PriComp;

        /// <summary>
        /// Botao e.mail vis�vel
        /// </summary>
        [Category("Virtual Software")]
        [Description("Botao E.mail")]
        public PrintableComponentLink PriComp
        {
            get { return _PriComp; }
            set { _PriComp = value; }
        }

        private Botao _AcaoBotao = Botao.botao;

        /// <summary>
        /// A��o padr�o Bot�o
        /// </summary>
        [Category("Virtual Software")]
        [Description("A��o padr�o Bot�o")]
        public Botao AcaoBotao
        {
            get{return _AcaoBotao; }
            set{_AcaoBotao = value; }
        }

        private bool _AbrirArquivoExportado = true;

        /// <summary>
        /// Abrir os arquivos exportados
        /// </summary>
        [Category("Virtual Software")]
        [Description("Abrir os arquivos exportados")]
        public bool AbrirArquivoExportado
        {
            get { return _AbrirArquivoExportado; }
            set { _AbrirArquivoExportado = value; }
        }

        /// <summary>
        /// Evita o uso dos novos formatos
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evita o uso dos novos formatos")]
        public bool NaoUsarxlsX
        {
            get
            {
                return _NaoUsarxlsX;
            }
            set
            {
                _NaoUsarxlsX = value;
            }
        }

        private bool _NaoUsarxlsX;
        #endregion



        /// <summary>
        /// Evento que ocorre quando algum dos bot�es � clicado
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento que ocorre quando algum dos bot�es � clicado")]
        public event BotaoEventHandler clicado;

        /// <summary>
        /// 
        /// </summary>
        [Category("Virtual Software")]
        [Description("Evento que ocorre quando alguma excessao � disparada")]
        public event ErroBotaoHandler NotificaErro;


        /// <summary>
        /// Evento que ocorre quando algum dos bot�es � clicado
        /// </summary>        
        private void OnClick(BotaoArgs e)
        {
            if (clicado != null)
                clicado(this, e);
        }

        /// <summary>
        /// Notifica o chamador
        /// </summary>
        /// <param name="e"></param>
        protected void NotificarErro(Exception e)
        {
            if (NotificaErro != null)
                NotificaErro(this, e);
        }


        /// <summary>
        /// Indica qual o bot�o do menu foi clicado
        /// </summary>
        public Botao BotaoClicado;

        private void dropDownButton1_Click(object sender, EventArgs e)
        {
            BotaoClicado = _AcaoBotao;
            BotaoArgs Arg = new BotaoArgs(BotaoClicado);
            OnClick(Arg);
            BotaoClicado = Arg.Botao;
            ChamadaX(Arg.Botao);                                   
        }

        private void barButtonItemX_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {                       
            if (e.Item == barButtonItemIMP)
                BotaoClicado = Botao.imprimir;
            else if (e.Item == barButtonItemTela)
                BotaoClicado = Botao.tela;
            else if (e.Item == barButtonItemPDF)
                BotaoClicado = Botao.pdf;
            else if (e.Item == barButtonItemEmail)
                BotaoClicado = Botao.email;
            else if (e.Item == barButtonplanilha)
                BotaoClicado = Botao.Planilha;
            BotaoArgs Arg = new BotaoArgs(BotaoClicado);
            OnClick(Arg);
            BotaoClicado = Arg.Botao;
            ChamadaX(Arg.Botao);
        }

        private void ChamadaX(Botao BotaoClicado)
        {
            switch (BotaoClicado)
            {
                case Botao.imprimir:
                    imprime();
                    break;
                case Botao.tela:
                    PreView();
                    break;
                case Botao.pdf:
                    GeraPDF();
                    break;
                case Botao.email:
                    GeraEmail();
                    break;
                case Botao.Planilha:
                    GeraPlanilha();
                    break;
                case Botao.botao:
                case Botao.nulo:
                default:
                    //OnClick(new BotaoArgs(BotaoClicado));
                    break;
            }
        }

        /*
        private SaveFileDialog _saveFileDialog1;

        private SaveFileDialog saveFileDialog1
        {
            get
            {
                if (_saveFileDialog1 == null)
                {
                    _saveFileDialog1 = new SaveFileDialog();
                    _saveFileDialog1.DefaultExt = "pdf";
                };
                return _saveFileDialog1;
            }
        }*/

        private void imprime()
        {
            if (_Impresso != null)
            {
                if (CreateDocAutomatico)
                    _Impresso.CreateDocument();
                _Impresso.Print();
            }
            if (_PriComp != null)
            {
                if (CreateDocAutomatico)
                    _PriComp.CreateDocument();
                _PriComp.Print("");
            }
            //OnClick(new BotaoArgs(Botao.imprimir));
        }

        private void PreView()
        {
            if (_Impresso != null)
            {
                if (CreateDocAutomatico)
                    _Impresso.CreateDocument();
                _Impresso.ShowPreviewDialog();                
            }
            if (_PriComp != null)
            {
                if (CreateDocAutomatico)
                    _PriComp.CreateDocument();
                _PriComp.ShowPreviewDialog();
            } 
            //OnClick(new BotaoArgs(Botao.tela));
        }      

        private void GeraPDF()
        {
            saveFileDialog1.DefaultExt = "pdf";
            saveFileDialog1.FilterIndex = 1;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)            
                SalvaGeral();
            
        }

        /// <summary>
        /// 
        /// </summary>
        protected virtual void GeraEmail()
        {
            throw new NotImplementedException();                        
        }

        private void GeraPlanilha()
        {
            saveFileDialog1.DefaultExt = _NaoUsarxlsX ? "xls" : "xlsx";
            saveFileDialog1.FilterIndex = NaoUsarxlsX ? 2 : 3;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)            
                SalvaGeral();
                
                
        }

        private void SalvaGeral()
        {
            string ex = (System.IO.Path.GetExtension(saveFileDialog1.FileName));
            if (_Impresso != null)
            {
                if (CreateDocAutomatico)
                    _Impresso.CreateDocument();
                if (ex.ToUpper() == ".XLS")
                    _Impresso.ExportToXls(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".XLSX")
                    _Impresso.ExportToXlsx(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".PDF")
                    _Impresso.ExportToPdf(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".CSV")
                    _Impresso.ExportToCsv(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".HTML")
                    _Impresso.ExportToHtml(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".TXT")
                    _Impresso.ExportToText(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".RTF")
                    _Impresso.ExportToRtf(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".BMP")
                    _Impresso.ExportToImage(saveFileDialog1.FileName,System.Drawing.Imaging.ImageFormat.Bmp);
                
            }
            if (_PriComp != null)
            {
                if (CreateDocAutomatico)
                    _PriComp.CreateDocument();
                if (ex.ToUpper() == ".XLS")
                    _PriComp.PrintingSystem.ExportToXls(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".XLSX")
                    _PriComp.PrintingSystem.ExportToXlsx(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".PDF")
                    _PriComp.PrintingSystem.ExportToPdf(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".CSV")
                    _PriComp.PrintingSystem.ExportToCsv(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".HTML")
                    _PriComp.PrintingSystem.ExportToHtml(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".TXT")
                    _PriComp.PrintingSystem.ExportToText(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".RTF")
                    _PriComp.PrintingSystem.ExportToRtf(saveFileDialog1.FileName);
                else if (ex.ToUpper() == ".BMP")
                    _PriComp.PrintingSystem.ExportToImage(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
            };
            if (_AbrirArquivoExportado)
            {
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {
                    process.StartInfo.FileName = saveFileDialog1.FileName;
                    process.Start();
                }
            }
        }

        private void cBotaoImp_EnabledChanged(object sender, EventArgs e)
        {
            dropDownButton1.Enabled = this.Enabled;
        }
               
    }


    /// <summary>
    /// Tipos de chamado do botao
    /// </summary>
    public enum Botao
    {
        /// <summary>
        /// Bot�o
        /// </summary>
        botao,
        /// <summary>
        /// Imprimir
        /// </summary>
        imprimir,
        /// <summary>
        /// Mostrar na tela
        /// </summary>
        tela,
        /// <summary>
        /// Enviar por e.mail
        /// </summary>
        email,
        /// <summary>
        /// Gerar pdf
        /// </summary>
        pdf,
        /// <summary>
        /// n�o faz nada, S� � chamado por processo
        /// </summary>
        nulo,
        /// <summary>
        /// 
        /// </summary>
        Planilha
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void ErroBotaoHandler(object sender, Exception e);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void BotaoEventHandler(object sender, BotaoArgs args);

    /// <summary>
    /// 
    /// </summary>
    public class BotaoArgs : EventArgs
    {
        /// <summary>
        /// 
        /// </summary>
        public readonly Botao Botao;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="botao"></param>
        public BotaoArgs(Botao botao)
        {
            Botao = botao;
        }
    }
}
