using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Teste3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /*
        private void cBotaoImp1_clicado(object sender, CompontesBasicos.Botoes.BotaoArgs args)
        {
            string Assemblies = "Assemblies:\r\n";

            System.Reflection.Assembly[] Ass = AppDomain.CurrentDomain.GetAssemblies();

            if (Ass != null)
                foreach (System.Reflection.Assembly As in Ass)
                    Assemblies += As.FullName + Environment.NewLine;
            memoEdit1.Text = Assemblies;
        }
        */
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            string Assemblies = "Assemblies:\r\n";

            System.Reflection.Assembly[] Ass = AppDomain.CurrentDomain.GetAssemblies();

            if (Ass != null)
                foreach (System.Reflection.Assembly As in Ass)
                    Assemblies += As.FullName + Environment.NewLine;
            memoEdit1.Text = Assemblies;
        }



        private void simpleButton1_Click_1(object sender, EventArgs e)
        {
            Teste();
        }

        private void Teste()
        {
            memoEdit1.Text = "";
            virCompet.virCompetencia C1 = cvirCompetencia1.Comp;
            virCompet.virCompetencia C2;
            memoEdit1.Text = String.Format("1) competenica inicial:{0}\r\n", C1);
            memoEdit1.Text += String.Format("2) ++C1 {0} - {1}\r\n", ++C1, C1);
            memoEdit1.Text += String.Format("3) C1++ {0} - {1}\r\n", C1++, C1);
            C2 = C1;
            memoEdit1.Text += String.Format("4) C2 = C1 == {0} equial {1}\r\n", C1 == C2, C1.Equals(C2));
            C2 = C1.CloneCompet();
            memoEdit1.Text += String.Format("5) C2 clone(C1) == {0} equial {1}\r\n", C1 == C2, C1.Equals(C2));
            C2 = C1.Add(1);
            memoEdit1.Text += String.Format("6) C2 = C1.Add(1) C1 {0} - C2 {1}\r\n", C1, C2);
            C2 = C1.CloneCompet(1);
            memoEdit1.Text += String.Format("7) C1.CloneCompet(1) C1 {0} - C2 {1}\r\n", C1, C2);
            C1 = new virCompet.virCompetencia();
            C2 = C1.CloneCompet(20);
            memoEdit1.Text += String.Format("8) C1 {0} - C2 {1} C1-C2 {2} C1-5 {3} C1 {4}\r\n", C1, C2, C1 - C2, C1 - 5, C1);
            for (DateTime D = new DateTime(2011, 10, 1); D < new DateTime(2011, 12, 1); D = D.AddDays(1))
            {
                C1 = new virCompet.virCompetencia(D);
                memoEdit1.Text += String.Format("Dia {0} - C1 {1}\r\n", D, C1);
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            virCompet.virCompetencia.BuscaLimitesCompPadrao = new virCompet.virBuscaLimitesComp();
            Teste();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            virCompet.virCompetencia.BuscaLimitesCompPadrao = new virCompet.virBuscaLimitesCompDCorte(10, virCompet.virBuscaLimitesCompDCorte.Pega_dias_mes.passado);
            virCompet.virCompetencia.regimePadrao = virCompet.Regime.balancete;
            Teste();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            dllC12.c12 c12Teste = new dllC12.c12();
            c12Teste.Tipoj = dllC12.c12.TipoJuros.simples;
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 22), -14.88M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -17.03M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -180.45M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -170.32M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -196.11M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -46.16M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 3, 6), -1.37M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 13), 165.92M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 13), 180.32M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 13), 42.44M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 13), 31.24M));
            c12Teste.Parcelas.Add(new dllC12.c12.parcela(new DateTime(2012, 2, 13), 156.61M));
            if (c12Teste.calcularTaxaParcelas(0, 1, 0.000001))
                MessageBox.Show(string.Format("juros: {0:n10}",c12Teste.Taxa));
            else
                MessageBox.Show("erro");
        }
    }
}