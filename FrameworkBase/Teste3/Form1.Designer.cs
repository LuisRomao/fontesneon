namespace Teste3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            virCompet.virCompetencia virCompetencia1 = new virCompet.virCompetencia();
            virCompet.virBuscaLimitesComp virBuscaLimitesComp1 = new virCompet.virBuscaLimitesComp();
            virCompet.virCompetencia virCompetencia2 = new virCompet.virCompetencia();
            this.memoEdit1 = new DevExpress.XtraEditors.MemoEdit();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.cConfImpCopia1 = new dllConfigImpressoras.cConfImpCopia();
            this.cBotaoImp1 = new dllBotao.cBotaoImp();
            this.cvirCompetencia1 = new virCompetComp.cvirCompetencia();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // memoEdit1
            // 
            this.memoEdit1.Location = new System.Drawing.Point(35, 118);
            this.memoEdit1.Name = "memoEdit1";
            this.memoEdit1.Size = new System.Drawing.Size(718, 141);
            this.memoEdit1.TabIndex = 1;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(414, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(75, 23);
            this.simpleButton1.TabIndex = 4;
            this.simpleButton1.Text = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click_1);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(414, 41);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(75, 23);
            this.simpleButton2.TabIndex = 5;
            this.simpleButton2.Text = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Location = new System.Drawing.Point(414, 70);
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Size = new System.Drawing.Size(75, 23);
            this.simpleButton3.TabIndex = 6;
            this.simpleButton3.Text = "simpleButton3";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // cConfImpCopia1
            // 
            this.cConfImpCopia1.CaixaAltaGeral = true;
            this.cConfImpCopia1.Doc = System.Windows.Forms.DockStyle.None;
            this.cConfImpCopia1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cConfImpCopia1.Location = new System.Drawing.Point(75, 306);
            this.cConfImpCopia1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cConfImpCopia1.Name = "cConfImpCopia1";
            this.cConfImpCopia1.Size = new System.Drawing.Size(615, 190);
            this.cConfImpCopia1.somenteleitura = false;
            this.cConfImpCopia1.TabIndex = 3;
            this.cConfImpCopia1.Titulo = null;
            // 
            // cBotaoImp1
            // 
            this.cBotaoImp1.AbrirArquivoExportado = true;
            this.cBotaoImp1.AcaoBotao = dllBotao.Botao.botao;
            this.cBotaoImp1.BotaoEmail = true;
            this.cBotaoImp1.BotaoImprimir = true;
            this.cBotaoImp1.BotaoPDF = true;
            this.cBotaoImp1.BotaoTela = true;
            this.cBotaoImp1.Impresso = null;
            this.cBotaoImp1.Location = new System.Drawing.Point(119, 38);
            this.cBotaoImp1.Name = "cBotaoImp1";
            this.cBotaoImp1.NaoUsarxlsX = false;
            this.cBotaoImp1.PriComp = null;
            this.cBotaoImp1.Size = new System.Drawing.Size(171, 37);
            this.cBotaoImp1.TabIndex = 2;
            this.cBotaoImp1.Titulo = "Imprimir";
            // 
            // cvirCompetencia1
            // 
            this.cvirCompetencia1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cvirCompetencia1.Appearance.Options.UseBackColor = true;
            this.cvirCompetencia1.CaixaAltaGeral = true;
            this.cvirCompetencia1.CompetenciaBind = 200001;
            virCompetencia1.Ano = 2013;
            virCompetencia1.BuscaLimitesComp = virBuscaLimitesComp1;
            virCompetencia1.CompetenciaBind = 201310;
            virCompetencia1.Mes = 10;
            this.cvirCompetencia1.CompMaxima = virCompetencia1;
            virCompetencia2.Ano = 2000;
            virCompetencia2.BuscaLimitesComp = virBuscaLimitesComp1;
            virCompetencia2.CompetenciaBind = 200001;
            virCompetencia2.Mes = 1;
            this.cvirCompetencia1.CompMinima = virCompetencia2;
            this.cvirCompetencia1.Doc = System.Windows.Forms.DockStyle.None;
            this.cvirCompetencia1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cvirCompetencia1.IsNull = false;
            this.cvirCompetencia1.Location = new System.Drawing.Point(542, 38);
            this.cvirCompetencia1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cvirCompetencia1.Name = "cvirCompetencia1";
            this.cvirCompetencia1.PermiteNulo = false;
            this.cvirCompetencia1.Size = new System.Drawing.Size(117, 24);
            this.cvirCompetencia1.somenteleitura = false;
            this.cvirCompetencia1.TabIndex = 7;
            this.cvirCompetencia1.Titulo = null;
            // 
            // simpleButton4
            // 
            this.simpleButton4.Location = new System.Drawing.Point(12, 579);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(241, 23);
            this.simpleButton4.TabIndex = 8;
            this.simpleButton4.Text = "Testar C12";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 614);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.cvirCompetencia1);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.cConfImpCopia1);
            this.Controls.Add(this.cBotaoImp1);
            this.Controls.Add(this.memoEdit1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.memoEdit1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.MemoEdit memoEdit1;
        private dllBotao.cBotaoImp cBotaoImp1;
        private dllConfigImpressoras.cConfImpCopia cConfImpCopia1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private virCompetComp.cvirCompetencia cvirCompetencia1;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
    }
}

