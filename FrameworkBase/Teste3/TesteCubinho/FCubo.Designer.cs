﻿namespace Teste3
{
    partial class FCubo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cCubinhoTeste1 = new Teste3.TesteCubinho.cCubinhoTeste();
            this.SuspendLayout();
            // 
            // cCubinhoTeste1
            // 
            this.cCubinhoTeste1.CaixaAltaGeral = true;
            this.cCubinhoTeste1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cCubinhoTeste1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cCubinhoTeste1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            this.cCubinhoTeste1.Location = new System.Drawing.Point(0, 0);
            this.cCubinhoTeste1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cCubinhoTeste1.Name = "cCubinhoTeste1";
            this.cCubinhoTeste1.Size = new System.Drawing.Size(785, 548);
            this.cCubinhoTeste1.somenteleitura = false;
            this.cCubinhoTeste1.TabIndex = 0;
            this.cCubinhoTeste1.Titulo = null;
            // 
            // FCubo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 548);
            this.Controls.Add(this.cCubinhoTeste1);
            this.Name = "FCubo";
            this.Text = "FCubo";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.ResumeLayout(false);

        }

        #endregion

        private TesteCubinho.cCubinhoTeste cCubinhoTeste1;
    }
}