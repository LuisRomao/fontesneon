﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using dllVirCubinho;

namespace Teste3.TesteCubinho
{
    public partial class cCubinhoTeste :dllVirCubinho.cVirCubinho
    {
        public cCubinhoTeste()
        {
            InitializeComponent();
        }

        protected override void CadastraCampos()
        {
            GrupoCampos G1 = new GrupoCampos("G1");
            GrupoCampos G2 = new GrupoCampos("G2");
            GrupoCampos.ConjutoGrupos.Add(G1);
            GrupoCampos.ConjutoGrupos.Add(G2);

            Campo.ConjutoCampos.AddRange(new Campo[] { 
                new Campo("Desc Campo1", "Campo1",typeof(string),100,G1),
                new Campo("Desc Campo2", "Campo2",typeof(string),100,G1),
                new Campo("Desc Campo decimal", "Campo3",typeof(decimal),100,G2),
                new Campo("Desc Campo inteiro", "Campo4",typeof(int),100,G2)});
        }

        protected override void PopulaCombo()
        {
            comboBoxEdit1.Text = "x teste x";
            comboBoxEdit1.Properties.Items.Add("T1");
            comboBoxEdit1.Properties.Items.Add("T2");
            comboBoxEdit1.Properties.Items.Add("T3");
        }

        

        protected override string RecuperaConfiguracaoDoBanco(string nome)
        {            
            return "";
        }
    }

    
}
