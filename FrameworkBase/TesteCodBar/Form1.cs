﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TesteCodBar
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if (textImpressora.Text == "")
            {
                PrintDialog PD = new PrintDialog();
                if (PD.ShowDialog() == DialogResult.OK)
                {
                    textImpressora.Text = PD.PrinterSettings.PrinterName;
                }
            }
            
            string MascaraCopia = "";
            if (memoEdit1.Text == "")
            {
                MascaraCopia +=
                    //"L\r" +
                "^02c0001" +
                "^02m\r" +
                "^02L\r" +
                "z" +
                "D11\r" +
                    //abcdeeeffffgggg
                "121100000000000Zero\r" +

                "121100001000100xCem\r" +
                "121100002000200xDuzentos\r" +
                "121100003000300300\r" +
                "121100004000400 40\r" +
                "121100005000500500\r" +
                "Q0001\r" +
                "E\r" +
                "";
            }
            else
            
                MascaraCopia = memoEdit1.Text;
            CodBar.Impress ImpCopia = new CodBar.Impress(CodBar.LinguagemCodBar.Windows, MascaraCopia);
            ImpCopia.NomeImpressora = textImpressora.Text;
            ImpCopia.Reset();
            //ImpCopia.SetCampo("%Temperatura%", spinEdit1.Text);
            ImpCopia.ImprimeDOC();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            CodBar.Impress ImpCopia = new CodBar.Impress(CodBar.LinguagemCodBar.DPL);
            ImpCopia.NomeImpressora = textImpressora.Text;
            ImpCopia.ImprimeGabarito();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            CodBar.Impress ImpCopia = new CodBar.Impress(CodBar.LinguagemCodBar.EPL2);
            ImpCopia.NomeImpressora = textImpressora.Text;
            ImpCopia.ImprimeGabarito();
        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            CodBar.Impress ImpCopia = new CodBar.Impress(CodBar.LinguagemCodBar.Windows);
            ImpCopia.NomeImpressora = textImpressora.Text;
            ImpCopia.ImprimeGabarito();
        }
    }
}
