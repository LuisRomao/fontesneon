using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace VirMSSQL
{
    /// <summary>
    /// Buscador para banco de CEP
    /// </summary>
    public class CEP:TableAdapter
    {
        private static CEP stCEP;

        /// <summary>
        /// CEP padrao
        /// </summary>
        public static CEP STCEP {
            get {
                if (stCEP == null)
                    stCEP = new CEP();
                return stCEP;
            }
        }

        /// <summary>
        /// Bairro do cep
        /// </summary>
        public string Bairro;
        /// <summary>
        /// Cidade do CEP
        /// </summary>
        public string Cidade;
        /// <summary>
        /// UF
        /// </summary>
        public string Estado;
        /// <summary>
        /// Logradouro
        /// </summary>
        public string Rua;

        /// <summary>
        /// CEP Formatado
        /// </summary>
        public string CEPFormatado;
         
        /// <summary>
        /// Construtor
        /// </summary>
        public CEP() {
            TrocaCatalog("CEP");
        }

        /// <summary>
        /// C�digo da cidade no banco Neon
        /// </summary>
        /// <remarks>Esta fun��o deveria ser colocada em um componete derivado do tipo CEPNeon</remarks>
        public int nCidadeNeon;

        /// <summary>
        /// Se a cidade foi inclu�da agora;
        /// </summary>
        public bool NovaCidade;

        private string ComandoBuscaLogradouro =
"SELECT     tblLogradouros.UF, tblLogradouros.Descricao AS Rua, tblCidades.Descricao AS Cidade, tblBairros.Descricao AS Bairro\r\n" +
"FROM         tblLogradouros INNER JOIN\r\n" +
"                      tblBairros ON tblLogradouros.CodigoBairro = tblBairros.Codigo INNER JOIN\r\n" +
"                      tblCidades ON tblLogradouros.CodigoCidade = tblCidades.Codigo\r\n" +
"WHERE     (tblLogradouros.CEP = @P1);";
        private string ComandoBuscaCidade =
"SELECT     CID,CIDUf\r\n" +
"FROM         CIDADES\r\n" +
"WHERE     (CIDNome = @P1);";
        private string InsereCidade =
"INSERT INTO CIDADES (CIDNome, CIDUf)\r\n" +
"VALUES     (@P1,@P2);";
        private string ComandoCorrigeUF =
"UPDATE    CIDADES\r\n" +
"SET              CIDUf = @P1\r\n" +
"WHERE     (CID = @P2);";

        /// <summary>
        /// BuscaPorCEP
        /// </summary>
        /// <param name="CEP"></param>
        /// <returns></returns>
        public bool BuscaPorCEP(int CEP) { 
            return BuscaPorCEP(CEP.ToString().PadLeft(8,'0'));
        }

        /// <summary>
        /// BuscaPorCEP
        /// </summary>
        /// <param name="CEP"></param>
        /// <returns></returns>
        public bool BuscaPorCEP(string CEP) {
            string CEPLimpo = "";
            foreach (Char car in CEP)
                if (Char.IsDigit(car))
                    CEPLimpo += car;
            DataRow Retorno = BuscaSQLRow(ComandoBuscaLogradouro,CEPLimpo);
            if (Retorno == null)
                return false;
            else {
                Bairro = Retorno["Bairro"].ToString().ToUpper();
                Cidade = Retorno["Cidade"].ToString().ToUpper();
                Estado = Retorno["UF"].ToString().ToUpper();
                Rua = Retorno["Rua"].ToString().ToUpper();
                CEPFormatado = CEPLimpo.Insert(5, "-");
                DataRow CIDrow = ST().BuscaSQLRow(ComandoBuscaCidade, Cidade);
                if (CIDrow == null)
                {
                    try
                    {
                        AbreTrasacaoSQL("VirMSSQL CEP 104 BuscaPorCEP");
                        nCidadeNeon = ST().IncluirAutoInc(InsereCidade, Cidade, Estado);
                        CommitSQL();
                    }
                    catch(Exception e)
                    {
                        VircatchSQL(e);
                        return false;
                    }
                    NovaCidade = true;
                }
                else
                {
                    nCidadeNeon = (int)CIDrow["CID"];
                    NovaCidade = false;
                    if (Estado != CIDrow["CIDUf"].ToString())
                    {
                        ST().ExecutarSQLNonQuery(ComandoCorrigeUF, Estado, nCidadeNeon);
                        NovaCidade = true;
                    }
                    
                }
                return true;
            }
        }
    }
}
