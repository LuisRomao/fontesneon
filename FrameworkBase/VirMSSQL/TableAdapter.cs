using System;
using System.Data;
using System.Data.SqlClient;
using VirDB;
using VirDB.Bancovirtual;
using System.Text;


namespace VirMSSQL
{
    /// <summary>
    /// TableAdapter para MSSQL
    /// </summary>
    public class TableAdapter : VirtualTableAdapter
    {
        /// <summary>
        /// Esta boleana indica que estamos no servidor de processos portanto os objetos est�ticos s�o compartilhados com outros usu�rios e devemos ser muito cuidadosos no seu uso.
        /// </summary>
        public static bool Servidor_De_Processos = false;

        private static void RegistrarErro(string Local)
        {
            throw new System.Exception(string.Format("Uso de vari�vel est�tica no servidor de processos:{0}", Local));
        }

        /// <summary>
        /// Rastrear erro de concorrencia
        /// </summary>
        /// <param name="DR"></param>
        /// <returns></returns>
        public string RastreiaConcorrencia(DataRow DR)
        {            
            StringBuilder SB = new StringBuilder();
            SB.AppendLine("#########  VERIFICA��O DE CONCORRENCIA  #########");
            int ID = (int)DR[0, DataRowVersion.Original];
            string Tabela = DR.Table.TableName;
            string CampoID = DR.Table.Columns[0].ColumnName;
            string Comando = string.Format("select * from {0} where {1} = @P1", Tabela, CampoID);
            SB.AppendFormat("ID: {0} = {1}\r\nTabela: {2}\r\nComando: {3}\r\n\r\nLocal => Lido\r\n\r\n", CampoID, ID, Tabela, Comando);
            DataRow DRLido = BuscaSQLRow(Comando, ID);
            foreach (DataColumn DC in DR.Table.Columns)
            {
                if (DRLido.Table.Columns.Contains(DC.ColumnName))
                    SB.AppendFormat("{0,-10}: |{1}| => |{2}| {3}\r\n", DC.ColumnName, DR[DC, DataRowVersion.Original], DRLido[DC.ColumnName], DR[DC, DataRowVersion.Original].ToString() == DRLido[DC.ColumnName].ToString() ? "ok" : "??????????????????");
                else
                    SB.AppendFormat("{0,-10}: {1}\r\n", DC.ColumnName, DR[DC, DataRowVersion.Original]);
            }
            SB.AppendLine("\r\n#########  VERIFICA��O DE CONCORRENCIA  #########");
            return SB.ToString();
        }

        /// <summary>
        /// Cria uma nova conexao manualmente
        /// </summary>
        /// <param name="StringDeConexao"></param>
        /// <remarks>N�o aplic�vel para banco MSSQL</remarks>
        public override void CriaCone(string StringDeConexao)
        {
            throw new Exception("The method or operation is not implemented.");
        }
        
        /// <summary>
        /// Data e hora do servidor
        /// </summary>
        /// <returns></returns>
        public override DateTime DataServidor()
        {
            DateTime Retorno;
            if (BuscaEscalar("select CURRENT_TIMESTAMP", out Retorno))
                return Retorno;
            else
                return DateTime.MinValue;
        }
        
        /// <summary>
        /// TableAdapter est�tico
        /// </summary>
        protected override VirtualTableAdapter STTableAdapterEspecifico
        {
            get 
            {
                if (Servidor_De_Processos)
                    RegistrarErro("VirMSSQL.Tableadapter 56");
                return ST(Tipo); 
            }
        }

        /// <summary>
        /// Verifica se est� em transa��o
        /// </summary>
        protected override bool EstaEmTransST
        {
            get { return (GuardaConeLocal != null); }
        }

        /// <summary>
        /// Troca o catalog do string de conex�o
        /// </summary>
        /// <param name="Novo"></param>
        public void TrocaCatalog(string Novo)
        {
            SqlConnectionStringBuilder Buider = new SqlConnectionStringBuilder(BancoVirtual.StringConeFinal(TiposDeBanco.SQL));
            Buider.InitialCatalog = Novo;
            Cone.ConnectionString = Buider.ConnectionString;
        }

        
        /// <summary>
        /// Adapter do objeto derivado
        /// </summary>        
        /// <remarks>Para os objetos staticos retorna null</remarks>
        public SqlDataAdapter VirAdapter
        {
            get
            {
                System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
                System.Reflection.PropertyInfo PropAdap = GetType().GetProperty("Adapter", BF);
                if (PropAdap != null)
                    return (SqlDataAdapter)PropAdap.GetValue(this, null);
                else
                    return null;
            }
        }


        #region Conexao
        private SqlConnection _Cone;

        /// <summary>
        /// Conex�o local
        /// </summary>
        public SqlConnection Cone
        {
            get
            {
                if (_Cone == null)
                {
                    System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                    if (PropCon != null)
                        _Cone = (SqlConnection)PropCon.GetValue(this, null);
                    else
                        _Cone = new SqlConnection(BancoVirtual.StringConeFinal(Tipo));
                };
                return _Cone;
            }
            set
            {
                _Cone = value;

                System.Reflection.PropertyInfo PropCon = GetType().GetProperty("Connection");
                if (PropCon != null)
                    PropCon.SetValue(this, value, null);


            }
        }

        /// <summary>
        /// Fecha Conexao
        /// </summary>
        /// <returns></returns>
        protected override bool FechaCone()
        {
            if (ManterConeAberta <= 0)
                if (_Cone != null)
                    if (Cone.State != ConnectionState.Closed)
                        Cone.Close();
                
            return true;
        }

        /// <summary>
        /// Abre conexao
        /// </summary>
        /// <returns></returns>
        protected override bool AbreCone()
        {           
            if (Cone.State == ConnectionState.Closed)
                Cone.Open();
            return true;        
        }

        #endregion



        private static TableAdapter stTableAdapter;
        private static TableAdapter stTableAdapterFilial;
        private static TableAdapter stTableAdapterMorto;
        private static TableAdapter stTableAdapterInt1;
        private static TableAdapter stTableAdapterInt2;


        
        /// <summary>
        /// Fornece a instancia est�tinca para SQL
        /// </summary>
        public static TableAdapter STTableAdapter
        {
            get
            {
                if (Servidor_De_Processos)
                    RegistrarErro("VirMSSQL.Tableadapter 176");
                if (stTableAdapter == null)
                {
                    stTableAdapter = new TableAdapter(TiposDeBanco.SQL,true);
                };
                return stTableAdapter;
            }

        }
         
        /// <summary>
        /// Fornece a instancia est�tinca para o tipo definido
        /// </summary>
        /// <param name="TB">Tipo 
        /// S� s�o v�idos os SQL,Interne1 e Internet2
        /// </param>
        /// <returns>Instancia est�tica</returns>
        public static TableAdapter ST(TiposDeBanco TB = TiposDeBanco.SQL)
        {
            if (Servidor_De_Processos)
                RegistrarErro("VirMSSQL.Tableadapter 196");
            switch (TB)
            {
                case TiposDeBanco.SQL:
                    if (stTableAdapter == null)
                        stTableAdapter = new TableAdapter(TB, true);                    
                    return stTableAdapter;
                case TiposDeBanco.Filial:
                    if (stTableAdapterFilial == null)
                        stTableAdapterFilial = new TableAdapter(TB, true);                    
                    return stTableAdapterFilial;
                case TiposDeBanco.SQLMorto:
                    if (stTableAdapterMorto == null)
                        stTableAdapterMorto = new TableAdapter(TB);
                    return stTableAdapterMorto;
                case TiposDeBanco.Internet1:
                    if (stTableAdapterInt1 == null)
                        stTableAdapterInt1 = new TableAdapter(TB);
                    return stTableAdapterInt1;
                case TiposDeBanco.Internet2:
                    if (stTableAdapterInt2 == null)
                        stTableAdapterInt2 = new TableAdapter(TB);
                    return stTableAdapterInt2;
                case TiposDeBanco.Access:
                case TiposDeBanco.AccessH:
                case TiposDeBanco.FB:
                    throw new Exception("Tipo de banco inv�lido");
                default:
                    throw new NotImplementedException(string.Format("Enumera��o n�o tratada: {0}", TB));
            }

        }

        #region Construtores

        /*
        /// <summary>
        /// TableAdapter para MSSQL
        /// </summary> 
        public TableAdapter()
        {
            Tipo = TiposDeBanco.SQL;
        }*/

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="_Tipo">Tipo 
        /// S� s�o v�idos os SQL,Interne1 e Internet2
        /// </param>
        /// <param name="Controlador">Componente controla transa��es</param>
        public TableAdapter(TiposDeBanco _Tipo = TiposDeBanco.SQL, bool Controlador = false)
        {
            Tipo = _Tipo;            
            if ((Tipo != TiposDeBanco.SQL)
                &&
                (Tipo != TiposDeBanco.Internet1)
                &&
                (Tipo != TiposDeBanco.Internet2)
                &&
                (Tipo != TiposDeBanco.SQLMorto)
                &&
                (Tipo != TiposDeBanco.Filial)
               )
                throw new Exception("Parametros inv�lidos");
            TrocarStringDeConexao();
            ControladorDeTrans = Controlador;
        } 
        #endregion

        private static bool Dupla_Local_Filial;

        /// <summary>
        /// Inicia uma trasa��o em 2 bancos de dados
        /// </summary>
        /// <param name="Identificador"></param>
        /// <returns></returns>
        public static bool AbreTrasacaoSQLDupla(string Identificador)
        {
            Dupla_Local_Filial = true;
            return ST(TiposDeBanco.SQL).AbreTrasacaoLocal(Identificador) && ST(TiposDeBanco.Filial).AbreTrasacaoLocal(Identificador);
          
        }

        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador">Identificador</param>
        /// <param name="VirtualTableAdapters">TableAdapters envolvidos</param>
        /// <returns></returns>
        public static bool AbreTrasacaoSQL(string Identificador, params TableAdapter[] VirtualTableAdapters)
        {
            TiposDeBanco Tipo = VirtualTableAdapters.Length == 0 ? TiposDeBanco.SQL : VirtualTableAdapters[0].Tipo;            
            foreach (TableAdapter TA in VirtualTableAdapters)
                if (TA.Tipo != Tipo)
                    throw new Exception("AbreTrasacaoSQL com tipos difierentes");
            return AbreTrasacaoSQL(Tipo, Identificador, VirtualTableAdapters) != null;            
        }

        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador">Identificador</param>
        /// <param name="VirtualTableAdapters">TableAdapters envolvidos</param>
        /// <param name="Tipo"></param>
        /// <returns></returns>
        public static TableAdapter AbreTrasacaoSQL(TiposDeBanco Tipo,string Identificador, params TableAdapter[] VirtualTableAdapters)
        {
            if (ST(Tipo).AbreTrasacaoLocal(Identificador, VirtualTableAdapters))
                return ST(Tipo);
            else
                return null;
        }

        /// <summary>
        /// Entra na transa��o se existir
        /// </summary>
        /// <param name="VirtualTableAdapters"></param>
        /// <returns></returns>
        public static bool EmbarcaEmTrans(params VirtualTableAdapter[] VirtualTableAdapters)
        { 
            TiposDeBanco Tipo = VirtualTableAdapters.Length == 0 ? TiposDeBanco.SQL : VirtualTableAdapters[0].Tipo;
            foreach(TableAdapter TA in VirtualTableAdapters)
                if(TA.Tipo != Tipo)
                    throw new Exception("Tipos diferentes de banco de dados em trasa��o");
            return EmbarcaEmTrans(Tipo, VirtualTableAdapters);
        }

        private static bool EmbarcaEmTrans(TiposDeBanco Tipo, params VirtualTableAdapter[] VirtualTableAdapters)
        {
            if (ST(Tipo).SubTrans == 0)
                return false;
            foreach (TableAdapter TA in VirtualTableAdapters)            
                if (TA.EntrarEmTrans(ST(Tipo)))
                    ST(Tipo).VirtualTableAdaptersImplantados.Add(TA);            
            return true;
        }

        private bool ControladorDeTrans = false;

        /// <summary>
        /// Coloca os tableAdapters na transa��o controlado por este objeto
        /// </summary>
        /// <param name="VirtualTableAdapters"></param>
        /// <returns></returns>
        public bool AcolheTrans(params VirtualTableAdapter[] VirtualTableAdapters)
        {
            if (!ControladorDeTrans)
                throw new Exception("TableAdapter n�o � um controlador");
            foreach (TableAdapter TA in VirtualTableAdapters)
                if(TA.Tipo != Tipo)
                    throw new Exception("Tipos diferentes de banco de dados em trasa��o");
            if (SubTrans == 0)
                return false;
            foreach (TableAdapter TA in VirtualTableAdapters)
                if (TA.EntrarEmTrans(this))
                    VirtualTableAdaptersImplantados.Add(TA);
            return true;
        }

        /*
        /// <summary>
        /// Termina uma transa��o com commit. Usar para grupo (st)
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans.</remarks>        
        public static bool CommitSQL()
        {
            //return STTableAdapter.Commit();
            return CommitSQL(TiposDeBanco.SQL);
        }*/
        
        /// <summary>
        /// Termina uma transa��o com commit. Usar para grupo (st)
        /// </summary>        
        /// <param name="Tipo"></param>
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans.</remarks>        
        public static bool CommitSQL(TiposDeBanco Tipo =  TiposDeBanco.SQL)
        {            
            return ST(Tipo).Commit();
        }

        /// <summary>
        /// Commit para transa��o dupla
        /// </summary>
        /// <returns></returns>
        public static bool CommitSQLDuplo()
        {
            if (!Dupla_Local_Filial)
                throw new Exception("CommitSQLDuplo sem trasa��o simples");
            Dupla_Local_Filial = false;
            return ST(TiposDeBanco.SQL).Commit() && ST(TiposDeBanco.Filial).Commit();
        }

        /// <summary>
        /// Termina uma transa��o com Rollback - Usar em bloco cath
        /// A fun��o do virch � fazer o Rollback quando ocorrer um exception (pode ter sido provoda por uma subtran)
        /// </summary>        
        /// <returns>Retorna true se a opera��o foi executada</returns>
        /// <param name="e">Exception anterior - usar null se for a primeira de transa��o dupla (dois bancos)</param>
        /// <param name="Tipo"></param>
        /// <remarks>Se estiver em sub-transacao decrementa SubTrans</remarks>  
        public static bool VircatchSQL(Exception e, TiposDeBanco Tipo = TiposDeBanco.SQL)
        {
            bool retorno = ST(Tipo).Vircatch(e);
            if(Dupla_Local_Filial)
                ST(Tipo == TiposDeBanco.SQL ? TiposDeBanco.Filial : TiposDeBanco.SQL).Vircatch(null);
            Dupla_Local_Filial = false;
            return retorno;
        }

        

        

        


        

        #region Transacao

        /*
        /// <summary>
        /// Inicia uma transacao Local e implanta nos VirtualTableAdapters, Usar DENTRO do try com vircath
        /// </summary>
        /// <param name="Identificador"></param>
        /// <param name="VirtualTableAdapters"></param>
        /// <returns></returns>                
        public override bool AbreTrasacaoLF(string Identificador, params VirtualTableAdapter[] VirtualTableAdapters)
        {
            if (STTableAdapter.Equals(this))
                return base.AbreTrasacaoLF(Identificador, VirtualTableAdapters);
            else
            {
                VirMSSQL.TableAdapter[] VirtualTableAdaptersNovo = new TableAdapter[VirtualTableAdapters.Length + 1];
                VirtualTableAdaptersNovo[0] = this;
                for (int i = 0; i < VirtualTableAdapters.Length; i++)
                    VirtualTableAdapters[i + 1] = VirtualTableAdapters[i];
                return AbreTrasacaoSQL(Identificador, VirtualTableAdaptersNovo);
            }
        }*/



        /// <summary>
        /// Transa��o do componete
        /// </summary>
        public SqlTransaction Tran;

        /// <summary>
        /// Quarda a conexao real do componente para incorpor�lo na transaco STD
        /// </summary>
        private SqlConnection GuardaConeLocal;

        /// <summary>
        /// Inicia Transacao
        /// </summary>
        /// <param name="Nivel">Nivel de isolamento</param>
        /// <returns>Se a trasacao foi iniciada</returns>       
        protected override bool IniciaTrasacaoReal(IsolationLevel Nivel)
        {
            if (Tran != null)
            {
                FimTrasacaoReal(false);
                throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de abertura de transa��o com outra j� em andamento", SubTrans));
            }
            else
            {
                if (!AbreCone())
                    return false;
                Tran = Cone.BeginTransaction(Nivel);
                ImplantaTrans(true);
                return true;
            }
        }

        /// <summary>
        ///Coloca o componente na transacao de _Dono_Trans que pode ser STTableAdapter
        /// </summary>
        /// <remarks>Este m�todo � para que v�rios objetos participem da mesma transacao: A transa��o de STTableAdapter\r\nGera exce��o se STTableAdapter nao estiver em transacao</remarks>       
        protected override bool EntrarEmTrans(VirtualTableAdapter _Dono_Trans)
        {
            TableAdapter Dono_Trans = (TableAdapter)_Dono_Trans;
            if (GuardaConeLocal != null)
                return false;
            if (Tran != null)
                throw new Exception("Objeto em trasa��o local");
            else
            {
                //EstaEmTransST = true;
                if (Dono_Trans.Tran == null)
                    throw new Exception("STTableAdapter n�o est� em transacao");
                Tran = Dono_Trans.Tran;
                GuardaConeLocal = Cone;
                Cone = Dono_Trans.Cone;
                ImplantaTrans(true);
                return true;
            }
        }

        /// <summary>
        /// Retorna se a transa�ao est�tica esta ativa
        /// </summary>
        /// <returns></returns>
        public override bool STTransAtiva()
        {
            return (TableAdapter.ST().Tran != null);
        }

        /// <summary>
        /// Coloca o adapter sob a transacao Tran
        /// </summary>
        /// <param name="Inicia">Inicio ou termino</param>
        protected override void ImplantaTrans(bool Inicia)
        {
            if (VirAdapter == null)
                return;
            SqlTransaction Tranx = Inicia ? Tran : null;
            if (VirAdapter.InsertCommand != null)
                VirAdapter.InsertCommand.Transaction = Tranx;

            if (VirAdapter.DeleteCommand != null)
                VirAdapter.DeleteCommand.Transaction = Tranx;

            if (VirAdapter.UpdateCommand != null)
                VirAdapter.UpdateCommand.Transaction = Tranx;

            if (VirAdapter.SelectCommand != null)
                VirAdapter.SelectCommand.Transaction = Tranx;
            System.Reflection.BindingFlags BF = System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance;
            System.Reflection.PropertyInfo PropCom = GetType().GetProperty("CommandCollection", BF);
            if (PropCom != null)
            {
                SqlCommand[] Comandos = (SqlCommand[])PropCom.GetValue(this, null);
                for (int i = 0; (i < Comandos.Length); i = (i + 1))
                    if ((Comandos[i] != null))
                        Comandos[i].Transaction = Tranx;
            }
        }

        /// <summary>
        /// Termina trasacao
        /// </summary>
        /// <param name="Commit">True para commit e false para rollback</param>
        /// <returns>Esta retornando sempre true</returns>
        protected override bool FimTrasacaoReal(bool Commit)
        {
            ImplantaTrans(false);
            if (GuardaConeLocal != null)
            {
                Cone = GuardaConeLocal;
                GuardaConeLocal = null;
                Tran = null;
            }
            else
            {
                if (Tran == null)
                    throw new InvalidOperationException(String.Format("Estado inv�lido: SubTrans = {0}\r\n Tentativa de termino de transa��o com Trans = null", SubTrans));
                if (Commit)
                    Tran.Commit();
                else
                    try
                    {
                        Tran.Rollback();
                    }
                    catch { }
                Tran = null;
                FechaCone();
            };
            return true;
        }

        
        #endregion

        /// <summary>
        /// Executa um comando SQL.
        /// </summary>
        /// <remarks>Os paramentros devem obrigatoriamentes ser nomeados como @P1, @P2 ...</remarks>
        /// <param name="comando">Comando SQL: select, update ou delete</param>
        /// <param name="TipoComando">
        /// Tipo de a��o/retorno
        /// pode retornar um unico registro em forma de ArrayList,
        /// escalar,
        /// DataTable
        /// ou ArrayList com os valores do primeiro registro
        /// </param>
        /// <param name="Paramentros">
        /// Paramentros do comando SQL usar sintaxe:        
        /// </param>
        public override object ExecutarSQL(string comando, ComandosBusca TipoComando, params object[] Paramentros)
        {
            object oRetorno;
            if (!AbreCone())
                return null;


            using (SqlCommand Comando = new SqlCommand(comando, Cone, Tran))
            {
                if (Paramentros != null)
                {
                    int i = 1;
                    foreach (object Parametro in Paramentros)
                    {
                        Comando.Parameters.AddWithValue("@P" + i.ToString(), Parametro);
                        i++;
                    }
                }
                switch (TipoComando)
                {
                    case ComandosBusca.ArrayList:
                        System.Collections.ArrayList retorno = new System.Collections.ArrayList();
                        SqlDataReader RespostaSQL = Comando.ExecuteReader();
                        if (RespostaSQL.Read())
                            for (int i = 0; i < RespostaSQL.FieldCount; i++)
                                retorno.Add(RespostaSQL[i]);
                        RespostaSQL.Close();
                        oRetorno = retorno;
                        break;
                    case ComandosBusca.DataRow:
                        DataTable TabelaRetorno2 = new DataTable();
                        int registros = (new SqlDataAdapter(Comando).Fill(TabelaRetorno2));
                        if (registros > 0)
                            oRetorno = TabelaRetorno2.Rows[0];
                        else
                            oRetorno = null;
                        break;
                    case ComandosBusca.DataTable:
                        DataTable TabelaRetorno = new DataTable();
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(Comando))
                        {
                            sqlDataAdapter.Fill(TabelaRetorno);
                        }
                        oRetorno = TabelaRetorno;
                        break;
                    case ComandosBusca.Escalar:
                        oRetorno = Comando.ExecuteScalar();
                        break;
                    case ComandosBusca.Reader:
                        oRetorno = Comando.ExecuteReader();
                        break;
                    case ComandosBusca.NonQuery:
                        oRetorno = Comando.ExecuteNonQuery();
                        break;
                    default:
                        oRetorno = null;
                        break;
                }
            }
            if (Tran == null)
                FechaCone();
            return oRetorno;
        }

        /// <summary>
        /// Executa o comando e retorna o ID
        /// </summary>
        /// <param name="comando">Comando tipo insert</param>
        /// <param name="Parametros">Parametos do camando marcados como @P1,@P2... </param>
        /// <remarks>O comando deve ser do tipo insert e com um campo auto inc na tabela</remarks>
        /// <returns>ID do novo registro. Retorna -1 se der falha</returns>
        public override int IncluirAutoInc(string comando, params object[] Parametros)
        {
            int alterados = ExecutarSQLNonQuery(comando, Parametros);
            if (alterados != 1)
                return -1;
            else
            {
                int ID;
                if (BuscaEscalar("SELECT @@identity", out ID))
                    return ID;
                else
                    return -1;
            }
        }

        /// <summary>
        /// Verifica campos originais alterados
        /// </summary>
        /// <param name="DR"></param>
        /// <param name="Relatorio"></param>
        /// <param name="tabela"></param>
        /// <param name="CampoID"></param>
        /// <returns></returns>
        public static bool ChecarOriginais(DataRow DR,out string Relatorio, string tabela = null, string CampoID = null)
        {
            bool retorno = true;
            if (string.IsNullOrEmpty(tabela))
                tabela = DR.Table.TableName;
            if (string.IsNullOrEmpty(CampoID))
                CampoID = DR.Table.Columns[0].ColumnName;
            Relatorio = string.Format("{0}: {1} = {2}\r\n",tabela, CampoID,DR[CampoID]);
            DataRow rowBanco = TableAdapter.ST().BuscaSQLRow(string.Format("select * from {0} where {1} = {2}", tabela, CampoID, DR[CampoID]));
            foreach (DataColumn DC in DR.Table.Columns)
            {
                if ((rowBanco[DC.ColumnName] == null) || (DR[DC.ColumnName, DataRowVersion.Original] == null))
                {
                    if ((rowBanco[DC.ColumnName] == null) && (DR[DC.ColumnName, DataRowVersion.Original] != null))
                    {
                        Relatorio += string.Format("Campo |{0}| banco |{1}| -> original lido |{2}|\r\n", DC.ColumnName, "null", DR[DC.ColumnName, DataRowVersion.Original]);                                                
                        retorno = false;
                    }
                    else if ((rowBanco[DC.ColumnName] != null) && (DR[DC.ColumnName, DataRowVersion.Original] == null))
                    {
                        Relatorio += string.Format("Campo |{0}| banco |{1}| -> original |{2}|\r\n", "null", rowBanco[DC.ColumnName], DR[DC.ColumnName, DataRowVersion.Original]);
                        retorno = false;
                    }
                }
                else
                    if (rowBanco[DC.ColumnName].ToString() != DR[DC.ColumnName, DataRowVersion.Original].ToString())
                    {
                        Relatorio += string.Format("Campo |{0}| banco |{1}| -> original |{2}|\r\n", DC.ColumnName, rowBanco[DC.ColumnName], DR[DC.ColumnName, DataRowVersion.Original]);
                        retorno = false;
                    }
            }
            if (!retorno)
                Console.WriteLine("**************** ERRO****************\r\n{0}\r\n",Relatorio);
            return retorno;
        }

        /// <summary>
        /// Entra na transa��o ST se existir.
        /// </summary>        
        /// <returns>se estava em transa��o</returns>
        public virtual bool EmbarcaEmTransST()
        {
            return EmbarcaEmTrans(this);
        }
    }
}
