using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace virImpressoraCh
{
    /// <summary>
    /// 
    /// </summary>
    public partial class cConfigCheque : CompontesBasicos.ComponenteBase
    {
        /// <summary>
        /// 
        /// </summary>
        public cConfigCheque()
        {
            InitializeComponent();            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1 - Preparando leitura");
            int Banco = (int)spinbanco.Value;
            int cvn = 0;
            int ce1 = 0;
            int can = 0;
            int cda = 0;
            int lvn = 0;
            int le1 = 0;
            int le2 = 0;
            int lbe = 0;
            int lan = 0;
            int cpr = 0;
            ImpCheque.ImpChequeSt.Teste = true;
            if(ImpCheque.ImpChequeSt.ModeloImpressora == ImpCheque.Modelos.Perto)
                MessageBox.Show("2 - Modelo = perto");
            else
                MessageBox.Show("2 - Modelo = " + ImpCheque.ImpChequeSt.ModeloImpressora.ToString());
            if (ImpCheque.ImpChequeSt.LeLayout(Banco, ref cvn, ref lvn, ref ce1, ref le1, ref le2, ref can, ref lan, ref cda, ref lbe, ref cpr))
            {
                spincvn.Value = cvn;
                spince1.Value = ce1;
                spincan.Value = can;
                spincda.Value = cda;
                spinlvn.Value = lvn;
                spinle1.Value = le1;
                spinle2.Value = le2;
                spinlbe.Value = lbe;
                spinlan.Value = lan;
                spincpr.Value = cpr;
            }
            else
                MessageBox.Show("Erro de leitura");
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("1 - Preparando programação");
            int Banco = (int)spinbanco.Value;
            int cvn = (int)spincvn.Value;
            int ce1 = (int)spince1.Value;
            int can = (int)spincan.Value;
            int cda = (int)spincda.Value;
            int lvn = (int)spinlvn.Value;
            int le1 = (int)spinle1.Value;
            int le2 = (int)spinle2.Value;
            int lbe = (int)spinlbe.Value;
            int lan = (int)spinlan.Value;
            int cpr = (int)spincpr.Value;
            if (ImpCheque.ImpChequeSt.ProgramaLayout(Banco, cvn, lvn, ce1, le1, le2, can, lan, cda, lbe, cpr))
            {
                MessageBox.Show("Programação ok");
                if (ImpCheque.ImpChequeSt.Imprime(Banco, 1000.00M, "LOCAL", "FAVORECIDO", DateTime.Today))
                    System.Windows.Forms.MessageBox.Show("ok");
                else
                    System.Windows.Forms.MessageBox.Show("falha");
            }
            else
                MessageBox.Show("Erro na programação");
        }
    }
}
