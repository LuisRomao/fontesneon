﻿namespace TesteRobo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPathSaida = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPathEntrada = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdRoboBradesco = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.txtPathRobo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtPathRobo);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtPathSaida);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtPathEntrada);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cmdRoboBradesco);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 101);
            this.panel1.TabIndex = 2;
            // 
            // txtPathSaida
            // 
            this.txtPathSaida.Location = new System.Drawing.Point(332, 71);
            this.txtPathSaida.Name = "txtPathSaida";
            this.txtPathSaida.Size = new System.Drawing.Size(454, 20);
            this.txtPathSaida.TabIndex = 3;
            this.txtPathSaida.Text = "C:\\Temp\\Neon\\ArquivosEnvio\\ROBO_BRADESCO\\SAIDA\\";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(222, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Diretório de Saída:";
            // 
            // txtPathEntrada
            // 
            this.txtPathEntrada.Location = new System.Drawing.Point(332, 42);
            this.txtPathEntrada.Name = "txtPathEntrada";
            this.txtPathEntrada.Size = new System.Drawing.Size(454, 20);
            this.txtPathEntrada.TabIndex = 2;
            this.txtPathEntrada.Text = "C:\\Temp\\Neon\\ArquivosEnvio\\ROBO_BRADESCO\\ENTRADA\\";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(222, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Diretório de Entrada:";
            // 
            // cmdRoboBradesco
            // 
            this.cmdRoboBradesco.Location = new System.Drawing.Point(12, 12);
            this.cmdRoboBradesco.Name = "cmdRoboBradesco";
            this.cmdRoboBradesco.Size = new System.Drawing.Size(168, 23);
            this.cmdRoboBradesco.TabIndex = 0;
            this.cmdRoboBradesco.Text = "Robo Bradesco";
            this.cmdRoboBradesco.UseVisualStyleBackColor = true;
            this.cmdRoboBradesco.Click += new System.EventHandler(this.cmdRoboBradesco_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(0, 101);
            this.textBox1.MaxLength = 0;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(798, 454);
            this.textBox1.TabIndex = 5;
            this.textBox1.WordWrap = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // txtPathRobo
            // 
            this.txtPathRobo.Location = new System.Drawing.Point(332, 12);
            this.txtPathRobo.Name = "txtPathRobo";
            this.txtPathRobo.Size = new System.Drawing.Size(454, 20);
            this.txtPathRobo.TabIndex = 6;
            this.txtPathRobo.Text = "C:\\Temp\\Neon\\ArquivosEnvio\\ROBO_BRADESCO\\";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(222, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Diretório Robo:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 555);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdRoboBradesco;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox txtPathSaida;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPathEntrada;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPathRobo;
        private System.Windows.Forms.Label label3;
    }
}