﻿/*
MR - 04/12/2014 10:00 -           - Ajuste no erro do looping de arquivos (Alterações indicadas por *** MRC - INICIO - TRIBUTOS (04/12/2014 10:00) ***)
*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TesteRobo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Robo.Bradesco _RoboBradesco;
        private Robo.Bradesco RoboBradesco
        {
            get
            {
                return _RoboBradesco ?? (_RoboBradesco = new Robo.Bradesco(CompontesBasicosProc.FuncoesBasicasProc.EstaEmProducao, Framework.DSCentral.EMP));
                //return _RoboBradesco ?? (_RoboBradesco = new Robo.Bradesco(true, 1)); //FORÇA O TESTE EM PRODUÇÃO (CUIDADO !!!)
            }
        }

        private void cmdRoboBradesco_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox1.Text += RoboBradesco.Processa(txtPathRobo.Text, txtPathEntrada.Text, txtPathSaida.Text);
            textBox1.Text += "\r\n";
            textBox1.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
        }
    }
}
