﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using DocBacarios;

namespace dllvirNFE
{
    /// <summary>
    /// Nota fiscal eletrônica PL_006g
    /// </summary>
    public class NotaNFE
    {
        private bool valida;

        /// <summary>
        /// 
        /// </summary>
        public Exception UltimoErro;

        private TNfeProc nota;

        /// <summary>
        /// 
        /// </summary>
        public TNfeProc Nota
        {
            get { return nota; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Valida 
        {
            get { return valida; }
        }

        #region prodriedades da nota
        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CNPJemi
        {
            get
            {
                if (Valida)
                    return new CPFCNPJ(Nota.NFe.infNFe.emit.Item);
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public CPFCNPJ CNPJdest
        {
            get
            {
                if (Valida)
                    return new CPFCNPJ(Nota.NFe.infNFe.dest.Item);
                else
                    return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Numero
        {
            get
            {
                if (Valida)
                    return int.Parse(Nota.NFe.infNFe.ide.nNF);
                else
                    return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime DataEmissao
        {
            get
            {
                if (Valida)
                {
                    string strNota = Nota.NFe.infNFe.ide.dEmi;
                    return new DateTime(int.Parse(strNota.Substring(0, 4)), int.Parse(strNota.Substring(5, 2)), int.Parse(strNota.Substring(8, 2)));
                }
                else
                    return DateTime.MinValue;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorNF
        {
            get
            {
                if (Valida)
                {
                    string strNota = Nota.NFe.infNFe.total.ICMSTot.vNF.Replace(".",",");
                    return decimal.Parse(strNota);
                }
                else
                    return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorPIS
        {
            get
            {
                if (Valida)
                {
                    string strNota = Nota.NFe.infNFe.total.ICMSTot.vPIS.Replace(".", ",");
                    return decimal.Parse(strNota);
                }
                else
                    return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorCOFINS
        {
            get
            {
                if (Valida)
                {
                    string strNota = Nota.NFe.infNFe.total.ICMSTot.vCOFINS.Replace(".", ",");
                    return decimal.Parse(strNota);
                }
                else
                    return 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public decimal ValorICMS
        {
            get
            {
                if (Valida)
                {
                    string strNota = Nota.NFe.infNFe.total.ICMSTot.vICMS.Replace(".", ",");
                    return decimal.Parse(strNota);
                }
                else
                    return 0;
            }
        }         
        #endregion

        /// <summary>
        /// 
        /// </summary>
        public NotaNFE(string ArquivoXML)
        {
            using (FileStream fs = new FileStream(ArquivoXML, FileMode.Open))
            {


                try
                {
                    nota = (TNfeProc)new System.Xml.Serialization.XmlSerializer(typeof(TNfeProc)).Deserialize(fs);
                    valida = true;
                    //spinEdit1.EditValue = Nota.NFe.infNFe.ide.cNF;
                    //CNPJ.Text = Nota.NFe.infNFe.emit.Item;
                }
                catch (System.Exception ex)
                {
                    UltimoErro = new Exception("Nota eletronica inválida:", ex);
                }
            }
            
        }
    }
}
