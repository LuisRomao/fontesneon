namespace TestaSintegra
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.cEditaDados1 = new dllSintegra.Componentes.cEditaDados();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditI = new DevExpress.XtraEditors.DateEdit();
            this.dateEditF = new DevExpress.XtraEditors.DateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(12, 12);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(225, 23);
            this.simpleButton1.TabIndex = 0;
            this.simpleButton1.Text = "Carrega do Banco de dados";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.dateEditF);
            this.panelControl1.Controls.Add(this.dateEditI);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(736, 50);
            this.panelControl1.TabIndex = 5;
            // 
            // cEditaDados1
            // 
            this.cEditaDados1.CaixaAltaGeral = true;
            this.cEditaDados1.Doc = System.Windows.Forms.DockStyle.Fill;
            this.cEditaDados1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cEditaDados1.Estado = CompontesBasicos.EstadosDosComponentes.Registrado;
            
            this.cEditaDados1.Location = new System.Drawing.Point(0, 50);
            this.cEditaDados1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.cEditaDados1.Name = "cEditaDados1";
            this.cEditaDados1.Size = new System.Drawing.Size(736, 535);
            this.cEditaDados1.somenteleitura = false;
            this.cEditaDados1.TabIndex = 4;
            this.cEditaDados1.Titulo = null;
            // 
            // simpleButton2
            // 
            this.simpleButton2.Location = new System.Drawing.Point(485, 12);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(229, 23);
            this.simpleButton2.TabIndex = 1;
            this.simpleButton2.Text = "Salva dados emp.";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // dateEditI
            // 
            this.dateEditI.EditValue = null;
            this.dateEditI.Location = new System.Drawing.Point(272, 12);
            this.dateEditI.Name = "dateEditI";
            this.dateEditI.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditI.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditI.Size = new System.Drawing.Size(100, 20);
            this.dateEditI.TabIndex = 2;
            // 
            // dateEditF
            // 
            this.dateEditF.EditValue = null;
            this.dateEditF.Location = new System.Drawing.Point(379, 12);
            this.dateEditF.Name = "dateEditF";
            this.dateEditF.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditF.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditF.Size = new System.Drawing.Size(100, 20);
            this.dateEditF.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 585);
            this.Controls.Add(this.cEditaDados1);
            this.Controls.Add(this.panelControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditI.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditF.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private dllSintegra.Componentes.cEditaDados cEditaDados1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.DateEdit dateEditF;
        private DevExpress.XtraEditors.DateEdit dateEditI;
    }
}

