using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TestaSintegra
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private dllSintegra.GeraSintegra GS;

        private string Limpa(string orig) {
            string retorno = "";
            foreach (char car in orig.ToCharArray())
            {
                if (char.IsDigit(car))
                    retorno += car;
            }
            return retorno;
        }

        private dDadosEstacao.DadosEmpresaRow linhaprincipal;
        private dDadosEstacao NossosDados;
        private dllSintegra.dDados.Empresa10_11Row NovaEmpresa;

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            if ((dateEditI.EditValue == null) || (dateEditF.EditValue == null))
            {
                MessageBox.Show("Dada n�o informada");
                return;
            }

            DateTime Di = dateEditI.DateTime;
            DateTime Df = dateEditF.DateTime; 

            NossosDados = new dDadosEstacao();
            if (System.IO.File.Exists("dDadosEstacao.xlm"))
                NossosDados.ReadXml("dDadosEstacao.xlm");
            else
            {
                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    NossosDados.DadosEstacao.AddDadosEstacaoRow(folderBrowserDialog1.SelectedPath);
                    NossosDados.DadosEmpresa.AddDadosEmpresaRow(NossosDados.DadosEmpresa.NewDadosEmpresaRow());
                    NossosDados.WriteXml("dDadosEstacao.xlm");
                }
            };
            string Caminho = @"c:\Clientes\matercol";
            if (NossosDados.DadosEstacao.Count > 0) {
                Caminho = NossosDados.DadosEstacao[0].CaminhoBanco;
            };
            string STRConec = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+Caminho+";Extended Properties=\"dBASE IV\"";
            //GS = new dllSintegra.GeraSintegra(@"c:\lixo\testeNovoSintegra.txt");
            GS = new dllSintegra.GeraSintegra("");
            linhaprincipal = NossosDados.DadosEmpresa[0];
            NovaEmpresa = GS.DDados.Empresa10_11.NewEmpresa10_11Row();
            if (!linhaprincipal.IsCNPJNull())
            {

                NovaEmpresa.CNPJ = linhaprincipal.CNPJ;
                NovaEmpresa.IE = linhaprincipal.IE;
                NovaEmpresa.Nome = linhaprincipal.Nome;
                NovaEmpresa.Cidade = linhaprincipal.Cidade;
                NovaEmpresa.UF = linhaprincipal.UF;
                //NovaEmpresa.FAX = 0;

                NovaEmpresa.Logradouro = linhaprincipal.Logradouro;
                NovaEmpresa.Numero = linhaprincipal.Numero;
                NovaEmpresa.Complemento = linhaprincipal.Complemento;
                NovaEmpresa.Bairro = linhaprincipal.Bairro;
                NovaEmpresa.CEP = linhaprincipal.CEP;
                NovaEmpresa.Contato = linhaprincipal.Contato;
                NovaEmpresa.Telefone = linhaprincipal.Telefone;
            };
            NovaEmpresa.DataIni = Di;
            NovaEmpresa.DataFim = Df;
            GS.DDados.Empresa10_11.AddEmpresa10_11Row(NovaEmpresa);

            

            dImportacao DImportacao = new dImportacao();
            dImportacaoTableAdapters.NotasTableAdapter NotasTableAdapter = new TestaSintegra.dImportacaoTableAdapters.NotasTableAdapter();
            dImportacaoTableAdapters.ProdutosTableAdapter ProdutosTableAdapter = new TestaSintegra.dImportacaoTableAdapters.ProdutosTableAdapter();
            dImportacaoTableAdapters.ItensNotaTableAdapter ItensNotaTableAdapter = new TestaSintegra.dImportacaoTableAdapters.ItensNotaTableAdapter();

            NotasTableAdapter.Connection.ConnectionString = STRConec;      
            ProdutosTableAdapter.Connection.ConnectionString = STRConec;
            ItensNotaTableAdapter.Connection.ConnectionString = STRConec;

            NotasTableAdapter.Fill(DImportacao.Notas,Di,Df );                        
            ItensNotaTableAdapter.Fill(DImportacao.ItensNota, Di, Df);
            int NF = 0;
            foreach (dImportacao.NotasRow notasrow in DImportacao.Notas) {
                dllSintegra.dDados.Notas50Row NovaLinha = GS.DDados.Notas50.NewNotas50Row();
                if (notasrow.IsCGCFORNull())
                {
                    //MessageBox.Show("Aten�ao CNPJ n�o cadastrado:" + notasrow.NOMFOR);
                }
                else
                {
                    DocBacarios.CPFCNPJ cnpj = new DocBacarios.CPFCNPJ(notasrow.CGCFOR);
                    if (cnpj.Tipo == DocBacarios.TipoCpfCnpj.INVALIDO)
                    {
                        //MessageBox.Show("Aten�ao CNPJ inv�lido:" + notasrow.CGCFOR + " - " + notasrow.NOMFOR);
                        //NovaLinha.CNPJ
                    }
                    else
                        NovaLinha.CNPJ = cnpj.Valor;
                };
                if(notasrow.IsINCFORNull())
                    NovaLinha.IE = "nao cadastrado";
                else
                    NovaLinha.IE = Limpa(notasrow.INCFOR);
                NovaLinha.DataNota = notasrow.EMISSAO;
                if(!notasrow.IsESTFORNull())
                    NovaLinha.UF = notasrow.ESTFOR;
                if ((NovaLinha.IsUFNull()) || (NovaLinha.UF == "SP"))
                    NovaLinha.CFOP = 1101;
                else
                    NovaLinha.CFOP = 2101;
                NovaLinha.Modelo = 1;
                NovaLinha.Serie = "1";
                NovaLinha.Numero = ++NF;
                NovaLinha.ValorTotal =0;
                NovaLinha.BaseICMS = 0;
                NovaLinha.ValorICMS = 0;
                NovaLinha.ValorIsentaNT = 0;
                NovaLinha.ValorOutras = 0;
                NovaLinha.Aliquota = (decimal)notasrow.ICMS;
                NovaLinha.CodFornecedor = notasrow.FORNEC;
                NovaLinha.numeroPedido = notasrow.NRPEDIDO;
                GS.DDados.Notas50.AddNotas50Row(NovaLinha);

                int N = 1;
                decimal TotalNT = 0;
                foreach (dImportacao.ItensNotaRow itemrow in notasrow.GetItensNotaRows()) {
                    dllSintegra.dDados.Itens54Row NovaLinha54 = GS.DDados.Itens54.NewItens54Row();
                    if(!NovaLinha.IsCNPJNull())
                         NovaLinha54.CNPJ = NovaLinha.CNPJ;
                    NovaLinha54.Modelo = 1;
                    NovaLinha54.Serie = "1";
                    NovaLinha54.Numero = NF;
                    //NovaLinha54.CFOP = 1126;
                    NovaLinha54.CFOP = NovaLinha.CFOP;
                    NovaLinha54.CST = "000";
                    NovaLinha54.N = N++;
                    NovaLinha54.CodProd = itemrow.CODIGO;
                    NovaLinha54.Quantidade = (decimal)itemrow.QTDE;
                    NovaLinha54.ValorTotal = (decimal)itemrow.PRTOTAL;
                    TotalNT += (decimal)itemrow.PRTOTAL;
                    NovaLinha54.ValorDesconto = 0;
                    NovaLinha54.BaseICMS = (decimal)itemrow.PRTOTAL;
                    NovaLinha54.BaseSubT = 0;
                    NovaLinha54.ValorIPI = 0;
                    NovaLinha54.Aliquota = (decimal)itemrow.ICMS;
                    GS.DDados.Itens54.AddItens54Row(NovaLinha54);    
                }
                NovaLinha.ValorTotal = TotalNT;
                NovaLinha.BaseICMS = TotalNT;                
            };


            
            ProdutosTableAdapter.Fill(DImportacao.Produtos, Di, Df);

            foreach (dImportacao.ProdutosRow prodrow in DImportacao.Produtos)
            {
                dllSintegra.dDados.Produtos75Row NovaLinha75 = GS.DDados.Produtos75.NewProdutos75Row();
                NovaLinha75.DataI = Di;
                NovaLinha75.DataF = Df;
                NovaLinha75.CodProd = prodrow.CODIGO;
                NovaLinha75.NCM = "";
                NovaLinha75.Descricao = prodrow.DESCNOSSA;
                if(!prodrow.IsUNIVENDANull())
                     NovaLinha75.Unidade = prodrow.UNIVENDA.Trim();                
                NovaLinha75.AliquotaIPI = 0;
                NovaLinha75.AliquotaICMS = (decimal)prodrow.ICMS;
                NovaLinha75.RedBaseICMS = 0;                
                NovaLinha75.BaseICMSST = 0;
                GS.DDados.Produtos75.AddProdutos75Row(NovaLinha75);
            };
            cEditaDados1.GS = GS;
            //GS.Grava();
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {
            GS.Grava();
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)
        {
            if (!NovaEmpresa.IsCNPJNull())
                linhaprincipal.CNPJ = NovaEmpresa.CNPJ;
            if (!NovaEmpresa.IsIENull())
                linhaprincipal.IE = NovaEmpresa.IE;
            if (!NovaEmpresa.IsNomeNull())
                linhaprincipal.Nome = NovaEmpresa.Nome;
            if (!NovaEmpresa.IsCidadeNull())
                linhaprincipal.Cidade = NovaEmpresa.Cidade;
            if (!NovaEmpresa.IsUFNull())
                linhaprincipal.UF = NovaEmpresa.UF;
            //NovaEmpresa.FAX = 0;
            if (!NovaEmpresa.IsLogradouroNull())
                linhaprincipal.Logradouro = NovaEmpresa.Logradouro;
            if (!NovaEmpresa.IsNumeroNull())
                linhaprincipal.Numero = NovaEmpresa.Numero;
            if (!NovaEmpresa.IsComplementoNull())
                linhaprincipal.Complemento = NovaEmpresa.Complemento;
            if (!NovaEmpresa.IsBairroNull())
                linhaprincipal.Bairro = NovaEmpresa.Bairro;
            if (!NovaEmpresa.IsCEPNull())
                linhaprincipal.CEP = NovaEmpresa.CEP;
            if (!NovaEmpresa.IsContatoNull())
                linhaprincipal.Contato = NovaEmpresa.Contato;
            if (!NovaEmpresa.IsTelefoneNull())
                linhaprincipal.Telefone = NovaEmpresa.Telefone;
            NossosDados.WriteXml("dDadosEstacao.xlm");
        }
    }
}